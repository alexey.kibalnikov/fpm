Program Prog_7;
Uses Crt,Graph;
Const size=500; h=0.0001;{step for U",U',P'}
      U_color=14;
      razn_color=12;
Type  Vect=array[0..size] of extended;     { Error del abs}
 Function U(x:extended):extended; begin U:=abs(exp(sin(Pi*x*5))+3*ln(cos(x)))-2 end;
 Function P(x:extended):extended; begin P:=sin(7*Pi*x)+2 end; {0<P(x)<=C1}
 Function G(x:extended):extended; begin G:=cos(Pi*x)+1 end; {G(x)>=0}

 Function F(t:extended):extended; begin  {-(PU')'+GU=F;   F:=-PU"-P'U'+GU }
F:=-P(t)*(U(t+h)-2*U(t)+U(t-h))/h/h-(P(t+h)-P(t))*(U(t+h)-U(t))/h/h+G(t)*U(t) end;
{---------------------------------------------------------------------------}
 Procedure Ody_raznost(var Rez:vect; n:word);
  var Alfa,Betta:Vect;
      A,B,C,h:extended; i:word;
  begin
   h:=1/n;      i:=n-1;  B:=(P((2*i+1)*h/2)+P((2*i-1)*h/2))/h/h+G(i*h);
                A:=-P((2*i-1)*h/2)/h/h; C:=-P((2*i+1)*h/2)/h/h;
   Alfa[n-2]:=-A/B;
   Betta[n-2]:=(F((n-1)*h)-U(1)*C)/B;

   for i:=n-2 downto 2 do begin
                B:=(P((2*i+1)*h/2)+P((2*i-1)*h/2))/h/h+G(i*h);
                A:=-P((2*i-1)*h/2)/h/h; C:=-P((2*i+1)*h/2)/h/h;
    Alfa[i-1]:=-A/(Alfa[i]*C+B);
    Betta[i-1]:=(F(i*h)-Betta[i]*C)/(Alfa[i]*C+B)
   end;
          i:=1; B:=(P((2*i+1)*h/2)+P((2*i-1)*h/2))/h/h+G(i*h);
                A:=-P((2*i-1)*h/2)/h/h; C:=-P((2*i+1)*h/2)/h/h;
   Rez[1]:=(-U(0)*A+F(h)-Betta[1]*C)/(B+Alfa[1]*C);

   for i:=1 to n-2 do
   Rez[i+1]:=Alfa[i]*Rez[i]+Betta[i];
   Rez[0]:=U(0); Rez[n]:=U(1);

   A:=0; for i:=0 to n do if A<(Rez[i]-U(i*h)) then A:=Rez[i]-U(i*h);
   write(' maxіYi-U(Xi)і<=C(1/n)^2 ,i=1..',n-1,'; max=',A);readln
 end;{Ody_raznost}
{---------------------------------------------------------------------------}
 Procedure Paint(V:vect; n:word);
  var M,D:integer;   key:byte;
      i,C,x1,x2:word; mes:string;
  begin
   D:=Detect; InitGraph(D,M,' '); x1:=110; x2:=530;
   C:=round(abs(U(0))+1); if abs(U(1)+1)>abs(U(0)) then C:=round(abs(U(1)+1));
  repeat
   SetFillStyle(1,0); Bar(0,0,650,480);
   SetFillStyle(1,14); Bar(8,395,105,410);
   SetColor(1); OutTextXY(10,400,'-(PU'')''+GU=F');
   SetColor(10); OutTextXY(15,215,'Mashtab');
   OutTextXY(30,228,'');OutTextXY(30,242,'');
   OutTextXY(21,235,'');OutTextXY(40,235,'');
   SetColor(15); Line(x1-30,240,x2+5,240); Line(x1,5,x1,475);
   line(x2,243,x2,237); line(x1-3,10,x1+3,10); line(x1-3,470,x1+3,470);
   OutTextXY(x2+5,252,'1'); OutTextXY(x1-10,245,'0');
SetFillStyle(1,U_color);     Bar(20,415,25,420); OutTextXY(40,415,'U(Xi)');
SetFillStyle(1,Razn_color); Bar(20,425,25,430); OutTextXY(40,425,'Raznost');
   Str(C,mes); OutTextXY(92,7,mes); mes:='-'+mes; OutTextXY(84,467,mes);
   Str(n,mes); OutTextXY(x2-20,460,'n='+mes);
   for i:=x1 to x2 do if (-C<=U((i-x1)/(x2-x1))) and (U((i-x1)/(x2-x1))<=C)
    then PutPixel(i,round(-230/C*U((i-x1)/(x2-x1))+240),U_color);
   for i:=0 to n do if (-C<=V[i]) and (V[i]<=C)
    then PutPixel(round((x2-x1)/n*i+x1),round(-230/C*V[i]+240),Razn_color);

   key:=ord(readkey); if key=0 then key:=ord(readkey); i:=50;
    Case key of
{^}   72,56: inc(C);
{V}   80,50: if C<>1 then dec(C);
{<-}  75,52: if x2>x1+i then x2:=x2-i;
{->}  77,54: if x2<530 then x2:=x2+i
    end;{Case}
 until (key<>72)and(key<>56)and(key<>80)and(key<>50)and(key<>75)and(key<>52)and(key<>77)and(key<>54);
 CloseGraph end;{----------------------------------------------------- Paint}
 Var V:vect;
     n:word;
Begin    writeln(' Prog #7, Raznostn. metod for OdY');
 repeat write(' [2..',size,'] n='); readln(n) until (2<=n) and (n<=size);
 Ody_raznost(V,n);
 Paint(V,n)
End.