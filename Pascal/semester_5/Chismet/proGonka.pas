Program Prog_3;
Uses Crt;
Const size=4; n=5;
      ft=12; m=8;
Type matr=array[1..size,1..size] of extended;
     vect=array[1..size] of extended;
Const Cm:matr=((2,4,0,2),        { 3} { -6}
               (4,19,0,4),       {-4} {-16} {19<-9}
               (0,0,1,0),        {-3} { -3}
               (2,4,0,3));       { 2} { -4}
      Cv:vect=(-6,-16,-3,-4);  {___Y_____f___}
      Lm:matr=((1,2, 0, 0),      { 3} { -1}
               (7,3, 4, 0),      {-2} { 19}
               (0,1,-7,-2),      { 1} {-15}
               (0,0, 5, 6));     { 3} { 23}
      Lv:vect= (-1,19,-15,23);
Procedure Print(var A:matr; var f:vect);
 var i,j:byte;
 begin TextColor(7); {Print A}
  write('Щ':6);for i:=1 to 52 do write(' '); writeln('Л');
   for i:=1 to size do begin
    if i=size div 2+1 then write('A=':5,'К') else write('К':6);
    for j:=1 to size do write(A[i,j]:ft:m,' '); write('К');
    writeln end;
  write('Ш':6); for i:=1 to 52 do write(' '); writeln('М');
                     {Print V}
  write('Щ':6);for i:=1 to 13 do write(' '); writeln('Л');
   for i:=1 to size do begin
    if i=size div 2+1 then write('f=':5,'К') else write('К':6);
    write(f[i]:ft:m,' '); write('К');
    writeln end;
  write('Ш':6); for i:=1 to 13 do write(' '); writeln('М')
end;{Print}
Procedure Print_rez (var A:matr; var y,f:vect);
 var i,j:byte; v:vect;
 begin
  GotoXY(28,3*size div 2 +5); TextColor(3); writeln('y=');
  GotoXY(30,size+4);   writeln('Щ',' ':25,'Л');
  for i:=1 to size do begin GotoXY(30,size+4+i); writeln('К ',y[i],' К') end;
  GotoXY(30,2*size+5); writeln('Ш',' ':25,'М');
  writeln; TextColor(14);
  for i:=1 to size do begin v[i]:=0;            {R=Ay-f}
   for j:=1 to size do v[i]:=v[i]+A[i,j]*y[j] end; writeln('Щ':30,' ':25,'Л');
  for i:=1 to size do if i<>size div 2+1 then writeln('К ':31,v[i]-f[i],' К')
                        else writeln('R=Ay-f=К ':31,v[i]-f[i],' К');
  writeln('Ш':30,' ':25,'М'); TextColor(7)
end;{Print_rez}
{----------------------- *** Ўс­ЎЂ­ я ч сть *** -----------------------------}
{variant 1}
Procedure L_matr(var L,A:matr);
 var i,j,k:byte; s:extended;
 begin
  L[1,1]:=sqrt(A[1,1]);
  for i:=2 to size do L[i,1]:=A[i,1]/L[1,1];
  for i:=2 to size do     begin s:=0;
   for k:=1 to i-1 do s:=s+sqr(L[i,k]);
   L[i,i]:=sqrt(A[i,i]-s) end;
  for i:=2 to size do
   for j:=i+1 to size do       begin s:=0;
    for k:=1 to i-1 do s:=s+L[i,k]*L[j,k];
    L[j,i]:=(A[j,i]-s)/L[i,i]  end
end;{L_matr}

Procedure Ay_f (var L:matr; var y,f:vect); {rez->y}
 var i,j:byte; v:vect;
 begin v:=f;                               {Lv=f}
  for i:=1 to size do begin
   for j:=1 to i-1 do v[i]:=v[i]-L[i,j]*v[j];
   v[i]:=v[i]/L[i,i]  end;
       y:=v;                               {transp(L)y=v}
  for j:=size downto 1 do begin
   for i:=size downto j+1 do y[j]:=y[j]-L[i,j]*y[i];
   y[j]:=y[j]/L[j,j]      end
end;{Lv_f}
{variant 2 -----------------------------------------------------------------}
Procedure proGonka(var M:matr; var y,f:vect); {rez->y}
 var Alfa,Beta,a,b,c:vect;
     i:byte;
 begin                                      {bc...}
  for i:=2 to size do a[i]:=M[i,i-1];       {abc..}
  for i:=1 to size do b[i]:=M[i,i];         {.....}
  for i:=1 to size-1 do c[i]:=M[i,i+1];     {...ab}

  Alfa[size-1]:=-a[size]/b[size];
  Beta[size-1]:=f[size]/b[size];
  for i:= size-1 downto 2 do begin
   Alfa[i-1]:=-a[i]/(Alfa[i]*c[i]+b[i]);
   Beta[i-1]:=(f[i]-Beta[i]*c[i])/(Alfa[i]*c[i]+b[i]) end;

  y[1]:=(f[1]-c[1]*Beta[1])/(b[1]+c[1]*Alfa[1]);
  for i:=1 to size-1 do
   y[i+1]:=Alfa[i]*y[i]+Beta[i]
end;{Gonka}
{----------------------------------------------------------------------------}
 Var A,L:matr; f,y:vect; i:byte;
Begin
 ClrScr;
 A:=Cm; f:=Cv; writeln(' ':27,'variant [1]  Ay=f');
 Print(A,f);
 L_matr(L,A);
 Ay_f(L,y,f);
 Print_rez(A,y,f);
 readln; ClrScr;
 A:=Lm; f:=Lv; writeln(' ':27,'variant [2]  Ay=f');
 Print(A,f);
 proGonka(A,y,f);
 Print_rez(A,y,f);
 readln
End.