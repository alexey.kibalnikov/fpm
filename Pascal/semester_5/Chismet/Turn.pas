Program Prog_5;
Uses Crt;
Const size=5; f{ormat}=12;mnt{antisa}=8; spase=65;
Type  matr=array[1..size,1..size] of extended;
Const C:matr=((1, 2, 3, 0, 5),
              (2,-1,-6, 0, 8),
              (3,-6, 8, 0,10),
              (0, 0, 0, 9, 0),
              (5, 8,10, 0,-7));
Procedure Print(var A:matr; mes:string);
 var i,j:byte;
 begin
  write('╔':6);for i:=1 to spase do write(' '); writeln('╗');
  for i:=1 to size do begin
   if i=size div 2+1 then write(mes:5,'║') else write('║':6);
   for j:=1 to size do write(A[i,j]:f:mnt,' '); write('║');
   writeln end;
  write('╚':6); for i:=1 to spase do write(' '); writeln('╝');
end;{Print}
{----------------------------------------------------------------------------}
Procedure Mult(var M:matr; A,B:matr; n:byte; transp:boolean);
 var i,j,k:byte;
 begin
  for i:=1 to size do
   for j:=1 to n do  begin M[i,j]:=0;
    for k:=1 to size do
     if not transp then M[i,j]:=M[i,j]+A[i,k]*B[k,j]
      else M[i,j]:=M[i,j]+A[k,i]*B[k,j] end
end;{Mult}

Procedure V_matr(var V,A:matr);
 var i,j,rez_i,rez_j:byte; max:extended;
 begin max:=0;
 for i:=1 to size do
  for j:=1 to size do
   if (i<>j) and (abs(A[i,j])>=max) then
    begin max:=abs(A[i,j]); rez_i:=i; rez_j:=j end;
 for i:=1 to size do
  for j:=1 to size do
   if i<>j then V[i,j]:=0 else V[i,j]:=1;
 i:=rez_i; j:=rez_j;
 if A[i,i]=A[j,j] then max:=Pi/4 else begin
   max:=2*A[i,j]/(A[i,i]-A[j,j]);
   max:=arctan(max)/2 end;      {max теперь это угол x, для sin(x),cos(x)}
 V[i,i]:=cos(max); V[i,j]:=-sin(max);
 V[j,i]:=sin(max); V[j,j]:=cos(max)
end;{V_matr}

Function Stop(var A:matr; eps:extended):boolean;
 var i,j:byte; s:extended;
 begin s:=0;
  for i:=1 to size do
   for j:=1 to i-1 do s:=s+abs(A[i,j]); {i>j}
  if s<eps then Stop:=true else Stop:=false
end;{Stop}

Procedure Epsilon(var E,A,A_old,V:matr);
 var B:matr; i,j:byte;
 begin
  for j:=1 to size do begin
   for i:=1 to size do B[i,1]:=V[i,j];
    Mult(B,A_old,B,1,false);
   for i:=1 to size do E[i,j]:=B[i,1]-A[j,j]*V[i,j] end
end;{Epsilon}
{----------------------------------------------------------------------------}
 Var A,V,M,A_copy:matr; eps,max:extended;
     count:word; i,j:byte;
Begin
 A:=C; A_copy:=A;
 ClrScr; TextColor(7); writeln(' Turn Matrix for faining of own namber');
 Print(A,'A=');
 write('Input eps=','█':19); GotoXY(11,9); readln(eps); ClrScr;
 for i:=1 to size do
  for j:=1 to size do if i<>j then M[i,j]:=0 else M[i,j]:=1; {M:=E}
 count:=0;
 repeat
  V_matr(V,A);
  Mult(A,V,A,size,true);
  Mult(A,A,V,size,false);
  Mult(M,M,V,size,false);
  inc(count);
 until Stop(A,eps);

 TextColor(2); write('k=',count);
 TextColor(14); writeln('      eps=',eps);
 TextColor(7); Print(A,'newA=');
 writeln('Rezult (a):');TextColor(2);
 for i:=1 to size do writeln(' ':24,A[i,i]);
 TextColor(4); writeln('Eps=old(A)u-au':43); TextColor(7);
 Epsilon(M,A,A_copy,M); TextColor(3);
 for j:=1 to size do begin GotoXY(1,16);
  writeln('Vector nevjazki:             Eps[',j,']');
  for i:=1 to size do writeln (' ':24,M[i,j]);
  writeln; write('Press Enter'); readln
  end
End.