Program Prog_6;
Uses  Crt,Graph;
Const U_color=14;
      E_color=12;
      I_color=3;
      x0=0; x1=1; y0=0; y1=1;
      h=1e-6;{step for U"}
      lim=50;
Type my_type=real;
     vect=array[0..lim] of my_type;
     matr=array[0..lim,0..lim] of my_type;
{----------------------------------------------------------------------------}

 Function U(x,y:my_type):my_type; begin
  U:=(x-x0)*(x-x1)*(y-y0)*(y-y1)*50*(3*x*x-2*y) end;

 Function F(x,y:my_type):my_type; begin
  F:=(-U(x+h,y)+4*U(x,y)-U(x-h,y)-U(x,y+h)-U(x,y-h))/h/h end;

{----------------------------------------------------------------------------}

Type Ellipse = object
       Yk,Z,W:matr;
       n,count:word;
       eps,tay:my_type;
       C,a,b:word; {mashtab for paint}

       procedure Init;
       procedure Main;
      private
       procedure Maska;
       procedure Paint_Rez(color:word);
       procedure Expresed;
       procedure Implied;
     end;{Ellipse}

 Procedure Ellipse.Init;
  begin
   {repeat write('[3..',lim,'], n='); readln(n) until (3<=n)and(n<=lim);}
   writeln('[3..',lim,'], n=20, t.k tk=?, m=?'); n:=20;
   repeat write ('(0..1) eps='); readln(eps) until (0<eps)and(eps<1);
   {write ('recomend',1/sqr(n)/4,' tay='); readln(tay);} tay:=1/n/n/4;
 end;{------------------------------------------------------------------ Init}

 Procedure Ellipse.Maska;
  var mes:string;
  begin
   SetFillStyle(1,0); Bar(0,0,650,480);
   SetFillStyle(1,14); Bar(8,395,97,410);
   SetColor(1); OutTextXY(10,400,'U"=PU''+GU+F');
   SetColor(10); OutTextXY(15,215,'Mashtab');
   OutTextXY(30,228,'');OutTextXY(30,242,'');
   OutTextXY(21,235,'');OutTextXY(40,235,'');

   SetColor(15); Line(a-30,240,b+5,240); Line(a,5,a,475);
   line(b,243,b,237); line(a-3,10,a+3,10); line(a-3,470,a+3,470);
   Str(n,mes); OutTextXY(b-20,430,'n= '+mes);
   Str(eps:0,mes); OutTextXY(b-20,440,'e='+mes);

   SetFillStyle(1,U_color); Bar(20,415,25,420); OutTextXY(35,415,'U(Xi,Yj)');
   SetFillStyle(1,E_color); Bar(20,425,25,430); OutTextXY(35,425,'Expresed');
   SetFillStyle(1,I_color); Bar(20,435,25,440); OutTextXY(35,435,'Implied');
   Str(c,mes); OutTextXY(82,7,mes); mes:='-'+mes; OutTextXY(74,467,mes)
  end;{---------------------------------------------------------------- Maska}

 Procedure Ellipse.Paint_Rez;
  var k,x_new,y_new,x_old,y_old:word;
  begin
   SetColor(color);
   x_old:=a;
   y_old:=round(-230/C*Yk[0,0]+240);
   for k:=1 to n do
    begin
     x_new:=round(a+(b-a)*k/n);
     y_new:=round(-230/C*Yk[k,k]+240);
      if (-C<=round(Yk[k,k])) and (round(Yk[k,k])<=C) then
       Line(x_old,y_old,x_new,y_new);
      x_old:=x_new;
      y_old:=y_new
    end
  end;{------------------------------------------------------------ Paint_Rez}

 Procedure Ellipse.Main;
  var M,D:integer;
      arg1,arg2: my_type;
      k:word; key:byte;
      mes:string[5];
  begin
   Init;
   D:=Detect; InitGraph(D,M,' '); a:=100; b:=530; C:=1;
  repeat
   Maska;
   for k:=0 to b-a+1 do
    begin
     arg1:=x0+(x1-x0)*k/(b-a+1);
     arg2:=y0+(y1-y0)*k/(b-a+1);
     if (-C<=U(arg1,arg2)) and (U(arg1,arg2)<=C) then
      PutPixel(a+k,round(-230/C*U(arg1,arg2)+240),U_color);
    end;
             Expresed;
             Paint_Rez(E_color);
             Str(count,mes); OutTextXY(b-20,450,'count='+mes);
             Implied;
             Paint_Rez(I_color);
             Str(count,mes); OutTextXY(b-20,460,'count='+mes);

   key:=ord(readkey); if key=0 then key:=ord(readkey); k:=50;
    Case key of
{^}   72,56: inc(C);
{V}   80,50: if C<>1 then dec(C);
{<-}  75,52: if b>a+k then b:=b-k;
{->}  77,54: if b<530 then b:=b+k
    end;{Case}
 until (key<>72)and(key<>56)and(key<>80)and(key<>50)and(key<>75)and(key<>52)and(key<>77)and(key<>54);
 CloseGraph end;{------------------------------------------------------- Main}

{============================================================================}

 Procedure Ellipse.Expresed;
  var i,j:word;
      Norma,max:my_type;
  begin count:=0;
   for i:=0 to n do
    for j:=0 to n do begin Yk[i,j]:=0; Z[i,j]:=0 end;
    repeat
     for i:=1 to n-1 do
     for j:=1 to n-1 do
      Yk[i,j]:=((Z[i+1,j]+Z[i-1,j]+Z[i,j+1]+Z[i,j-1]-4*Z[i,j])*n*n+F(i/n,j/n))*tay+Z[i,j];
{---------- Norma ----------}
     max:=0;
     for j:=1 to n-1 do begin
      Norma:=0;
      for i:=1 to n-1 do  Norma:=Norma+abs(Yk[i,j]-Z[i,j]);
      if max<Norma then max:=Norma
     end;
     inc(count);
     if (max<=eps) then  break;
     Z:=Yk;
    until false
 end;{-------------------------------------------------------------- Expresed}

 Procedure Ellipse.Implied;
  var m,tk,Norma,max,Fij:my_type;
      i,j:word;
      dc,nc,ec,pc,y1c,y2c,h:my_type;
  begin count:=0;

      h:=1/n;
      dc:=2/h/h*sin(pi*h/2);
      nc:=sin(pi*h/2);
      ec:=2*sqrt(nc)/(1+sqrt(nc));
      pc:=(1-ec)/(1+ec);
      y1c:=dc/(1+sqrt(nc))/2;
      y2c:=dc/4/sqrt(nc);
      tk:=2/(y1c+y2c)/(1+pc/2);
      m:=2*sqrt(nc)/dc;

   tk:=0.00062;                                    {??????????}
   m:=0.000062;                                    {??????????}

   for i:=0 to n do           {Init}
    begin
     Z[i,n]:=0; Z[n,i]:=0;
     W[i,0]:=0; W[0,i]:=0;
     for j:=0 to n do Yk[i,j]:=0
    end;

  repeat
{-------- B1z=F-AYk --------}
   for j:=n-1 downto 1 do
    for i:=n-1 downto 1 do
     begin {Fij:=f-AYk}
      Fij:=((Yk[i+1,j]+Yk[i-1,j]+Yk[i,j+1]+Yk[i,j-1]-4*Yk[i,j])*n*n+F(i/n,j/n));
      Z[i,j]:=(Fij-(Z[i+1,j]+Z[i,j+1])*m{*n*n})/(1-2*m{*n*n})
     end;
{--------- B2w=Z -----------}
   for j:=1 to n-1 do
    for i:=1 to n-1 do
     W[i,j]:=(Z[i,j]-(W[i-1,j]+W[i,j-1])*m{*n*n})/(1-2*m{*n*n});
{------- Yk+1=Yk+tk*W ------}
   for i:=0 to n do
    for j:=0 to n do Yk[i,j]:=Yk[i,j]+tk*W[i,j];
{---------- Norma ----------}
   max:=0;
    for j:=1 to n-1 do begin
     Norma:=0;
     for i:=1 to n-1 do Norma:=Norma+abs(W[i,j]);
     Norma:=Norma*tk;
     if max<Norma then max:=Norma
    end; inc(count)
  until max<eps
 end;{--------------------------------------------------------------- Implied}

 Var E:Ellipse;
 Begin E.Main End.