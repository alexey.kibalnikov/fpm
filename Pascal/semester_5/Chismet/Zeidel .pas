Program Prog_4;
Uses Crt,Graph;
Const size=5; ft=12; m=8;
Type matr=array[1..size,1..size] of extended;
     vect=array[1..size] of extended;
Const Cm:matr=((2, 4,0,2, 6),   { 3}
               (4, 9,0,4,13),   {-4}
               (0, 0,1,0, 0),   {-3}
               (2, 4,0,3, 6),   { 2}
               (6,13,0,6,20));  { 1}
      Cv:vect=(0,-3,-3,2,-2);
Procedure Print(var A:matr; var f:vect);
 var i,j:byte;
 begin           {Print A}
  write('╔':6);for i:=1 to 65 do write(' '); writeln('╗');
   for i:=1 to size do begin
    if i=size div 2+1 then write('A=':5,'║') else write('║':6);
    for j:=1 to size do write(A[i,j]:ft:m,' '); write('║');
    writeln end;
  write('╚':6); for i:=1 to 65 do write(' '); writeln('╝');
                 {Print V}
  write('╔':6);for i:=1 to 13 do write(' '); writeln('╗');
   for i:=1 to size do begin
    if i=size div 2+1 then write('f=':5,'║') else write('║':6);
    write(f[i]:ft:m,' '); write('║');
    writeln end;
  write('╚':6); for i:=1 to 13 do write(' '); writeln('╝')
end;{Print}
{----------------------- *** основная часть *** -----------------------------}
Function Norma(var A:matr; var y,f:vect):extended;
 var v:vect; i,j:byte; s:extended;
 begin
  for i:=1 to size do begin v[i]:=0;
   for j:=1 to size do v[i]:=v[i]+A[i,j]*y[j] end;{v=Ay}
  s:=0; for i:=1 to size do s:=s+sqr(f[i]-v[i]);
  Norma:=sqrt(s);
end;{Norma}

Procedure New_y(var new_y:vect; y,f:vect; var A:matr);
 var i,j:byte; s1,s2:extended;
 begin
  for i:=1 to size do begin
   s1:=0; for j:=1 to i-1 do s1:=s1+A[i,j]*new_y[j];
   s2:=0; for j:=i+1 to size do s2:=s2+A[i,j]*y[j];
   new_y[i]:=(f[i]-s1-s2)/A[i,i] end
end;{New_y}
{----------------------------------------------------------------------------}
 Var A:matr; y,f:vect; e,eps:extended;
     i,j:byte; count:word;
     d,mode:integer; mes:string;
Begin
 ClrScr;
 writeln(' Ay=f');
 A:=Cm; f:=Cv;
 Print(A,f);
 write('Input eps='); readln(eps);
 writeln('Введите начальные приближения :');
 for i:=1 to size do begin write ('Yo[',i,']='); readln(y[i]) end;
 d:=detect;
 InitGraph(d,mode,' ');
 count:=0; e:=Norma(A,y,f);
 line(20,GetMaxY-30,GetMaxX-30,GetMaxY-30);
 line(30,10,30,GetMaxY-30); line(25,10,35,10); OutTextXY(9,9,'1');
 SetColor(12);OutTextXY(2,1,'Norma');OutTextXY(570,460,'Count');
 OutTextXY(20,460,'0');SetColor(14);
 OutTextXY(5,GetMaxY-30-round(30+(GetMaxY-30-30)*eps)-5,'eps');
 e:=Norma(A,y,f);
 while e>eps do begin
  New_y(y,y,f,A);
  e:=Norma(A,y,f);

  SetColor(15); line(2*count+30,GetMaxY-25,2*count+30,GetMaxY-35);
  PutPixel(2*count+30,GetMaxY-30-round(30+(GetMaxY-30-30)*eps),14);
  PutPixel(2*count+30,GetMaxY-30-round(30+(GetMaxY-30-30)*e),12);
  inc(count)
 end;
 SetColor(12); Str(count,mes); mes:='Count='+mes;OutTextXY(400,20,mes);
 Str(eps,mes);  mes:='eps='+mes; SetColor(14); OutTextXY(400,10,mes);
 SetColor(2);
 for i:=1 to size do begin Str(y[i],mes); mes:='y='+mes;
                      OutTextXY(400,20+10*i,mes) end;
 readln; CloseGraph
End.