Uses Crt,Graph;
Type Rec=record
      fio:string[15];
      tel:string[9];
      add:string[15];
     end;{Rec}
     FL_Rec=file of Rec;

Procedure Menu(var W:FL_Rec);
      Procedure Text;
       begin
        SetColor(12); OutTextXY(30,20,'Обработка записей:');SetColor(15);
        OutTextXY(60,50,'Добавление');
        OutTextXY(60,70,'Удаление');
        OutTextXY(60,90,'Коррекция');
        OutTextXY(60,110,'Просмотр');
        OutTextXY(60,130,'Выход')
       end;{Text}

    Procedure See(var F:Fl_Rec);
     {See:просмотр}
     Var h:rec;key:byte;x:string;
     begin Reset(f);
      SetColor(9);Rectangle(240,10,500,150);Line(240,35,500,35);
      SetColor(11); OutTextXY(300,20,'Просмотр записей:');
      SetColor(10);
      OutTextXY(260,60,'фамилия:');
      OutTextXY(260,80,'телефон:');
      OutTextXY(260,100,'адрес:');
     repeat
      key:=ord(readkey);SetColor(0);
      OuttextXY(330,60,h.fio);
      OuttextXY(330,80,h.tel);
      OuttextXY(330,100,h.add);
      if (key=80) or (key=50) then Seek(F,FilePos(F)-1);
      if FilePos(F)=FileSize(F)then Seek(F,0);
      read(f,h);SetColor(14);
      OuttextXY(330,60,h.fio);
      OuttextXY(330,80,h.tel);
      OuttextXY(330,100,h.add);
     until key=13;
   end;{See}

       Procedure Add(var F:FL_Rec);
       {Add:добавление записи}
       var H:Rec;
           c:char;
       begin ClrScr; CloseGraph;
        repeat
         Seek(F,FileSize(F));
         writeln;writeln(' Добавление записи: ');writeln;
         with H do
          begin
           write('    фамилия : ');readln(fio);
           write('    телефон : ');readln(tel);
           write('    адрес   : ');readln(add);
          end;{with}
         write(F,H);
         write('добавить новую запись (Y/N) : ');readln(c);
        until (c<>'y')and(c<>'Y');
       end;{Add}

    Procedure Del(var F:Fl_Rec);
    {Del:удаление}
     var h:Rec; s:string;
         key:byte; Q:Fl_Rec;
         k:longint; flag:char;
     begin
      ClrScr; CloseGraph; Reset(F);
      repeat
       ClrScr; Read(F,h);
       writeln('Удаление записи : ');writeln;
       writeln('фамилия : ',h.fio);
       writeln('телефон : ',h.tel);
       writeln('адрес : ',h.add);
       key:=ord(readkey);
       if (key=80) or (key=50) then Seek(F,FilePos(F)-1);
       if FilePos(F)=FileSize(F)then Seek(F,0);
       k:=FilePos(F);
      until key=13; writeln;
      write(' Вы хотите удалить эту запись ? (Y/N) ');readln(flag);
      if (flag='y')or(flag='Y') then
       begin
        Assign(Q,'Copy.bat');
        Reset(F); Rewrite(Q);
        while not eof(F) do
         begin Read(F,h);
          if (FilePos(F)<>k) and (k<>FileSize(F)) then Write(Q,h)
         end;
        Close(Q); Erase(F);
        Rename(Q,'mybaza.bat');
        writeln('  Запись удалена !');
        readln
        end;
     end;{del}

          Procedure Corr(var F:Fl_Rec);
           var h:Rec; key:byte;
               k:longint; flag:char;
          {Corr:коррекция записи}
           begin
            CloseGraph; Reset(F);
            repeat
             ClrScr; Read(F,h);
             writeln('Удаление записи : ');writeln;
             writeln('фамилия : ',h.fio);
             writeln('телефон : ',h.tel);
             writeln('адрес : ',h.add);
             key:=ord(readkey);
             if (key=80) or (key=50) then Seek(F,FilePos(F)-1);
             if FilePos(F)=FileSize(F)then Seek(F,0);
             k:=FilePos(F);
            until key=13; writeln;
            write(' Вы хотите корректировать эту запись ? (Y/N) ');readln(flag);
            if (flag='y')or(flag='Y') then
             begin
              writeln; writeln('  Коррекция :'); writeln;
              with H do
               begin
                write('    фамилия : ');readln(fio);
                write('    телефон : ');readln(tel);
                write('    адрес   : ');readln(add);
               end;{with}
              Seek(F,k-1); Write(F,h);
              Close(F);
              writeln('  Коррекция произведена !');
             readln
             end;
           end;{Corr}

{ ---------- Menu -----------}
 var M,D:integer;
     key,go:byte;
  begin
   D:=detect;
   InitGraph(D,M,' ');
   go:=0;Text;
   SetColor(14); Rectangle(20,10,180,150);Line(20,35,180,35);
   SetFillStyle(1,14); FloodFill(200,200,0);
   repeat
    SetColor(2); Rectangle(50,46+20*go,150,62+20*go);
    key:=ord(ReadKey);
    SetColor(0); Rectangle(50,46+20*go,150,62+20*go);
    Case key of
     72,56: if go=0 then go:=4 else go:=(go-1) mod 5;
     80,50: go:=(go+1) mod 5
    end;{Case}
   until key=13;
  Case go of
   0:Add(W);
   1:Del(W);
   2:Corr(W);
   3:See(W);
  end;{Case}
 CloseGraph;
 if go<>4 then Menu(W);
end;{Menu}
 {-------------------------------------------------------------------------}
Var Fle:FL_Rec;
Begin
 Assign(Fle,'mybaza.bat');
 {Rewrite(Fle);{}
 Reset(Fle);
 Menu(Fle);
 Close(Fle);
End.