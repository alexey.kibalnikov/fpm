Program Min_Way;
{Нахождение минимального пути в графе}
Uses Graph;
Const lim=12;
Type
 sf=1..lim;
 matr=array [sf,sf] of byte;    {матрица инциденций}
 XY=array [1..2,sf] of integer; {координаты вершин}
Var A:matr;
    flag:byte;                  {равны ребра или нет}
    n:sf;                       {размерность}
Procedure Crt;
 var i:byte;
 begin
  for i:=1 to 24 do writeln;
 end;{Crt}
Procedure Rd (var F:matr;var n:sf;var s:byte);
{Rd:Ввод матрицы F}
 var m,i,j,g:sf;
     help: set of byte;
 begin
  Randomize;
  repeat write('Введите кол-во вершин графа (1..',lim,') n= ');readln(n);
  until n in [1..lim];
  writeln;
  writeln(' 1. Все ребра равной длины');
  writeln(' 2. Ребра разной длинны');writeln;
  repeat write(' Выбирите номер из списка : ');readln(s)
  until s in [1,2];
  writeln;
  writeln(' 1. Заполнить матрицу самостоятельно ');
  writeln(' 2. Заполнить случайным образом ');writeln;
  repeat write(' Выбирите номер из списка : ');readln(g)
  until g in [1,2];
  Case s of
   1:help:= [0,1];
   2:help:= [0..99]
  end;
  if g=1 then
   begin writeln;
    Case s of
     1:writeln(' ':8,'Внимание! вводите только 0..1 ');
     2:writeln(' ':8,'Внимание! вводите только 0..99 ')end;
    writeln('Ввод элементов матрицы :')
   end;
  for i:=1 to n do
   for j:=1 to n do
    begin
     if g=1 then begin
      repeat write(' [':7,chr(i+96),'->',chr(j+96),']    A[',i,',',j, '] = ');readln(m);
       if (m>=1) and (m<>A[j,i]) and(A[j,i]<>0) and (s=2) then
        repeat write('  Длина 0 или ',A[j,i],' A[',i,',',j,'] = ');readln(m)
        until (m=0) or (m=A[j,i]);
      until m in help;
      F[i,j]:=m
      end else
      Case s of
       1:A[i,j]:=random(2);
       2:begin
          A[i,j]:=random(2);
          if (A[i,j]=1) and ((A[j,i]=0) or (i=j)) then A[i,j]:=random(99)+1
           else A[i,j]:=A[j,i]
         end
      end{Case};
    end{for j}
end;{Rd}
Procedure Wrt (F:matr;n:sf);
{Wrt:Печатает матрицу F}
 var i,j:sf;
 begin
  Crt;writeln(' ':8,'Матрица инциденций :');writeln;
  write('  ':5);for i:=1 to n do write(chr(i+96):3);writeln;
  for i:=1 to n do
   begin
    write(' ':3,chr(i+96),' ');
    for j:=1 to n do write(F[i,j]:3);
    writeln
   end;{for i}
  writeln;
  write(' ':8,'Для продолжения нажмине Enter ');readln
end;{Wrt}
Procedure Rem (F:matr;P:XY;n:sf);
{Rem:Подпись длин ребер}
 var i,j:sf;
     x,y:integer;
     u:string;
     q:real;
 begin
  SetColor(15);
  for i:=1 to n do
   for j:=1 to n do
    if (F[i,j]>=1) then
      begin
       if (P[1,j]-P[1,i])=0 then {q-угол наклона ребра}
        if P[2,i]>P[2,j] then q:=Pi/2 else q:=-Pi/2
         else q:=arctan((P[2,j]-P[2,i])/(P[1,j]-P[1,i]));
       if P[1,i]>=P[1,j] then q:=q+Pi;
       x:=round(40*cos(q)+P[1,i]);
       y:=round(40*sin(q)+P[2,i]);
       if i=j then y:=P[2,j]-5;
       Str(F[i,j],u);
       OutTextXY(x,y,u);u:=' '
      end;
end;{Rem}
Procedure Road (F:matr;Z:XY;n,a,b:sf;s:byte);
{Road:Построение пути}
 const max=32000;
 var i,j,k:sf;
     R:array [sf] of integer;{длина пути}
     C:array [sf] of string; {все пути из a}
     Way,num:string; {найденный путь, номер вершины}
 begin
  for i:=1 to n do
   if F[a,i]>=1 then C[i]:=chr(a+64)+chr(i+64) else C[i]:=' ';
  if s=2 then {если разной длины, то 0:=max}
   for i:=1 to n do
    if F[a,i]>=1 then R[i]:=F[a,i] else R[i]:=max;
  if ((C[b]=' ') and (s=1)) or (s=2) then
   for k:=2 to n do
    begin
     for i:=1 to n do
      if C[i]<>' 'then
       for j:=1 to n do
        Case s of
         1:begin
            if (F[i,j]=1) and ((C[j]=' ') or (length(C[j])> length(C[i])+1))
             then C[j]:=C[i]+chr(j+64);
            if C[b]<>' 'then break {найден минимальный путь}
           end;{1}
         2:begin
            if (F[i,j]>=1) and (R[j]>F[i,j]+R[i]) then
             begin
              R[j]:=F[i,j]+R[i];
              C[j]:=C[i]+chr(j+64)
             end
           end{2}
        end{Case}
    end;{for k}
  Way:=C[b];a:=1; {Way: хранит найденый путь}
  if Way<>' ' then {если он существует, то}
   begin
    SetColor(1);
    SetLineStyle(0,0,3);
    SetFillStyle(1,1);
    for i:=2 to length(Way) do {выдиление ребер мннимального пути}
     line(Z[1,ord(way[i-1])-64],Z[2,ord(way[i-1])-64],Z[1,ord(way[i])-64],Z[2,ord(way[i])-64]);
    if s=2 then Rem(F,Z,n);
    for i:=1 to length(Way) do {выдиление вершин и их подпись}
     begin
      SetColor(1);
      PieSlice(Z[1,ord(way[i])-64],Z[2,ord(way[i])-64],0,360,12);
      SetColor(15);
      num:=' ';Str(a,num);
      OutTextXY(Z[1,ord(way[i])-64]-3,Z[2,ord(way[i])-64]-3,num);inc(a);
     end;
    if Way[1]=Way[length(Way)] then {если петля или цикл}
     begin
      SetColor(1);
      PieSlice(Z[1,ord(way[1])-64],Z[2,ord(way[1])-64],0,360,12);
      SetColor(15);
      OutTextXY(Z[1,ord(way[i])-64]-11,Z[2,ord(way[i])-64]-3,'1,'+num)
     end;
    SetColor(10);
    if s=2 then begin {подпись стоимостей ребер}
     Str(R[b],num);
     OutTextXY(10,42,'Стоимость: '+num)
    end;{if}num:=' ';
    for i:=1 to length(Way)-1 do num:=num+Way[i]+' '+chr(16)+' ';
    Way:=num+Way[i+1];
    OutTextXY(10,30,'Mинимальный путь :'+Way);
   end else begin {пути нет}
    SetColor(12);
    OutTextXY(10,30,'Такого пути в графе нет')
   end
end;{Road}
Procedure Init(F:XY;L:matr);
{Init:Ввод двух вершин для обработки}
 var b,c:sf;
     a:char;
     i:byte;
 begin
  for i:=1 to 27 do writeln;
  write(' ':35);
  SetColor(15);
  OutTextXY(30,420,'Внимание! Вводите символы английского алфавита A..');
  OutTextXY(430,420,chr(n+64));
  OutTextXY(30,438,'Введите вершину (начало пути): ');readln(a);
  A:=UpCase(a);
  if not (A in ['A'..chr(n+64)]) then
   begin
    SetColor(12);
    OutTextXY(500,438,'Error');
    Exit
   end;
  b:=ord(A)-64;
  SetColor(10);
  OutTextXY(500,438,'Ok');
  SetColor(1);
  SetFillStyle(1,1);
  PieSlice(F[1,b],F[2,b],0,360,12);
  SetColor(15);
  OutTextXY(F[1,b]-3,F[2,b]-3,'1');
  write(' ':35);
  OutTextXY(30,452,'Введите вершину  (конец пути): ');readln(a);
  A:=UpCase(a);
  if not (A in ['A'..chr(n+64)]) then
   begin
    SetColor(12);
    OutTextXY(500,452,'Error');
    Exit
   end;
  SetColor(10);
  OutTextXY(500,452,'Ok');
  c:=ord(A)-64;
  Road(L,F,n,b,c,flag) {поиск минимального пути}
end;{Init}
Procedure Paint (F:matr;n:sf;s:byte);
{Paint:Графичкское представление графа}
 var x,y,i,j,k,D,M:integer;
     x2,y2,x3,y3:integer;
     w:char;
     q:real;
     P:XY;
 begin
  D:=Detect;
  InitGraph(D,M,' ');
  setcolor(15);
  x:=GetMaxX div 2;
  y:=GetMaxY div 2-20;
  OutTextXY(150,10,'Графическое представление заданного графа :');
  for i:=1 to n do
   begin {создается массив P[] хранящий координаты вершин}
    P[1,i]:=round((y+30)*cos((i-1)*2*Pi/n-Pi/2)+x);
    P[2,i]:=round((y-45)*sin((i-1)*2*Pi/n-Pi/2)+y);
    if P[1,i]>=x then k:=P[1,i]+10 {подписываются вершины}
     else k:=P[1,i]-20;
    if P[2,i]>=y then j:=P[2,i]+10
     else j:=P[2,i]-20;
    OutTextXY(k,j,chr(i+64));
   end;
  for i:=1 to n do {построение ребер}
   for j:=1 to n do
    if (F[i,j]>=1) then
     if i<>j then
      begin
       if F[j,i]>=1 then SetColor(14) {не ориентированное ребро}
       else begin {ориентированное ребро}
         if (P[1,j]-P[1,i])=0 then {q-угол наклона ребра}
          if P[2,i]<P[2,j] then q:=Pi/2 else q:=-Pi/2
         else q:=arctan((P[2,j]-P[2,i])/(P[1,j]-P[1,i]));
         if P[1,i]<=P[1,j] then q:=q+Pi;
         x:=round(10*cos(q)+P[1,j]); {построение стрелки}
         y:=round(10*sin(q)+P[2,j]);
         x2:=round(30*cos(q+3*Pi/180)+P[1,j]);
         y2:=round(30*sin(q+3*Pi/180)+P[2,j]);
         x3:=round(30*cos(q-3*Pi/180)+P[1,j]);
         y3:=round(30*sin(q-3*Pi/180)+P[2,j]);
         SetColor(12);
         SetLineStyle(0,0,3);
         line(x,y,x2,y2);
         line(x2,y2,x3,y3);
         line(x,y,x3,y3);
         SetLineStyle(0,0,1)
        end;
       line(P[1,i],P[2,i],P[1,j],P[2,j])
      end else begin {построение петли}
       SetColor(7);
       circle(P[1,i],P[2,i],10);
       circle(P[1,i],P[2,i],12)
      end;
  for i:=1 to n do
   begin {построение вершин}
    SetColor(3);
    SetFillStyle(1,3);
    PieSlice(P[1,i],P[2,i],0,360,5)
   end;
  if s=2 then Rem(F,P,n);{подпись стоимости ребер}
  Init(P,A); {ввод вершин для обработки}
  SetColor(15);write(' ':56);
  OutTextXY(30,470,'Вы хотите ввести другие вершины для расчетов (Y/N):');readln(w);
  if UpCase(w)='Y' then Paint(A,n,flag);{'Y'-работа с новыми вершинами}
  CloseGraph
end;{Paint}
 {---------------------- *** Основная программа *** ------------------------}
Begin
 Crt;Rd(A,n,flag); {ввод матрицы инциденций}
 Wrt(A,n);         {печать введенной матрицы}
 Paint(A,n,flag)   {графическое представление графа и построение пути}
End.