Program e;
uses Crt;
var
 summ:array [1..32404] of byte;
 help:array [1..32404] of byte;
 lim,i,s,k:word;
begin
 summ[1]:=5;
 help[1]:=5;
 ClrScr;
 writeln(' Вычисление числа e (с необходимой точностью)');
 repeat
  write(' Введите количество цифр после запятой [0..32000] ');
  read(lim);
  readln
 until (lim>=0) and (lim<=32000);                {проверка числа}

 for s:=3 to lim+4 do                            {s- очередной множитель n!}
  begin
   gotoXY(50,1);
   writeln('Число e вычислено на ',s/lim*100:3:0,' %');{слежение за выполнением}

    for i:=1 to lim+4 do                         {help[]=heip[]\s}
     begin
      help[i+1]:=(help[i] mod s)*10+help[i+1];
      help[i]:=(help[i] div s)
     end;{heip[]/s (for i)}

    for i:=lim+4 downto 1 do                     {summ[]=summ[]+help[]}
     begin
      if summ[i]+help[i]<10 then summ[i]:=summ[i]+help[i] else
       begin                                     {0.1886+0.3117=0.5003}
        summ[i]:=(summ[i]+help[i]) mod 10;
        k:=i-1;
        while summ[k]=9 do
         begin
          summ[k]:=0;
          k:=k-1
         end;
        summ[k]:=summ[k]+1
       end
     end{summ[]+help[] (for i)}

  end;{for s}

 k:=4;                                           {печать числа}
 s:=1;
 ClrScr;
 write('e=2.');
 for i:=1 to lim do
  begin
   write(summ[i]);
   k:=k+1;
    if k=1840 then                               {k-кол-во эл. на экране}
     begin
      s:=s+1;
      k:=0;
      writeln;
      write( '         Нажмите клавишу enter для перехода на страницу ',s,' ');
      readln;
      ClrScr
     end
  end;{печати (for i)}
 writeln;
 write('Число e вычисленно  (количество цифр после запятой:  ',lim,')');
 readln
end.
