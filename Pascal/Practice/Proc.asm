.model Large,Pascal
.186
public TsT
.stack
.code
 TsT proc FAR 
     	arg	 h:dword,size:word,color:byte=sz 
	uses	es,bx,cx
	mov	cx,size
	les	bx,h
@for:	mov	al,es:[bx]
	cmp	al,color
	jne	@ne
	inc	bx
	loop 	@for
	mov	ax,1
	jmp	@go
@ne:	mov	ax,0
@go:	ret	
 Tst endp
end