Unit Help_obj;
{===========================================================================}
                          Interface
{===========================================================================}
Const pole_color=14;
      barrier_color=12;
      sprite_color=2;
      barrier_size_a=20;barrier_size_b=100;{ширина и длина барьера}
      sprite_size=10;                      {радиус спрайта}
      alfa_size=30;                        {длина }
      x00=40;x11=590;y00=40;y11=340;       {рабочее поле}
      step=10;                             {шаг при размещении ->,<- и т.д.}
Type B_ptr=^Barrier_rec;
     Barrier_rec= record
      x1,y1,x2,y2,count:word;              {x..y координаты барьера}
      next:B_ptr
     end;{-------- Barrier}
{____________________________________________________________________________}

     Sprite_rec= record
      x,y,step_x,step_y:real;              {xy-координаты центра спрайта}
      count:word;
     end;{--------- Sprite}
{____________________________________________________________________________}

     Sprite_obj= object
                Note:   Sprite_rec;        {информация о спрайте}
      procedure Show;                      {отображает спрайт}
      procedure Go;                        {реакция на клавиши, расстановка}
                          Private
                Movement_or_Go:boolean;    {0-установка или 1-движение ???}
                Mem_ptr:pointer;           {для Get_Screen_Seg, Fre...}
                x1,y1,x2,y2:word;          {атрибуты для Get...,Fre...}
      function  Test:boolean;              {возможно ли занять рабочее поле}
     end;{----- Sprite_obj}
{____________________________________________________________________________}

     Barrier_obj= object
                Head: B_ptr;               {список барьеров}
     procedure First;                      {полная расстановка барьеров}
     destructor Done;
                          Private
                Mem_ptr: pointer;
                x1,y1,x2,y2:word;          {внутренее представление}
                correct: word;             {для поворота барьеров}
                Put_Flag:boolean;          {0-установленн, 1-не устан.(Esc)}
      procedure Show;                      {отображает барьер}
      procedure Go;                        {ставит один барьер,Esc- выход}
      function  Test:boolean;
      procedure Transform;                 {в запись и заносит в список _v,_h}
      procedure Rnd;                       {генерирует расстановку барьеров}
     end;{---- Barrier_obj}
{____________________________________________________________________________}

     Alfa_obj= object
                stp_x,stp_y:real;          {формируемые значения}
                x0,y0:word;                {координаты центра спрайта}
      procedure Go;                        {поворот}
      destructor Done;
                          Private
                Mem_ptr:pointer;
                x1,y1,x2,y2:word;          {вспомогательные координаты}
      procedure Show;
     end;{------- Alfa_obj}
{============================================================================}
                          Implementation
{============================================================================}
Uses Graph,Crt;
Type  help_ptr=^ar;
      ar=  array [1..1] of byte;

 {-------- Asm -------- Asm -------- Asm -------- Asm -------- Asm --------}
Function TsT (h:pointer; size:word; color:byte):boolean; far; external;
{$L proc.obj}

Procedure Get_Screen_Seg (x1,y1,x2,y2:word;Var h:pointer);{прямоугольная}
var  help: help_ptr;
     i,j,n,buf:word;
 begin
  if x2<x1 then begin n:=x1; x1:=x2; x2:=n end;
  if y2<y1 then begin n:=y1; y1:=y2; y2:=n end;
  if h=nil then GetMem(h,(x2-x1+1)*(y2-y1+1)); n:=0; help:=h;
  for i:=0 to y2-y1 do
   begin
    buf:=x1;
    for j:=0 to x2-x1 do
     begin help^[n]:=GetPixel(x1,y1); inc(n); inc(x1) end;
    x1:=buf; inc(y1)
   end{for_i}
 end;{----- Get_Screen_Seg}
Procedure Fre_Screen_Seg (x1,y1,x2,y2:word; h:pointer);
var  help: help_ptr;
     i,j,n,buf:word;
 begin
  if x2<x1 then begin n:=x1; x1:=x2; x2:=n end;
  if y2<y1 then begin n:=y1; y1:=y2; y2:=n end; n:=0; help:=h;
  for i:=0 to y2-y1 do
   begin
    buf:=x1;
    for j:=0 to x2-x1 do
     begin PutPixel(x1,y1,help^[n]); inc(n); inc(x1) end;
    x1:=buf; inc(y1)
   end{for_i}
 end;{____________________________________________________________ Screen_Seg}

Procedure Sprite_obj.Show;
 begin  SetColor(sprite_color);
 with note do
  if not Movement_or_Go then               {установка спрайта}
    begin
     x1:=round(x)-sprite_size-1; y1:=round(y)-sprite_size-1;
     x2:=round(x)+sprite_size+1; y2:=round(y)+sprite_size+1;
     Get_Screen_Seg(x1,y1,x2,y2,Mem_ptr); SetColor(0);
     SetLineStyle(0,0,1); Circle(round(x),round(y),sprite_size);
     PutPixel(round(x),round(y)-sprite_size,pole_color)
   end else begin  {движение, оно возможно,т.к. отработает Graph_obj.Correct}
    Fre_Screen_Seg(x1,y1,x2,y2,Mem_ptr);
    x:=x+step_x; y:=y+step_y;
    x1:=round(x)-sprite_size-1; y1:=round(y)-sprite_size-1;
    x2:=round(x)+sprite_size+1; y2:=round(y)+sprite_size+1;
    Get_Screen_Seg(x1,y1,x2,y2,Mem_ptr); SetColor(sprite_color);
    Circle(round(note.x),round(note.y),sprite_size);
    SetFillStyle(1,sprite_color);
    FloodFill(round(note.x)+3,round(note.y)+3,sprite_color)
       end{with}
 end;{--------------- Show}
Procedure Sprite_obj.Go;
                                  Procedure Message;
                                   var mes:string;
                                   begin
 SetFillStyle(1,3); Bar(x00,y11+50,x11,y11+115);
 SetColor(12); OutTextXY(385,410,'Put Sprite is NECESSARY');
 SetColor(11); mes:='Buttons for movement: ';
 mes:=mes+chr(27)+','+chr(24)+','+chr(25)+','+chr(26); OutTextXY(60,410,mes);
 OutTextXY(60,430,'Put ENTER for EXIT out of Sprite statement')
                                   end;   {Message}
 var key: byte; help:word;
     Flag:boolean;
     Buf: array [0..3] of word;
 begin{Go}
  Message; Movement_or_Go:=false; Flag:=false; Mem_ptr:=nil;
  note.x:=x00+sprite_size+step; note.y:=y00+sprite_size+step;
  with Note do begin
   x1:=round(x)-sprite_size-1; y1:=round(y)-sprite_size-1;
   x2:=round(x)+sprite_size+1; y2:=round(y)+sprite_size+1 end;
  repeat
   Buf[0]:=x1;Buf[1]:=y1;Buf[2]:=x2;Buf[3]:=y2; Show;
   key:=ord(readkey); if key=0 then key:=ord(readkey);
   if Mem_ptr<>nil then Fre_Screen_Seg(Buf[0],Buf[1],Buf[2],Buf[3],Mem_ptr);
   With note do begin
    Case key of             {реакция на клавиотуру с "перепрыжкой"}
{^}   72,56:if y-sprite_size-step>y00 then y:=y-step
              else y:=y11-sprite_size-step;
{V}   80,50:if y+sprite_size+step<y11 then y:=y+step
              else y:=y00+sprite_size+step;
{<-}  75,52:if x-sprite_size-step>x00 then x:=x-step
              else x:=x11-sprite_size-step;
{->}  77,54:if x+sprite_size+step<x11 then x:=x+step
              else x:=x00+sprite_size+step;
{Enter}  13:Flag:=Test
    end;{Case}
    x1:=round(x)-sprite_size-1; y1:=round(y)-sprite_size-1;
    x2:=round(x)+sprite_size+1; y2:=round(y)+sprite_size+1
   end;{with}
  until Flag;                             {установка пока не поставишь Sprite}
  SetColor(Sprite_color); SetLineStyle(0,0,1);
  Circle(round(note.x),round(note.y),sprite_size);
  SetFillStyle(1,sprite_color); Movement_or_Go:=true;
  FloodFill(round(note.x)+3,round(note.y)+3,sprite_color)
{Move...:=true => при дальнейшем обращении к Show будет выполнятся ветвь else}
 end;{--------------- Test}
Function Sprite_obj.Test;
 begin Test:=TsT(Mem_ptr,4*(sprite_size+1)*(sprite_size+1),pole_color) end;

Procedure Barrier_obj.Show;
 begin Get_Screen_Seg(x1,y1,x2,y2,Mem_ptr); SetLineStyle(1,0,1);
       SetColor(0); Rectangle(x1,y1,x2,y2) end;
Procedure Barrier_obj.Go;
                                  Procedure Message;
                                   var mes:string;
                                   begin
 SetFillStyle(1,3); Bar(x00,y11+50,x11,y11+115);
 SetColor(11); mes:='Buttons for movement: ';
 mes:=mes+chr(27)+','+chr(24)+','+chr(25)+','+chr(26);
 mes:=mes+',Enter ; Space:'+'turn'; OutTextXY(60,410,mes);
 OutTextXY(60,430,'Put ESC for EXIT out of barrier statement')
                                   end;{Message}
 var key:byte; help:word;
     Buf:array [0..3] of word;
 begin{Go}
  SetColor(barrier_color); SetLineStyle(0,0,1); Message;
  Put_Flag:=false;                         {барьер не установлен}
  repeat
   Buf[0]:=x1;Buf[1]:=y1;Buf[2]:=x2;Buf[3]:=y2; Show;
   key:=ord(readkey); if key=0 then key:=ord(readkey);
   Fre_Screen_Seg(Buf[0],Buf[1],Buf[2],Buf[3],Mem_ptr);
    Case key of             {реакция на клавиотуру с "перепрыжкой"}
{^}   72,56: if y1-step>y00 then begin y1:=y1-step; y2:=y2-step end
              else begin help:=y2-y1; y2:=y11-step; y1:=y2-help end;
{V}   80,50: if y2+step<y11 then begin y1:=y1+step; y2:=y2+step end
              else begin help:=y2-y1; y1:=y00+step; y2:=y1+help end;
{<-}  75,52: if x1-step>x00 then begin x1:=x1-step; x2:=x2-step end
              else begin help:=x2-x1; x2:=x11-step; x1:=x2-help end;
{->}  77,54: if x2+step<x11 then begin x1:=x1+step; x2:=x2+step end
              else begin help:=x2-x1; x1:=x00+step; x2:=x1+help end;
{поворот}32:begin x1:=x1-correct; x2:=x2+correct;
                  y1:=y1+correct; y2:=y2-correct;
 if correct>0 then {вертик->горизон ,т.е. за пределы возможно вышли x1,x2 ???}
   if (x1-step)<=x00 then begin x1:=x00+step; x2:=x1+barrier_size_b end
     else if (x2+step)>=x11 then begin x2:=x11-step; x1:=x2-barrier_size_b end
 else              {горизон->вертик ,т.е. за пределы возможно вышли y1,y2 ???}
   if (y1-step)<=y00 then begin y1:=y00+step; y2:=y1+barrier_size_b end
     else if (y2+step)>=y11 then begin y2:=y11-step;y1:=y2-barrier_size_b end;
                  correct:=-correct end;{32:}
{Enter}  13:Put_Flag:=true
   end{Case}
  until (key=13) or (key=27);               {ord(Enter)=13,ord(Esc)=27}
  if Put_Flag and Test then Transform
   else Fre_Screen_Seg(x1,y1,x2,y2,Mem_ptr)
 end;{----------------- Go}
Function Barrier_obj.Test;
 begin Test:=TsT(Mem_ptr,barrier_size_a*barrier_size_b,pole_color) end;
Procedure Barrier_obj.Rnd;
 var i,j,R:word; XX,YY:word; {относительные координаты сегмена для генерации}
 begin Randomize; XX:=x00+step;
  for i:=0to(x11-x00-barrier_size_b)div(barrier_size_b+step+3*sprite_size)do
   begin YY:=y00+step;
    for j:=0to(y11-y00-barrier_size_b)div(barrier_size_b+step+3*sprite_size)do
     begin R:=round(random);
      if R=1 then begin R:=round(random);        {устанавливать или нет ???}
       if R=1 then begin                         {вертикальный барьер}
        x1:=XX+round(random(barrier_size_b-barrier_size_a));
        x2:=x1+barrier_size_a; y1:=YY; y2:=y1+barrier_size_b
       end else begin                            {горизонтальный барьер}
        y1:=YY+round(random(barrier_size_b-barrier_size_a));
        y2:=y1+barrier_size_a; x1:=XX; x2:=x1+barrier_size_b
       end;
       Show; Transform; YY:=YY+barrier_size_b+step+3*sprite_size end{if}
     end;{for j} XX:=XX+barrier_size_b+step+3*sprite_size
   end{for i}
 end;{------------- Random}
Procedure Barrier_obj.Transform;
 var new_ptr:B_ptr;
 begin
 SetColor(barrier_color); SetlineStyle(0,0,3); Rectangle(x1+1,y1+1,x2-1,y2-1);
  SetFillStyle(8,barrier_color); FloodFill(x1+5,y1+5,barrier_color);
  New(new_ptr); new_ptr^.next:=Head; Head:=new_ptr; new_ptr^.count:=0;
  new_ptr^.x1:=x1-1; new_ptr^.x2:=x2+1; new_ptr^.y1:=y1-1; new_ptr^.y2:=y2+1
 end;{---------- Transform}
Procedure Barrier_obj.First;
 var flag:boolean;
                                  Procedure Menu;
                                  var mes:string;
                                      key:byte; correct:shortint;
                                   begin
 SetColor(15); OutTextXY(370,397,'Barrier arrengment');
 mes:='Buttons for movement: ';mes:=mes+chr(26)+','+chr(27);
 mes:=mes+', Enter'; SetColor(11); OutTextXY(60,410,mes);
 OutTextXY(60,430,'Put the necessary variant menu');
 SetFillStyle(1,14); Bar(350,415,435,430); Bar(460,415,545,430);
 SetColor(12); OutTextXY(360,420,' CUSTOM        RANDOM'); correct:=55;
 SetLineStyle(0,0,3); Put_Flag:=true; SetColor(1); Rectangle(455,410,550,435);
 repeat
  key:=ord(readkey); if key=0 then key:=ord(readkey);
  if (key=52)or(key=54)or(key=75)or(key=77) then
   begin Put_Flag:=not Put_Flag; correct:=-correct;
    SetColor(3); Rectangle(400-correct,410,495-correct,435);
    SetColor(1); Rectangle(400+correct,410,495+correct,435) end
 until key=13;                     end;{Menu}
                                  Procedure Init;
                                   var help_ptr:B_ptr;
                                   begin
  Mem_ptr:=nil; Head:=nil;
  New(help_ptr); with help_ptr^ do begin   {границы рабочго поля}
   next:=Head; x1:=x00;x2:=x11;y1:=y00-barrier_size_a;y2:=y00+1; count:=0 end;
  Head:=help_ptr;  New(help_ptr);
  with help_ptr^ do begin
   next:=Head; x1:=x00;x2:=x11;y1:=y11-1;y2:=y11+barrier_size_a; count:=0 end;
  Head:=help_ptr;  New(help_ptr);
  with help_ptr^ do begin
   next:=Head; x1:=x00-barrier_size_a;x2:=x00+1;y1:=y00;y2:=y11; count:=0 end;
  Head:=help_ptr;  New(help_ptr);
  with help_ptr^ do begin
   next:=Head; x1:=x11-1;x2:=x11+barrier_size_a;y1:=y00;y2:=y11; count:=0 end;
  Head:=help_ptr;                  end;{Init}
 begin {First}
  Init; Menu;
  if Put_Flag then Rnd else
   begin
    x1:=x00+step; y1:=y00+step; x2:=x1+barrier_size_a; y2:=y1+barrier_size_b;
    correct:=(barrier_size_b-barrier_size_a) div 2;
    repeat Go until not Put_flag end;
 end;{-------------- First}
Destructor Barrier_obj.Done; begin FreeMem(Mem_ptr,(x2-x1+1)*(y2-y1+1)) end;
 {______________________________________________________________ Barrier_obj}

Procedure Alfa_obj.Show;
begin Get_Screen_Seg(x1,y1,x2,y2,Mem_ptr); SetLineStyle(3,0,1);
      SetColor(2); line(x1,y1,x2,y2) end;
Procedure Alfa_obj.Go;
                                  Procedure Message;
                                   var mes:string;
                                   begin
 SetFillStyle(1,3); Bar(x00,y11+50,x11,y11+115); SetColor(11);
 mes:='Buttons for movement: ';mes:=mes+chr(24)+','+chr(25);
 OutTextXY(60,410,mes);
 OutTextXY(60,430,'Put ENTER for EXIT out of Alfa statement')
                                   end;{Message}
 const lng_a=15; lng_b=55; st=3;
 var key: byte; Rez_a:word; correct:real;
     Buf: array [0..3] of word;
 begin
  Rez_a:=0; Message; Mem_ptr:=nil;
  x1:=x0; x2:=x0; y1:=y0-lng_a; y2:=y0-lng_b;
  repeat
   Buf[0]:=x1;Buf[1]:=y1;Buf[2]:=x2;Buf[3]:=y2; Show;
   key:=ord(readkey); if key=0 then key:=ord(readkey);
   Fre_Screen_Seg(Buf[0],Buf[1],Buf[2],Buf[3],Mem_ptr);
    Case key of             {реакция на клавиотуру с "перепрыжкой"}
{^}   72,56: if (Rez_a-st)<=0 then Rez_a:=360-st
              else Rez_a:=Rez_a-st;
{V}   80,50: Rez_a:=(Rez_a+st) mod 360
    end;{Case}
   if (key=50)or(key=56)or(key=72)or(key=80) then begin
    x1:=round((lng_a)*cos((Rez_a-1)*Pi/180-Pi/2)+x0);
    y1:=round((lng_a)*sin((Rez_a-1)*Pi/180-Pi/2)+y0);
    x2:=round((lng_b)*cos((Rez_a-1)*Pi/180-Pi/2)+x0);
    y2:=round((lng_b)*sin((Rez_a-1)*Pi/180-Pi/2)+y0) end
  until (key=13);                          {ord(Enter)=13}
  Fre_Screen_Seg(Buf[0],Buf[1],Buf[2],Buf[3],Mem_ptr);
  stp_x:=cos(Rez_a*Pi/180-Pi/2); stp_y:=sin(Rez_a*Pi/180-Pi/2);
  if abs(stp_x)>abs(stp_y) then correct:=abs(1/stp_x)
                           else correct:=abs(1/stp_y);
  stp_x:=stp_x*correct; stp_y:=stp_y*correct;
 end;{----------------- Go}
Destructor Alfa_obj.Done; begin FreeMem(Mem_ptr,abs((x2-x1+1)*(y2-y1+1))) end;
 {_________________________________________________________________ Alfa_obj}
End.