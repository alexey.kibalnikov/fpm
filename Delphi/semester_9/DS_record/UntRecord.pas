unit UntRecord;

{=========================================================================}
interface
{=========================================================================}
uses
  SysUtils;

const
  ix_Byte      = Integer(0);
  ix_Word      = Integer(1);
  ix_Integer   = Integer(2);
  ix_LongInt   = Integer(3);
  ix_Double    = Integer(4);
  ix_Boolean   = Integer(5);
  ix_String    = Integer(6);
  ix_TDateTime = Integer(7);
  ix_Binary    = Integer(8);

type
  myException = class (Exception) end;

{ TField =================================================================}

  TField = class
  protected
    F_LongName: String;
    F_Name    : String;
    F_Type    : Integer;
    F_Length  : Word;
    F_Format  : String;
    F_Data: Pointer; //array of byte; <- изменение !!!
    function GetData: Pointer;
    procedure SetData(Data: Pointer);
    function GetInteger: LongInt; virtual;
    procedure SetInteger (Value: LongInt); virtual;
    function GetBoolean: Boolean; virtual;
    procedure SetBoolean (Value: Boolean); virtual;
    function GetFloat: Double; virtual;
    procedure SetFloat(value:Double);virtual;
    function GetDateTime: TDateTime; virtual;
    procedure SetDateTime (Value: TDateTime); virtual;
    function GetVariant: Variant; virtual;
    procedure SetVariant (Value: Variant); virtual;
    function GetString: String; virtual;
    procedure SetString (Value: String); virtual;
    function GetText: String; virtual;
    procedure SetText (Value: String); virtual;
  public
    procedure Done(); virtual;
    property LongName : String read F_LongName write F_LongName;
    property Name : String read F_Name write F_Name;
    property ValueType : Integer read F_Type;
    property Length : Word read F_Length;
    property Format : String read F_Format write F_Format;
    property FieldType : Integer read F_Type;

    property asInteger : Integer read getInteger write setInteger;
    property asBoolean : Boolean read getBoolean write setBoolean;
    property asFloat   : Double read getFloat write setFloat;
    property asDateTime: TDateTime read getDateTime write setDateTime;
    property asVariant : Variant read getVariant write setVariant;
    property asString  : String read getString write setString;
    property asText    : String read getText write setText;
  end;//TField

{ TIntField --------------------------------------------------------------}

  TIntField = class (TField)
  protected
    function getInteger: LongInt; override;
    procedure setInteger(Value: LongInt); override;
    function getBoolean: Boolean; override;
    function getFloat: Double; override;
    function getDateTime: TDateTime; override;
    function getVariant: Variant; override;
    function getString: String; override;
    procedure setString (Value: String); override;
    function getText: String; override;
  public
    constructor Create(Value: LongInt=0; typeID: Integer= ix_Integer);
  end;//TIntField

{ TFloatField ------------------------------------------------------------}

  TFloatField = class (TField)
  protected
    function getInteger: LongInt; override;
    function getBoolean: Boolean; override;
    function getFloat: Double; override;
    procedure setFloat(Value: Double); override;
    function getDateTime: TDateTime; override;
    function getVariant: Variant; override;
    function getString: String; override;
    procedure setString (Value: String); override;
    function getText: String; override;
  public
    constructor Create(Value: Double=0);
  end;//TIntField

{ TDateTimeField ---------------------------------------------------------}

  TDateTimeField = class (TField)
  protected
    function getBoolean: Boolean; override;
    function getFloat:Double; override;
    procedure setFloat(Value: Double); override;
    function getDateTime: TDateTime; override;
    procedure setDateTime(Value: TDateTime); override;
    function getVariant: Variant; override;
    function getString: String; override;
    procedure setString(Value: String); override;
    function getText: String; override;
  public
    constructor Create(Value: TDateTime=0);
  end;//TDateTimeField

{ TBinaryField -----------------------------------------------------------}

  TBinaryField = class(TField)
  protected
    function getInteger: LongInt; override;
    function getBoolean: Boolean; override;
    function getFloat: Double; override;
    function getDateTime: TDateTime; override;
    function getVariant: Variant; override;
    procedure setVariant(Value: Variant); override;
    function getString: String; override;
  public
    constructor Create(Value: Pointer=nil; Length: Word=0);
  end;//TBinaryField

{ TStringField -----------------------------------------------------------}

  TStringField = class (TField)
  protected
    function getInteger: LongInt; override;
    procedure setInteger(Value: LongInt); override;
    function getBoolean: Boolean; override;
    function getFloat: Double; override;
    procedure setFloat(Value:Double); override;
    function getDateTime: TDateTime; override;
    procedure setDateTime(Value: TDateTime); override;
    function getVariant: Variant; override;
    function getString: String; override;
    procedure setString(Value: String); override;
    function getText: String; override;
    procedure setText(Value: String); override;
  public
    constructor Create(Value: String='');
  end;//TStringField

{ TBooleanField ----------------------------------------------------------}

  TBooleanField = class (TField)
  protected
    function getString: String; overload;
    function getBoolean: Boolean; override;
    procedure setBoolean(Value: Boolean); override;
  public
    constructor Create(Value: Boolean=false);
  end;//TBooleanField

{ TRecord ================================================================}

  TFieldArray = array of TField;

  TRecord = class
  private
    R_Fields: TFieldArray;
    function GetFieldsCount: Word;
  public
    property FieldsCount: Word read GetFieldsCount;
    property Fields: TFieldArray read R_Fields write R_Fields;
    procedure Done;
    function FieldByName(name: String): TField;
    function FieldByIndex(index:Integer): TField;
    function Insert(Field: TField; index: Integer=-1): TField;
    function Delete(Field: TField): Boolean; overload;
    function Delete(index: Integer): Boolean; overload;
  end;
{=========================================================================}
implementation
{=========================================================================}
uses
  Variants;

{ TField -----------------------------------------------------------------}

procedure TField.Done;
begin
  FreeMem(F_Data);
  self.Free;
end;

function TField.GetData: Pointer;
begin
  Move(F_Data^, result^, F_Length);
end;

procedure TField.SetData(data: Pointer);
begin
  Move(data^, F_Data^, F_Length);
end;

function TField.getInteger: LongInt;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> integer');
end;

procedure TField.setInteger(value: LongInt);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> integer');
end;

function TField.getBoolean: Boolean;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> boolean');
end;

procedure TField.setBoolean(value: Boolean);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> boolean');
end;

function TField.getFloat: Double;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> float');
end;

procedure TField.setFloat(value: Double);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> double');
end;

function TField.getDateTime: TDateTime;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> dateTime');
end;

procedure TField.setDateTime(value: TDateTime);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> dateTime');
end;

function TField.getString: String;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> string');
end;

procedure TField.setString(value: String);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> string');
end;

function TField.getText: String;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> text');
end;

procedure TField.setText(value: String);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> text');
end;

function TField.getVariant: Variant;
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить get --> variant');
end;

procedure TField.setVariant(value: Variant);
begin
  raise EConvertError.Create('Ќ≈Ћ№«я применить set --> variant');
end;

{ TIntField --------------------------------------------------------------}

constructor TIntField.Create(Value: LongInt=0; typeID: Integer= ix_Integer);
begin
  case typeID of
    ix_Byte    : F_Length := sizeof(byte);
    ix_Word    : F_Length := sizeof(word);
    ix_Integer : F_Length := sizeof(integer);
    ix_LongInt : F_Length := sizeof(longint);
  else
    raise myException.Create('Ќеизвестный тип (typeID)');
  end;
  F_Type := typeID;
  GetMem(F_Data, F_Length);
  SetData(@value);
end;

function TIntField.getInteger: LongInt;
begin
  result := 0;
  Move(F_Data^, result, F_Length);
end;

procedure TIntField.setInteger(value: LongInt);
begin
  Move(value, F_Data^, F_Length);
end;

function TIntField.getBoolean: Boolean;
begin
  if getInteger = 0 then
    result := false
  else
    result := true;
end;

function TIntField.getFloat: Double;
begin
  result := getInteger;
end;

function TIntField.getDateTime: TDateTime;
begin
  result := getFloat;
end;

function TIntField.getString: String;
begin
  result := IntToStr(getInteger);
end;

procedure TIntField.setString(value: String);
var
  i: Integer;
begin
  i := StrToInt(value);
  Move(i, F_Data^, F_Length);
end;

function TIntField.getText: String;
begin
  result := SysUtils.Format(F_Format, [asInteger]);
end;

function TIntField.getVariant: Variant;
begin
  result := asInteger;
end;

{ TFloatField ------------------------------------------------------------}

constructor TFloatField.Create(value: Double=0);
begin
  F_Length := SizeOf(double);
  F_Type := ix_Double;
  GetMem(F_Data, F_Length);
  SetData(@value);
end;

function TFloatField.getInteger: LongInt;
begin
  result := Round(getFloat);
end;

function TFloatField.getBoolean: Boolean;
begin
  if getInteger = 0 then
    result := false
  else
    result := true;
end;

function TFloatField.getFloat: Double;
begin
  Move(F_Data^, result, F_Length);
end;

procedure TFloatField.setFloat(value: Double);
begin
  Move(value, F_Data^, F_Length);
end;

function TFloatField.getDateTime: TDateTime;
begin
  result := getFloat;
end;

function TFloatField.getString: String;
begin
  result := FloatToStr(getFloat);
end;

procedure TFloatField.setString(value: String);
var
  f: Double;
begin
  f := StrToFloat(value);
  Move(f, F_Data^, F_Length);
end;

function TFloatField.getText: String;
begin
  result := SysUtils.Format(F_Format, [asFloat]);
end;

function TFloatField.getVariant: Variant;
begin
  result := asFloat;
end;

{ TDateTimeField ---------------------------------------------------------}

constructor TDateTimeField.Create(value: TDateTime=0);
begin
  F_Length := SizeOf(TDateTime);
  F_Type:= ix_TDateTime;
  GetMem(F_Data, F_Length);
  SetData(@value);
end;

function TDateTimeField.getBoolean: Boolean;
begin
  if getFloat=0 then
    result := false
  else
    result := true;
end;

function TDateTimeField.getFloat: Double;
begin
  result := getDateTime;
end;

procedure TDateTimeField.setFloat(value: Double);
begin
  setDateTime(value);
end;

function TDateTimeField.getDateTime: TDateTime;
begin
  Move(F_Data^, result, F_Length);
end;

procedure TDateTimeField.setDateTime(value: TDateTime);
begin
  Move(value, F_Data^, F_Length);
end;

function TDateTimeField.getString: String;
begin
  result := DateTimeToStr(getDateTime);
end;

procedure TDateTimeField.setString(value:String);
var
  d: TDateTime;
begin
  d := StrToDateTime(value);
  Move(d, F_Data^, F_Length);
end;

function TDateTimeField.getText: String;
begin
  result := SysUtils.Format(F_Format, [asDateTime]);
end;

function TDateTimeField.getVariant: Variant;
begin
  result := asDateTime;
end;

{ TBinaryField -----------------------------------------------------------}

constructor TBinaryField.Create(value: Pointer = nil; length: Word = 0);
begin
  F_Length :=length;
  F_Type := ix_Binary;
  GetMem(F_Data, F_Length);
  if value <> nil then
    SetData(value);
end;

function TBinaryField.getInteger: LongInt;
begin
  result := 0;
  if F_Length < sizeof(LongInt) then
    Move(F_Data^, result, F_Length)
  else
    raise myException.Create('Ќ≈Ћ№«я применить get --> integer');
end;

function TBinaryField.getFloat: Double;
begin
  result := 0;
  if F_Length < sizeof(double) then
    Move(F_Data^, result, F_Length)
  else
    raise myException.Create('Ќ≈Ћ№«я применить get --> double');
end;

function TBinaryField.getDateTime: TDateTime;
begin
  result := getFloat;
end;
//------------------------------------------------------------------------------
function TBinaryField.getBoolean: Boolean;
begin
  if F_Length = 0 then
    result := false
  else
    result:=true;
end;

function TBinaryField.getVariant: Variant;
var
  i: Integer;
begin
  result := VarArrayCreate([0, F_Length-1], varSmallInt);
//  for i := 0 to F_Length-1 do result[i] := F_Data[0]; исходник
  Move(F_Data^, pChar(@result)[0], F_Length);
end;

function TBinaryField.getString: String;
var
  i: Integer;
begin
  result := '';
  for i := 0 to F_Length-1 do
    result := IntToHex(StrToInt(pChar(F_Data)[i]), 1) + result;
  result := '0x' + result;
end;

procedure TBinaryField.setVariant(value: Variant);
var
  i, j: Integer;
begin
  if not VarIsArray(value) then
    raise myException.Create('value не входит varArray');
  if VarArrayDimCount(value) <> 1 then
    raise myException.Create('value слишком большой');
  if VarArrayHighBound(value, 1) - VarArrayLowBound(value, 1) + 1 <> F_Length then
    raise myException.Create('value имеет некорректный размер');
  j := 0;
  for i := VarArrayLowBound(value, 1) to VarArrayHighBound(value, 1) do
    begin
//    F_Data[j] := value[i]; // исходник !!!
      Variant((@pChar(F_Data)[j])^) := value[i];
      inc(j);
    end;
end;

{ TString ----------------------------------------------------------------}

constructor TStringField.Create(value: String='');
begin
  F_Length := System.Length(value);
  F_Type := ix_String;
  GetMem(F_Data, F_Length);
  SetData(@(value[1]));
end;

function TStringField.getInteger: LongInt;
begin
  result := StrToInt(getString);
end;

procedure TStringField.setInteger(value: LongInt);
var
  s: String;
begin
  s := IntToStr(value);
  F_Length := System.Length(s);
  GetMem(F_Data, F_Length);
  SetData(@(s[1]));
end;

function TStringField.getBoolean: Boolean;
begin

end;

function TStringField.getFloat: Double;
begin
  result := StrToFloat(getString);
end;

procedure TStringField.setFloat(value:Double);
var
  s: String;
begin
  s := FloatToStr(value);
  F_Length := System.Length(s);
  GetMem(F_Data, F_Length);
  SetData(@(s[1]));
end;

function TStringField.getDateTime: TDateTime;
begin
  result := StrToDateTime(getString);
end;

procedure TStringField.setDateTime(value: TDateTime);
var
  s: String;
begin
  s := DateTimeToStr(value);
  F_Length := System.Length(s);
  GetMem(F_Data, F_Length);
  SetData(@(s[1]));
end;

function TStringField.getString: String;
begin
  SetLength(result, F_Length);
  Move(F_Data^, result[1], F_Length);
end;

procedure TStringField.setString(value: String);
begin
  F_Length := System.Length(value);
  GetMem(F_Data, F_Length);
  SetData(@(value[1]));
end;

function TStringField.getText:String;
begin
  result := SysUtils.Format(F_Format, [getString]);
end;

procedure TStringField.setText(value: String);
var
  s: String;
begin
  s := SysUtils.Format(F_Format, [value]);
  F_Length := System.Length(s);
  GetMem(F_Data, F_Length);
  SetData(@(s[1]));
end;

function TStringField.getVariant: Variant;
begin
  result := getString;
end;

{ TBooleanField ----------------------------------------------------------}

constructor TBooleanField.Create(value: Boolean=false);
begin
  if value then
    F_Length := 1
  else
    F_Length := 0;
  F_Type := ix_Boolean;
end;

function TBooleanField.getString: String;
begin
  if F_Length = 1 then
    result := 'true'
  else
    result := 'false';
end;

function TBooleanField.getBoolean: Boolean;
begin
  if F_Length = 0 then
    result := false
  else
    result := true;
end;

procedure TBooleanField.setBoolean(value:Boolean);
begin
  if value then
    F_Length := 1
  else
    F_Length := 0;
end;

{ TRecord ================================================================}

procedure TRecord.Done;
var
  i: Integer;
begin
  for i := 0 to Length(R_Fields)-1 do
    R_Fields[i].Done;
  SetLength(R_Fields, 0);
  Self.Free;
end;

function TRecord.GetFieldsCount: Word;
begin
  result := Length(R_Fields);
end;

function TRecord.FieldByName(name: String): TField;
var
  i: Integer;
begin
  result := nil;
  for i := 0 to Length(R_Fields)-1 do
    if R_Fields[i].name = name then
      begin
        result := R_Fields[i];
        exit;
      end;
end;

function TRecord.FieldByIndex(index: Integer): TField;
begin
  if (index < -1) or (index > Length(R_Fields)) then
    raise Exception.Create('¬ыход за пределы индекса');
  if index = -1 then
    result := R_Fields[FieldsCount-1]
  else
    result := R_Fields[index-1];
end;

function TRecord.Insert(Field: TField; index: Integer=-1): TField;
var
  i: Integer;
begin
  if (index < -1) or (index > Length(R_Fields)) then
    raise Exception.Create('¬ыход за пределы индекса');
  SetLength(R_Fields, Length(R_Fields)+1);
  if index = -1 then
    R_Fields[Length(R_Fields)-1] := field
  else
    begin
      for i := Length(R_Fields)-1 to index do
        R_Fields[i] := R_Fields[i-1];
      R_Fields[index-1] := Field;
    end;
  result := Field;
end;

function TRecord.Delete(Field: TField): Boolean;
var
  i, j: Integer;
begin
  for i := 0 to Length(R_Fields) - 1 do
    if R_Fields[i] = Field then
      begin
        R_Fields[i].Done;
        for j := i to Length(R_Fields) - 2 do
          R_Fields[j] := R_Fields[j+1];
        SetLength(R_Fields, Length(R_Fields) - 1);
        result := true;
        exit;
      end;
  result := false;
end;

function TRecord.Delete(index: Integer): Boolean;
var
  i: Integer;
begin
  if (index < -1) or (index > Length(R_Fields)) then
    raise Exception.Create('¬ыход за пределы индекса');
  if index = -1 then
    begin
      R_Fields[Length(R_Fields)-1].Done;
      SetLength(R_Fields, Length(R_Fields)-1);
    end
  else
    begin
      R_Fields[index-1].Done;
      for i := index-1 to Length(R_Fields) - 2 do
            R_Fields[i] := R_Fields[i+1];
      SetLength(R_Fields, Length(R_Fields) - 1);
    end;
  result := true;
end;

end.
 