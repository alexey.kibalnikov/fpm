program Prog2;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Dialogs,
  Variants, //VarArrayCreate
  UntRecord in 'UntRecord.pas';

VAR
  REC: TRecord;
  index: Integer;

procedure IndexPrev;
var
  F: TField;
begin
  if index > 1 then
    begin
      dec(index);
      F := REC.FieldByIndex(index);
    end
  else
    ShowMessage('Not found');
end;//IndexPrev

procedure IndexNext;
var
  F: TField;
begin
  if index < REC.FieldsCount then
    begin
      inc(index);
      F := REC.FieldByIndex(index);
    end
  else
    ShowMessage('Not found');
end;//IndexNext

procedure SaveField(memoData, _Name, _longName, _Format: string; SaveFlag: integer);
var
  F: TField;
  i: Integer;
  V: Variant;
begin
  try
    if (index <= REC.FieldsCount) and (index > 0) then
      begin
        F := REC.FieldByIndex(index);
        F.Name := _Name;
        F.LongName := _LongName;
        F.Format := _Format;

        case SaveFlag of
          0: F.asString := memoData;
          1: F.asText := memoData;
          2: F.asInteger := StrToInt(memoData);
          3: F.asFloat := StrToFloat(memoData);
          4: F.asDateTime := StrToDateTime(memoData);
          5: if memoData<>'' then
               F.asBoolean := true
             else
               F.asBoolean := false;
          6: if F.FieldType= ix_Binary then
               begin
                 V := VarArrayCreate([0, length(memoData)-1], varSmallInt);
                 for i := 1 to Length(memoData) do
                   V[i-1]:= Ord(memoData[i]);
                 F.asVariant := V;
               end
             else
               F.asVariant := memoData;
        end;
    end;
  except on e: Exception do
    ShowMessage(e.Message);
  end; // try
end;//SaveField

procedure ShowField(F: TField; var memoData, _Name, _LongName, _Format, _Type: string; var _Length: longint; ShowFlag: integer);
var
  i: Integer;
begin
  _Name := F.Name;
  _LongName := F.LongName;
  _Format := F.Format;
  _Length := F.Length;
  case ShowFlag of
    0: memoData := F.asString;
    1: memoData := F.asText;
    2: memoData := IntToStr(F.asInteger);
    3: memoData := FloatToStr(F.asFloat);
    4: memoData := DateTimeToStr(F.asDateTime);
    5: if F.asBoolean then
         memoData :='true'
       else
         memoData := 'false';
    6: if VarIsArray(F.asVariant) then
          begin
            memoData := '';
            for i := VarArrayLowBound(F.asVariant, 1) to VarArrayHighBound(F.asVariant, 1) do
              memoData := memoData + Chr(byte(F.asVariant[i]));
          end
        else
          memoData := F.asVariant;
  end;//Case
  case f.fieldType of
        ix_Byte      :  _Type := 'Byte';
        ix_Integer   :  _Type := 'Integer';
        ix_Word      :  _Type := 'Word';
        ix_LongInt   :  _Type := 'LongInt';
        ix_Double    :  _Type := 'Double';
        ix_TDateTime :  _Type := 'TDateTime';
        ix_String    :  _Type := 'String';
        ix_Binary    :  _Type := 'Binary';
        ix_Boolean   :  _Type := 'Boolean';
  end;//Case
end;//ShowField

procedure Display(ShowFlag: integer);
var
  F: TField;
  memoData, Name, LongName, Format, _Type: string;
  Length: longint;
begin
  try
    if (index <= REC.FieldsCount) and (index > 0) then
      begin
        F := REC.FieldByIndex(index);
        ShowField(F, memoData, Name, LongName, Format, _Type, Length, ShowFlag);
        writeln('memoData= '+memoData);
        writeln('Name= '+Name);
        writeln('LongNane= '+LongName);
        writeln('Format= '+Format);
        writeln('Type= '+_Type);
        writeln('Length= '+IntToStr(Length));
      end
    else ShowMessage('������ ���� ���');
  except on e: Exception do
    ShowMessage(e.Message);
  end;
end;//Display

procedure CreateField(CreateFlag: integer);
begin
  case CreateFlag of
    0: REC.Insert(TStringField.Create);
    1: REC.Insert(TIntField.Create);
    2: REC.Insert(TFloatField.Create);
    3: REC.Insert(TDateTimeField.Create);
    4: REC.Insert(TBooleanField.Create);
    5: REC.Insert(TBinaryField.Create(nil,10));
  end;
  inc(index);
  Display(CreateFlag);
end;//CreateField

BEGIN
  { TODO -oUser -cConsole Main : Insert code here }
  try
    rec := TRecord.Create;
    index := 0;

  finally

    rec.Done;
  end;//try

END.
