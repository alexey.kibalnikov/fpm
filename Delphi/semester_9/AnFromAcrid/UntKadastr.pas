unit UntKadastr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ShellApi, ComObj, Word97, Word2000, Menus,
  ComCtrls;

type
  vect=array of real;

  TMainFrame = class(TForm)
    cbRegion: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    edtPrice: TEdit;
    Label3: TLabel;
    cbParams: TComboBox;
    Label5: TLabel;
    cbParams2: TComboBox;
    edtPercent: TEdit;
    Label8: TLabel;
    edtAllPercent: TEdit;
    Label9: TLabel;
    edtSize: TEdit;
    Label10: TLabel;
    edtRez: TEdit;
    btnAddParam: TButton;
    btnDelParam: TButton;
    btnRun: TButton;
    sgParams: TStringGrid;
    btnSaveDoc: TButton;
    btnOpenDoc: TButton;
    sdSave: TSaveDialog;
    mnAbout: TMainMenu;
    btnAbout: TMenuItem;
    btnChange: TButton;
    btnRemParams: TMenuItem;
    Label4: TLabel;
    odOpen: TOpenDialog;
    Label6: TLabel;
    DTPDate: TDateTimePicker;
    procedure FormCreate(Sender: TObject);
    procedure cbRegionSelect(Sender: TObject);
    procedure cbParamsSelect(Sender: TObject);
    procedure cbParams2Select(Sender: TObject);
    procedure btnAddParamClick(Sender: TObject);
    procedure btnDelParamClick(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure btnOpenDocClick(Sender: TObject);
    procedure btnSaveDocClick(Sender: TObject);
    procedure edtPriceChange(Sender: TObject);
    procedure edtSizeChange(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure btnRemParamsClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure cbRegionEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainFrame : TMainFrame;
  UE,PR : vect;

implementation
  uses   UntProc;
{$R *.dfm}

procedure TMainFrame.FormCreate(Sender: TObject);
 begin
   DTPDate.Date:=Date;
   DecimalSeparator:=_DecimalSeparator_;
   ReadRegion(UE);
   ReadParams;
end;

procedure TMainFrame.cbRegionSelect(Sender: TObject);
 begin
   edtRez.Text:='';
   edtPrice.Text:=FloatToStr(UE[cbRegion.ItemIndex]);
end;

procedure TMainFrame.cbParamsSelect(Sender: TObject);
begin SelectParams(PR); end;

procedure TMainFrame.cbParams2Select(Sender: TObject);
begin edtPercent.Text:=FloatToStr(PR[cbParams2.ItemIndex]); end;

procedure TMainFrame.btnAddParamClick(Sender: TObject);
  var s: string;
 begin
   edtRez.Text:='';
   if (cbParams.Text<>'') and (cbParams2.Text<>'') and (edtPercent.Text<>'') then
     begin
       s:=cbParams.Text+' '+cbParams2.Text+' '+edtPercent.Text+'%';
       if sgParams.Cells[0,0]<>'' then sgParams.RowCount:=sgParams.RowCount+1;
       sgParams.Cells[0,sgParams.RowCount-1]:=s;
       edtAllPercent.Text:=FloatToStr(StrToFloat(edtAllPercent.Text)+StrToFloat(edtPercent.Text));
     end else
       MessageDlg('Не заполнены необходимые поля', mtInformation, [mbOk], 0);
end;

procedure TMainFrame.btnDelParamClick(Sender: TObject);
  var i:  integer;
      s,st:  string;
 begin
   edtRez.Text:='';
   if sgParams.RowCount<>1 then
     begin
       s:=sgParams.Cells[0,sgParams.Row];
       i:=Length(s)-1;
       st:='';
       while s[i]<>' ' do begin st:=s[i]+st; dec(i); end;
       edtAllPercent.Text:=FloatToStr(StrToFloat(edtAllPercent.Text)-StrToFloat(st));
       sgParams.Rows[sgParams.Row].Clear;
       for i:=sgParams.Row to sgParams.RowCount-2 do
         sgParams.Cells[0,i]:=sgParams.Cells[0,i+1];
       sgParams.Rows[sgParams.RowCount-1].Clear;
       sgParams.RowCount:=sgParams.RowCount-1;
     end else
       sgParams.Rows[sgParams.Row].Clear;
end;

procedure TMainFrame.btnRunClick(Sender: TObject);
 begin
   if (edtPrice.Text<>'') and (edtAllPercent.Text<>'') and (edtSize.Text<>'') then
     edtRez.Text:= Format('%*.*f',[9,2,StrToFloat(edtPrice.Text)*(1+StrToFloat(edtAllPercent.Text)/100)*StrToFloat(edtSize.Text)])
    else
     MessageDlg('Не заполнены необходимые поля', mtInformation, [mbOk], 0);
end;

procedure TMainFrame.btnOpenDocClick(Sender: TObject);
  var s: pChar;
 begin
   odOpen.Filter:='MS Word files (*.doc)|*.doc';
   odOpen.InitialDir:=GetCurrentDir;
   odOpen.FileName:='result.doc';
   if odOpen.Execute then
     begin
       s:=pChar(odOpen.FileName);
       ShellExecute(Application.Handle, 'open',s,nil,nil,SW_SHOWNORMAL);
     end;
end;

procedure TMainFrame.btnSaveDocClick(Sender: TObject);
  var Word:     OleVariant;
      Document: OleVariant;
      i: integer;
      s,sf,sd:  string;
 begin
   sdSave.Filter:='MS Word files (*.doc)|*.doc';
   sdSave.InitialDir:=GetCurrentDir;
   sdSave.fileName:='result.doc';
   if sdSave.Execute then
     begin
       if (cbRegion.Text<>'') and (edtPrice.Text<>'') and
          (edtAllPercent.Text<>'') and (edtSize.Text<>'') and
          (edtRez.Text<>'') then
         begin
           Word:=CreateOleObject('Word.Application');
           Document:=Word.Documents.Add;
           Word.Selection.InsertAfter('Рыночная стоимость жилья на '+DateToStr(DTPDate.Date)+#13+#10);
           Word.Selection.Font.Bold:=1;
           Word.Selection.Font.Size:=28;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(#13+#10+'Район:');
           Word.Selection.Font.Size:=14;
           Word.Selection.Font.Bold:=1;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(' '+cbRegion.Text+#13);
           Word.Selection.Font.Bold:=0;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter('Стоимость 1 кв.м.:');
           Word.Selection.Font.Size:=14;
           Word.Selection.Font.Bold:=1;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(' '+edtPrice.Text+' у.е.'+#13);
           Word.Selection.Font.Bold:=0;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(#13+#10+'Параметры:'+#13+#10);
           Word.Selection.Font.Size:=18;
           Word.Selection.Font.Bold:=1;
           Word.Selection.Collapse(wdCollapseEnd);
           Word.Selection.Font.Size:=14;

           i:=0;
           while i<=sgParams.RowCount-1 do
             begin
               Word.Selection.InsertAfter('        '+sgParams.Cells[0,i]+#13);
               Word.Selection.Font.Bold:=0;
               Word.Selection.Collapse(wdCollapseEnd);
               inc(i);
             end;

           Word.Selection.InsertAfter('Суммарный процент:');
           Word.Selection.Font.Bold:=1;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(' '+edtAllPercent.Text+' %'+#13);
           Word.Selection.Font.Bold:=0;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter('Общая площадь жилья:');
           Word.Selection.Font.Bold:=1;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(' '+edtSize.Text+' кв.м.'+#13+#10);
           Word.Selection.Font.Bold:=0;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(#13+#10+'Стоимость жилья:');
           Word.Selection.Font.Bold:=1;
           Word.Selection.Font.Size:=18;
//           Word.Selection.Font.Color:=clTeal;
           Word.Selection.Collapse(wdCollapseEnd);

           Word.Selection.InsertAfter(' '+edtRez.Text+' у.е.'+#13);
           Word.Selection.Font.Bold:=0;
           Word.Selection.Collapse(wdCollapseEnd);

           s:=sdSave.FileName; i:=Length(s); sf:=''; sd:='';
           while s[i]<>'\' do begin sf:=s[i]+sf; dec(i); end; dec(i);
           while i>0 do begin sd:=s[i]+sd; dec(i); end;

           Word.ChangeFileOpenDirectory(sd);
           Document.SaveAs(FileName:=sf);
           Document.Close;
           Word.Quit;
         end else
           MessageDlg('Не заполнены необходимые поля', mtInformation, [mbOk], 0);
     end;
end;

procedure TMainFrame.edtPriceChange(Sender: TObject);
 begin edtRez.Text:=''; end;

procedure TMainFrame.edtSizeChange(Sender: TObject);
 begin edtRez.Text:=''; end;

procedure TMainFrame.btnAboutClick(Sender: TObject);
begin MessageDlg('Разработчик программы:'+#10#13+'Хрюкина Анна'+#10#13+'ФПМ, 5 курс, 2004',mtInformation,[mbOk],0); end;

procedure TMainFrame.btnChangeClick(Sender: TObject);
 begin
   cbRegion.Tag:=1;
   ShellExecute(Application.Handle, 'open',_region_,nil,nil,SW_SHOWNORMAL);
   ShellExecute(Application.Handle, 'close',_region_,nil,nil,SW_SHOWNORMAL);
end;

procedure TMainFrame.btnRemParamsClick(Sender: TObject);
begin ShellExecute(Application.Handle, 'open',_termins_,nil,nil,SW_SHOWNORMAL); end;

procedure TMainFrame.Button1Click(Sender: TObject);
begin
   cbRegion.Tag:=1;
   cbRegion.Items.Clear;
   ReadRegion(UE);
   edtPrice.Text:='';
end;

procedure TMainFrame.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if cbRegion.Tag=1 then
    begin
      MessageDlg('Данные о ценах на жилье по районам изменены',mtInformation,[mbOk],0);
      cbRegion.Items.Clear;
      ReadRegion(UE);
      edtPrice.Text:='';
      cbRegion.Tag:=0;
    end;
end;

procedure TMainFrame.cbRegionEnter(Sender: TObject);
begin
  if cbRegion.Tag=1 then
    begin
      MessageDlg('Данные о ценах на жилье по районам изменены',mtInformation,[mbOk],0);
      cbRegion.Items.Clear;
      ReadRegion(UE);
      edtPrice.Text:='';
      cbRegion.Tag:=0;
    end;
end;

end.

