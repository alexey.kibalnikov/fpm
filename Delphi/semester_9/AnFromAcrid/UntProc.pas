unit UntProc;

interface
{--------------------------------------------------------------------------}
uses UntKadastr;

const _DecimalSeparator_='.';
      _CSVSeparator_=';';
      _region_='region.doc';
      _termins_='termins.doc';
      _Params_='params.csv';

procedure ReadRegion(var UE : vect);
procedure ReadParams;
procedure SelectParams(var PR : vect);

{--------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------}
  uses  SysUtils, Dialogs;

procedure ReadRegion;
  var
    f:  text;
    s,st:string;
    i,j:integer;
 begin
   assignfile(f,_region_);
   reset(f);
   j:=0;
   while not eof(f) do
     begin
       readln(f,s);
       i:=1;
       st:='';
       while s[i]<>_CSVSeparator_ do begin st:=st+s[i]; inc(i); end;
       MainFrame.cbRegion.Items.Add(st);
       inc(j);
     end;
     closefile(f);
     SetLength(UE,j);
     reset(f);
     j:=0;
     while not eof(f) do
       begin
         readln(f,s);
         i:=Length(s);
         st:='';
         while s[i]<>_CSVSeparator_ do begin st:=s[i]+st; dec(i); end;
         UE[j]:=StrToFloat(st);
         inc(j);
       end;
     closefile(f);
end;// ReadRegion

procedure ReadParams;
  var f:  text;
      s,st: string;
      i,j: integer;
      fl: Boolean;
 begin
   assignfile(f,_Params_);
   reset(f);
   while not eof(f) do
     begin
       readln(f,s);
       fl:=True;
       i:=1;
       st:='';
       while s[i]<>_CSVSeparator_ do begin st:=st+s[i]; inc(i); end;
       for j:=0 to MainFrame.cbParams.Items.Count-1 do
         if st=MainFrame.cbParams.Items[j] then fl:=false;
       if fl then MainFrame.cbParams.Items.Add(st);
     end;
   closefile(f);
end;// ReadParams

procedure SelectParams;
  var f:  text;
      s,st: string;
      i,j,k: integer;
      fl: Boolean;
 begin
   MainFrame.cbParams2.Items.Clear;
   MainFrame.cbParams2.Text:='';
   k:=0;
   assignfile(f,_params_);
   reset(f);
   while not eof(f) do
     begin
       readln(f,s);
       fl:=False;
       i:=1;
       st:='';
       while s[i]<>_CSVSeparator_ do begin st:=st+s[i]; inc(i); end;
       if st=MainFrame.cbParams.Text then
         begin
           fl:=True;
           st:='';
           inc(i);
           while s[i]<>_CSVSeparator_ do begin st:=st+s[i]; inc(i); end;
           inc(k);
         end;
       if fl then MainFrame.cbParams2.Items.Add(st);
     end;
   closefile(f);
   SetLength(PR,k);
   k:=0;
   reset(f);
   while not eof(f) do
     begin
       readln(f,s);
       fl:=False;
       i:=1;
       st:='';
       while s[i]<>_CSVSeparator_ do begin st:=st+s[i]; inc(i); end;
       if st=MainFrame.cbParams.Text then
         begin
           fl:=True;
           st:='';
           inc(i);
           while s[i]<>_CSVSeparator_ do inc(i);
           j:=Length(s);
           inc(i);
           while i<=j do begin st:=st+s[i]; inc(i); end;
           inc(k);
         end;
       if fl then PR[k-1]:=StrToFloat(st);
     end;
  closefile(f);
end;// SelectParams

end.
