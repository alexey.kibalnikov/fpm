object MainFrame: TMainFrame
  Left = 248
  Top = 41
  BiDiMode = bdRightToLeft
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1054#1094#1077#1085#1082#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1080' '#1082#1074#1072#1088#1090#1080#1088#1099
  ClientHeight = 429
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mnAbout
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 120
    Top = 0
    Width = 182
    Height = 20
    Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1078#1080#1083#1100#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 27
    Width = 266
    Height = 20
    Caption = #1057#1088#1077#1076#1085#1103#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' ('#1082#1074'.'#1084'./ '#1091'.'#1077'.)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 77
    Width = 375
    Height = 40
    Caption = 
      #1053#1072#1080#1073#1086#1083#1077#1077' '#1079#1085#1072#1095#1080#1084#1099#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1099', '#1074#1083#1080#1103#1102#1097#1080#1077'            '#1085#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' '#1082#1074 +
      #1072#1088#1090#1080#1088#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label5: TLabel
    Left = 318
    Top = 146
    Width = 16
    Height = 20
    Caption = '%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 3
    Top = 344
    Width = 70
    Height = 20
    Caption = #1042#1089#1077#1075#1086' %'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 8
    Top = 52
    Width = 256
    Height = 20
    Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100' '#1078#1080#1083#1100#1103' ('#1082#1074'.'#1084'.)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 0
    Top = 371
    Width = 225
    Height = 22
    Caption = #1048#1090#1086#1075#1086#1074#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' ('#1091'.'#1077'.)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 3
    Top = 146
    Width = 97
    Height = 20
    Caption = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 324
    Top = 368
    Width = 81
    Height = 33
    AutoSize = False
    Caption = #1054#1082#1088#1091#1075#1083#1077#1085#1085#1072#1103'  '#1076#1086' '#1076#1074#1091#1093' '#1079#1085#1072#1082#1086#1074
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object cbRegion: TComboBox
    Left = 312
    Top = 2
    Width = 89
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnEnter = cbRegionEnter
    OnSelect = cbRegionSelect
  end
  object edtPrice: TEdit
    Left = 272
    Top = 27
    Width = 129
    Height = 21
    TabOrder = 1
    OnChange = edtPriceChange
  end
  object cbParams: TComboBox
    Left = 2
    Top = 123
    Width = 401
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    OnSelect = cbParamsSelect
  end
  object cbParams2: TComboBox
    Left = 102
    Top = 146
    Width = 215
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    OnSelect = cbParams2Select
  end
  object edtPercent: TEdit
    Left = 334
    Top = 146
    Width = 68
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 4
  end
  object edtAllPercent: TEdit
    Left = 78
    Top = 344
    Width = 53
    Height = 21
    TabStop = False
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 5
    Text = '0'
  end
  object edtSize: TEdit
    Left = 272
    Top = 52
    Width = 129
    Height = 21
    TabOrder = 6
    OnChange = edtSizeChange
  end
  object edtRez: TEdit
    Left = 228
    Top = 370
    Width = 93
    Height = 27
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 7
  end
  object btnAddParam: TButton
    Left = 3
    Top = 172
    Width = 197
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = btnAddParamClick
  end
  object btnDelParam: TButton
    Left = 205
    Top = 171
    Width = 197
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    OnClick = btnDelParamClick
  end
  object btnRun: TButton
    Left = 135
    Top = 342
    Width = 268
    Height = 26
    Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' '#1078#1080#1083#1100#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    OnClick = btnRunClick
  end
  object sgParams: TStringGrid
    Left = 3
    Top = 202
    Width = 399
    Height = 137
    Color = clWhite
    ColCount = 1
    DefaultColWidth = 373
    DefaultRowHeight = 20
    FixedColor = clWhite
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 11
  end
  object btnSaveDoc: TButton
    Left = 205
    Top = 400
    Width = 197
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1092#1072#1081#1083' '#1089' '#1088#1072#1089#1095#1077#1090#1072#1084#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    OnClick = btnSaveDocClick
  end
  object btnOpenDoc: TButton
    Left = 3
    Top = 400
    Width = 197
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083' '#1089' '#1088#1072#1089#1095#1077#1090#1072#1084#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    OnClick = btnOpenDocClick
  end
  object btnChange: TButton
    Left = 3
    Top = 0
    Width = 113
    Height = 25
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1094#1077#1085#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    OnClick = btnChangeClick
  end
  object DTPDate: TDateTimePicker
    Left = 314
    Top = 100
    Width = 89
    Height = 21
    CalAlignment = dtaLeft
    Date = 37987.8415925926
    Time = 37987.8415925926
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 15
  end
  object sdSave: TSaveDialog
  end
  object mnAbout: TMainMenu
    Left = 64
    object btnAbout: TMenuItem
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      OnClick = btnAboutClick
    end
    object btnRemParams: TMenuItem
      Caption = #1055#1086#1103#1089#1085#1077#1085#1080#1077' '#1090#1077#1088#1084#1080#1085#1086#1074
      OnClick = btnRemParamsClick
    end
  end
  object odOpen: TOpenDialog
    Left = 32
  end
end
