object MainFrame: TMainFrame
  Left = 125
  Top = 73
  BorderStyle = bsDialog
  Caption = 'MainFrame // '#1050#1080#1073#1072#1083#1100#1085#1080#1082#1086#1074' '#1040#1083#1077#1082#1089#1077#1081', '#1060#1055#1052', 5 '#1082#1091#1088#1089', 2004 '#1075#1086#1076'.  '
  ClientHeight = 431
  ClientWidth = 615
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Pole: TImage
    Left = 3
    Top = 32
    Width = 608
    Height = 361
  end
  object Label1: TLabel
    Left = 0
    Top = 8
    Width = 610
    Height = 24
    Caption = #1055#1086#1089#1090#1088#1086#1077#1085#1080#1077' '#1074#1099#1087#1091#1082#1083#1086#1081' '#1086#1073#1086#1083#1086#1095#1082#1080' '#1084#1077#1090#1086#1076#1086#1084' "'#1079#1072#1074#1086#1088#1072#1095#1080#1074#1072#1085#1080#1103' '#1087#1086#1076#1072#1088#1082#1072'"'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 280
    Top = 404
    Width = 88
    Height = 20
    Caption = 'Pixel Count='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btnRandomPixel: TButton
    Left = 397
    Top = 400
    Width = 105
    Height = 25
    Caption = 'RandomPixel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = btnRandomPixelClick
  end
  object btnCreateShel: TButton
    Left = 507
    Top = 400
    Width = 105
    Height = 25
    Caption = 'CreateShel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = btnCreateShelClick
  end
  object edtPixelCount: TEdit
    Left = 368
    Top = 400
    Width = 25
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 2
    ParentFont = False
    TabOrder = 2
    Text = '20'
  end
end
