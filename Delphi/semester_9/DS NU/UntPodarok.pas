unit UntPodarok;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TCord = (_X_, _Y_);
  Vector = array [0..100, TCord] of integer;
  TFlags = (Up, Down);

  TMainFrame = class(TForm)
    Pole: TImage;
    Label1: TLabel;
    btnRandomPixel: TButton;
    btnCreateShel: TButton;
    edtPixelCount: TEdit;
    Label2: TLabel;
    procedure btnCreateShelClick(Sender: TObject);
    procedure btnRandomPixelClick(Sender: TObject);
  private
    SeePointFlag: TFlags;// откуда смотрим: над прямой, под прямой
    PixelVector: Vector; // хранит сгенерированные точки
    countPixel: integer; // кол-во точек при генерации
    MarkShelPixel: integer;// элемент массива PixelVector (последний его
                          // индекс) который принадлежит выпуклой оболочке
    constructor Init;
    procedure Main;
    procedure ChangElem(i_old, i_new : integer);
    function TestPixel_Line(i_baza, i_test, i_any: integer):BOOL;
    function TestAllPixel_Line(i_baza, i_test: integer):BOOL;
    function NextElemShel:BOOL;
    { Private declarations }
  public
    { Public declarations }
  end;
var
  MainFrame: TMainFrame;

implementation

{$R *.dfm}

procedure TMainFrame.btnCreateShelClick(Sender: TObject);
var
  i: integer;
begin
  if btnCreateShel.Tag = 0 then
   begin // фильтр
     MessageDlg('Нажмите кнопку [RandomPixel]',mtInformation,[mbOK],0);
     Exit;
   end;
  Main; // построение оболочки
  with Pole do // графически представление результата
    with Canvas do
      begin
       Pen.Color := clRed;
       for i := 0 to MarkShelPixel-1 do
         begin
           MoveTo(PixelVector[i,_X_], PixelVector[i,_Y_]);
           LineTo(PixelVector[i+1,_X_], PixelVector[i+1,_Y_]);
           Rectangle(PixelVector[i,_X_]-2,PixelVector[i,_Y_]-2,PixelVector[i,_X_]+2,PixelVector[i,_Y_]+2);
           TextOut(PixelVector[i,_X_]+2, PixelVector[i,_Y_]+2, IntToStr(i));
         end;
         MoveTo(PixelVector[i,_X_], PixelVector[i,_Y_]);
         LineTo(PixelVector[0,_X_], PixelVector[0,_Y_]);
         Rectangle(PixelVector[i,_X_]-2,PixelVector[i,_Y_]-2,PixelVector[i,_X_]+2,PixelVector[i,_Y_]+2);
         TextOut(PixelVector[i,_X_]+2, PixelVector[i,_Y_]+2, IntToStr(i));
      end;
end;//btnCreateShelClick

procedure TMainFrame.btnRandomPixelClick(Sender: TObject);
var
  i, x, y, MaxX, MaxY: integer;
begin
   countPixel := StrToInt(edtPixelCount.Text)-1;
   if countPixel < 2 then
     begin // фильтр
       MessageDlg('Нельзя вводить меньше трех точек',mtWarning,[mbOK],0);
       countPixel := 2;
       edtPixelCount.Text := '3';
     end;
   btnCreateShel.Tag := 1;
   with Pole do
     with Canvas do
       begin
// рабочее поле
         Pen.Color := clBlue;
         Brush.Color := clSkyBlue;
         Brush.Style := bsSolid;
         Rectangle(0, 0, ClientWidth, ClientHeight);
         Brush.Style := bsCross;
         Brush.Color := clCream;
         Rectangle(5, 5, ClientWidth-5, ClientHeight-5);
// точки
         Pen.Color := clBlack;
         Randomize;
         MaxX := Pole.ClientWidth - 20;
         MaxY := Pole.ClientHeight - 20;

         for i := 0 to countPixel do
           begin
             x := random(MaxX) + 10;
             y := random(MaxY) + 10;
             PixelVector[i, _X_] := x;
             PixelVector[i, _Y_] := y;
             Rectangle(x-3, y-3, x+3, y+3);
           end;
       end;{Canvas}
end;//btnRandomPixelClick

constructor TMainFrame.Init;
var
  i, mark, z: integer;
begin
// ищим один элемент ЗАРАНИЕ попадающий в оболочку (самый левый)
   mark := 0;
   z := PixelVector[mark, _X_];
   for i:=1 to countPixel do
     if PixelVector[i, _X_] < z then
       begin
         mark := i;
         z := PixelVector[i, _X_]
       end;
   ChangElem(0,mark); // найден первый элемент оболочки V[0,x],V[0,y]
   MarkShelPixel := 0;
   SeePointFlag := Up;
end;//Init

procedure TMainFrame.ChangElem(i_old,i_new : integer);
// обмен значений в массиве между двумя индексами
// первые элементы массива составляют выпуклую оболочку [0..MarkShelPixel]
var
  buf: integer;
begin
   buf := PixelVector[i_old, _X_];
   PixelVector[i_old, _X_] := PixelVector[i_new, _X_];
   PixelVector[i_new, _X_] := buf;
   buf := PixelVector[i_old, _Y_];
   PixelVector[i_old, _Y_] := PixelVector[i_new, _Y_];
   PixelVector[i_new, _Y_] := buf;
end;//ChangElem

function TMainFrame.TestPixel_Line(i_baza,i_test,i_any: integer):BOOL;
// i_baza - индекс (последней) вершины, которая уже точно вошла в оболочку
// i_test - индекс очередной тестируемой вершины
// i_any - все остальные, т.е. i_test+1,...
// Result - не выходит ли i_any из-под прямой [i_baza,i_test], тогда
// i_test ОТВЕРГАЕТСЯ
var
  x1, y1, x2, y2, x_test, y_test: integer;
begin
   x1 := PixelVector[i_baza, _X_];
   y1 := PixelVector[i_baza, _Y_];
   x2 := PixelVector[i_test, _X_];
   y2 := PixelVector[i_test, _Y_];
   x_test:=PixelVector[i_any, _X_];
   y_test:=PixelVector[i_any, _Y_];
   if x2 = x1 then // вертикальная прямая K=Const/0;
     begin
       Result := False;
       if (SeePointFlag=Up)   and (y_test<y1) then
         Result := True;
       if (SeePointFlag=Down) and (y_test>y1) then
         Result := True;
       Exit;
     end;
// иначе строится уравнение прямой и подставляется тестируемая точка
// точка либо над прямой либо под ней, в зависимости от SeePointFlag
   if SeePointFlag = Up then
     if y_test<((x_test-x1)*(y2-y1)/(x2-x1)+y1) then
       Result := True
     else
       Result := False
   else // SeePointFlag = Down
     if y_test>((x_test-x1)*(y2-y1)/(x2-x1)+y1) then
       Result := True
     else
       Result := False;
end;//TestPixel_Line

function TMainFrame.TestAllPixel_Line(i_baza,i_test: integer):BOOL;
// перебор ВСЕХ точек, для прооверки принадлежности i_test оболочке
type
  TSeePoint = (Left, Right);

var
  i: integer;
  SeePoint: TSeePoint; // для вертикальных прямых
begin
   Result := True;
// Рассматривается вертикальная прямая, проверяется ВСЕ ЛИ точки
// лежат тевее либо правее нее (без разницы), если ДА, то
// тестируемая точка (i_test) принадлежит оболочке
   if PixelVector[i_baza, _X_] = PixelVector[i_test, _X_] then
     begin // вертикальная прямая
       SeePoint := Left;
       if (i_baza <> 0) and (i_test <> 0) then
         if PixelVector[i_baza, _X_]>PixelVector[0, _X_] then
           SeePoint:=Right;
       for i := 0 to MarkShelPixel do
         begin
           if SeePoint = Left then
             if PixelVector[i_baza, _X_]<PixelVector[0, _X_] then
               begin
                 Result := False;
                 Exit;
               end;
           if SeePoint = Right then
             if PixelVector[i_baza, _X_]>PixelVector[0, _X_] then
               begin
                 Result := False;
                 Exit;
               end;
         end; // for i
       Exit;
     end;
// Рассмотрение наклонной прямой
   for i := 0 to countPixel do
     if (i <> i_baza) and (i <> i_test) then // их проверять ГЛУПО
       if TestPixel_Line(i_baza, i_test, i) then
         begin
           Result := False;
           Exit;
         end;
end;//TestAllPixel_Line

function TMainFrame.NextElemShel: BOOL;
// Последовательный подбор точек, которые входят в оболочку
// NB: если не найдем, то меняем  SeePointFlag, т.е. над прямой
// уже ВСЕ нашли и теперь ищим под прямой (такая вот хитрость) !!!
var
  i :integer;
begin
   Result := False;
   for i := MarkShelPixel + 1 to countPixel do
     if TestAllPixel_Line(MarkShelPixel, i) then
       begin
         inc(MarkShelPixel);
         ChangElem(i,MarkShelPixel);
         Result := True;
         Exit;
       end;
end;//NextElemShel

procedure TMainFrame.Main();
var
  i: integer;
begin
  Init;
  for i := 0 to countPixel do
    if not NextElemShel then
      SeePointFlag := Down; // это она и есть !!!!!
end;//Main

end.

