unit UntAVL;
{=========================================================================}
interface
{=========================================================================}
uses
  SysUtils;

type
{ Обработка исключений ---------------------------------------------------}
  clException = class (Exception) end;

{ Узел AVL-дерева --------------------------------------------------------}
  tFlags = byte;
  tFlagsContext = (_L_,_LR_,_R_,_protected_,_not_protected_,_del_); // маркер узлов
  ptrKeyAVL = pointer;
  tKeyAVL = integer;
  ptrDataAVL = pChar;
  tDataAVL = array of byte;
  ptrNodeAVL = ^tNodeAVL;
  tChildType = (_left_, _right_);
  tChild = array [tChildType] of ptrNodeAVL;

  tNodeAVL = record
    Child: tChild;
    Flags: tFlags; // семантика описана в tFlagsContext
    CountProtected: integer;
    Data: record end; // NB не занимает место !!!!!
  end;// tNodeAVL

const
{ GetMem(ptr, SizeOf(tNodeAVL) + KeyLen + DataLen),т.к. Data: record end;}
  KeyLen  = integer(SizeOf(ptrKeyAVL));
  DataLen = integer(SizeOf(ptrDataAVL));

type
  ptrListNodesAVL = ^tListNode;
  tListNode = record
    ptrNode: ptrNodeAVL;
    Next: ptrListNodesAVL;
  end;//ptrListNodeAVL

{ Enumerator -------------------------------------------------------------}
  tDirection = (increment, decrement); // направление для Next у Ennumerator-a
  tFlagsDel = (all, one);
  clAVL_Tree = class; // объявление ссылки на класс (до описания класса)

  clNodesEnumeratorAVL = class
  protected
    function GetKey: ptrKeyAVL;
    function GetData: ptrDataAVL;
  public
    property ptrKey: ptrKeyAVL read GetKey;
    property ptrData: ptrDataAVL read GetData;
    constructor Create(_clTree_: clAVL_Tree);
    destructor Destroy; virtual;
    function Next(Direction: tDirection): boolean; // удалось или нет
  private
    clTree: clAVL_Tree;
    ptrListProtectedNodes: ptrListNodesAVL; // список вершин защищенных Enumerator-ом
    ptrNodePresetn: ptrNodeAVL; // узел на который сейчас смотрим
//--> вспомогательные методы
    procedure SetPtrNodePresetn;
    function BackupNext(Direction: tDirection): boolean;
    function Backup(Direction: tDirection): ptrNodeAVL; // для IncrementMethod и DecrementMethod
    procedure AddListProtected(var ptrNode: ptrNodeAVL);
    procedure DelListProtected(FlagsDel: tFlagsDel);
  end;//clNodesEnumeratorAVL

{ AVL-дерево -------------------------------------------------------------}
  tFlagsOrder = (PreOrder, InOrder, PostOrder); // для обхода дерева
  tOptions = (equ, nequ, bigger, smaller, min, max); // для поиска
  tBalance = (BalanceL, BalanceR);

  fCompareKeyMethod =  function (Key1, Key2: ptrKeyAVL): integer;
  fEventMassageMethod = procedure (mes: string);

  clAVL_Tree = class
  public // protected
    ptrKey: ptrKeyAVL;
    ptrData: ptrDataAVL; // array of byte
    constructor Create(_multiKey_: boolean; _CompareKey_: fCompareKeyMethod; _EventMassage_: fEventMassageMethod);
    destructor Destroy; virtual;
    procedure OrderNode(FlagsOrder: tFlagsOrder);
    procedure Add(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL);
    procedure Del(ptrKey: ptrKeyAVL); overload; // удаляет ВСЕ !!!
    procedure Del(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL); overload;
    function  Search(ptrKey: ptrKeyAVL; Options: tOptions): clNodesEnumeratorAVL;
  private
    ptrRoot: ptrNodeAVL;
    multiKey: boolean;   // допускаются ли совпадениия
//--> метод вывода сообщений о ходе работы
    EventMessage: fEventMassageMethod;
//--> методы сравнения
    CompareKey: fCompareKeyMethod; // для ключей
    function CompareData (Data1, Data2: ptrDataAVL): integer; // для данных
    function CompareKeyAndData(Key1, Key2: ptrKeyAVL; Data1, Data2: ptrDataAVL): integer;
    function  CmpForDel(Key1, Key2: ptrKeyAVL; Data1, Data2: ptrDataAVL; FladsDel: tFlagsDel): integer;
//--> методы для работы с параметрами узла
    procedure SetFlagsDetector(var Flags: tFlags; Context: tFlagsContext; ptrNode: ptrNodeAVL = nil);
    function GetFlagsDetector(Flags: tFlags; Context: tFlagsContext): boolean;
    procedure SetKey(ptrNode: ptrNodeAVL; ptrKey: ptrKeyAVL);
    function GetKey(ptrNode: ptrNodeAVL): ptrKeyAVL;
    procedure SetData(ptrNode: ptrNodeAVL; ptrData: ptrDataAVL);
    function GetData(ptrNode: ptrNodeAVL): ptrDataAVL;
//--> вспомогательные методы
    procedure DoneMethod(var ptrNodes: ptrNodeAVL);
    procedure OrderNodesMethod(FlagsOrder: tFlagsOrder; ptrNodes: ptrNodeAVL);
    procedure AddBalanceLR(Balance: tBalance; var ptrNodes: ptrNodeAVL; var notBalance: boolean);
    procedure AddMethod(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL; var ptrNodes: ptrNodeAVL; var notBalance: boolean);
    procedure DelBalanceLR(Balance: tBalance; var ptrNodes: ptrNodeAVL; var notBalance: boolean);
    procedure DelNode(var ptrR, ptrG: ptrNodeAVL; var notBalance: boolean);
    procedure DelMethod(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL; FladsDel: tFlagsDel; var ptrNodes: ptrNodeAVL; var notBalance: boolean; var Rez: boolean);
    function  FoundForSearch(ptrKey: ptrKeyAVL; ptrNodes: ptrNodeAVL; Options: tOptions): boolean;
    procedure SearchMethod(_ptrKey_: ptrKeyAVL; var ptrNodes: ptrNodeAVL; Options: tOptions; var Rez: boolean; clEnumerator:clNodesEnumeratorAVL);
  end;//tAVL_Tree

{ Возможная реализация методов используемых при инициализации класса tTree}
function CompareKeyInteger(Key1, Key2: ptrKeyAVL): integer;
function CompareKeyString (Key1, Key2: ptrKeyAVL): integer;
procedure ShowEventMessage(mes: string);

{ Процедура передачи параметров ------------------------------------------}
procedure GetDataFromString(var Data: ptrDataAVL; Str: string);

{=========================================================================}
implementation
{=========================================================================}
uses
  Dialogs;

{ Процедуры и функции описанные вне классов ------------------------------}
procedure GetDataFromString(var Data: ptrDataAVL; Str: string);
{Преобразует строку Str  в байтовай массив}
var
  Len: Integer;
begin
  Str := Str + chr(0); // признак конца строки для pChar
  Len := Length(Str);
  GetMem(Data, Len);
  Move(Str[1], Data^, Len);
end;//GetDataFromString

procedure ShowEventMessage(mes: string);
{Cообщения об ошибках (можно перенаправить в любой поток)}
begin
  MessageDlg(mes, mtInformation, [mbOk], 0)
end;//ShowEventMessage

function CompareKeyInteger(Key1, Key2: ptrKeyAVL): integer;
begin
  Result := integer(Key1^) - integer(Key2^);
end;//CompareKeyInteger

function CompareKeyString(Key1, Key2: ptrKeyAVL): integer;
begin
  if String(Key1^) > String(Key2^) then
    Result := 1
  else
    if String(Key1^) < String(Key2^) then
      Result := -1
    else Result := 0
end;//CompareKeyString

{ Методы класса tTreeEnumerator ------------------------------------------}

constructor clNodesEnumeratorAVL.Create(_clTree_: clAVL_Tree);
begin
  inherited Create;
  clTree := _clTree_;
  if clTree = nil then
    raise clException.Create('Место возникновения: clNodesEnumeratorAVL.Create(ptrTree);'+#13#10
                             +'EXCEPTION: ptrTree = nil при инициализации');
  ptrListProtectedNodes := nil;
  ptrNodePresetn := nil;
end;//clNodesEnumeratorAVL.Init

destructor clNodesEnumeratorAVL.Destroy; // virtual
begin
  DelListProtected(all);
  inherited Destroy;
end;//clNodesEnumeratorAVL.Destroy

procedure clNodesEnumeratorAVL.SetPtrNodePresetn;
begin
  if ptrListProtectedNodes = nil then
    raise clException.Create('Место возникновения: clNodesEnumeratorAVL.SetPtrNodePresetn;'+#13#10
                             +'EXCEPTION: ptrListProtectedNodes = nil при инициализации');
  ptrNodePresetn := ptrListProtectedNodes^.ptrNode;
end;//clNodesEnumeratorAVL.SetPtrNodePresetn

function clNodesEnumeratorAVL.GetKey: ptrKeyAVL;
begin
  if ptrNodePresetn = nil then
    raise clException.Create('Место возникновения: clNodesEnumeratorAVL.GetKey;'+#13#10
                             +'EXCEPTION: ptrNodePresetn = nil при взятии значения');
// -> основная часть метода
  Result := clTree.GetKey(ptrNodePresetn);
end;//clNodesEnumeratorAVL.GetKey

function clNodesEnumeratorAVL.GetData: ptrDataAVL;
begin
  if ptrNodePresetn = nil then
    raise clException.Create('Место возникновения: clNodesEnumeratorAVL.GetData;'+#13#10
                             +'EXCEPTION: ptrNodePresetn = nil при взятии значения');
// -> основная часть метода
  Result := clTree.GetData(ptrNodePresetn);
end;//clNodesEnumeratorAVL.GetData

procedure clNodesEnumeratorAVL.AddListProtected(var ptrNode: ptrNodeAVL);
var
  ptr: ptrListNodesAVL;
begin
  if ptrNode <> nil then
    begin
      clTree.SetFlagsDetector(ptrNode^.Flags, _protected_, ptrNode); // маркер вершины
      new(ptr); // добавление узла в список ptrListProtectedNodes
      ptr^.ptrNode := ptrNode;
      ptr^.Next := ptrListProtectedNodes;
      ptrListProtectedNodes := ptr;
    end; //  if ptrNode <> nil
end;//clNodesEnumeratorAVL.AddListProtected

procedure clNodesEnumeratorAVL.DelListProtected(FlagsDel: tFlagsDel);
{Отчистка списка с удалением маркированных _del_ - узлов}
var
  ptr: ptrListNodesAVL;
  notBalance, Rez: boolean;
begin
  if ptrListProtectedNodes <> nil then // подготовка
    begin // отчистка списка с физическим удалением маркированных _del_ - узлов
      while ptrListProtectedNodes <> nil do
        begin // снятие атрибута _protected_
          with clTree do
            begin
              SetFlagsDetector(ptrListProtectedNodes^.ptrNode^.Flags, _not_protected_, ptrListProtectedNodes^.ptrNode);
              if GetFlagsDetector(ptrListProtectedNodes^.ptrNode^.Flags, _del_) and GetFlagsDetector(ptrListProtectedNodes^.ptrNode^.Flags, _not_protected_) then
                begin
                  EventMessage('Произошло физическое удаление узла Key= '+IntToStr(tKeyAVL(GetKey(ptrListProtectedNodes^.ptrNode)^))+#13#10
                                   +'Data= "'+pChar(GetData(ptrListProtectedNodes^.ptrNode))+'"');
                  notBalance := False;
                  DelMethod(GetKey(ptrListProtectedNodes^.ptrNode),GetData(ptrListProtectedNodes^.ptrNode),one,ptrRoot,notBalance,Rez);
                end;
            end; // with
          ptr := ptrListProtectedNodes^.Next;
          Dispose(ptrListProtectedNodes);
          ptrListProtectedNodes := ptr;
          if FlagsDel = one then Exit; // Если надо удалить только ПЕРВЫЙ элемент
        end;
    end; // списк отчищен
end;//clNodesEnumeratorAVL.DelListProtected

function clNodesEnumeratorAVL.BackupNext(Direction: tDirection): boolean;
var
  ptrList: ptrListNodesAVL;
  Cmp: Boolean;
begin
  Result := False;
  ptrList := ptrListProtectedNodes;
  with clTree do
    if ptrList <> nil then
      if ptrNodePresetn <> nil then
        while ptrList <> nil do
          begin
            Case Direction of
              increment: Cmp := CompareKeyAndData(GetKey(ptrList^.ptrNode),GetKey(ptrNodePresetn),GetData(ptrList^.ptrNode),GetData(ptrNodePresetn)) > 0;
              decrement: Cmp := CompareKeyAndData(GetKey(ptrList^.ptrNode),GetKey(ptrNodePresetn),GetData(ptrList^.ptrNode),GetData(ptrNodePresetn)) < 0;
            end; // Case

            if Cmp then
              if GetFlagsDetector(ptrList^.ptrNode^.Flags, _del_) then
                EventMessage('clTreeEnumeratorAVL.Next'+#13#10
                             +'очередной узел был удален'+#13#10
                             +'идет попытка найти следующий за ним')
              else
                begin
                  Result := True;
                  Exit;
                end;
            ptrList := ptrList^.Next; // "РЕКУРСИЯ" если узел удален
          end;
end;//clNodesEnumeratorAVL.BackupNext

function clNodesEnumeratorAVL.Backup(Direction: tDirection): ptrNodeAVL;
{откат + удаление узла из списка и снятие _protected_ если не _del_}
begin
  Result := nil;
  with clTree do
    if ptrListProtectedNodes <> nil then
      begin
        if ptrNodePresetn <> nil then
          Case Direction of
            increment:
              while CompareKeyAndData(GetKey(ptrListProtectedNodes^.ptrNode),GetKey(ptrNodePresetn),GetData(ptrListProtectedNodes^.ptrNode),GetData(ptrNodePresetn)) <= 0 do
                begin
                  DelListProtected(one); // они уже рассмотрены
                  if ptrListProtectedNodes = nil then
                    Exit;
                end;
            decrement:
              while CompareKeyAndData(GetKey(ptrListProtectedNodes^.ptrNode),GetKey(ptrNodePresetn),GetData(ptrListProtectedNodes^.ptrNode),GetData(ptrNodePresetn)) >= 0 do
                begin
                  DelListProtected(one); // они уже рассмотрены
                  if ptrListProtectedNodes = nil then
                    Exit;
                end;
          end; // Case

        if ptrListProtectedNodes <> nil then
          begin
            Result := ptrListProtectedNodes^.ptrNode;
            DelListProtected(one);
            if GetFlagsDetector(Result^.Flags, _del_) then
              Backup(Direction); // рекурсия, если удален !!!
          end;
      end; // with ptrTree^ and if
end;//clNodesEnumeratorAVL.Backup

function clNodesEnumeratorAVL.Next(Direction: tDirection): boolean;
{Следующая вершина при поиске (True, если найдена)}
type
  tDirInfo = record
    Left, Right: tChildType;
  end;
const
  tDirParams: array [tDirection] of tDirInfo = ((Left: _left_ ; Right: _right_),
                                                (Left: _right_; Right: _left_ ));
var
  Rez: ptrNodeAVL;
begin
  Result := False;
  if ptrNodePresetn <> nil then
    begin
// --> IncDecMethod()
      Rez := ptrNodePresetn;
      if Rez <> nil then
        begin
          if Rez^.Child[tDirParams[Direction].Right] = nil then
            if BackupNext(Direction) then
              Rez := Backup(Direction) // откат + удаление узла из списка
            else
              Rez := nil
          else
            begin // "накат" - РАЗ вправо и ДО КОНЦА влево + добавление в список
              AddListProtected(ptrNodePresetn); // не теряем Path от Root (уже рассмотрина)
              Rez := Rez^.Child[tDirParams[Direction].Right]; // <> nil
              AddListProtected(Rez);
              while Rez^.Child[tDirParams[Direction].Left] <> nil do
                begin
                  Rez := Rez^.Child[tDirParams[Direction].Left];
                  AddListProtected(Rez); // nil не добавится
                end;
              DelListProtected(one); // т.к. на этот узел сейчас смотрим
            end;
        end; // if Result <> nil
// --> IncDecMethod()

      if Rez <> nil then
        begin
          Result := True;
          ptrNodePresetn := Rez;
        end
      else
        clTree.EventMessage('clTreeEnumeratorAVL.Next'+#13#10
                              +'НЕТ следующего узла');
    end;
end;//clTreeEnumeratorAVL.Next

{ Методы класса clAVL_Tree ------------------------------------------------}

function clAVL_Tree.CompareData(Data1, Data2: ptrDataAVL): integer;
{Сравнение ссылок на данные}
var
  s1,s2 : string;
begin
  s1 := string(Data1);
  s2 := string(Data2);
  Result := CompareKeyString(@s1,@s2);
end;//clAVL_Tree.CompareData

function clAVL_Tree.CompareKeyAndData(Key1, Key2: ptrKeyAVL; Data1, Data2: ptrDataAVL): integer;
{При равенстве ключей происходит сравнение ссылок на данные (при myltiKey = True)}
var
  Rez: integer;
begin
  if multiKey then
    begin // допускаются повторения ключей в дереве
      Rez := CompareKey(Key1, Key2);
      if Rez = 0 then
        Rez := CompareData(Data1, Data2);
    end
  else // ключи в дереве уникальны
    Rez := CompareKey(Key1, Key2);
  Result := Rez;
end;//clAVL_Tree.CompareKeyAndData

constructor clAVL_Tree.Create(_multiKey_: boolean; _CompareKey_: fCompareKeyMethod; _EventMassage_: fEventMassageMethod);
begin
  inherited Create;
  multiKey := _multiKey_;
  ptrKey := nil;
  ptrData := nil;
  ptrRoot := nil;
  CompareKey := _CompareKey_;
  EventMessage := _EventMassage_;

  if not Assigned(CompareKey) then // это @CompareKey = nil
    raise clException.Create('Место возникновения: clAVL_Tree.Create(Clone, CompareKey, CompareData, EventMassage);'+#13#10
                             +'EXCEPTION: CompareKey = nil при инициализации');
  if not Assigned(EventMessage) then
    raise clException.Create('Место возникновения: clAVL_Tree.Create(Clone, CompareKey, CompareData, EventMassage);'+#13#10
                             +'EXCEPTION: EventMessage = nil при инициализации');
end;//clAVL_Tree.Init

procedure clAVL_Tree.DoneMethod(var ptrNodes: ptrNodeAVL);
{Удаление из памяти AVL-дерева (рекурсивый)}
begin
  if ptrNodes = nil then
    Exit;
  DoneMethod(ptrNodes^.Child[_left_]);
  DoneMethod(ptrNodes^.Child[_right_]);
  FreeMem(ptrNodes);
end;//clAVL_Tree.DoneMethod

destructor clAVL_Tree.Destroy;
{Удаление AVL-дерева, Enumerator-a, и созданного списка, т.е. ПОЛНАЯ отчистка}
begin
  EventMessage('Вся памаять ПОЛНОСТЬЮ возвращена в Heap');
  DoneMethod(ptrRoot); // удаление AVL - дерева
  inherited Destroy;
end;//clAVL_Tree.Done

procedure clAVL_Tree.SetFlagsDetector(var Flags: tFlags; Context: tFlagsContext; ptrNode: ptrNodeAVL = nil);
{Установка флажков в byte памяти ptrNode <> nil при _p_ or _n_p_}
var
  Help: tFlags;
begin
  Case Context of    // если писать в одну строку, то F shl 2 shr 4 ~ F shr 2
    _L_:             // xxxx--01
      begin
        Flags := Flags shr 2;
        Flags := Flags shl 2;
        Flags := Flags or 1;
      end;
    _LR_:            // xxxx--11
      begin
        Flags := Flags shr 2;
        Flags := Flags shl 2;
        Flags := Flags or 3;
      end;
    _R_:             // xxxx--10
      begin
        Flags := Flags shr 2;
        Flags := Flags shl 2;
        Flags := Flags or 2;
      end;
    _protected_:     // xxxx-1--
      begin
        if ptrNode = nil then
          raise clException.Create('Место возникновения: clAVL_Tree.clAVL_Tree.SetFlagsDetector(var Flags, Context, ptrNode = nil);'+#13#10
                                   +'EXCEPTION: ptrNode = nil при установке');
        inc(ptrNode^.CountProtected);
        Flags := Flags or 4;
      end;
    _not_protected_: // xxxx-0--
      begin
        if ptrNode = nil then
          raise clException.Create('Место возникновения: clAVL_Tree.clAVL_Tree.SetFlagsDetector(var Flags, Context, ptrNode = nil);'+#13#10
                                   +'EXCEPTION: ptrNode = nil при установке');
        dec(ptrNode^.CountProtected);
        if ptrNode^.CountProtected = 0 then
          begin
            Help := Flags shl 6;
            Help := Help shr 6;
            Flags := Flags shr 3;
            Flags := Flags shl 3;
            Flags := Flags or Help;
          end;
      end;
    _del_:           // xxxx1---
      Flags := Flags or 8;
  end;//Case
end;//clAVL_Tree.SetFlagsDetector

function clAVL_Tree.GetFlagsDetector(Flags : tFlags; Context: tFlagsContext): boolean;
{Взятие флажков из byte памяти}
begin
  Case Context of
    _L_:              // xxxx--01
      begin
        Flags := Flags shl 6;
        Flags := Flags shr 6;
        Result := Flags = 1;
      end;
    _LR_:             // xxxx--11
      begin
        Flags := Flags shl 6;
        Flags := Flags shr 6;
        Result := Flags = 3;
      end;
    _R_:              // xxxx--10
      begin
        Flags := Flags shl 6;
        Flags := Flags shr 6;
        Result := Flags = 2;
      end;
    _protected_:      // xxxx-1--
      begin
        Flags := Flags shl 5;
        Flags := Flags shr 7;
        Result := Flags = 1;
      end;
    _not_protected_:  // xxxx-0--
      begin
        Flags := Flags shl 5;
        Flags := Flags shr 7;
        Result := Flags = 0;
      end;
    _del_:            // xxxx1---
      begin
        Flags := Flags shl 4;
        Flags := Flags shr 7;
        Result := Flags = 1;
      end;
  else
    raise clException.Create('Место возникновения: clAVL_Tree.GetFlagsDetector(Flags, Context);'+#13#10
                             +'EXCEPTION: Context не принадлежит tFlagsContext при взятии');;
  end; // Case
end;//clAVL_Tree.GetFlagsDetector

procedure clAVL_Tree.SetKey(ptrNode: ptrNodeAVL; ptrKey: ptrKeyAVL);
begin
  if ptrNode = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.SetKey(ptrNode, ptrKey);'+#13#10
                               +'EXCEPTION: ptrNode = nil при установке');
  if ptrKey = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.SetKey(ptrNode, ptrKey);'+#13#10
                               +'EXCEPTION: ptrKey = nil при установке');
// -> основная часть метода
  Move(ptrKey^, pChar(@ptrNode^.Data)[0],keyLen);
end;//clAVL_Tree.SetKey

function clAVL_Tree.GetKey(ptrNode: ptrNodeAVL): ptrKeyAVL;
begin
  if ptrNode = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.GetKey(ptrNode);'+#13#10
                               +'EXCEPTION: ptrNode = nil при взятии');
// -> основная часть метода
  Result := @pChar(@ptrNode^.Data)[0];
end;//clAVL_Tree.GetKey

procedure clAVL_Tree.SetData(ptrNode: ptrNodeAVL; ptrData: ptrDataAVL);
begin
  if ptrNode = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.SetData(ptrNode, ptrData);'+#13#10
                               +'EXCEPTION: ptrNode = nil при установке');
  if ptrData = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.SetData(ptrNode, ptrData);'+#13#10
                               +'EXCEPTION: ptrData = nil при установке');
// -> основная часть метода
  Move(ptrData, pChar(@ptrNode^.Data)[keyLen], dataLen); // НЕ СОВСЕМ ТАК, КАК БЫЛО !!!
end;//clAVL_Tree.SetData

function clAVL_Tree.GetData(ptrNode: ptrNodeAVL): ptrDataAVL;
begin
  if ptrNode = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.GetData(ptrNode);'+#13#10
                               +'EXCEPTION: ptrNode = nil при взятии');
// -> основная часть метода
  Result := ptrDataAVL((@(pChar(@ptrNode^.Data)[keyLen]))^); // НЕ ПРОСТО :о)  т.к. pChar!!!
end;//clAVL_Tree.SetData

procedure clAVL_Tree.OrderNodesMethod(FlagsOrder: tFlagsOrder; ptrNodes: ptrNodeAVL);
{Обход вершин  AVL-дерева (рекурсивный)}
var
  Atrib: string;
begin
  if ptrNodes = nil then
    Exit;
  Case FlagsOrder of
    PreOrder:
      begin
        Atrib:='';
        if GetFlagsDetector(ptrNodes^.Flags, _protected_) then
          Atrib:='$p';
        if GetFlagsDetector(ptrNodes^.Flags, _del_) then
          Atrib:=Atrib+'$d';
        if (ptrNodes^.Child[_left_] = nil) and (ptrNodes^.Child[_Right_] = nil) then
          write('['+IntToStr(tKeyAVL(GetKey(ptrNodes)^))+Atrib+'] ')
        else // не лист
          write(IntToStr(tKeyAVL(GetKey(ptrNodes)^))+Atrib+' ');
        OrderNodesMethod(FlagsOrder, ptrNodes^.Child[_left_]);
        OrderNodesMethod(FlagsOrder, ptrNodes^.Child[_right_]);
      end;
    InOrder:
      begin
        OrderNodesMethod(FlagsOrder, ptrNodes^.Child[_left_]);
        Atrib:='';
        if GetFlagsDetector(ptrNodes^.Flags, _protected_) then
          Atrib:='$p';
        if GetFlagsDetector(ptrNodes^.Flags, _del_) then
          Atrib:=Atrib+'$d';
        if (ptrNodes^.Child[_left_] = nil) and (ptrNodes^.Child[_Right_] = nil) then
          write('['+IntToStr(tKeyAVL(GetKey(ptrNodes)^))+Atrib+'] ')
        else // не лист
          write(IntToStr(tKeyAVL(GetKey(ptrNodes)^))+Atrib+' ');
        OrderNodesMethod(FlagsOrder, ptrNodes^.Child[_right_]);
      end;
    PostOrder:
      begin
        OrderNodesMethod(FlagsOrder, ptrNodes^.Child[_left_]);
        OrderNodesMethod(FlagsOrder, ptrNodes^.Child[_right_]);
        Atrib:='';
        if GetFlagsDetector(ptrNodes^.Flags, _protected_) then
          Atrib:='$p';
        if GetFlagsDetector(ptrNodes^.Flags, _del_) then
          Atrib:=Atrib+'$d';
        if (ptrNodes^.Child[_left_] = nil) and (ptrNodes^.Child[_Right_] = nil) then
          write('['+IntToStr(tKeyAVL(GetKey(ptrNodes)^))+Atrib+'] ')
        else // не лист
          write(IntToStr(tKeyAVL(GetKey(ptrNodes)^))+Atrib+' ');
      end;
  end;//Case
  write('.');
end;//clAVL_Tree.OredrNodeMethod

procedure clAVL_Tree.OrderNode(FlagsOrder: tFlagsOrder);
{Обход вершин  AVL-дерева (public)}
begin
  write(#10+'OrderRoot= ');
  Case FlagsOrder of
    PreOrder:  writeLn('PreOrder [Root| L_Tree| R_Tree]');
    InOrder:   writeLn('InOrder [L_Tree| Root| R_Tree]');
    PostOrder: writeLn('PostOrder [L_Tree| R_Tree| Root]');
  end;
  writeLn('AtribRoot: "$p" - protected | "$d" - delete | "[]" - list | "." - end');
  OrderNodesMethod(FlagsOrder, ptrRoot);
  writeLn;
end;//clAVL_Tree.OrderRoot

// <-- "КРУЧЕ" макроса !!! :o)
type
  tBalInfo = record
    Left, Right: tChildType;
    L, R, LR: tFlagsContext;
  end;
const
  tBalParams: array [tBalance] of tBalInfo = ((Left: _left_ ; Right: _right_; L: _L_; R: _R_; LR: _LR_),
                                              (Left: _right_; Right: _left_; L: _R_; R: _L_; LR: _LR_ ));
// --> чтобы не использовать похожий блок кода AddBalanceL и AddBalanceR

procedure clAVL_Tree.AddBalanceLR(Balance: tBalance; var ptrNodes: ptrNodeAVL; var notBalance: boolean);
{Балансировка (рекурсивный)}
var
  ptr1, ptr2: ptrNodeAVL;
begin  // notBalance: левая ветвь стала длинее
  if GetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].R) then
    begin
      SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
      notBalance := False;
    end;
  if GetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR) then
    SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].R);
  if GetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].L) then
    begin // балансировка
      ptr1 := ptrNodes^.Child[tBalParams[Balance].left];
      if GetFlagsDetector(ptr1^.Flags, tBalParams[Balance].L) then
        begin // однократный LL-поворот
          ptrNodes^.Child[tBalParams[Balance].left] := ptr1^.Child[tBalParams[Balance].right];
          ptr1^.Child[tBalParams[Balance].right] := ptrNodes;
          SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
          ptrNodes := ptr1;
        end //LL
      else
        begin // двойной LR-поворот
          ptr2 := ptr1^.Child[tBalParams[Balance].right];
          ptr1^.Child[tBalParams[Balance].right] := ptr2^.Child[tBalParams[Balance].left];
          ptr2^.Child[tBalParams[Balance].left] := ptr1;
          ptrNodes^.Child[tBalParams[Balance].left] := ptr2^.Child[tBalParams[Balance].right];
          ptr2^.Child[tBalParams[Balance].right] := ptrNodes;
          if GetFlagsDetector(ptr2^.Flags, tBalParams[Balance].L) then
            SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].R)
          else
            SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
          if GetFlagsDetector(ptr2^.Flags, tBalParams[Balance].R) then
            SetFlagsDetector(ptr1^.Flags, tBalParams[Balance].L)
          else
            SetFlagsDetector(ptr1^.Flags, tBalParams[Balance].LR);
          ptrNodes := ptr2;
        end; // LR
      SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
      notBalance := False;
    end;// балансировка
end;//clAVL_Tree.AddBalanceLR

procedure clAVL_Tree.AddMethod(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL; var ptrNodes: ptrNodeAVL; var notBalance: boolean);
{Добавление (рекурсивный)}
begin
  if ptrNodes = nil then // включение
    begin
      GetMem(ptrNodes, SizeOf(tNodeAVL)+keyLen+dataLen);
      notBalance := True;
      with ptrNodes^ do
        begin
          Flags := 0;
          CountProtected := 0;
          Child[_left_] := nil;
          Child[_right_] := nil;
        end;
        SetKey(ptrNodes, ptrKey);
        SetData(ptrNodes, ptrData);
    end
  else // _ptrNodes_ <> nil
    begin
      if CompareKeyAndData(GetKey(ptrNodes),ptrKey,GetData(ptrNodes),ptrData) > 0 then
        begin
          AddMethod(ptrKey,ptrData,ptrNodes^.Child[_left_],notBalance);
          if notBalance then
            AddBalanceLR(BalanceL, ptrNodes, notBalance);// выросла левая ветвь
        end
      else // CompareKeyAndData(...) <= 0
        begin
          if CompareKeyAndData(GetKey(ptrNodes),ptrKey,GetData(ptrNodes),ptrData) < 0 then
            begin
              AddMethod(ptrKey,ptrData,ptrNodes^.Child[_right_],notBalance);
              if notBalance then
                AddBalanceLR(BalanceR, ptrNodes, notBalance);// выросла правая ветвь
            end
          else // CompareKeyAndData(...) = 0 -> значит узел уже имеется в дереве
            EventMessage('Произошла попытка добавления в AVL-дерево'+#13#10
                         +'узла, который уже содержится в дереве.'+#13#10
                         +'Новое значение НЕ ДОБАВЛЕННО');
        end;
    end; // elfif
end; // clAVL_Tree.AddMethod

procedure clAVL_Tree.Add(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL);
{Добавление (public)}
var
  NotBalans: boolean;
begin
  if ptrKey = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.Add(Key,Data);'+#13#10
                             +'EXCEPTION: Key = nil при добавлении');
  if ptrData = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.Add(Key,Data);'+#13#10
                             +'EXCEPTION: Data = nil при добавлении');
// -> основная часть метода
  NotBalans := False;
  AddMethod(ptrKey,ptrData,ptrRoot,NotBalans);
end;//clAVL_Tree.Add

procedure clAVL_Tree.DelBalanceLR(Balance: tBalance; var ptrNodes: ptrNodeAVL ; var notBalance: boolean);
{Балансировка (рекурсивный)}
var
  ptr1, ptr2: ptrNodeAVL;
  bal1, bal2: tFlags;
begin // notBalance: левая ветвь стала короче
  if GetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].L) then
    SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
  if GetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR) then
    begin
      SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].R);
      notBalance := False;
    end;
  if GetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].R) then
    begin // балансировка
      ptr1 := ptrNodes^.Child[tBalParams[Balance].right];
      bal1 := ptr1^.Flags;
      if not GetFlagsDetector(bal1, tBalParams[Balance].L) then // bal1 >= 0
        begin // однократный RR-поворот
          ptrNodes^.Child[tBalParams[Balance].right] := ptr1^.Child[tBalParams[Balance].left];
          ptr1^.Child[tBalParams[Balance].left] := ptrNodes;
          if GetFlagsDetector(bal1, tBalParams[Balance].LR) then
            begin
              SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].R);
              SetFlagsDetector(ptr1^.Flags, tBalParams[Balance].L);
              notBalance := False;
            end
          else // bal1 != _LR_
            begin
              SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
              SetFlagsDetector(ptr1^.Flags, tBalParams[Balance].LR);
            end;
          ptrNodes:=ptr1;
        end
      else // bal1 != _L_
        begin // двойной RL-поворот
          ptr2 := ptr1^.Child[tBalParams[Balance].left];
          bal2 := ptr2^.Flags;
          ptr1^.Child[tBalParams[Balance].left] := ptr2^.Child[tBalParams[Balance].right];
          ptr2^.Child[tBalParams[Balance].right] := ptr1;
          ptrNodes^.Child[tBalParams[Balance].right] := ptr2^.Child[tBalParams[Balance].left];
          ptr2^.Child[tBalParams[Balance].left] := ptrNodes;
          if GetFlagsDetector(bal2, tBalParams[Balance].R) then
            SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].L)
          else
            SetFlagsDetector(ptrNodes^.Flags, tBalParams[Balance].LR);
          if GetFlagsDetector(bal2, tBalParams[Balance].L) then
            SetFlagsDetector(ptr1^.Flags, tBalParams[Balance].R)
          else
            SetFlagsDetector(ptr1^.Flags, tBalParams[Balance].LR);
          ptrNodes := ptr2;
          SetFlagsDetector(ptr2^.Flags, tBalParams[Balance].LR);
        end;
    end; // балансировка
end;//clAVL_Tree.DelBalanceL

procedure clAVL_Tree.DelNode(var ptrR, ptrG: ptrNodeAVL; var notBalance: boolean);
{Вспомогательный метод для удаление узла (private)}
begin
  if ptrR^.Child[_right_] <> nil then
    begin
      DelNode(ptrR^.Child[_right_], ptrG, notBalance);
      if notBalance then
        DelBalanceLR(BalanceR, ptrR, notBalance);
    end
  else
    begin
      SetKey(ptrG, GetKey(ptrR));
      SetData(ptrG, GetData(ptrR));
      ptrG^.Flags := ptrR^.Flags;
      ptrG := ptrR;
      ptrR := ptrR^.Child[_left_];
      notBalance := True;
    end;
end;//clAVL_Tree.DelRoot

function clAVL_Tree.CmpForDel(Key1, Key2: ptrKeyAVL; Data1, Data2: ptrDataAVL; FladsDel: tFlagsDel): integer;
{Вспомогательный метод для сравнения узлов (private)}
begin
  Case FladsDel of
    all: Result := CompareKey(Key1, Key2);
    one: Result := CompareKeyAndData(Key1, Key2, Data1, Data2);
  else
    raise clException.Create('Место возникновения: clAVL_Tree.CmpForDel(Key1, Key2, Data1, Data2, FladsDel);'+#13#10
                             +'EXCEPTION: FladsDel не принадлежит tFlagsDel при взятии');;
  end;
end;//clAVL_Tree.CmpForDel

procedure clAVL_Tree.DelMethod(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL; FladsDel: tFlagsDel; var ptrNodes: ptrNodeAVL; var notBalance, Rez: boolean);
{Удаление узлов или узла в зависимости от _FladsDel_ (рекурсивный)}
var
  ptrG: ptrNodeAVL;
begin
  Rez := False;
  if ptrNodes = nil then // ключа в дереве нет
  else
    begin
      if CmpForDel(GetKey(ptrNodes),ptrKey,GetData(ptrNodes),ptrData,FladsDel) > 0 then
        begin // Key > x
          DelMethod(ptrKey,ptrData,FladsDel,ptrNodes^.Child[_left_],notBalance,Rez);
          if notBalance then
            DelBalanceLR(BalanceL, ptrNodes, notBalance);
        end;
      if CmpForDel(GetKey(ptrNodes),ptrKey,GetData(ptrNodes),ptrData,FladsDel) < 0 then
        begin // Key < x
          DelMethod(ptrKey,ptrData,FladsDel,ptrNodes^.Child[_right_],notBalance,Rez);
          if notBalance then
            DelBalanceLR(BalanceR, ptrNodes, notBalance);
        end;
      if CmpForDel(GetKey(ptrNodes),ptrKey,GetData(ptrNodes),ptrData,FladsDel) = 0 then
{-->}   begin // Key = x удаление узла
          if GetFlagsDetector(ptrNodes^.Flags, _protected_) then
            begin
              if (FladsDel = all) and GetFlagsDetector(ptrNodes^.Flags,_del_) then
                begin // чтобы не зациклица на удаленой защищенной вершине
                  DelMethod(ptrKey,ptrData,FladsDel,ptrNodes^.Child[_left_],notBalance,Rez);
                  DelMethod(ptrKey,ptrData,FladsDel,ptrNodes^.Child[_right_],notBalance,Rez);
                  Exit;
                end;

              SetFlagsDetector(ptrNodes^.Flags, _del_);
              Rez := True;
              EventMessage('Узел Key='+IntToStr(tKeyAVL(GetKey(ptrNodes)^))
                               +' Data= "'+pChar(GetData(ptrNodes))+'"'#13#10
                               +'защищен Enumerator- ом, он помечен для удаления, но физически присутствует в AVL- дереве');
            end
          else // не защищена Enumerator- ом
            begin // физическое удаление узла _ptrNodes_^
              ptrG := ptrNodes;
              if ptrG^.Child[_right_] = nil then
                begin
                  ptrNodes := ptrG^.Child[_left_];
                  notBalance := True;
                end
              else // ptr^.Child[_right_] <> nil
                if ptrG^.Child[_left_] = nil then
                  begin
                    ptrNodes := ptrG^.Child[_right_];
                    notBalance := True;
                  end
                else // ptr^.Child[_left_ AND _right_] <> nil
                  begin
                    DelNode(ptrG^.Child[_left_], ptrG, notBalance);
                    if notBalance then
                      DelBalanceLR(BalanceL, ptrNodes, notBalance);
                  end;
              Rez := True;
              Dispose(ptrG);
            end; // физическое удаление
        end; // Key = x удаление узла
    end;// _ptrNodes_ <> nil
end;//clAVL_Tree.DelMethod

procedure clAVL_Tree.Del(ptrKey: ptrKeyAVL); // overload;
{Удаление узлов, при multiKey = True - ВСЕХ (public)}
var
  notBalans, Rez: boolean;
begin
  if ptrKey = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.Del(Key);'+#13#10
                             +'EXCEPTION: Key = nil при удалении');
// -> основная часть метода
  if multiKey then
     EventMessage('Удалены ВСЕ!!! узлы у которых Key= '+IntToStr(tKeyAVL(ptrKey^)));
  notBalans := False;
  Rez := True;
  while Rez do
    DelMethod(ptrKey,nil,all,ptrRoot,NotBalans,Rez); // Del(_all_) !!!!!
end;//clAVL_Tree.Del overload

procedure clAVL_Tree.Del(ptrKey: ptrKeyAVL; ptrData: ptrDataAVL); // overload;
{Удаление узла, всегда не более одного (public)}
var
  NotBalans, Rez: boolean;
begin
  if not multiKey then
    EventMessage('Параметр Data при удалении не существенен,'+#13#10
                 +'т.к. Clone = False, можно использовать'+#13#10
                 +'другую сегнатуру метода Del( Key: KeyPtr )');

  if ptrKey = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.Del(Key, Data);'+#13#10
                             +'EXCEPTION: Key = nil при удалении');
  if ptrData= nil then
    raise clException.Create('Место возникновения: clAVL_Tree.Del(Key, Data);'+#13#10
                             +'EXCEPTION: Data = nil при удалении');
// -> основная часть метода
  NotBalans := False;
  DelMethod(ptrKey,ptrData,one,ptrRoot,NotBalans,Rez);
end;//clAVL_Tree.Del overload

function clAVL_Tree.FoundForSearch(ptrKey: ptrKeyAVL; ptrNodes: ptrNodeAVL; Options: tOptions): boolean;
{Вспомогательный метод, задает ветвь прямого вычисления в SearchMethod}
begin
  Case Options of
    equ: Result := CompareKey(GetKey(ptrNodes), ptrKey) = 0;
    nequ: Result := CompareKey(GetKey(ptrNodes), ptrKey) <> 0;
    bigger: Result := CompareKey(GetKey(ptrNodes), ptrKey) > 0;
    smaller: Result := CompareKey(GetKey(ptrNodes), ptrKey) < 0;
    min: Result := ptrNodes^.Child[_left_] = nil;
    max: Result := ptrNodes^.Child[_right_] = nil;
  else
    raise clException.Create('Место возникновения: clAVL_Tree.FoundForSearch(ptrKey, ptrNodes, Options);'+#13#10
                             +'EXCEPTION: Options не принадлежит tOptions при взятии');
  end; // Case
end;//clAVL_Tree.FoundForSearch

procedure clAVL_Tree.SearchMethod(_ptrKey_: ptrKeyAVL; var ptrNodes: ptrNodeAVL; Options: tOptions; var Rez: boolean; clEnumerator:clNodesEnumeratorAVL);
{Поиск узла (рекурсивная) поиск узла ТОЛЬКО по ключю, т.к. данные мы и ищем}
begin
  if ptrNodes = nil then // не найдено
    Exit;

  clEnumerator.AddListProtected(ptrNodes); // добавление узла в список ptrListProtectedNodes

  if FoundForSearch(_ptrKey_, ptrNodes, Options) then
    begin
      if GetFlagsDetector(ptrNodes^.Flags, _del_) then
        begin
          if multiKey = True then
            begin
              SearchMethod(_ptrKey_, ptrNodes^.Child[_left_], Options, Rez, clEnumerator);
              SearchMethod(_ptrKey_, ptrNodes^.Child[_right_], Options, Rez, clEnumerator);
            end;
        end;
      ptrKey := GetKey(ptrNodes);
      ptrData := GetData(ptrNodes); // тут не может возникнуть nil
      SetFlagsDetector(ptrNodes^.Flags, _protected_, ptrNodes);
      EventMessage('Значение найдено Key= '+IntToStr(tKeyAVL(GetKey(ptrNodes)^))+#13#10+'Data= "'+pChar(GetData(ptrNodes))+'"');
      Rez := True;
      Exit;
    end;
  Case Options of // рекурсия в зависимости от цели (Options)
    equ:
      begin
        if CompareKey(GetKey(ptrNodes), _ptrKey_) > 0 then
          SearchMethod(_ptrKey_, ptrNodes^.Child[_left_], Options,Rez,clEnumerator);
        if CompareKey(GetKey(ptrNodes), _ptrKey_) < 0 then
          SearchMethod(_ptrKey_, ptrNodes^.Child[_right_], Options,Rez,clEnumerator);
      end;
    nequ:
      begin // все равно в какую сторону
        SearchMethod(_ptrKey_, ptrNodes^.Child[_left_], Options,Rez,clEnumerator);
        SearchMethod(_ptrKey_, ptrNodes^.Child[_right_], Options,Rez,clEnumerator);
      end;
    bigger:  SearchMethod(_ptrKey_, ptrNodes^.Child[_right_], Options,Rez,clEnumerator);
    smaller: SearchMethod(_ptrKey_, ptrNodes^.Child[_left_], Options,Rez,clEnumerator);
    min:     SearchMethod(_ptrKey_, ptrNodes^.Child[_left_], Options,Rez,clEnumerator);
    max:     SearchMethod(_ptrKey_, ptrNodes^.Child[_right_], Options,Rez,clEnumerator);
  end; // Case
end;//clAVL_Tree.SearchMethod

function clAVL_Tree.Search(ptrKey: ptrKeyAVL; Options: tOptions): clNodesEnumeratorAVL;
{Поиск узла (public)}
var
  Rez: boolean;
begin
  EventMessage('Инициализирован процесс поиска и в случеае'+#13#10
              +'удачного завершения будет создан Enumerator'+#13#10
              +'Утилизация Enumerator-ов "лежит" на ВАС!!!');
  if (Options = min) or (Options = max) then
    EventMessage('Search(Key, Options) в данном случае Key не информативен, т.е. _min_,_max_ - в AVL-дереве');

  if ptrKey = nil then
    raise clException.Create('Место возникновения: clAVL_Tree.Search(Key, Options);'+#13#10
                             +'EXCEPTION: Key = nil при поиске');
// -> основная часть метода
  Result := clNodesEnumeratorAVL.Create(Self); // cсылка на класс
  Rez := False;
  SearchMethod(ptrKey, ptrRoot, Options, Rez, Result);
  if Rez then // т.е. SearchMethod шашел подходящий узел
    Result.SetPtrNodePresetn
  else
    begin
      EventMessage('Значение НЕ найдено, Enumerator утилизирован');
      Result.Destroy;
      Result := nil;
    end;
end;//clAVL_Tree.Search

END.
