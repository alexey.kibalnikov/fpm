unit UntFile;
{=========================================================================}
interface
{=========================================================================}
uses
  UntAVL;

const
  _CSVSeparator_ = ';';
  _DataFileName_ = 'DataRandom.csv';

type
  tRec= record
    Key: ptrKeyAVL;
    Data: ptrDataAVL;
  end;

  clDataClass = class
  protected
    function GetRoot(ix_Vect: longint): tRec;
  public
    count: longint;
    property Root[ix_Vect: longint]: tRec read GetRoot;
    procedure ReadDataFile();
    destructor Destroy();
  private
    Vector: array of tRec;
  end;
{=========================================================================}
implementation
{=========================================================================}
uses
  SysUtils;

destructor clDataClass.Destroy();
begin
  FreeMem(Vector);
  inherited Destroy;
end;//clDataClass.Done

procedure clDataClass.ReadDataFile();
var
  F:  text;
  Key: tKeyAVL;
  Data: string;
  s, st: string;
  i, k, cnt: longint;
begin
  AssignFile(F, _DataFileName_); // try для Close(F)

  try
    Reset(F);
    cnt := 0;
    while not eof(F) do
      begin
        readln(F, s);
        inc(cnt);
      end;
    count := cnt;
    GetMem(Vector, count * SizeOf(tRec));
    Reset(F);
    cnt := 0;
    while not eof(F) do
      begin
        readln(F, s);
        i := 1;
        st := '';
        while s[i] <> _CSVSeparator_ do
          begin
            st := st + s[i];
            inc(i);
          end;
        Key := StrToInt(st);
        Data := '';
        for k := i to length(s) - 1 do
          Data := Data + s[k+1];
        GetMem(Vector[cnt].Key, SizeOf(tKeyAVL));
        tKeyAVL(Vector[cnt].Key^) := Key;
        GetDataFromString(Vector[cnt].Data, Data);
        inc(cnt);
      end;
  finally
    CloseFile(F);
  end; // try
end;//clDataClass.ReadDataFile

function clDataClass.GetRoot(ix_Vect: integer): tRec;
begin
  Result:=Vector[ix_Vect];
end;//clDataClass.GetRoot

END.
