program AVL;
{$APPTYPE CONSOLE}

uses
  SysUtils,
  Dialogs,
  UntAVL in 'UntAVL.pas',
  UntFile in 'UntFile.pas';

VAR
// ��� ������������� AVL-������
  clTree: clAVL_Tree;
  clEnumerator: clNodesEnumeratorAVL;
  CompareKey:  fCompareKeyMethod;
  EventMassage: fEventMassageMethod;
// ��� �������� ������� ������
  clDataFile: clDataClass;
  Element: tRec;
  i: longint;
// ������
  ptrKey: ptrKeyAVL;
  ptrData: ptrDataAVL;
  i2: longint;
BEGIN
{ TODO -oUser -cConsole Main : Insert code here }
  try
    CompareKey := CompareKeyInteger;
    EventMassage := ShowEventMessage;
    clTree := clAVL_Tree.Create(True, CompareKey, EventMassage); // multiKey == True

    clDataFile := clDataClass.Create;
    clDataFile.ReadDataFile;

    writeLn('V derevo dobavlautsa sledyuwie yzli:'+#10);
    for i := 0 to clDataFile.count - 1 do
      begin
        Element := clDataFile.Root[i];
        clTree.Add(Element.Key, Element.Data);
        write('Key= '+IntToStr(tKeyAVL(Element.Key^)));
        writeln('  Data= '+pChar(Element.Data));
      end;
    ShowEventMessage('Go, go, go... ;o)');
//  clTree.Del(nil); // exception
//   clTree.Add(Element.Key, Element.Data); // �� �� ����� ��������
//  clTree.OrderNode(InOrder);

    if clEnumerator <> nil then
      clEnumerator.Destroy;
    // i := Key;
    clEnumerator := clTree.Search(@i, max);

    writeln('clEnumerator.Next(decrement)');
    for i2:=0 to 10000 do
      begin
        if clEnumerator.Next(decrement) then
          begin // �������
            ptrKey := clEnumerator.ptrKey;
            ptrData := clEnumerator.ptrData;
            writeln('Next='+IntToStr(tKeyAVL(ptrKey^))+' Data='+pChar(ptrData));
          end
        else
          Break;
      end;

    writeln('clEnumerator.Next(increment)');
    for i2:=0 to 10000 do
      begin
        if clEnumerator.Next(increment) then
          begin // �������
            ptrKey := clEnumerator.ptrKey;
            ptrData := clEnumerator.ptrData;
            writeln('Next='+IntToStr(tKeyAVL(ptrKey^))+' Data='+pChar(ptrData));
          end
        else
          Break;
      end;

    ShowEventMessage('���������� ���������� ��� EXCEPTION - ��');
  except // ��� ������ �� ����� ???
    on E: Exception do
      MessageDlg(E.Message, mtError, [mbOk], 0); // ������ �� "���������"
  end; // try
//finally, �.�. � ����� ������ ���������� ��������� ���� (������� ������)
  if clEnumerator <> nil then
    clEnumerator.Destroy;
  clTree.Destroy;
  clDataFile.Destroy;
  readln;
END.

