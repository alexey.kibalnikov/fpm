object List: TList
  Left = 360
  Top = 119
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1057#1087#1080#1089#1086#1082' '#1076#1086#1083#1078#1085#1080#1082#1086#1074
  ClientHeight = 201
  ClientWidth = 281
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 142
    Width = 133
    Height = 20
    Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dbgUser: TDBGrid
    Left = 4
    Top = 4
    Width = 273
    Height = 129
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
  end
  object btnList: TBitBtn
    Left = 169
    Top = 140
    Width = 75
    Height = 25
    TabOrder = 1
    OnClick = btnListClick
    Kind = bkOK
  end
  object Data: TDateTimePicker
    Left = 9
    Top = 171
    Width = 121
    Height = 21
    CalAlignment = dtaLeft
    Date = 37740.6469434028
    Time = 37740.6469434028
    DateFormat = dfShort
    DateMode = dmComboBox
    ImeMode = imOpen
    Kind = dtkDate
    ParseInput = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 2
  end
  object btnUp: TButton
    Left = 140
    Top = 171
    Width = 136
    Height = 25
    Caption = #1043#1083#1072#1074#1085#1086#1077' '#1052#1077#1085#1102
    TabOrder = 3
    OnClick = btnUpClick
  end
end
