object Rez: TRez
  Left = 198
  Top = 115
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1044#1080#1072#1075#1088#1072#1084#1084#1072
  ClientHeight = 305
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnPaint = btnLineClick
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 390
    Top = 0
    Width = 133
    Height = 20
    Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Pole: TPaintBox
    Left = 4
    Top = 5
    Width = 381
    Height = 295
    Color = clWhite
    Enabled = False
    ParentColor = False
  end
  object btnUp: TButton
    Left = 392
    Top = 274
    Width = 128
    Height = 25
    Caption = #1043#1083#1072#1074#1085#1086#1077' '#1052#1077#1085#1102
    TabOrder = 0
    OnClick = btnUpClick
  end
  object Data: TDateTimePicker
    Left = 392
    Top = 24
    Width = 124
    Height = 21
    CalAlignment = dtaLeft
    Date = 37740.6469434028
    Time = 37740.6469434028
    DateFormat = dfShort
    DateMode = dmComboBox
    ImeMode = imOpen
    Kind = dtkDate
    ParseInput = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
  end
  object clbLook: TCheckListBox
    Left = 391
    Top = 50
    Width = 125
    Height = 169
    OnClickCheck = clbLookClickCheck
    ItemHeight = 13
    TabOrder = 2
  end
  object btnLine: TBitBtn
    Left = 412
    Top = 242
    Width = 73
    Height = 25
    TabOrder = 3
    OnClick = btnLineClick
    Kind = bkOK
  end
  object cbClear: TCheckBox
    Left = 401
    Top = 224
    Width = 97
    Height = 17
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = False
    OnClick = cbClearClick
  end
end
