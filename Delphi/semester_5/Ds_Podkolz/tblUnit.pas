unit tblUnit;

interface

uses
  SysUtils, Classes, DB, DBTables;

type
  TdmTable = class(TDataModule)
    tbleBook: TTable;
    tbleUser: TTable;
    tbleMemo: TTable;
    tbleMemoN_Fio: TFloatField;
    tbleMemoN_Book: TFloatField;
    tbleBookId: TAutoIncField;
    tbleBookBook: TStringField;
    tbleBookCount: TFloatField;
    tbleUserId: TAutoIncField;
    tbleUserFIO: TStringField;
    tbleMemoId: TAutoIncField;
    tbleMemoFio: TStringField;
    tbleMemoBook: TStringField;
    tbleMemoData: TDateField;
    dsBook: TDataSource;
    dsUser: TDataSource;
    dsMemo: TDataSource;
    tbleUserCount: TWordField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmTable: TdmTable;

implementation

{$R *.dfm}

procedure TdmTable.DataModuleCreate(Sender: TObject);
var
 i : integer;
begin
 for i:=0 to ComponentCount-1 do
  begin
   if Components[i] is TTable then
    begin
     with Components[i] as TTable do
      begin
       DatabaseName:='C:\User\My Doc\Delphi\semester_5\Ds\';
       Open;
      end
    end
  end;
end;

procedure TdmTable.DataModuleDestroy(Sender: TObject);
var
 i : integer;
begin
 for i:=0 to ComponentCount-1 do
  begin
   if Components[i] is TTable then
    begin
     with Components[i] as TTable do
      begin
       Close;
      end
    end
  end;
end;


end.
