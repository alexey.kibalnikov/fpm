object Change: TChange
  Left = 209
  Top = 136
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1042#1099#1076#1072#1095#1072'/'#1055#1088#1080#1077#1084' '#1082#1085#1080#1075
  ClientHeight = 331
  ClientWidth = 448
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 14
    Top = 282
    Width = 49
    Height = 16
    Caption = #1074#1099#1076#1072#1095#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 2
    Top = 307
    Width = 63
    Height = 16
    Caption = #1074#1086#1079#1074#1088#1072#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 11
    Top = 261
    Width = 46
    Height = 20
    Caption = #1044#1072#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 40
    Top = 7
    Width = 64
    Height = 20
    Caption = #1057#1083#1080#1077#1085#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnUp: TButton
    Left = 224
    Top = 293
    Width = 174
    Height = 25
    Caption = #1043#1083#1072#1074#1085#1086#1077' '#1052#1077#1085#1102
    TabOrder = 0
    OnClick = btnUpClick
  end
  object Data: TDateTimePicker
    Left = 66
    Top = 280
    Width = 97
    Height = 21
    CalAlignment = dtaLeft
    Date = 0.766517592601303
    Time = 0.766517592601303
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 1
    OnChange = DataChange
  end
  object pcCh: TPageControl
    Left = 155
    Top = 5
    Width = 288
    Height = 267
    ActivePage = tsPush
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabIndex = 0
    TabOrder = 2
    OnChange = pcChChange
    object tsPush: TTabSheet
      Caption = #1042#1099#1076#1072#1090#1100' '#1082#1085#1080#1075#1091
      object dbgPush: TDBGrid
        Left = 4
        Top = 3
        Width = 271
        Height = 177
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
        OnCellClick = dbgUserCellClick
      end
      object rgSort2: TRadioGroup
        Left = 4
        Top = 181
        Width = 99
        Height = 48
        Caption = #1089#1086#1088#1090#1080#1088#1086#1074#1082#1072' '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          #1087#1086' '#1089#1087#1080#1089#1082#1091
          #1087#1086' '#1072#1083#1092#1072#1074#1080#1090#1091' ')
        ParentFont = False
        TabOrder = 1
        OnClick = rgSort2Click
      end
      object btnGet: TButton
        Left = 184
        Top = 200
        Width = 89
        Height = 25
        Caption = #1042#1099#1076#1072#1090#1100' '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = btnGetClick
      end
    end
    object tsPop: TTabSheet
      Caption = #1055#1088#1080#1085#1103#1090#1100' '#1082#1085#1080#1075#1091
      ImageIndex = 1
      object dbgPop: TDBGrid
        Left = 4
        Top = 5
        Width = 271
        Height = 176
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
      end
      object btnPop: TButton
        Left = 184
        Top = 195
        Width = 89
        Height = 25
        Caption = #1055#1088#1080#1085#1103#1090#1100' '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = btnPopClick
      end
      object cbAll: TCheckBox
        Left = 10
        Top = 197
        Width = 169
        Height = 17
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1077#1093' '#1076#1086#1083#1078#1085#1080#1082#1086#1074' ?'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 2
        OnClick = cbAllClick
      end
    end
  end
  object dbgUser: TDBGrid
    Left = 4
    Top = 39
    Width = 145
    Height = 175
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    OnCellClick = dbgUserCellClick
  end
  object rgSort: TRadioGroup
    Left = 5
    Top = 216
    Width = 143
    Height = 47
    Caption = #1089#1086#1088#1090#1080#1088#1086#1074#1082#1072' '
    ItemIndex = 0
    Items.Strings = (
      #1087#1086' '#1089#1087#1080#1089#1082#1091
      #1087#1086' '#1072#1083#1092#1072#1074#1080#1090#1091)
    TabOrder = 4
    OnClick = rgSortClick
  end
  object Data2: TDateTimePicker
    Left = 67
    Top = 307
    Width = 96
    Height = 21
    CalAlignment = dtaLeft
    Date = 37736.8177521991
    Time = 37736.8177521991
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 5
  end
end
