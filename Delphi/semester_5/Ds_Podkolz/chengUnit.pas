unit chengUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Grids, DBGrids, ExtCtrls, DB, DBCtrls;

const cor_date='01.03.00';
type
  TChange = class(TForm)
    btnUp: TButton;
    Label1: TLabel;
    Data: TDateTimePicker;
    pcCh: TPageControl;
    tsPush: TTabSheet;
    tsPop: TTabSheet;
    dbgUser: TDBGrid;
    dbgPush: TDBGrid;
    dbgPop: TDBGrid;
    rgSort2: TRadioGroup;
    rgSort: TRadioGroup;
    btnGet: TButton;
    btnPop: TButton;
    cbAll: TCheckBox;
    Label2: TLabel;
    Data2: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    procedure btnUpClick(Sender: TObject);
    procedure rgSort2Click(Sender: TObject);
    procedure rgSortClick(Sender: TObject);
    procedure btnGetClick(Sender: TObject);
    procedure dbgUserCellClick(Column: TColumn);
    procedure btnPopClick(Sender: TObject);
    procedure pcChChange(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure DataChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Change: TChange;

implementation

uses mUnit, tblUnit;

{$R *.dfm}

procedure TChange.btnUpClick(Sender: TObject);
begin
 First.Visible:=true;
 visible:=false;
 dmTable.tbleMemo.FlushBuffers;
 dmTable.tbleMemo.CancelRange;{for delUesr,Rez}
 dbgUser.DataSource:=nil;
 dbgPush.DataSource:=nil;
 dbgPop .DataSource:=nil;
end;


procedure TChange.rgSort2Click(Sender: TObject);
begin
 case rgSort2.ItemIndex of
  0: dmTable.tbleBook.IndexFieldNames:='ID';
  1: dmTable.tbleBook.IndexName:='ixBook';
 end;
end;

procedure TChange.rgSortClick(Sender: TObject);
begin
 case rgSort.ItemIndex of
  0: dmTable.tbleUser.IndexFieldNames:='ID';
  1: dmTable.tbleUser.IndexName:='ixFio';
 end;
end;

procedure TChange.btnGetClick(Sender: TObject);
 var mes:string;
begin
 if dmTable.tbleBookCount.Value<>0 then
  begin
   mes:='Выдать '+dmTable.tbleUserFIO.Value;
   mes:=' '+mes+' '+dmTable.tbleBookBook.Value+' ?';
   if MessageDlg(mes,mtInformation,[mbYes,mbNo],0)= mrYes then
    with dmTable do
     begin
      tbleMemo.Append;
      tbleMemoN_Fio.Value:=tbleUserId.Value;
      tbleMemoN_Book.Value:=tbleBookId.Value;
      tbleMemoData.Value:=Data2.Date;
      tbleMemo.Post;
      tbleBook.Edit;
      tbleBook.FieldByName('Count').Value:=tbleBookCount.Value-1;
      tbleBook.Post;
     end;
   end;
  if dmTable.tbleBookCount.Value=0 then
   btnGet.Enabled:=false;
end;

procedure TChange.dbgUserCellClick(Column: TColumn);
begin
 case pcCh.TabIndex of
  0: begin
      if dmTable.tbleBookCount.Value=0 then
       btnGet.Enabled:=false
      else btnGet.Enabled:=true;
     end;{0}
  1: begin
      dmTable.tbleMemo.SetRange([dmTable.tbleUserId.Value],
                [dmTable.tbleUserId.Value]);
      if dmTable.tbleMemoFio.Value='' then
       btnPop.Enabled:=false
      else btnPop.Enabled:=true;
      cbAll.Checked:=false;
     end;{1}
  end;{Case}
end;
procedure TChange.btnPopClick(Sender: TObject);
 var mes:string;
begin
 with dmTable do
  begin
   mes:=tbleMemoFio.Value+' принес книгу ';
   if tbleMemoBook.Value='' then
    mes:=mes+'которая удалена ?'
   else mes:=mes+tbleMemoBook.Value+' ?';
   if MessageDlg(mes,mtInformation,[mbYes,mbNo],0)= mrYes then
    begin
     mes:=FloatToStr(tbleMemoN_Book.Value);
     tbleMemo.Delete;

     with tbleBook do
      begin
       EditKey;
       FieldByName('id').Value := mes; //key
      end;
     if tbleBook.GotoKey then
      begin
       tbleBook.Edit;
       tbleBook.FieldByName('Count').Value:=tbleBookCount.Value+1;
       tbleBook.Post;
      end;
    end;
  end;
 if dmTable.tbleMemoFio.Value='' then
  btnPop.Enabled:=false
 else btnPop.Enabled:=true;
end;

procedure TChange.pcChChange(Sender: TObject);
begin
 Case pcCh.TabIndex of
  0: begin
      if dmTable.tbleBookCount.Value=0 then
       btnGet.Enabled:=false
      else btnGet.Enabled:=true;
     end;{0}
  1: begin
      dmTable.tbleMemo.SetRange([dmTable.tbleUserId.Value],
              [dmTable.tbleUserId.Value]);
      cbAll.Checked:=False;        
      if dmTable.tbleMemoFio.Value='' then
       btnPop.Enabled:=false
      else btnPop.Enabled:=true;
     end;{1}
  end;{Case}
end;

procedure TChange.cbAllClick(Sender: TObject);
begin
 if not cbAll.Checked then
  dmTable.tbleMemo.SetRange([dmTable.tbleUserId.Value],
                            [dmTable.tbleUserId.Value])
 else
  dmTable.tbleMemo.CancelRange;
 if dmTable.tbleMemoFio.Value='' then
  btnPop.Enabled:=false
 else btnPop.Enabled:=true;

end;

procedure TChange.DataChange(Sender: TObject);
begin
 Data2.Date:=Data.Date+StrToDate(cor_date);
end;

end.
