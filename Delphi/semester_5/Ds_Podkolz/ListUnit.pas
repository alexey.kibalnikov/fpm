unit ListUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, Grids, DBGrids;

type
  TList = class(TForm)
    dbgUser: TDBGrid;
    btnList: TBitBtn;
    Label1: TLabel;
    Data: TDateTimePicker;
    btnUp: TButton;
    procedure btnListClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  List: TList;

implementation

uses tblUnit, mUnit, DBTables, DB;

{$R *.dfm}

procedure TList.btnListClick(Sender: TObject);
begin
 with dmTable do
  begin
   tbleUser.CancelRange;
   tbleUser.First;
   while not dmTable.tbleUser.Eof do
    begin
     tbleUser.Edit;
     tbleUser['Count']:=0;
     tbleUser.Post;
     tbleUser.Next;
    end;
   tbleMemo.First;
   while not dmTable.tbleMemo.Eof do
    begin
     if tbleMemo['Data']<Data.Date then
      begin
       tbleUser.Locate('Id',tbleMemo['N_Fio'],[]{[loCaseInsensitive]});
       tbleUser.Edit;
       tbleUser['Count']:= tbleUser['Count']+1;
       tbleUser.Post;
      end;
     tbleMemo.Next;
    end;{while}
   with tbleUser do
    begin
     IndexName:='ixCount';
     EditRangeStart;
     FieldByName('Count').Value:=1;
     EditRangeEnd;
     FieldByName('Count').Value:=20;
     ApplyRange;
     tbleUserCount.Visible:=true;
    end;{with}
    tbleUser.First;
    tbleMemo.First;
  end;{with}
end;

procedure TList.btnUpClick(Sender: TObject);
begin
 dbgUser.DataSource:=nil;
 dmTable.tbleUserCount.Visible:=false;
 dmTable.tbleUser.CancelRange;
 dmTable.tbleUser.IndexFieldNames:='id';
 First.Visible:=true;
 Visible:=false;
end;

end.
