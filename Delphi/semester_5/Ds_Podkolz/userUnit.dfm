object User: TUser
  Left = 70
  Top = 120
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1082#1083#1080#1077#1085#1090#1086#1074' '
  ClientHeight = 206
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlUser: TPanel
    Left = -1
    Top = 0
    Width = 282
    Height = 225
    TabOrder = 0
    object dbgUser: TDBGrid
      Left = 7
      Top = 9
      Width = 264
      Height = 120
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = [fsBold]
    end
    object rgSort: TRadioGroup
      Left = 8
      Top = 136
      Width = 97
      Height = 64
      Caption = #1089#1086#1088#1090#1080#1088#1086#1074#1082#1072' '
      ItemIndex = 0
      Items.Strings = (
        #1087#1086' '#1089#1087#1080#1089#1082#1091
        #1087#1086' '#1072#1083#1092#1072#1074#1080#1090#1091' ')
      TabOrder = 1
      OnClick = rgSortClick
    end
    object btnUp: TButton
      Left = 112
      Top = 176
      Width = 163
      Height = 25
      Caption = #1043#1083#1072#1074#1085#1086#1077' '#1052#1077#1085#1102
      TabOrder = 2
      OnClick = btnUpClick
    end
    object btnAdd: TButton
      Left = 112
      Top = 142
      Width = 77
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
      TabOrder = 3
      OnClick = btnAddClick
    end
    object btnDel: TButton
      Left = 197
      Top = 141
      Width = 77
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 4
      OnClick = btnDelClick
    end
  end
  object pnlAdd: TPanel
    Left = 2
    Top = 6
    Width = 273
    Height = 126
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 257
      Height = 24
      Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1085#1086#1074#1086#1075#1086' '#1082#1083#1080#1077#1085#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 26
      Top = 45
      Width = 59
      Height = 16
      Caption = #1060#1072#1084#1080#1083#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edFio: TEdit
      Left = 104
      Top = 40
      Width = 121
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edFioChange
    end
    object BitBtn1: TBitBtn
      Left = 47
      Top = 89
      Width = 75
      Height = 25
      TabOrder = 1
      OnClick = BitBtn1Click
      Kind = bkCancel
    end
    object btnOk: TBitBtn
      Left = 142
      Top = 89
      Width = 75
      Height = 25
      Enabled = False
      TabOrder = 2
      OnClick = btnOkClick
      Kind = bkOK
    end
  end
end
