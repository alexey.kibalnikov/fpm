unit rezUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Grids, DBGrids, Buttons, DBCtrls, ActnList,
  ExtCtrls, CheckLst;

type
  TRez = class(TForm)
    btnUp: TButton;
    Data: TDateTimePicker;
    clbLook: TCheckListBox;
    btnLine: TBitBtn;
    Pole: TPaintBox;
    Label1: TLabel;
    cbClear: TCheckBox;
    procedure btnUpClick(Sender: TObject);
    procedure btnLineClick(Sender: TObject);
    procedure cbClearClick(Sender: TObject);
    procedure clbLookClickCheck(Sender: TObject);
    procedure pcRezChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var Rez: TRez;

implementation

uses mUnit, tblUnit, DB, DBTables;

{$R *.dfm}
procedure TRez.btnUpClick(Sender: TObject);
begin
 First.Visible:=true;
 Visible:=false;
end;

procedure TRez.btnLineClick(Sender: TObject);
 const Col:array[0..5]of integer=(clRed,clGreen,clYellow,clBlue,clFuchsia,clBackground);
 var i,j,book,cnt_Y,count,clr_count:word;
     x,y:integer;
     mes:string[4];
 begin
 with Pole do
  with Canvas do
   begin
    Pen.Color:=clBlue;
    Brush.Style:=bsSolid;
    Brush.Color:=clAqua;
    Rectangle(0,0,ClientWidth,Pole.ClientHeight);
    Rectangle(3,3,ClientWidth-3,ClientHeight-3);
{book for X}
    with dmTable do
     begin
      tbleBook.First;
      book:=0;
      while not tbleBook.Eof do
       begin
        inc(book);
        tbleBook.Next;
       end;
     end;{with}
    Pen.Color:=clBlack;
    Pen.Width:=2;
    x:=15;
    y:=ClientHeight-25;
{x} MoveTo(x,y);
    LineTo(ClientWidth-x,y);
    dmTable.tbleBook.First;
    for i:=0 to book-1 do
     begin
      MoveTo(x+5+round((ClientWidth-2*x-10)*i/(book-1)),y-5);
      LineTo(x+5+round((ClientWidth-2*x-10)*i/(book-1)),y+5);
      mes:=dmTable.tbleBookBook.Value;
      TextOut(x-7+round((ClientWidth-2*x-10)*i/(book-1)),y+7,mes+'.');
      dmTable.tbleBook.Next;
     end;{for}
{y} MoveTo(x+5,y);
    LineTo(x+5,10);
{?} cnt_Y:=5;
    for i:=0 to cnt_Y-1 do
     begin
      MoveTo(x,   y-round((2*y+5-ClientHeight)*i/(cnt_Y-1)));
      LineTo(x+10,y-round((2*y+5-ClientHeight)*i/(cnt_Y-1)));
      mes:=IntToStr(i);
      TextOut(x-10,y-5-round((2*y+5-ClientHeight)*i/(cnt_Y-1)),mes);
     end;
  end;{with}
{Main part for Line}
 clr_count:=0;
 dmTable.tbleUser.First;
 for i:=0 to clbLook.Items.Count-1 do {for User}
  with dmTable do
   begin
    if clbLook.Checked[i]=true then
     begin
      with Pole do
       with Canvas do begin
        MoveTo(x+5,y);
        Pen.Color:=Col[clr_count];
        Brush.Color:=Col[clr_count];
        clr_count:=(clr_count+1)mod 7;
       end;
      tbleBook.First;
      for j:=0 to book-1 do
       begin
        tbleMemo.First;
        count:=0;
        while not tbleMemo.Eof do
         begin
          if (tbleMemo['N_fio']=tbleUser['Id'])
           and(tbleMemo['N_Book']=tbleBook['Id'])
           and(tbleMemo['Data']<Data.Date)
            then inc(Count);
          tbleMemo.Next;
         end;{while}
{Paint Rez 'Count'}
        with Pole do
         with Canvas do
          begin
           LineTo(x+5+round((ClientWidth-2*x-10)*j/(book-1)),y-round((2*y+5-ClientHeight)*count/(cnt_Y-1)));
           Ellipse(x+5+round((ClientWidth-2*x-10)*j/(book-1))+4,y-round((2*y+5-ClientHeight)*count/(cnt_Y-1))+4,x+5+round((ClientWidth-2*x-10)*j/(book-1))-4,y-round((2*y+5-ClientHeight)*count/(cnt_Y-1))-4);
          end;
        tbleBook.Next;
       end;{for j}
     end;{if cblLook}
    tbleUser.Next;
   end;{with; for}
  dmTable.tbleUser.First;
  dmTable.tbleBook.First;
  dmTable.tbleMemo.First;
{Help color}
  clr_count:=0;
  for i:=0 to clbLook.Items.Count-1 do
   if clbLook.Checked[i]=true then
    begin
     clbLook.Canvas.Brush.Color:=Col[clr_count];
     clbLook.Canvas.Rectangle(2,1+13*i,13,12+13*i);
     clr_count:=(clr_count+1)mod 7;
    end;
end;

procedure TRez.cbClearClick(Sender: TObject);
 var i:word;
begin
 if cbClear.Checked=true then
  begin
   cbClear.Checked:=false;
   cbClear.Visible:=false;
   for i:=0 to clbLook.Items.Count-1 do
    clbLook.Checked[i]:=false;
  end;
end;

procedure TRez.clbLookClickCheck(Sender: TObject);
 var i:word;
begin
 cbClear.Visible:=false;
 for i:=0 to clbLook.Items.Count-1 do
  if clbLook.Checked[i]=true then
   begin
    cbClear.Visible:=true;
    Break;
   end;
end;

procedure TRez.pcRezChange(Sender: TObject);
begin
 cbClear.Checked:=true;
 cbClearClick(Sender);
end;

end.


