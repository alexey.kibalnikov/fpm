unit bookUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, StdCtrls, ExtCtrls, Buttons;

type
  TBook = class(TForm)
    dbgBook: TDBGrid;
    btnUp: TButton;
    btnDelet: TButton;
    btnAdd: TButton;
    edCount: TEdit;
    pnlBook: TPanel;
    edAdd: TEdit;
    pnlAdd: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    btnOk: TBitBtn;
    Label3: TLabel;
    rgSort: TRadioGroup;
    procedure btnUpClick(Sender: TObject);
    procedure rgSortClick(Sender: TObject);
    procedure btnDeletClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure edCountChange(Sender: TObject);
    procedure edCountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Book: TBook;

implementation

uses tblUnit, mUnit;

{$R *.dfm}

procedure TBook.btnUpClick(Sender: TObject);
begin
 First.Visible:=true;
 visible:=false;
 dmTable.tbleBook.FlushBuffers;
 dbgBook.DataSource:=nil;
end;

procedure TBook.rgSortClick(Sender: TObject);
begin
 case rgSort.ItemIndex of
 0: dmTable.tbleBook.IndexFieldNames:='ID';
 1: dmTable.tbleBook.IndexName:='ixBook';
end;
end;

procedure TBook.btnDeletClick(Sender: TObject);
 var mes:string;
begin
 mes:=dmTable.tbleBookBook.Value;
 mes:='’отите удалить '+mes+' ?';
 if MessageDlg(mes,mtInformation,[mbYes,mbNo],0)= mrYes
  then dmTable.tbleBook.Delete;
end;

procedure TBook.BitBtn1Click(Sender: TObject);
begin
 pnlAdd.Visible:=false;
 pnlBook.Enabled:=true;
end;

procedure TBook.btnOkClick(Sender: TObject);
begin
 pnlAdd.Visible:=false;
 pnlBook.Enabled:=true;
 with dmTable do
  begin
   tbleBook.Append;
   tbleBookBook.Value:=edAdd.Text;
   tbleBookCount.Value:=StrToFloat(edCount.Text);
   tbleBook.Post;
  end;
end;

procedure TBook.btnAddClick(Sender: TObject);
begin
 pnlAdd.Visible:=true;
 pnlBook.Enabled:=false;
 edAdd.Text:='';
 edCount.Text:='';
end;

procedure TBook.edCountChange(Sender: TObject);
begin
 if (edAdd.Text<>'') and (edCount.Text<>'') then
  btnOk.Enabled:=true
 else btnOk.Enabled:=false;
end;

procedure TBook.edCountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//       '0'              '9'         '-'        '+'
 if (not((48<=Key)and(Key<=57))) then
  begin
   MessageDlg('Ёто числовое (положительное) поле,'+chr(10)+chr(13)+'      вводите “ќЋ№ ќ ÷»‘–џ',mtError, [mbOk],0);
   edCount.Text:='';
   btnOk.Enabled:=false;
  end;
end;

end.



