unit userUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, ExtCtrls, Buttons;

type
  TUser = class(TForm)
    dbgUser: TDBGrid;
    btnUp: TButton;
    rgSort: TRadioGroup;
    pnlUser: TPanel;
    btnAdd: TButton;
    btnDel: TButton;
    pnlAdd: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edFio: TEdit;
    BitBtn1: TBitBtn;
    btnOk: TBitBtn;
    procedure btnUpClick(Sender: TObject);
    procedure rgSortClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure edFioChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  User: TUser;

implementation

uses tblUnit, mUnit, DBTables;

{$R *.dfm}

procedure TUser.btnUpClick(Sender: TObject);
begin
 First.Visible:=true;
 Visible:=false;
 dmTable.tbleUser.FlushBuffers;
 dbgUser.DataSource:=nil;
end;

procedure TUser.rgSortClick(Sender: TObject);
begin
 case rgSort.ItemIndex of
  0: dmTable.tbleUser.IndexFieldNames:='ID';
  1: dmTable.tbleUser.IndexName:='ixFio';
 end;
end;

procedure TUser.btnDelClick(Sender: TObject);
 var mes:string;
begin
 with dmTable do
  begin
   mes:=FloatToStr(tbleUserId.Value);
   if tbleMemo.Locate('N_Fio',mes,[loCaseInsensitive]) then
    begin
     mes:=tbleUserFIO.Value+' ¤вл¤етс¤ должником';
     mes:=mes+chr(10)+chr(13)+'      его нельз¤ удалить';
     MessageDlg(mes,mtError,[mbOk],0);
     exit;
    end;
   mes:='’отите удалить '+tbleUserFIO.Value+' ?';
   if MessageDlg(mes,mtInformation,[mbYes,mbNo],0)= mrYes
    then dmTable.tbleUser.Delete;
  end;{with}
end;

procedure TUser.edFioChange(Sender: TObject);
begin
 if edFio.Text<>''then btnOk.Enabled:=true
  else btnOk.Enabled:=false;
end;

procedure TUser.BitBtn1Click(Sender: TObject);
begin
 pnlAdd.Visible:=false;
 pnlUser.Enabled:=true;
end;

procedure TUser.btnAddClick(Sender: TObject);
begin
 pnlUser.Enabled:=false;
 pnlAdd.Visible:=true;
 edFio.Text:='';
end;

procedure TUser.btnOkClick(Sender: TObject);
begin
 pnlAdd.Visible:=false;
 pnlUser.Enabled:=true;
 with dmTable do
  begin
   tbleUser.Append;
   tbleUserFio.Value:=edFio.Text;
   tbleUser.Post;
  end;
end;

end.
