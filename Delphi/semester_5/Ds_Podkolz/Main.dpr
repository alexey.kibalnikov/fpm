program Main;

uses
  Forms,
  mUnit in 'mUnit.pas' {First},
  tblUnit in 'tblUnit.pas' {dmTable: TDataModule},
  bookUnit in 'bookUnit.pas' {Book},
  userUnit in 'userUnit.pas' {User},
  chengUnit in 'chengUnit.pas' {Change},
  rezUnit in 'rezUnit.pas' {Rez},
  ListUnit in 'ListUnit.pas' {List};
{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFirst, First);
  Application.CreateForm(TdmTable, dmTable);
  Application.CreateForm(TBook, Book);
  Application.CreateForm(TUser, User);
  Application.CreateForm(TChange, Change);
  Application.CreateForm(TRez, Rez);
  Application.CreateForm(TList, List);
  Application.Run;
end.
