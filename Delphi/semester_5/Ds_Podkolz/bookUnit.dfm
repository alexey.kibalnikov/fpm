object Book: TBook
  Left = 374
  Top = 119
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1082#1085#1080#1075
  ClientHeight = 204
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBook: TPanel
    Left = -8
    Top = 0
    Width = 289
    Height = 209
    TabOrder = 0
    object dbgBook: TDBGrid
      Left = 11
      Top = 5
      Width = 273
      Height = 120
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = [fsBold]
    end
    object btnUp: TButton
      Left = 125
      Top = 171
      Width = 157
      Height = 25
      Caption = #1043#1083#1072#1074#1085#1086#1077' '#1052#1077#1085#1102
      TabOrder = 1
      OnClick = btnUpClick
    end
    object btnAdd: TButton
      Left = 125
      Top = 136
      Width = 75
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
      TabOrder = 2
      OnClick = btnAddClick
    end
    object btnDelet: TButton
      Left = 207
      Top = 136
      Width = 75
      Height = 25
      Caption = #1059#1076#1072#1083#1077#1085#1080#1077' '
      TabOrder = 3
      OnClick = btnDeletClick
    end
    object rgSort: TRadioGroup
      Left = 12
      Top = 133
      Width = 105
      Height = 65
      Caption = #1089#1086#1088#1090#1080#1088#1086#1074#1082#1072' '
      ItemIndex = 0
      Items.Strings = (
        #1087#1086' '#1089#1087#1080#1089#1082#1091
        #1087#1086' '#1072#1083#1092#1072#1074#1080#1090#1091' ')
      TabOrder = 4
      OnClick = rgSortClick
    end
  end
  object pnlAdd: TPanel
    Left = 3
    Top = 5
    Width = 274
    Height = 156
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 23
      Top = 48
      Width = 69
      Height = 16
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 23
      Top = 80
      Width = 68
      Height = 16
      Caption = #1042' '#1085#1072#1083#1080#1095#1072#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 24
      Top = 8
      Width = 227
      Height = 24
      Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1085#1086#1074#1086#1081' '#1082#1085#1080#1075#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edAdd: TEdit
      Left = 101
      Top = 44
      Width = 131
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edCountChange
    end
    object edCount: TEdit
      Left = 101
      Top = 76
      Width = 133
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = edCountChange
      OnKeyDown = edCountKeyDown
    end
    object BitBtn1: TBitBtn
      Left = 46
      Top = 112
      Width = 75
      Height = 25
      TabOrder = 2
      OnClick = BitBtn1Click
      Kind = bkCancel
    end
    object btnOk: TBitBtn
      Left = 150
      Top = 112
      Width = 75
      Height = 25
      Enabled = False
      TabOrder = 3
      OnClick = btnOkClick
      Kind = bkOK
    end
  end
end
