unit mUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, ComCtrls, Buttons, Menus, ExtCtrls;
type
  TFirst = class(TForm)
    bUser: TButton;
    bBook: TButton;
    BitBtn1: TBitBtn;
    MainMenu: TMainMenu;
    About1: TMenuItem;
    btnChange: TButton;
    btnRez: TButton;
    btnList: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    procedure bUserClick(Sender: TObject);
    procedure bBookClick(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure btnRezClick(Sender: TObject);
    procedure btnListClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  First: TFirst;

implementation

uses userUnit, bookUnit, chengUnit, rezUnit, tblUnit, ListUnit;

{$R *.dfm}

procedure TFirst.bUserClick(Sender: TObject);
begin
 User.Visible:=true;
 Visible:=false;
 User.dbgUser.DataSource:=dmTable.dsUser;
end;

procedure TFirst.bBookClick(Sender: TObject);
begin
 Book.Visible:=true;
 Visible:=false;
 Book.dbgBook.DataSource:=dmTable.dsBook;
end;

procedure TFirst.About1Click(Sender: TObject);
begin
 MessageDlg('Работа Кибальникова Алексея, №12',mtInformation,[mbOk],0)
end;

procedure TFirst.btnChangeClick(Sender: TObject);
begin
 with Change do
  begin
   pcCh.ActivePageIndex:=0;
   Data.Date:=Date;
   Data2.Date:=Date+StrToDate(cor_date);
   dbgUser.DataSource:=dmTable.dsUser;
   dbgPush.DataSource:=dmTable.dsBook;
   dbgPop. DataSource:=dmTable.dsMemo;
  end;{with}
 Change.Visible:=true;
 Visible:=false;
end;

procedure TFirst.btnRezClick(Sender: TObject);
 var i:integer;
begin
 Rez.Data.Date:=Date+StrToDate(cor_date);
 Rez.Visible:=true;
 Rez.cbClear.Visible:=false;
 Visible:=false;
 for i:=Rez.clbLook.Items.Count-1 downto 0 do
  Rez.clbLook.Items.Delete(i);
 dmTable.tbleUser.First;
 while not dmTable.tbleUser.Eof do
  begin
   Rez.clbLook.Items.Add(dmTable.tbleUser['Fio']);
   dmTable.tbleUser.Next;
  end;
 dmTable.tbleUser.First;
end;

procedure TFirst.btnListClick(Sender: TObject);
begin
 List.Data.Date:=Date+StrToDate(cor_date);
 List.dbgUser.DataSource:=dmTable.dsUser;
 List.Visible:=true;
 Visible:=false;
end;

end.
