object First: TFirst
  Left = 193
  Top = 100
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1052#1077#1085#1102' '
  ClientHeight = 133
  ClientWidth = 303
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object bUser: TButton
    Left = 8
    Top = 8
    Width = 137
    Height = 25
    Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1082#1083#1080#1077#1085#1090#1086#1074' '
    TabOrder = 0
    OnClick = bUserClick
  end
  object bBook: TButton
    Left = 8
    Top = 40
    Width = 137
    Height = 25
    Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1082#1085#1080#1075
    TabOrder = 1
    OnClick = bBookClick
  end
  object BitBtn1: TBitBtn
    Left = 224
    Top = 104
    Width = 75
    Height = 25
    Caption = '&'#1042#1099#1093#1086#1076
    TabOrder = 2
    Kind = bkClose
  end
  object btnChange: TButton
    Left = 8
    Top = 72
    Width = 137
    Height = 25
    Caption = #1042#1099#1076#1072#1095#1072'/'#1055#1088#1080#1077#1084' '#1082#1085#1080#1075
    TabOrder = 3
    OnClick = btnChangeClick
  end
  object Panel1: TPanel
    Left = 151
    Top = 6
    Width = 148
    Height = 93
    BevelInner = bvLowered
    BevelOuter = bvSpace
    BorderStyle = bsSingle
    TabOrder = 4
    object Label1: TLabel
      Left = 19
      Top = 2
      Width = 103
      Height = 20
      Caption = #1054#1090#1095#1077#1090#1085#1086#1089#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnRez: TButton
      Left = 5
      Top = 56
      Width = 133
      Height = 25
      Caption = #1044#1080#1072#1075#1088#1072#1084#1084#1072' '
      TabOrder = 0
      OnClick = btnRezClick
    end
    object btnList: TButton
      Left = 5
      Top = 24
      Width = 133
      Height = 25
      Caption = #1057#1087#1080#1089#1086#1082' '#1076#1086#1083#1078#1085#1080#1082#1086#1074
      TabOrder = 1
      OnClick = btnListClick
    end
  end
  object MainMenu: TMainMenu
    Left = 184
    Top = 48
    object About1: TMenuItem
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
      OnClick = About1Click
    end
  end
end
