object dmTable: TdmTable
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 341
  Top = 125
  Height = 145
  Width = 187
  object tbleBook: TTable
    Exclusive = True
    TableName = 'Book.db'
    Left = 16
    Top = 8
    object tbleBookId: TAutoIncField
      FieldName = 'Id'
      ReadOnly = True
      Visible = False
    end
    object tbleBookBook: TStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077
      FieldName = 'Book'
      Size = 10
    end
    object tbleBookCount: TFloatField
      DisplayLabel = #1042' '#1085#1072#1083#1080#1095#1072#1077
      FieldName = 'Count'
    end
  end
  object tbleUser: TTable
    Exclusive = True
    TableName = 'User.db'
    Left = 72
    Top = 8
    object tbleUserId: TAutoIncField
      FieldName = 'Id'
      ReadOnly = True
      Visible = False
    end
    object tbleUserFIO: TStringField
      DisplayLabel = #1060#1072#1084#1080#1083#1080#1103' '
      FieldName = 'FIO'
    end
    object tbleUserCount: TWordField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '
      FieldName = 'Count'
      Visible = False
    end
  end
  object tbleMemo: TTable
    Exclusive = True
    TableName = 'Memo.db'
    Left = 128
    Top = 8
    object tbleMemoN_Fio: TFloatField
      FieldName = 'N_Fio'
      Visible = False
    end
    object tbleMemoN_Book: TFloatField
      FieldName = 'N_Book'
      Visible = False
    end
    object tbleMemoId: TAutoIncField
      FieldName = 'Id'
      ReadOnly = True
      Visible = False
    end
    object tbleMemoFio: TStringField
      DisplayLabel = #1044#1086#1083#1078#1085#1080#1082
      FieldKind = fkLookup
      FieldName = 'Fio'
      LookupDataSet = tbleUser
      LookupKeyFields = 'Id'
      LookupResultField = 'FIO'
      KeyFields = 'N_Fio'
      ReadOnly = True
      Size = 15
      Lookup = True
    end
    object tbleMemoBook: TStringField
      DisplayLabel = #1044#1086#1083#1075
      FieldKind = fkLookup
      FieldName = 'Book'
      LookupDataSet = tbleBook
      LookupKeyFields = 'Id'
      LookupResultField = 'Book'
      KeyFields = 'N_Book'
      ReadOnly = True
      Size = 15
      Lookup = True
    end
    object tbleMemoData: TDateField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'Data'
    end
  end
  object dsBook: TDataSource
    DataSet = tbleBook
    Left = 16
    Top = 56
  end
  object dsUser: TDataSource
    DataSet = tbleUser
    Left = 72
    Top = 56
  end
  object dsMemo: TDataSource
    DataSet = tbleMemo
    Left = 128
    Top = 56
  end
end
