object First: TFirst
  Left = 145
  Top = 4
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = #1056#1072#1073#1086#1090#1072' '#1050#1080#1073#1072#1083#1100#1085#1080#1082#1086#1074#1072' '#1040#1083#1077#1082#1089#1077#1103
  ClientHeight = 521
  ClientWidth = 523
  Color = clInactiveCaptionText
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pbPole: TPaintBox
    Left = 3
    Top = 5
    Width = 258
    Height = 513
    Color = clTeal
    Enabled = False
    ParentColor = False
    OnPaint = pbPolePaint
  end
  object Label7: TLabel
    Left = 265
    Top = 468
    Width = 29
    Height = 20
    Caption = 'X0='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 265
    Top = 494
    Width = 29
    Height = 20
    Caption = 'X1='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 402
    Top = 468
    Width = 52
    Height = 20
    Caption = 'Count='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edY0: TEdit
    Left = 295
    Top = 468
    Width = 103
    Height = 21
    Color = clCream
    ReadOnly = True
    TabOrder = 1
  end
  object edY1: TEdit
    Left = 295
    Top = 495
    Width = 103
    Height = 21
    Color = clCream
    ReadOnly = True
    TabOrder = 2
  end
  object sgRunge_Kutt: TStringGrid
    Left = 266
    Top = 221
    Width = 255
    Height = 241
    Color = clCream
    ColCount = 4
    DefaultColWidth = 30
    DefaultRowHeight = 16
    RowCount = 102
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 3
    ColWidths = (
      30
      36
      82
      79)
  end
  object edCount: TEdit
    Left = 454
    Top = 468
    Width = 66
    Height = 21
    Color = clCream
    TabOrder = 4
  end
  object BitBtn1: TBitBtn
    Left = 403
    Top = 493
    Width = 118
    Height = 26
    TabOrder = 0
    OnClick = btnRezClick
    Kind = bkOK
  end
  object pnlGran: TPanel
    Left = 265
    Top = 4
    Width = 256
    Height = 81
    BevelInner = bvLowered
    TabOrder = 5
    object Label14: TLabel
      Left = 2
      Top = 3
      Width = 251
      Height = 24
      Caption = '        '#1043#1088#1072#1085#1080#1095#1085#1099#1077' '#1091#1089#1083#1086#1074#1080#1103'       '
      Color = clSkyBlue
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentColor = False
      ParentFont = False
    end
    object Label10: TLabel
      Left = 29
      Top = 29
      Width = 29
      Height = 20
      Caption = 'Ao='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 29
      Top = 53
      Width = 29
      Height = 20
      Caption = 'Bo='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 126
      Top = 32
      Width = 59
      Height = 20
      Caption = 'X0(Ao)='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 127
      Top = 55
      Width = 59
      Height = 20
      Caption = 'X0(Bo)='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edAo: TEdit
      Left = 63
      Top = 29
      Width = 33
      Height = 21
      Color = clSkyBlue
      TabOrder = 0
      Text = '0'
    end
    object edBo: TEdit
      Left = 63
      Top = 53
      Width = 33
      Height = 21
      Color = clSkyBlue
      TabOrder = 1
      Text = '1'
    end
    object edX0_Ao: TEdit
      Left = 192
      Top = 31
      Width = 34
      Height = 21
      Color = clSkyBlue
      TabOrder = 2
      Text = '1'
    end
    object edX0_Bo: TEdit
      Left = 192
      Top = 54
      Width = 34
      Height = 21
      Color = clSkyBlue
      TabOrder = 3
      Text = '1'
    end
  end
  object pnlNew: TPanel
    Left = 265
    Top = 88
    Width = 256
    Height = 97
    BevelInner = bvLowered
    TabOrder = 6
    object Label15: TLabel
      Left = 4
      Top = 2
      Width = 249
      Height = 24
      Caption = '   '#1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1076#1083#1103' '#1053#1100#1102#1090#1086#1085#1072'  '
      Color = clMoneyGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentColor = False
      ParentFont = False
    end
    object Label1: TLabel
      Left = 83
      Top = 28
      Width = 44
      Height = 20
      Caption = 'delta='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 28
      Width = 35
      Height = 20
      Caption = 'eps='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 166
      Top = 28
      Width = 39
      Height = 21
      Caption = 'beta='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 50
      Top = 70
      Width = 29
      Height = 20
      Caption = 'X0='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 138
      Top = 69
      Width = 29
      Height = 20
      Caption = 'X1='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 28
      Top = 48
      Width = 191
      Height = 20
      Caption = #1053#1072#1095#1072#1083#1100#1085#1086#1077' '#1087#1088#1080#1073#1083#1080#1078#1077#1085#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edDelta: TEdit
      Left = 128
      Top = 29
      Width = 33
      Height = 21
      Color = clMoneyGreen
      TabOrder = 0
      Text = '1e-4'
    end
    object edEps: TEdit
      Left = 45
      Top = 29
      Width = 33
      Height = 21
      Color = clMoneyGreen
      TabOrder = 1
      Text = '1e-3'
    end
    object edBetta: TEdit
      Left = 213
      Top = 28
      Width = 33
      Height = 21
      Color = clMoneyGreen
      TabOrder = 2
      Text = '0.5'
    end
    object edX0: TEdit
      Left = 83
      Top = 69
      Width = 33
      Height = 21
      Color = clMoneyGreen
      TabOrder = 3
      Text = '1'
    end
    object edX1: TEdit
      Left = 170
      Top = 69
      Width = 33
      Height = 21
      Color = clMoneyGreen
      TabOrder = 4
      Text = '-0.1'
    end
  end
  object Panel1: TPanel
    Left = 265
    Top = 188
    Width = 256
    Height = 29
    BevelInner = bvLowered
    TabOrder = 7
    object Label17: TLabel
      Left = 2
      Top = 2
      Width = 197
      Height = 24
      Caption = '   '#1055#1072#1088#1072#1084#1077#1090#1088' '#1089#1080#1089#1090#1077#1084#1099'  '
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentColor = False
      ParentFont = False
    end
    object Label6: TLabel
      Left = 200
      Top = 4
      Width = 20
      Height = 20
      Caption = 'A='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edA: TEdit
      Left = 220
      Top = 3
      Width = 33
      Height = 22
      Color = clInfoBk
      TabOrder = 0
      Text = '10'
    end
  end
end
