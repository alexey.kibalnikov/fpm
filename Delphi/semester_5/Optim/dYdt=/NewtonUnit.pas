unit NewtonUnit;
                            interface
{--------------------------------------------------------------------------}
uses mainUnit;
function Newton(F:func; X:vect; delta,eps,betta:extended; Aa,Ao,Bo:Integer; X_Ao,X_Bo:extended; var count:Integer):vect;
{--------------------------------------------------------------------------}
                            implementation
{--------------------------------------------------------------------------}
Function Gaus (var A:matr;var f:vect):vect; {Ay=f}
 var i,j,k,zr:word;
     Save:extended;
 begin
  for k:=0 to size-1 do {obnylyaem ocheredno'i stiklbe's}
   begin
    for zr:=0 to size-1 do if A[k,zr]<>0 then break;
    for i:=0 to size-1 do
     begin
      Save:=A[i,zr]/A[k,zr];
      for j:=zr to size-1 do
       if i<>zr then A[i,j]:=A[i,j]-A[k,j]*Save;
      if i<>zr then f[i]:=f[i]-f[zr]*Save;
     end;{for i}
   end;{for k}
 for k:=0 to size-1 do
  begin
   for zr:=0 to size-1 do if A[k,zr]<>0 then break;
   Gaus[zr]:=f[zr]/A[k,zr];
  end;
end;
{==========================================================================}
 Function Newton;
 var i,k:word;
     PRZ:matr;
     X1,X2,Z,FX:vect;
     Nor:extended;
 begin
  count:=0;
  Z:=X;
  FX:=F(Z,Aa,Ao,Bo,X_Ao,X_Bo);
  Nor:=0;
  for i:=0 to size-1 do Nor:=Nor+sqr(FX[i]);
  Nor:=Sqrt(Nor);
  while Nor>eps do
   begin
    for k:=0 to size-1 do
     for i:=0 to size-1 do
      begin
       X1:=Z;
       X2:=Z;
       X1[i]:=X1[i]+delta;   {identity(N)<i>}
       X2[i]:=X2[i]-delta;
       PRZ[k,i]:=(F(X1,Aa,Ao,Bo,X_Ao,X_Bo)[k]-F(X2,Aa,Ao,Bo,X_Ao,X_Bo)[k])/2/delta;
      end;
    for i:=0 to size-1 do FX[i]:=-FX[i];
    X1:=Gaus(PRZ,FX);
    for i:=0 to size-1 do Z[i]:=Z[i]+betta*X1[i];
    FX:=F(Z,Aa,Ao,Bo,X_Ao,X_Bo);
    Nor:=0;
    for i:=0 to size-1 do Nor:=Nor+sqr(FX[i]);
    Nor:=sqrt(Nor);
    inc(count);
   end;{while}
 Newton:=z;
end;{------------------- Newton}

end.
