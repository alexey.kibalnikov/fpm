unit Runge_Kutt_Unit;
{--------------------------------------------------------------------------}
                    interface
{--------------------------------------------------------------------------}
uses mainUnit;
Function Runge_Kutt(Y:vect; Ao,Bo:integer; n:word; Aa:integer; X_Ao,X_Bo:extended; flag:boolean):vect;
{--------------------------------------------------------------------------}
                    implementation
uses Grids,SysUtils,Graphics, Controls;
{--------------------------------------------------------------------------}
 function D(t:extended; Aa:integer; Y:vect):vect;
  begin
   D[0]:=Y[1];
   D[1]:=sqr(Aa)*Y[0];
  end;
{==========================================================================}
 Function Runge_Kutt;
 var Yn,k1,k2,k3,k4,Help:vect;
     tn,step:extended;
     {Canvas}
     y1,y2:vect;
     x1,x2,i,x_setka,y_setka:word;
     C,a,b,zz:word;
  begin
tn:=Ao; Yn:=Y;
Runge_Kutt[0]:=Yn[0]-X_Ao;
      if flag then
       begin
        with First.sgRunge_Kutt do
         begin
          Cells[0,1]:='0';
          Cells[1,1]:=FloatToStr(tn);
          Cells[2,1]:=FloatToStr(Yn[0]);
          Cells[3,1]:=FloatToStr(Yn[1]);
         end;{with sg}
        with First.pbPole.Canvas do
         begin
          zz:=10;
          C:=round(Aa*X_Ao);
          if Aa*X_Bo>C then C:=round(Aa*X_Bo);
          X_setka:=6;
          Y_setka:=7;
          a:=First.pbPole.ClientWidth-2*zz;
          b:=First.pbPole.ClientHeight-2*zz;
          Pen.Color:=clBlue;
          Pen.Width:=1;
          Brush.Style:=bsSolid;
          Brush.Color:=clAqua;
          Rectangle(zz-3,zz-3,a+zz+3,b+zz+3);
          Rectangle(zz,zz,a+zz,b+zz);
          Brush.Color:=clNavy;
          Font.Color:=clWhite;

          for i:=1 to X_setka*(Bo-Ao)-1 do
           begin
            x1:=round(a/(X_setka*(Bo-Ao)-1)*i+zz-a/2/(X_setka*(Bo-Ao)-1));
            MoveTo(x1,zz);
            LineTo(x1,b+zz);
           end;
          for i:=1 to Y_setka*(Bo-Ao)-1 do
           begin
            x1:=round(b/(Y_setka*(Bo-Ao)-1)*i+zz-b/2/(Y_setka*(Bo-Ao)-1));
            MoveTo(zz,x1);
            LineTo(a+zz,x1);
           end;

         TextOut(zz,zz,' '+IntToStr(C)+' ');
         TextOut(a-3,b-3,' '+IntToStr(Bo)+' ');
         TextOut(zz,b-3,'-'+IntToStr(C)+'/'+IntToStr(Ao)+' ');

         x1:=zz;
         y1[0]:=round(-(b div 2-zz)/C*Yn[0]+(b+2*zz) div 2);
         y1[1]:=round(-(b div 2-zz)/C*Yn[1]+(b+2*zz) div 2);
         Pen.Width:=2;
        end;{with Canvas}
       end;{if flag}

{the main part of Runge_Kutt metod}
step:=(Bo-Ao)/n;
for i:=1 to n do
 begin
  tn:=Ao+(i-1)*step;
  k1:=D(tn,       Aa, Yn  );
  Help[0]:=Yn[0]+k1[0]*step/2;
  Help[1]:=Yn[1]+k1[1]*step/2;
  k2:=D(tn+step/2,Aa, Help);
  Help[0]:=Yn[0]+k2[0]*step/2;
  Help[1]:=Yn[1]+k2[1]*step/2;
  k3:=D(tn+step/2,Aa, Help);
  Help[0]:=Yn[0]+k3[0]*step;
  Help[1]:=Yn[1]+k3[1]*step;
  k4:=D(tn+step,  Aa, Help);

  Yn[0]:=Yn[0]+(k1[0]+2*k2[0]+2*k3[0]+k4[0])*step/6;
  Yn[1]:=Yn[1]+(k1[1]+2*k2[1]+2*k3[1]+k4[1])*step/6;

      if flag then
       begin
        with First.sgRunge_Kutt do
         begin
          Cells[0,i+1]:=IntToStr(i);
          Cells[1,i+1]:=FloatToStr(Ao+i*step);
          Cells[2,i+1]:=FloatToStr(Yn[0]);
          Cells[3,i+1]:=FloatToStr(Yn[1]);
         end;{with sg}
        with First.pbPole.Canvas do
         begin
          tn:=Ao+i*step;
          x2:=round(a*(tn-Ao)/(Bo-Ao)+zz);
          y2[0]:=round(-(b div 2-zz)/C*Yn[0]+(b+2*zz) div 2);
          y2[1]:=round(-(b div 2-zz)/C*Yn[1]+(b+2*zz) div 2);

          Pen.Color:=clYellow;
          MoveTo(x1,round(y1[0]));
          lineTo(X2,round(y2[0]));
          Pen.Color:=clRed;
          MoveTo(x1,round(y1[1]));
          lineTo(X2,round(y2[1]));
          x1:=x2; y1:=y2;
         end;{with Canvas}
       end;{if flag}
   end;{for i}
Runge_Kutt[1]:=Yn[0]-X_Bo;
 end;{----------------------------------------------------------- Runge_Kutt}

end.

