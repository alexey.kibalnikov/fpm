unit mainUnit;
interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtCtrls, Buttons;

Const size=2;{0..size}
Type matr=array[0..size-1,0..size-1] of extended;
     vect=array[0..size-1] of extended;
     func=function(X:vect; Aa,Ao,Bo:integer; X_Ao,X_Bo:extended):vect;

  TFirst = class(TForm)
    edY0: TEdit;
    edY1: TEdit;
    sgRunge_Kutt: TStringGrid;
    pbPole: TPaintBox;
    edCount: TEdit;
    BitBtn1: TBitBtn;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    pnlGran: TPanel;
    pnlNew: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edAo: TEdit;
    edBo: TEdit;
    edX0_Ao: TEdit;
    edX0_Bo: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edDelta: TEdit;
    edEps: TEdit;
    edBetta: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    edX0: TEdit;
    edX1: TEdit;
    Label16: TLabel;
    Panel1: TPanel;
    Label17: TLabel;
    Label6: TLabel;
    edA: TEdit;
    procedure btnRezClick(Sender: TObject);
    procedure pbPolePaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  First: TFirst;

implementation
Uses NewtonUnit, Runge_Kutt_Unit;
{$R *.dfm}

Function F(X:vect; Aa,Ao,Bo:integer; X_Ao,X_Bo:extended):vect;
 begin F:=Runge_Kutt(X,Ao,Bo,100,Aa,X_Ao,X_Bo,false) end;{F}


procedure TFirst.btnRezClick(Sender: TObject);
 var X:vect;
     Eps,Delta,Betta:extended;
     Aa,Ao,Bo,count:Integer;
     X_Ao,X_Bo:extended;
begin
 X[0]:=StrToFloat(edX0.Text);
 X[1]:=StrToFloat(edX1.Text);
 Eps:=StrToFloat(edEps.Text);
 Delta:=StrToFloat(edDelta.Text);
 Betta:=StrToFloat(edBetta.Text);
 Aa:=StrToInt(edA.Text);
 Ao:=StrToInt(edAo.Text);
 Bo:=StrToInt(edBo.Text);
 X_Ao:=StrToFloat(edX0_Ao.Text);
 X_Bo:=StrToFloat(edX0_Bo.Text);

 X:=Newton(F,X,Delta,Eps,Betta,Aa,Ao,Bo,X_Ao,X_Bo,count); {alfa=?}
 edY0.Text:=FloatToStr(X[0]);
 edY1.Text:=FloatToStr(X[1]);
 edCount.Text:=IntToStr(count);
 Runge_Kutt(X,Ao,Bo,100,Aa,X_Ao,X_Bo,true);

end;


procedure TFirst.pbPolePaint(Sender: TObject);
begin
  DecimalSeparator:='.';
   with First.sgRunge_Kutt do
    begin
     Cells[0,0]:='№';
     Cells[1,0]:='   tn';
     Cells[2,0]:='   X0';
     Cells[3,0]:='   X1';
    end;
   with pbPole.Canvas do
    begin
     Brush.Style:=bsSolid;
     Brush.Color:=clAqua;
     Rectangle(0,0,pbPole.ClientWidth,pbPole.ClientHeight);
    end;
end;

end.
