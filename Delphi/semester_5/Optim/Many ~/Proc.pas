unit Proc;
interface
 Type vect=array [0..1] of extended;
 function Grad (X:vect;e,Alfa,Dv:extended; var count:longint):vect;
 function F(Xo,Yo:extended):extended;
 implementation {epx<=1e-9}
 const h=1e-10; l=1e-5; {for F',F"}
 function F(Xo,Yo:extended):extended; begin
F:=sqr(Xo*Xo)+sqr(Yo*Yo)+sqrt(2+Xo*Xo+Yo*Yo)-2*Xo+3*Yo end;
{--------------------------------------------------------------------------}
 function Grad;
  var Grd,Next:vect; cnt:longint;
      help,Save:extended;
  begin count:=0; cnt:=0;
   Save:=F(X[0],X[1]); Next:=X;
   repeat X:=Next;
    {F'} help:=Save;
    Grd[0]:=(F(X[0]+h,X[1])-help)/h;
    Grd[1]:=(F(X[0],X[1]+h)-help)/h;
    repeat {Alfa or Alfa/Dv =?}
     Next[0]:=X[0]-Alfa*Grd[0];
     Next[1]:=X[1]-Alfa*Grd[1];
     Save:=F(Next[0],Next[1]);
     if Save>help then Alfa:=Alfa/Dv;
     inc(cnt);
         if cnt=1e+4 then {undef}
     begin count:=0; exit end;
    until Save<help;
    inc(count);
   until sqrt(sqr(Grd[0])+sqr(Grd[1]))<e;
   Grad:=X;
  end;{Grad}

end.
