unit Form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls, ActnList;
type
  TFirst = class(TForm)
    edEps: TEdit;
    edXo: TEdit;
    edYo: TEdit;
    Label1: TLabel;
    pTitle: TPanel;
    Label2: TLabel;
    pRez: TPanel;
    Label3: TLabel;
    btStart: TBitBtn;
    Grad: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    edGrad_X: TEdit;
    edGrad_Y: TEdit;
    edGrad_Count: TEdit;
    Label12: TLabel;
    pParam: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    edDiv: TEdit;
    edAlfa: TEdit;
    procedure btStartClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  First: TFirst;

implementation
 uses Proc;
{$R *.dfm}


procedure TFirst.btStartClick(Sender: TObject);
var count:longint;
    X,Min:vect;
    e,Alfa,Dv:extended;
begin
 with First do begin
  DecimalSeparator:='.';
  X[0]:=StrToFloat(edXo.text);
  X[1]:=StrToFloat(edYo.text);
  e:=StrToFloat(edEps.text);
  Alfa:=StrToFloat(edAlfa.text);
  Dv:=StrToFloat(edDiv.Text);
 {проверка параметров}

 {вычисления} count:=0;
  if Dv>=1 then
    Min:=Proc.Grad(X,e,Alfa,Dv,count);
  if count>0 then begin
   edGrad_X.Text:=FloatToStr(Min[0]);
   edGrad_Y.Text:=FloatToStr(Min[1]);
   edGrad_Count.Text:=FloatToStr(count);
  end else begin
   edGrad_X.Text:='undef';
   edGrad_Y.Text:='undef';
   edGrad_Count.Text:='undef';
  end;
 end{with}
end;

end.

