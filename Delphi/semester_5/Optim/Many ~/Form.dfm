�
 TFIRST 0�  TPF0TFirstFirstLeft� Top}BorderStylebsDialogCaption   Вариант #155 for AcridClientHeight� ClientWidthWColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPixelsPerInch`
TextHeight TPanelpTitleLeft�Top Width\HeighthCtl3DParentCtl3DTabOrder TLabelLabel2LeftTopWidth3HeightCaptionD   Поиск безусловного минимума функции ColorclActiveBorderFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TLabelLabel3LeftTop4Width>HeightCaptionH   по начальному приближению  [         ,         ] Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel12Left
TopKWidthuHeightCaption   с точностью e=Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel1LeftTopWidthNHeightCaption%F(x,y):=x^4+y^4+(2+x^2+y^2)^1/2-2x+3yFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedEpsLeft� TopKWidthgHeightBorderStylebsNoneColorclScrollBarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength

ParentFontTabOrder Text
0.00000001  TEditedXoLeft� Top2Width#HeightBorderStylebsNoneColorclScrollBarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText0  TEditedYoLeftTop3Width&HeightBorderStylebsNoneColorclScrollBarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText0   TPanelpRezLeft�TopgWidthYHeight� TabOrder 	TGroupBoxGradLeftTopWidth� Height� Caption   метод градиента EnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel4LeftTop"WidthHeightCaptionX:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTop@WidthHeightCaptionY:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftTopaWidth7HeightCaptionCount:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedGrad_XLeft+Top!WidthyHeightColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TEditedGrad_YLeft+TopCWidthyHeightColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEditedGrad_CountLeftOTopdWidthUHeightColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder    TBitBtnbtStartLeft� Top� WidthAHeightBiDiModebdLeftToRightParentBiDiModeTabOrder OnClickbtStartClickKindbkOK  	TGroupBoxpParamLeft� TopiWidth� HeightfCaption   параметры Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel6LeftTopWidth&HeightCaption   шаг:  TLabelLabel8LeftTop@WidthhHeightCaption   дробление:  TEditedDivLeftnTop?Width"HeightColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder Text3.3  TEditedAlfaLeft/Top WidthaHeightColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText0.4    