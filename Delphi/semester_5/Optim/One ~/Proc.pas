unit Proc;
interface
 function Dihat(a,b,e,param:extended; var count:word):extended;
 function Sechen(a,b,e:extended; var count:word):extended;
 function Kasat(a,b,e:extended; var count:word):extended;
 function Grad(a,b,e,param:extended; var count:word):extended;
 function Nuton(a,b,e,param:extended; var count:word):extended;
implementation
 const h=1e-9; {for F',F"}
 function F(x:extended):extended; 
  begin F:=ln(1+x*x)-sin(x) end;
{--------------------------------------------------------------------------}
 function Dihat;
  var c:extended;
  begin count:=0;
   repeat
    c:=(a+b)/2;
    if F(c-param)<F(c+param) then b:=c else a:=c;
    inc(count)
   until (b-a)<e;
   Dihat:=c
  end;{Dihat}
 function Sechen;
  var X1,X2,c,En,buf_C,Save,Fx:extended;
   flag:boolean;
  begin count:=0;
   c:=(b+a)/2;
   X1:=a+(3-sqrt(5))/2*(b-a);
   X2:=a+(sqrt(5)-1)/2*(b-a);
   Fx:=F(X1); flag:=true;
   repeat
    buf_C:=c; Save:=Fx;
    if flag then Fx:=F(X2) else Fx:=F(X1);
    if (Save<=Fx) and flag or (Fx<=Save) and not flag then {F(X1)<=F(X2)}
     begin b:=X2; X2:=X1; c:=X1;
           if flag then begin flag:=false; Fx:=Save end;   {Save=F(X1)}
           X1:=a+(3-sqrt(5))/2*(b-a)  end
    else begin a:=X1; X1:=X2; c:=X2;
           if not flag then begin flag:=true; Fx:=Save end;{Save=F(X2)}
           X2:=a+(sqrt(5)-1)/2*(b-a)  end;
    inc(count);
    En:=exp(count*ln((sqrt(5)-1)/2))
   until En<=e;
   Sechen:=c;
  end;{Sechen}
 function Kasat;
  var c,help:extended;
  begin count:=0;
   repeat
    c:=((F(b)-F(a))*h+a*(F(a+h)-F(a))+b*(F(b)-F(b+h)))/(F(a+h)-F(a)-F(b+h)+F(b));
    help:=(F(c+h)-F(c))/h;
    if help<0 then a:=c else b:=c;
    inc(count)
   until abs(help)<e;
   Kasat:=c
  end;{Kasat}
 function Grad;
  var Xn,Next,help:extended;
  begin count:=0; Next:=(a+b)/2;
   repeat
    Xn:=Next;
    help:=(F(Xn+h)-F(Xn))/h;
    Next:=Xn-param*help;
    inc(count);
    if count=60000 then begin
     count:=0; break end;
   until abs(help)<e;
   Grad:=Next
  end;{Grad}
 function Nuton;
  var Xn,Next,help:extended;
  begin count:=0; Next:=(a+b)/5;
   repeat
    Xn:=Next;
    help:=(F(Xn+h)-F(Xn));
    Next:=Xn-param*h*help/(F(Xn+h)-2*F(Xn)+F(Xn-h));
    inc(count);
    if count=60000 then begin
     count:=0; break end;
   until (abs(help)/h+abs(Next-Xn))<e;
   Nuton:=Next
  end;{Nuton}
end.
