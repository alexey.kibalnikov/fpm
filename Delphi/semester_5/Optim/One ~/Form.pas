unit Form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls;

type
  TFirst = class(TForm)
    edEps: TEdit;
    edA: TEdit;
    edB: TEdit;
    Label1: TLabel;
    pTitle: TPanel;
    Label2: TLabel;
    pRez: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    btStart: TBitBtn;
    edDihat: TEdit;
    edSechen: TEdit;
    edKasat: TEdit;
    edGrad: TEdit;
    Label8: TLabel;
    edDihat_c: TEdit;
    edSechen_c: TEdit;
    edGrad_c: TEdit;
    edKasat_c: TEdit;
    Label9: TLabel;
    edNuton: TEdit;
    edNuton_c: TEdit;
    pParam: TGroupBox;
    edDihat_p: TEdit;
    edSechen_p: TEdit;
    edGrad_p: TEdit;
    edNuton_p: TEdit;
    edKasat_p: TEdit;
    Flag: TCheckBox;
    mHelp: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FlagClick(Sender: TObject);
    procedure btStartClick(Sender: TObject);
    procedure pParamEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  First: TFirst;

implementation
 uses Proc;

{$R *.dfm}



procedure TFirst.FormCreate(Sender: TObject);
 var B:extended;
begin
 DecimalSeparator:='.';
 B:=Pi/4; First.edB.text:=FloatToStr(B);
 B:=StrToFloat(First.edEps.text); B:=B/2;
 First.edDihat_p.text:=FloatToStr(B);
end;

procedure TFirst.FlagClick(Sender: TObject);
begin
 First.pTitle.Enabled:=not First.pTitle.Enabled;
 First.pParam.Enabled:=not First.pParam.Enabled;
 if First.mHelp.Tag=-1 then
  begin
   First.mHelp.Visible:=true;
   First.mHelp.Tag:=0;
  end;
end;




procedure TFirst.btStartClick(Sender: TObject);
var count:word;
    a,b,e,pD,pG,pN:extended;
begin
 First.mHelp.Visible:=false;
 a:=StrToFloat(First.edA.text);
 b:=StrToFloat(First.edB.text);
 e:=StrToFloat(First.edEps.text);
 pD:=StrToFloat(First.edEps.text);
 pD:=pD/2;
 First.edDihat_p.text:=FloatToStr(pD);
 pG:=StrToFloat(First.edGrad_p.text);
 pN:=StrToFloat(First.edNuton_p.text);
 {проверка параметров}

 {вычисления}
 First.edDihat.Text:=FloatToStr(Proc.Dihat(a,b,e,pD,count));
 First.edDihat_c.Text:=FloatToStr(count);
 First.edSechen.Text:=FloatToStr(Proc.Sechen(a,b,e,count));
 First.edSechen_c.Text:=FloatToStr(count);
 First.edKasat.Text:=FloatToStr(Proc.Kasat(a,b,e,count));
 First.edKasat_c.Text:=FloatToStr(count);

 First.edGrad.Text:=FloatToStr(Proc.Grad(a,b,e,pG,count));
 First.edGrad_c.Text:=FloatToStr(count);
 if count=0 then  begin
  First.edGrad.Text:='undef';
  First.edGrad_c.Text:='undef';
 end;
 First.edNuton.Text:=FloatToStr(Proc.Nuton(a,b,e,pN,count));
 First.edNuton_c.Text:=FloatToStr(count);
 if count=0 then begin
  First.edNuton.Text:='undef';
  First.edNuton_c.Text:='undef';
 end;

end;


procedure TFirst.pParamEnter(Sender: TObject);
begin
 First.mHelp.Visible:=false;
end;

end.
