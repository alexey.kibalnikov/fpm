�
 TFIRST 0�  TPF0TFirstFirstLeft� Top}BorderStylebsDialogCaption   Вариант #14 for AcridClientHeightClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight TPanelpTitleLeft�Top Width�Height9Ctl3DEnabledParentCtl3DTabOrder TLabelLabel2LeftTopWidth�HeightCaptiona   Поиск минимума функции                                                   наColorclActiveBorderFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TLabelLabel1Left� TopWidth� HeightCaptionF(x):=ln(1+x^2)-sin(x)Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3LeftTop WidthLHeightCaptionK   промежутке [       ,                   ] с точностью e=Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedEpsLeftUTop WidthgHeightBorderStylebsNoneColorclScrollBarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength

ParentFontTabOrder Text
0.00000001  TEditedALeftlTopWidthHeightBorderStylebsNoneColorclScrollBarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText0  TEditedBLeft� TopWidthHHeightBorderStylebsNoneColorclScrollBarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TPanelpRezLeft�Top8Width�Height� EnabledTabOrder TLabelLabel4LeftTop!WidthoHeightCaption   Дихатомии :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTop;Width� HeightCaption    Золотого сечения:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6LeftTopXWidth{HeightCaption   Касательных:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftTop� WidthUHeightCaption   Ньютона:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8LeftTopWidth�HeightCaption`   Название метода           Результат Итерации Параметры  Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel9LeftToptWidthfHeightCaption   Градиента:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedDihatLeft� Top%WidthKHeightColor	cl3DLightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrder   TEditedSechenLeft� Top?WidthKHeightColor	cl3DLightTabOrder  TEditedKasatLeft� Top[WidthKHeightColor	cl3DLightTabOrder  TEditedGradLeft� TopwWidthLHeightColor	cl3DLightTabOrder  TEdit	edDihat_cLeftTop%WidthCHeightColor	cl3DLightTabOrder  TEdit
edSechen_cLeftTop?WidthCHeightColor	cl3DLightTabOrder  TEdit	edKasat_cLeftTop[WidthCHeightColor	cl3DLightTabOrder  TEditedGrad_cLeftTopwWidthCHeightColor	cl3DLightTabOrder  TEditedNutonLeft� Top� WidthLHeightColor	cl3DLightTabOrder  TEdit	edNuton_cLeftTop� WidthCHeightColor	cl3DLightTabOrder	  TMemomHelpTag�LeftTopWidthYHeight� ColorclMoneyGreenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Lines.Strings    )                        ВНИМАНИЕ !!!:             В программе нет 'фильтров' 6               на  введеные параметры,4                   Если зависнет, то  я +                      НЕ ВИНОВАТ :-)) 
ParentFontTabOrder
Visible   TBitBtnbtStartLeftkTop� WidthAHeightBiDiModebdLeftToRightParentBiDiModeTabOrder OnClickbtStartClickKindbkOK  	TGroupBoxpParamLeft[TopQWidthbHeight� EnabledTabOrderOnEnterpParamEnter TEdit	edDihat_pLeftTopWidthYHeightColor	cl3DLightTabOrder   TEdit
edSechen_pLeftTop&WidthYHeightColor	cl3DLightReadOnly	TabOrderText             НЕТ   TEditedGrad_pLeftTop]WidthZHeightColor	cl3DLightTabOrderText0.75  TEdit	edNuton_pLeftTopxWidthYHeightColor	cl3DLightTabOrderText0.97722  TEdit	edKasat_pLeftTopAWidthYHeightColor	cl3DLightReadOnly	TabOrderText             НЕТ    	TCheckBoxFlagLeftTop� Width!HeightHelpType	htKeywordCaption7   Сделать доступными  параметрыFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClick	FlagClick   