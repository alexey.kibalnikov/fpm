unit SDBGrid;

interface

uses
  Windows,  // tRect
  Classes,  // tNotifyEvent
  Graphics, // clWhite
  Grids,    // tDrawCellEvent
  DBGrids,  // tColumn
  StdCtrls, // tEdit
  JvDBGrid; // TJvDBGrid

type

  tSDBGrid = class;
  { Деиствия при работе с индексами колонок, доступных для инкрементного поиска: }
  tFilterIndexColumnEnableAction = (
    fica_Create,        // удаляются все прошлые индексы и создаются новые, переданные в качестве параметра
    fica_Add,           // к числу уже имеющихся индексов добавляются новые
    fica_Add_ShowOnly,  // удаляются все прошлые индексы и создаются новые, для колонок видимых на экране
    fica_Add_ALL,       // добавляются индексы для всех колонок из DataSet-a
    fica_Del,           // из имеющихся индексов удаляются переданные в качестве параметра
    fica_Clear);        // удаляются все созданные индексы

  tGridFilter = class
  private
    fSDBGrid: tSDBGrid;
    fFilterEnable: array of tEdit;
    function GetFilter(ACol: Integer): tEdit;
    procedure ClearFilterEnable;
    { рассылка события о изменении }
    procedure OnFilterKeyPress(Sender: TObject; var Key: Char);
  public
    constructor Create(ASDBGrid: tSDBGrid);
    destructor Destroy; override;
    { показать фильтр }
    procedure FilterVisible(Value: Boolean);
    { определяет положение поля поиска (столбец в fSGrid-е и ширину поля Edit-a) }
    procedure SyncPositionFilter;
    { установить колонки fSDBGrid-а, которые подддерживают инкрементный поиск }
    procedure FilterIndexColumnEnable(Action: tFilterIndexColumnEnableAction; Value: array of Integer);
    { поменять местами фильтры, при смене положения столбца в fSGrid-е }
    procedure FilterMoved(AFromIndex, AToIndex: Integer);
    { взять фильтр для конкретной колонки Grid-a }
    property Filter[ACol: Integer]: tEdit read GetFilter; // индексация с 0
  end;

  tSDBGrid = class(TJvDBGrid)
  private
    fGridFilter: tGridFilter;
    fFilter: Boolean;
    { из скольких строк стстоит заголовок одна\две }
    fTitleLinesCount: Integer;
    // -> перекрытые методы
    fLayoutChanged: tNotifyEvent;
    fDrawCell: tDrawCellEvent;
    fSetColumnAttributes: tNotifyEvent;
    fColumnMoved: tMovedEvent;
    fTopLeftChanged: tNotifyEvent;
    procedure SetFilter(Value: Boolean);
    function GetActiveRecord: Integer;
    { определяет общую высоту заголовка }
    procedure CalcTitle;
  protected
    { Пересчет размеров заголовка при изменении размеров колонки }
    procedure LayoutChanged; override;
    { Своя прорисовка, для заголовка Title }
    procedure DrawCell(ACol, ARow: Integer; ARect: TRect; State: TGridDrawState); override;
    { Cинхронизация: изменение ширины колонки }
    procedure SetColumnAttributes; override;
    { Cинхронизация: изменение позиции колонки }
    procedure ColumnMoved(FromIndex, ToIndex: Longint); override;
    { Cинхронизация: горизонтальный скролинг(изменение колонки, которая оказывается первой видимой в гриде)}
    procedure TopLeftChanged; override;
  published
    { отображать строку инкрементного поиска }
    property Filter: Boolean read fFilter write SetFilter;
    { активная запись }
    property ActiveRecord: Integer read GetActiveRecord;
    // -> перекрытые методы
    property OnLayoutChanged: tNotifyEvent read fLayoutChanged write fLayoutChanged;
    property OnDrawCell: tDrawCellEvent read fDrawCell write fDrawCell;
    property OnSetColumnAttributes: tNotifyEvent read fSetColumnAttributes write fSetColumnAttributes;
    property OnColumnMoved: tMovedEvent read fColumnMoved write fColumnMoved;
    property OnTopLeftChanged: tNotifyEvent read fTopLeftChanged write fTopLeftChanged;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { установить колонки fSDBGrid-а, которые подддерживают инкрементный поиск }
    procedure FilterIndexColumnEnable(Action: tFilterIndexColumnEnableAction; Value: array of Integer);
    // -> вспомогательные функции, позволяющие узнать скрытые свойства
    { ширина индикатора грида }
    function IndicatorWidth: Integer;
    { толщина линий сетки }
    function SizeGridLineVert: Integer;
    function SizeGridLineHorz: Integer;
    { Прямоугольник для ячейки }
    function CellRect(ACol, ARow: Longint): TRect;
    { Прямоугольник для заголовка колонки }
    function TitleRect(ACol: Integer): TRect;
    { Перерисовка толко заголовков }
    procedure RefreshTitles;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Sonar Controls', [tSDBGrid]);
end;

const
  TITLE_LINESIZE = 20; // высота однои строки заголовка
  TITLE_FILTER = 2; // заголовок состоит из двух сторк: шапка + фильтр
  TITLE_DEFAULT = 1; // ... из одной строки - шапка

type
  // Вспомогательные типы, позволяющие получить доступ к скрытым полям
  tPublicGrid = class( TCustomGrid);

// <tGridFilter>

constructor tGridFilter.Create(ASDBGrid: tSDBGrid);
begin
  fSDBGrid := ASDBGrid;
  fFilterEnable := nil;
end;

destructor tGridFilter.Destroy;
begin
  ClearFilterEnable;
  inherited;
end;

procedure tGridFilter.ClearFilterEnable;
var
  Edit: tEdit;
begin
  for Edit in fFilterEnable do
    Edit.Free;
  SetLength(fFilterEnable, 0);
end;

{ рассылка события о изменении }
procedure tGridFilter.OnFilterKeyPress(Sender: TObject; var Key: Char);
begin
  if Sender is tEdit then
    begin
      ShowMessage(fSDBGrid.DataSource.DataSet.Fields[tEdit(Sender).Tag].DisplayName);
      if tEdit(Sender).Text = '' then
        // рассылка события об отчистке фильтрации
      else
        // рассылка события с текстом для фильтрации
    end;
end;

procedure tGridFilter.FilterVisible(Value: Boolean);
var
  Edit: tEdit;
begin
  for Edit in fFilterEnable do
    Edit.Visible := Value;
end;

{ определяет положение поля поиска (столбец в Grid-е и ширину поля Edit-a)}
procedure tGridFilter.SyncPositionFilter;

  function RectWidth(R: TRect): Integer;
  begin
    Result := R.Right - R.Left;
  end;

var
  I: Integer;
  Edit: tEdit;
begin
  FilterVisible(False);
  if Assigned(fSDBGrid.DataLink) and fSDBGrid.DataLink.Active and fSDBGrid.Filter then
    { Продегаем по всем колонкам. Для колонки у которой создан Filter настраиваем его }
    for I := 0 to fSDBGrid.DataSource.DataSet.Fields.Count- 1 do
      begin
        Edit := Filter[I];
        if Assigned(Edit) then
          begin
            Edit.Visible := True;
            Edit.Top := (TITLE_LINESIZE * fSDBGrid.fTitleLinesCount) div TITLE_FILTER;
            Edit.Left := fSDBGrid.TitleRect(I+ 1).Left;
            Edit.Width := RectWidth(fSDBGrid.TitleRect(I+ 1));
          end;
      end;
end;

{ установить колонки fSDBGrid-а, которые подддерживают инкрементный поиск }
procedure tGridFilter.FilterIndexColumnEnable(Action: tFilterIndexColumnEnableAction; Value: array of Integer);

  { Индекс попадает в область область допустимых значений (границы) }
  function ColInGrid(ACol: Integer): Boolean;
  begin
    Result := (0<=ACol) and (ACol<fSDBGrid.DataSource.DataSet.Fields.Count);
  end;

  { Индекс уже существует }
  function ColFind(ACol: Integer): Boolean;
  begin
    Result := Assigned(Filter[ACol]);
  end;

  { Создает новое поле Filter-a, для колонки ACol }
  procedure CreateEditByColIndex(ACol: Integer);
  var
    ix: Integer;
  begin
    if not ColInGrid(ACol) or ColFind(ACol) then
      Exit;

    ix := Length(fFilterEnable);
    SetLength(fFilterEnable, ix + 1);

    fFilterEnable[ix] := tEdit.Create(fSDBGrid);
    fFilterEnable[ix].Parent := fSDBGrid;
    fFilterEnable[ix].Visible := False;
    fFilterEnable[ix].OnKeyPress := OnFilterKeyPress;
    fFilterEnable[ix].Tag := ACol;
  end;

  { Удаляет поле Filter-a, для колонки ACol }
  procedure DeleteEditByColIndex(ACol: Integer);
  var
    I, ixFind: Integer;
  begin
    if not (ColInGrid(ACol) and ColFind(ACol)) then
      Exit;

    { Обычное удаление из контейнера. Поиск того элемента который надо удалить.
      Непосредственное удаление, а потом перемещение элементов, следующих за удаленным
      и уменьшение размера контейнера. }
    ixFind := -1;
    for I := 0 to Length(fFilterEnable)- 1 do
      if fFilterEnable[I].Tag = ACol then
        begin
          ixFind := I;
          Break;
        end;
    if ixFind >= 0 then
      begin
        fFilterEnable[ixFind].Free;
        for I := ixFind to Length(fFilterEnable)- 2 do
          fFilterEnable[I] := fFilterEnable[I+ 1];
        SetLength(fFilterEnable, Length(fFilterEnable)- 1);
      end;
  end;

var
  I: Integer;
begin
  if not (Assigned(fSDBGrid.DataLink) and fSDBGrid.DataLink.Active) then
    Exit;

  Case Action of
    fica_Create:
      begin
        ClearFilterEnable;
        for I in Value do
          CreateEditByColIndex(I);
      end;
    fica_Add:
      for I in Value do
        CreateEditByColIndex(I);
    fica_Add_ShowOnly: // сформировать значения из всех колонок fSDBGrid-a, видимых на экране
      begin
        ClearFilterEnable;
        for I := 0 to fSDBGrid.FieldCount- 1 do
          CreateEditByColIndex(I);
      end;
    fica_Add_ALL:      // сформировать значения из всех колонок fSDBGrid-a, содержащихся в DataSet-e
      for I := 0 to fSDBGrid.DataSource.DataSet.Fields.Count- 1 do
        CreateEditByColIndex(I);
    fica_Del:
      for I in Value do
        DeleteEditByColIndex(I);
    fica_Clear:        // отчистить значения индексов
      ClearFilterEnable;
  end;
  SyncPositionFilter;
end;

{ поменять местами фильтры, при смене положения столбца в fSGrid-е }
procedure tGridFilter.FilterMoved(AFromIndex, AToIndex: Integer);
var
  EditFrom, Edit: tEdit;
  I: Integer;
begin
  dec(AFromIndex); // индексация FilterEdit-ов c 0, а в переданных параметрах (колонки) с 1
  dec(AToIndex);

  { При перемещении столбца в DBGrid-е происходит следующее: колонка с индексом FromIndex
    помещается в позицию ToIndex, а колонки заключенные между этими индексами перемещаются
    на одну позицию превее или левее, в зависимости от проделанного перемещения.
    Этот код перемещает уже созданные FilterEdit-ы в след за их колонками в Grid-e. }
  EditFrom := Filter[AFromIndex];
  if Assigned(EditFrom) then
    EditFrom.Tag := - 1;

  if AToIndex < AFromIndex then // Drop <- Drag
    for I := AFromIndex- 1 downto AToIndex do
      begin
        Edit := Filter[I];
        if Assigned(Edit) then
          Edit.Tag := I+ 1;
      end
  else // Drag -> Drop
    for I := AFromIndex+ 1 to AToIndex  do
      begin
        Edit := Filter[I];
        if Assigned(Edit) then
          Edit.Tag := I- 1;
      end;

  if Assigned(EditFrom) then
    EditFrom.Tag := AToIndex;
end;

function tGridFilter.GetFilter(ACol: Integer): tEdit;
var
  Edit: tEdit;
begin
  Result := nil;
  for Edit in fFilterEnable do
    if Edit.Tag = ACol then
      begin
        Result := Edit;
        Break;
      end;
end;

// </tFilter>

// <tSDBGrid>

procedure tSDBGrid.CalcTitle;
begin
  RowHeights[0] := TITLE_LINESIZE * fTitleLinesCount;
end;

{ Своя прорисовка, для заголовка Title }
procedure tSDBGrid.DrawCell(ACol, ARow: Integer; ARect: TRect; State: TGridDrawState);

  function RectHeight(R: TRect): Integer;
  begin
    Result := R.Bottom - R.Top;
  end;

  { отчистка полей отвоеванных у заголовка }
  procedure ClearFilterTitel(ACol: Integer; ARect: tRect);
  begin
    { Рисуем прямоугольники в отваеванном пространстве под "шапкой" Grid-a (отчистка).
      Там где не допускается фильтраия - зеленые }
    if not Assigned(fGridFilter.Filter[ACol]) then
      begin
        Canvas.Brush.Color := clGradientActiveCaption;
        Canvas.FillRect(ARect);
      end;
  end;

  procedure DrawTitleCell(ARect: TRect; Column: TColumn);
    procedure Paint3dRect(DC: HDC; ARect: TRect);
    {Рамочка вокруг заданного прямоугольника}
    begin
      InflateRect(ARect, 1, 1);
      DrawEdge(DC, ARect, BDR_RAISEDINNER, BF_BOTTOMRIGHT);
      DrawEdge(DC, ARect, BDR_RAISEDINNER, BF_TOPLEFT);
    end;
    procedure WriteText(ACanvas: TCanvas; ARect: TRect; const Text: string; Alignment: TAlignment);
    const
      ALIGN_FLAGS: array[TAlignment] of Integer =
        (DT_LEFT or DT_SINGLELINE or DT_EXPANDTABS or DT_NOPREFIX,
         DT_RIGHT or DT_SINGLELINE or DT_EXPANDTABS or DT_NOPREFIX,
         DT_CENTER or DT_SINGLELINE or DT_EXPANDTABS or DT_NOPREFIX);
    var
      DrawRect: TRect;
    begin
      DrawRect := Rect(ARect.Left + 1, ARect.Top + 1, ARect.Right, ARect.Bottom);
      DrawText(ACanvas.Handle, PChar(Text), Length(Text), DrawRect,
        ALIGN_FLAGS[Alignment] or DT_VCENTER or DT_END_ELLIPSIS);
    end;
  begin
    Canvas.FillRect(ARect);
    InflateRect(ARect, -1, -1);
    WriteText(Canvas, ARect, Column.Title.Caption, Column.Title.Alignment);
    Paint3dRect(Canvas.Handle, ARect);
  end;

var
  DataCol: Integer;
  FRect: TRect;
  AColor: TColor;
begin
  if csDesigning in ComponentState then
    Exit;

  DataCol := ACol;
  if dgIndicator in Options then
    Dec(DataCol);
  AColor := Canvas.Brush.Color;
  if (dgTitles in Options) and (gdFixed in State) and (ARow = 0) and (ACol <> 0) then
    begin
      { Стандартное действие DBGrid }
      if csLoading in ComponentState then
        begin
          Canvas.Brush.Color := Color;
          Canvas.FillRect(ARect);
          Exit;
        end;

      FRect := ARect;
      Canvas.Brush.Color := Columns[DataCol].Title.Color;
      Canvas.Font := Columns[DataCol].Title.Font;

      if Filter then
        begin
          { Рисуем заголовки Title }
          FRect := ARect;
          FRect.Bottom := RectHeight(FRect) div fTitleLinesCount;
          DrawTitleCell(FRect, Columns[DataCol]);
          { Рисуем прямоугольники под заголовками }
          ARect.Top := FRect.Bottom;
          ClearFilterTitel(DataCol, ARect);
        end
      else
        DrawTitleCell(FRect, Columns[DataCol]);
    end
  else
    begin
      { Стандартное действие DBGrid }
      Canvas.Brush.Color := AColor;
      Canvas.Font := Font;
      inherited DrawCell(ACol, ARow, ARect, State);
    end;
  if Assigned(OnDrawCell) then
    OnDrawCell(Self, ACol, ARow, ARect, State);
end;

{ Пересчет размеров заголовка при изменении размеров колонки }
procedure tSDBGrid.LayoutChanged;
begin
  inherited;
  CalcTitle;
  if Assigned(OnLayoutChanged) then
    OnLayoutChanged(Self);
end;

procedure tSDBGrid.SetFilter(Value: Boolean);
begin
  fFilter := Value;
  fGridFilter.FilterVisible(fFilter);
  if fFilter then
    fTitleLinesCount := TITLE_FILTER
  else
    fTitleLinesCount := TITLE_DEFAULT;
  CalcTitle;
  if fFilter then
    fGridFilter.SyncPositionFilter;
end;

function tSDBGrid.IndicatorWidth: Integer;
begin
  Result := ColWidths[0] * IndicatorOffset;
end;

{ Возвращает ширину вертикальной разделительной линии сетки }
function tSDBGrid.SizeGridLineVert: Integer;
begin
  with tPublicGrid(Self) do
    if [goFixedVertLine, goVertLine]*Options = [] then
      Result := 0
    else
      Result := GridLineWidth;
end;

{ Возвращает высоту горизонтальной разделительной линии сетки }
function tSDBGrid.SizeGridLineHorz: Integer;
begin
  with tPublicGrid(Self) do
    if [goFixedHorzLine, goHorzLine] * Options = [] then
      Result := 0
    else
      Result := GridLineWidth;
end;

{ Прямоугольник для ячейки }
function tSDBGrid.CellRect(ACol, ARow: Longint): TRect;
begin
  Result := inherited CellRect(ACol, ARow);
end;

{ Прямоугольник для заголовка колонки }
function tSDBGrid.TitleRect(ACol: Integer): TRect;
begin
  Result := CellRect(ACol, 0);
end;

{ Перерисовка толко заголовков }
procedure tSDBGrid.RefreshTitles;
begin
  inherited InvalidateTitles;
end;

{ Возвращает активную запись }
function tSDBGrid.GetActiveRecord: Integer;
begin
  Result := DataLink.ActiveRecord;
end;

constructor tSDBGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fGridFilter := tGridFilter.Create(Self);
  Filter := False;
  AutoSizeColumns := True;
end;

destructor tSDBGrid.Destroy;
begin
  fGridFilter.Free;
  inherited;
end;

procedure tSDBGrid.FilterIndexColumnEnable(Action: tFilterIndexColumnEnableAction; Value: array of Integer);
begin
  fGridFilter.FilterIndexColumnEnable(Action, Value);
end;

{ Cинхронизация: изменение ширины колонки }
procedure tSDBGrid.SetColumnAttributes;
begin
  inherited;
  fGridFilter.SyncPositionFilter;
  if Assigned(OnSetColumnAttributes) then
    OnSetColumnAttributes(Self);
end;

{ Cинхронизация: изменение позиции колонки }
procedure tSDBGrid.ColumnMoved(FromIndex, ToIndex: Longint);
begin
  inherited;
  fGridFilter.FilterMoved(FromIndex, ToIndex);
  fGridFilter.SyncPositionFilter;
  if Assigned(OnColumnMoved) then
    OnColumnMoved(Self, FromIndex, ToIndex);
end;

{ Cинхронизация: горизонтальный скролинг(изменение колонки, которая оказывается первой видимой в гриде)}
procedure tSDBGrid.TopLeftChanged;
begin
  inherited;
  fGridFilter.SyncPositionFilter;
  if Assigned(OnTopLeftChanged) then
    OnTopLeftChanged(Self);
end;

// </tSDBGrid>

end.
