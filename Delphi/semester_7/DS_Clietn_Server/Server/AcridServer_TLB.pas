unit AcridServer_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 03.06.04 19:10:29 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\User\My_Doc\Delphi\DS_Clietn_Server\Server\AcridServer.tlb (1)
// LIBID: {F512D3C0-7D7F-11D8-B0EB-BD9930084878}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\W\SYSTEM\midas.dll)
//   (2) v2.0 stdole, (C:\W\SYSTEM\STDOLE2.TLB)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, Midas, StdVCL, Variants, Windows;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AcridServerMajorVersion = 1;
  AcridServerMinorVersion = 0;

  LIBID_AcridServer: TGUID = '{F512D3C0-7D7F-11D8-B0EB-BD9930084878}';

  IID_ICoClassAcridServer: TGUID = '{F512D3C1-7D7F-11D8-B0EB-BD9930084878}';
  CLASS_CoClassAcridServer: TGUID = '{F512D3C3-7D7F-11D8-B0EB-BD9930084878}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICoClassAcridServer = interface;
  ICoClassAcridServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CoClassAcridServer = ICoClassAcridServer;


// *********************************************************************//
// Interface: ICoClassAcridServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F512D3C1-7D7F-11D8-B0EB-BD9930084878}
// *********************************************************************//
  ICoClassAcridServer = interface(IAppServer)
    ['{F512D3C1-7D7F-11D8-B0EB-BD9930084878}']
    procedure sm_DelTovar(ID_T: Integer); safecall;
    procedure sm_DelPostavshik(ID_P: Integer); safecall;
    procedure sm_DelNakladnaya(ID_N: Integer); safecall;
    procedure sm_DelSchet(ID_S: Integer); safecall;
    procedure sm_DelPay(ID_P: Integer); safecall;
    function  sm_AddTovar(ID_T: Integer; const Name: WideString; NDS: Smallint): Integer; safecall;
    function  sm_AddPostavshik(ID_P: Integer; const FIO: WideString; const Tel: WideString; 
                               const Addr: WideString): Integer; safecall;
    function  sm_AddNakladnaya(ID_N: Integer; ID_P: Integer; const Shifr: WideString;
                               Data: TDateTime; NDS_Sum: Single; const predoplata: WideString;
                               const otgruzka: WideString; ostatok: Single): Integer; safecall;
    function  sm_AddSchet(ID_S: Integer; ID_N: Integer; ID_T: Integer; Koi_vo: Smallint;
                          NDS_Sum: Single): Integer; safecall;
    function  sm_AddPay(ID_P: Integer; ID_N: Integer; Data: TDateTime; NDS_Sum: Single): Integer; safecall;
    procedure sm_QueryClear; safecall;
    procedure sm_QueryExec; safecall;
    procedure sm_QueryAdd(const S: WideString); safecall;
    procedure sm_QueryClear2; safecall;
    procedure sm_QueryExec2; safecall;
    procedure sm_QueryAdd2(const S: WideString); safecall;
    function  smUpdateEvenst(const OldStr: WideString): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  ICoClassAcridServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F512D3C1-7D7F-11D8-B0EB-BD9930084878}
// *********************************************************************//
  ICoClassAcridServerDisp = dispinterface
    ['{F512D3C1-7D7F-11D8-B0EB-BD9930084878}']
    procedure sm_DelTovar(ID_T: Integer); dispid 1;
    procedure sm_DelPostavshik(ID_P: Integer); dispid 7;
    procedure sm_DelNakladnaya(ID_N: Integer); dispid 8;
    procedure sm_DelSchet(ID_S: Integer); dispid 9;
    procedure sm_DelPay(ID_P: Integer); dispid 10;
    function  sm_AddTovar(ID_T: Integer; const Name: WideString; NDS: Smallint): Integer; dispid 12;
    function  sm_AddPostavshik(ID_P: Integer; const FIO: WideString; const Tel: WideString; 
                               const Addr: WideString): Integer; dispid 2;
    function  sm_AddNakladnaya(ID_N: Integer; ID_P: Integer; const Shifr: WideString; 
                               Data: TDateTime; NDS_Sum: Single; const predoplata: WideString; 
                               const otgruzka: WideString; ostatok: Single): Integer; dispid 3;
    function  sm_AddSchet(ID_S: Integer; ID_N: Integer; ID_T: Integer; Koi_vo: Smallint; 
                          NDS_Sum: Single): Integer; dispid 4;
    function  sm_AddPay(ID_P: Integer; ID_N: Integer; Data: TDateTime; NDS_Sum: Single): Integer; dispid 5;
    procedure sm_QueryClear; dispid 13;
    procedure sm_QueryExec; dispid 14;
    procedure sm_QueryAdd(const S: WideString); dispid 15;
    procedure sm_QueryClear2; dispid 17;
    procedure sm_QueryExec2; dispid 18;
    procedure sm_QueryAdd2(const S: WideString); dispid 19;
    function  smUpdateEvenst(const OldStr: WideString): WideString; dispid 16;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CoCoClassAcridServer provides a Create and CreateRemote method to          
// create instances of the default interface ICoClassAcridServer exposed by              
// the CoClass CoClassAcridServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCoClassAcridServer = class
    class function Create: ICoClassAcridServer;
    class function CreateRemote(const MachineName: string): ICoClassAcridServer;
  end;

implementation

uses ComObj;

class function CoCoClassAcridServer.Create: ICoClassAcridServer;
begin
  Result := CreateComObject(CLASS_CoClassAcridServer) as ICoClassAcridServer;
end;

class function CoCoClassAcridServer.CreateRemote(const MachineName: string): ICoClassAcridServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CoClassAcridServer) as ICoClassAcridServer;
end;

end.
