unit UntStoredProc;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, AcridServer_TLB, StdVcl, Provider, IBStoredProc, IBQuery, DB,
  IBCustomDataSet, IBTable, IBDatabase, IBEvents, Dialogs;

type
  TCoClassAcridServer = class(TRemoteDataModule, ICoClassAcridServer)
    IBDBaza: TIBDatabase;
    IBTransaction: TIBTransaction;
    IBTPostavshik: TIBTable;
    IBQuery: TIBQuery;
    IBSPAddPostavshik: TIBStoredProc;
    DSPTovar: TDataSetProvider;
    DSPPostavshik: TDataSetProvider;
    IBTNakladnaya: TIBTable;
    IBTPay: TIBTable;
    IBTSchet: TIBTable;
    IBTovar: TIBTable;
    IBSPAddTovar: TIBStoredProc;
    IBSPAddNakladnaya: TIBStoredProc;
    IBSPAddSchet: TIBStoredProc;
    IBSPAddPay: TIBStoredProc;
    IBSPDelSchet: TIBStoredProc;
    IBSPDelPostavshik: TIBStoredProc;
    IBSPDelTovar: TIBStoredProc;
    IBSPDelNakladnaya: TIBStoredProc;
    IBSPDelPay: TIBStoredProc;
    DSPNakladnaya: TDataSetProvider;
    DSPSchet: TDataSetProvider;
    DSPPay: TDataSetProvider;
    IBSPAddTovarOUTID: TIntegerField;
    IBSPAddPostavshikOUTID: TIntegerField;
    IBSPAddNakladnayaOUTID: TIntegerField;
    IBSPAddSchetOUTID: TIntegerField;
    IBSPAddPayOUTID: TIntegerField;
    DSPQuery: TDataSetProvider;
    IBEvents: TIBEvents;
    DSPQuery2: TDataSetProvider;
    IBQuery2: TIBQuery;
    IBTovarID_T: TIntegerField;
    IBTovarNAME: TIBStringField;
    IBTovarNDS: TSmallintField;
    IBTPostavshikID_P: TIntegerField;
    IBTPostavshikFIO: TIBStringField;
    IBTPostavshikTEL: TIBStringField;
    IBTPostavshikADDR: TIBStringField;
    IBTNakladnayaID_N: TIntegerField;
    IBTNakladnayaID_P: TIntegerField;
    IBTNakladnayaSHIFR: TIBStringField;
    IBTNakladnayaDATA: TDateField;
    IBTNakladnayaSUM_NDS: TIBBCDField;
    IBTNakladnayaPREDOPLATA: TIBStringField;
    IBTNakladnayaOTGRUZKA: TIBStringField;
    IBTSchetID_S: TIntegerField;
    IBTSchetID_N: TIntegerField;
    IBTSchetID_T: TIntegerField;
    IBTSchetKOL_VO: TIntegerField;
    IBTSchetNDS_SUM: TIBBCDField;
    IBTPayID_P: TIntegerField;
    IBTPayID_N: TIntegerField;
    IBTPayDATA: TDateField;
    IBTPayNDS_SUM: TIBBCDField;
    IBTNakladnayaOSTATOK: TIBBCDField;
    procedure RemoteDataModuleCreate(Sender: TObject);
    procedure RemoteDataModuleDestroy(Sender: TObject);
    procedure IBEventsEventAlert(Sender: TObject; EventName: String;
      EventCount: Integer; var CancelAlerts: Boolean);
  private
    { Private declarations }
    EventString : String;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    procedure sm_DelTovar(ID_T: Integer); safecall;
    procedure sm_DelPostavshik(ID_P: Integer); safecall;
    procedure sm_DelNakladnaya(ID_N: Integer); safecall;
    procedure sm_DelSchet(ID_S: Integer); safecall;
    procedure sm_DelPay(ID_P: Integer); safecall;
    function  sm_AddTovar(ID_T: Integer; const Name: WideString; NDS: Smallint): Integer; safecall;
    function  sm_AddPostavshik(ID_P: Integer; const FIO: WideString; const Tel: WideString; 
                               const Addr: WideString): Integer; safecall;
    function  sm_AddNakladnaya(ID_N: Integer; ID_P: Integer; const Shifr: WideString;
                               Data: TDateTime; NDS_Sum: Single; const predoplata: WideString;
                               const otgruzka: WideString; ostatok: Single): Integer; safecall;
    function  sm_AddSchet(ID_S: Integer; ID_N: Integer; ID_T: Integer; Koi_vo: Smallint;
                          NDS_Sum: Single): Integer; safecall;
    function  sm_AddPay(ID_P: Integer; ID_N: Integer; Data: TDateTime; NDS_Sum: Single): Integer; safecall;    procedure sm_QueryAdd(const S: WideString); safecall;
    procedure sm_QueryClear; safecall;
    procedure sm_QueryExec; safecall;
    procedure sm_QueryAdd2(const S: WideString); safecall;
    procedure sm_QueryClear2; safecall;
    procedure sm_QueryExec2; safecall;
    function  smUpdateEvenst(const OldStr: WideString): WideString; safecall;
  public
    { Public declarations }
  end;

implementation
uses UntFirst;

{$R *.DFM}

class procedure TCoClassAcridServer.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TCoClassAcridServer.sm_QueryAdd;
begin IBQuery.SQL.Add(s); end;

procedure TCoClassAcridServer.sm_QueryClear;
begin
  if IBQuery.Transaction.InTransaction then IBQuery.Transaction.Commit;
  IBQuery.SQL.Clear;
end;

procedure TCoClassAcridServer.sm_QueryExec;
begin
  if (pos('SELECT',IBQuery.SQL[0])>0) or (pos('select',IBQuery.SQL[0])>0) then
    IBQuery.Open
  else
    IBQuery.ExecSQL;
end;

procedure TCoClassAcridServer.sm_QueryAdd2;
begin IBQuery2.SQL.Add(s); end;

procedure TCoClassAcridServer.sm_QueryClear2;
begin
  if IBQuery2.Transaction.InTransaction then IBQuery2.Transaction.Commit;
  IBQuery2.SQL.Clear;
end;

procedure TCoClassAcridServer.sm_QueryExec2;
begin
  if (pos('SELECT',IBQuery2.SQL[0])>0) or (pos('select',IBQuery2.SQL[0])>0) then
    IBQuery2.Open
  else
    IBQuery2.ExecSQL;
end;

procedure TCoClassAcridServer.sm_DelTovar;
begin
  if IBSPDelTovar.Transaction.InTransaction then IBSPDelTovar.Transaction.Commit;
  IBSPDelTovar.Params[0].Value :=ID_T;
  IBSPDelTovar.ExecProc;
  if IBSPDelTovar.Transaction.InTransaction then IBSPDelTovar.Transaction.Commit;
end;

procedure TCoClassAcridServer.sm_DelPostavshik;
begin
  if IBSPDelPostavshik.Transaction.InTransaction then IBSPDelPostavshik.Transaction.Commit;
  IBSPDelPostavshik.Params[0].Value :=ID_P;
  IBSPDelPostavshik.ExecProc;
  if IBSPDelPostavshik.Transaction.InTransaction then IBSPDelPostavshik.Transaction.Commit;
end;

procedure TCoClassAcridServer.sm_DelNakladnaya;
begin
  if IBSPDelNakladnaya.Transaction.InTransaction then IBSPDelNakladnaya.Transaction.Commit;
  IBSPDelNakladnaya.Params[0].Value :=ID_N;
  IBSPDelNakladnaya.ExecProc;
  if IBSPDelNakladnaya.Transaction.InTransaction then IBSPDelNakladnaya.Transaction.Commit;
end;

procedure TCoClassAcridServer.sm_DelSchet;
begin
  if IBSPDelSchet.Transaction.InTransaction then IBSPDelSchet.Transaction.Commit;
  IBSPDelSchet.Params[0].Value :=ID_S;
  IBSPDelSchet.ExecProc;
  if IBSPDelSchet.Transaction.InTransaction then IBSPDelSchet.Transaction.Commit;
end;

procedure TCoClassAcridServer.sm_DelPay;
begin
  if IBSPDelPay.Transaction.InTransaction then IBSPDelPay.Transaction.Commit;
  IBSPDelPay.Params[0].Value :=ID_P;
  IBSPDelPay.ExecProc;
  if IBSPDelPay.Transaction.InTransaction then IBSPDelPay.Transaction.Commit;
end;

function TCoClassAcridServer.sm_AddTovar;
begin
 if IBSPAddTovar.Transaction.InTransaction then IBSPAddTovar.Transaction.Commit;
  IBSPAddTovar.Params[1].Value :=ID_T;
  IBSPAddTovar.Params[2].Value :=Name;
  IBSPAddTovar.Params[3].Value :=NDS;
  IBSPAddTovar.ExecProc;
  if IBSPAddTovar.Transaction.InTransaction then IBSPAddTovar.Transaction.Commit;
  Result := IBSPAddTovar.Params[0].Value;
end;

function TCoClassAcridServer.sm_AddPostavshik;
begin
 if IBSPAddPostavshik.Transaction.InTransaction then IBSPAddPostavshik.Transaction.Commit;
  IBSPAddPostavshik.Params[1].Value :=ID_P;
  IBSPAddPostavshik.Params[2].Value :=FIO;
  IBSPAddPostavshik.Params[3].Value :=Tel;
  IBSPAddPostavshik.Params[4].Value :=Addr;
  IBSPAddPostavshik.ExecProc;
  if IBSPAddPostavshik.Transaction.InTransaction then IBSPAddPostavshik.Transaction.Commit;
  Result := IBSPAddPostavshik.Params[0].Value;
end;

function TCoClassAcridServer.sm_AddNakladnaya;
begin
 if IBSPAddNakladnaya.Transaction.InTransaction then IBSPAddNakladnaya.Transaction.Commit;
  IBSPAddNakladnaya.Params[1].Value :=ID_N;
  IBSPAddNakladnaya.Params[2].Value :=ID_P;
  IBSPAddNakladnaya.Params[3].Value :=Shifr;
  IBSPAddNakladnaya.Params[4].Value :=Data;
  IBSPAddNakladnaya.Params[5].Value :=NDS_Sum;
  IBSPAddNakladnaya.Params[6].Value :=predoplata;
  IBSPAddNakladnaya.Params[7].Value :=otgruzka;
  IBSPAddNakladnaya.Params[8].Value :=ostatok;
  IBSPAddNakladnaya.ExecProc;
  if IBSPAddNakladnaya.Transaction.InTransaction then IBSPAddNakladnaya.Transaction.Commit;
  Result := IBSPAddNakladnaya.Params[0].Value;
end;

function TCoClassAcridServer.sm_AddSchet;
begin
 if IBSPAddSchet.Transaction.InTransaction then IBSPAddSchet.Transaction.Commit;
  IBSPAddSchet.Params[1].Value :=ID_S;
  IBSPAddSchet.Params[2].Value :=ID_N;
  IBSPAddSchet.Params[3].Value :=ID_T;
  IBSPAddSchet.Params[4].Value :=Koi_vo;
  IBSPAddSchet.Params[5].Value :=NDS_Sum;
  IBSPAddSchet.ExecProc;
  if IBSPAddSchet.Transaction.InTransaction then IBSPAddSchet.Transaction.Commit;
  Result := IBSPAddSchet.Params[0].Value;
end;

function TCoClassAcridServer.sm_AddPay;
begin
 if IBSPAddPay.Transaction.InTransaction then IBSPAddPay.Transaction.Commit;
  IBSPAddPay.Params[1].Value :=ID_P;
  IBSPAddPay.Params[2].Value :=ID_N;
  IBSPAddPay.Params[3].Value :=Data;
  IBSPAddPay.Params[4].Value :=NDS_Sum;
  IBSPAddPay.ExecProc;
  if IBSPAddPay.Transaction.InTransaction then IBSPAddPay.Transaction.Commit;
  Result := IBSPAddPay.Params[0].Value;
end;

procedure TCoClassAcridServer.RemoteDataModuleCreate(Sender: TObject);
begin
  EventString:='00000';
  inc(First.ClientCount);
  First.TrayIcon.Hint:='Подключено клиентов: '+IntToStr(First.ClientCount);
end;

procedure TCoClassAcridServer.RemoteDataModuleDestroy(Sender: TObject);
begin
  dec(First.ClientCount);
  First.TrayIcon.Hint:='Подключено клиентов: '+IntToStr(First.ClientCount);
end;

function  TCoClassAcridServer.smUpdateEvenst(const OldStr: WideString): WideString; safecall;
var
 i: Integer;
begin
 if OldStr='' then Result:= EventString
 else
  begin
   Result:=OldStr;
   for i:=1 to length(EventString) do
    if EventString[i]='1' then
     begin
      Result[i]:='1';
      EventString[i]:='0';
     end;
  end;
end;

procedure TCoClassAcridServer.IBEventsEventAlert(Sender: TObject;
  EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
begin
 if EventName='TOVAR' then EventString[1]:='1';
 if EventName='POSTAVSHIK' then EventString[2]:='1';
 if EventName='NAKLADNAYA' then EventString[3]:='1';
 if EventName='SCHET_FAKTURA' then EventString[4]:='1';
 if EventName='PAY' then EventString[5]:='1';
end;

initialization
  TComponentFactory.Create(ComServer, TCoClassAcridServer,
    Class_CoClassAcridServer, ciMultiInstance, tmApartment);
end.
