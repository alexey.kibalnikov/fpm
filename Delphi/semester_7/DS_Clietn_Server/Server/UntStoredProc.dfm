object CoClassAcridServer: TCoClassAcridServer
  OldCreateOrder = False
  OnCreate = RemoteDataModuleCreate
  OnDestroy = RemoteDataModuleDestroy
  Left = 204
  Top = 105
  Height = 303
  Width = 362
  object IBDBaza: TIBDatabase
    DatabaseName = 'D:\Bazads.gdb'
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey'
      'lc_ctype=WIN1251')
    LoginPrompt = False
    DefaultTransaction = IBTransaction
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 88
    Top = 8
  end
  object IBTransaction: TIBTransaction
    Active = False
    DefaultDatabase = IBDBaza
    AutoStopAction = saNone
    Left = 32
    Top = 24
  end
  object IBTPostavshik: TIBTable
    Tag = 2
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'ID_P'
        DataType = ftInteger
      end
      item
        Name = 'FIO'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TEL'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ADDR'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <
      item
        Name = 'RDB$PRIMARY2'
        Fields = 'ID_H'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'RDB$FOREIGN8'
        Fields = 'ID_S'
      end>
    StoreDefs = True
    TableName = 'POSTAVSHIK'
    Left = 88
    Top = 104
    object IBTPostavshikID_P: TIntegerField
      FieldName = 'ID_P'
    end
    object IBTPostavshikFIO: TIBStringField
      FieldName = 'FIO'
      Size = 30
    end
    object IBTPostavshikTEL: TIBStringField
      FieldName = 'TEL'
      Size = 30
    end
    object IBTPostavshikADDR: TIBStringField
      FieldName = 'ADDR'
      Size = 30
    end
  end
  object IBQuery: TIBQuery
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    Left = 304
    Top = 56
  end
  object IBSPAddPostavshik: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDPostavshik'
    Left = 88
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INFIO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INTEL'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INADDR'
        ParamType = ptInput
      end>
    object IBSPAddPostavshikOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDHISTORY.OUTID'
    end
  end
  object DSPTovar: TDataSetProvider
    Tag = 1
    DataSet = IBTovar
    Constraints = True
    Left = 32
    Top = 72
  end
  object DSPPostavshik: TDataSetProvider
    Tag = 2
    DataSet = IBTPostavshik
    Constraints = True
    Left = 88
    Top = 56
  end
  object IBTNakladnaya: TIBTable
    Tag = 3
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'NAKLADNAYA'
    Left = 144
    Top = 120
    object IBTNakladnayaID_N: TIntegerField
      FieldName = 'ID_N'
    end
    object IBTNakladnayaID_P: TIntegerField
      FieldName = 'ID_P'
    end
    object IBTNakladnayaSHIFR: TIBStringField
      FieldName = 'SHIFR'
      Size = 30
    end
    object IBTNakladnayaDATA: TDateField
      FieldName = 'DATA'
    end
    object IBTNakladnayaSUM_NDS: TIBBCDField
      FieldName = 'SUM_NDS'
      Precision = 18
      Size = 4
    end
    object IBTNakladnayaPREDOPLATA: TIBStringField
      FieldName = 'PREDOPLATA'
      Size = 1
    end
    object IBTNakladnayaOTGRUZKA: TIBStringField
      FieldName = 'OTGRUZKA'
      Size = 1
    end
    object IBTNakladnayaOSTATOK: TIBBCDField
      FieldName = 'OSTATOK'
      Precision = 18
      Size = 4
    end
  end
  object IBTPay: TIBTable
    Tag = 5
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'PAY'
    Left = 256
    Top = 120
    object IBTPayID_P: TIntegerField
      FieldName = 'ID_P'
    end
    object IBTPayID_N: TIntegerField
      FieldName = 'ID_N'
    end
    object IBTPayDATA: TDateField
      FieldName = 'DATA'
    end
    object IBTPayNDS_SUM: TIBBCDField
      FieldName = 'NDS_SUM'
      Precision = 18
      Size = 4
    end
  end
  object IBTSchet: TIBTable
    Tag = 4
    Database = IBDBaza
    Transaction = IBTransaction
    ObjectView = True
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'SCHET_FAKTURA'
    Left = 200
    Top = 104
    object IBTSchetID_S: TIntegerField
      FieldName = 'ID_S'
    end
    object IBTSchetID_N: TIntegerField
      FieldName = 'ID_N'
    end
    object IBTSchetID_T: TIntegerField
      FieldName = 'ID_T'
    end
    object IBTSchetKOL_VO: TIntegerField
      FieldName = 'KOL_VO'
    end
    object IBTSchetNDS_SUM: TIBBCDField
      FieldName = 'NDS_SUM'
      Precision = 18
      Size = 4
    end
  end
  object IBTovar: TIBTable
    Tag = 1
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'ID_T'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'NDS'
        DataType = ftSmallint
      end>
    StoreDefs = True
    TableName = 'TOVAR'
    Left = 32
    Top = 120
    object IBTovarID_T: TIntegerField
      FieldName = 'ID_T'
    end
    object IBTovarNAME: TIBStringField
      FieldName = 'NAME'
      Size = 30
    end
    object IBTovarNDS: TSmallintField
      FieldName = 'NDS'
    end
  end
  object IBSPAddTovar: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDTovar'
    Left = 32
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_T'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INNAME'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'INNDS'
        ParamType = ptInput
      end>
    object IBSPAddTovarOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDSTUDENT.OUTID'
    end
  end
  object IBSPAddNakladnaya: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDNakladnaya'
    Left = 144
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_N'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INSHIFR'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INDATA'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Name = 'INSUM_NDS'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INPREDOPLATA'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INOTGRUZKA'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Name = 'INOSTATOK'
        ParamType = ptInput
      end>
    object IBSPAddNakladnayaOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDLECTURE.OUTID'
    end
  end
  object IBSPAddSchet: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDSchet'
    Left = 200
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_S'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_N'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_T'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INKOL_VO'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Name = 'INNDS_SUM'
        ParamType = ptInput
      end>
    object IBSPAddSchetOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDSESSION.OUTID'
    end
  end
  object IBSPAddPay: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDPAY'
    Left = 256
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_N'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INDATA'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Name = 'INNDS_SUM'
        ParamType = ptInput
      end>
    object IBSPAddPayOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDPREDMET.OUTID'
    end
  end
  object IBSPDelSchet: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELSchet'
    Left = 200
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_S'
        ParamType = ptInput
      end>
  end
  object IBSPDelPostavshik: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELPostavshik'
    Left = 88
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end>
  end
  object IBSPDelTovar: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELTovar'
    Left = 32
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_T'
        ParamType = ptInput
      end>
  end
  object IBSPDelNakladnaya: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELNakladnaya'
    Left = 144
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_N'
        ParamType = ptInput
      end>
  end
  object IBSPDelPay: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELPay'
    Left = 256
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end>
  end
  object DSPNakladnaya: TDataSetProvider
    Tag = 3
    DataSet = IBTNakladnaya
    Constraints = True
    Left = 144
    Top = 72
  end
  object DSPSchet: TDataSetProvider
    Tag = 4
    DataSet = IBTSchet
    Constraints = True
    Left = 200
    Top = 56
  end
  object DSPPay: TDataSetProvider
    Tag = 5
    DataSet = IBTPay
    Constraints = True
    Left = 256
    Top = 72
  end
  object DSPQuery: TDataSetProvider
    DataSet = IBQuery
    Constraints = True
    Left = 304
    Top = 8
  end
  object IBEvents: TIBEvents
    AutoRegister = False
    Database = IBDBaza
    Events.Strings = (
      'NAKLADNAYA'
      'PAY'
      'POSTAVSHIK'
      'SCHET_FAKTURA'
      'TOVAR')
    Registered = False
    OnEventAlert = IBEventsEventAlert
    Left = 144
    Top = 16
  end
  object DSPQuery2: TDataSetProvider
    DataSet = IBQuery2
    Constraints = True
    Left = 304
    Top = 104
  end
  object IBQuery2: TIBQuery
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    Left = 304
    Top = 152
  end
end
