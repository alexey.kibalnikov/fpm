program AcridServer;

uses
  Forms,
  AcridServer_TLB in 'AcridServer_TLB.pas',
  UntFirst in 'UntFirst.pas' {First},
  UntStoredProc in 'UntStoredProc.pas' {CoClassAcridServer: TRemoteDataModule};

{$R *.TLB}

{$R *.res}

begin
  Application.Initialize;
  Application.ShowMainForm:=False; 
  Application.CreateForm(TFirst, First);
  Application.Run;
end.
