unit UntFirst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cbClasses, CaptionButton, FormTrayIcon, Menus,
  AppAutoRun, abfComponents, fcLabel;

type
  TFirst = class(TForm)
    TrayIcon: TFormTrayIcon;
    PMServer: TPopupMenu;
    mrClose: TMenuItem;
    mrRun: TMenuItem;
    AppAutoRun: TAppAutoRun;
    mrAdd: TMenuItem;
    mrDel: TMenuItem;
    BlendLabel1: TfcLabel;
    fcLabel1: TfcLabel;
    fcLabel2: TfcLabel;
    procedure FormCreate(Sender: TObject);
    procedure mrCloseClick(Sender: TObject);
    procedure TrayIconMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure abfShutdown1QueryShutdown(Sender: TObject;
      var CanShutdown: Boolean);
    procedure mrAddClick(Sender: TObject);
    procedure mrDelClick(Sender: TObject);
  private
    { Private declarations }
  public
     ClientCount: Integer;
    { Public declarations }
  end;

var
  First: TFirst;

implementation

uses UntStoredProc;

{$R *.dfm}

procedure TFirst.FormCreate(Sender: TObject);
begin ClientCount:=0;  end;

procedure TFirst.mrCloseClick(Sender: TObject);
begin Close; end;
procedure TFirst.TrayIconMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if ClientCount<>0 then First.mrClose.Visible:=False
   else First.mrClose.Visible:=True;
end;

procedure TFirst.abfShutdown1QueryShutdown(Sender: TObject;
  var CanShutdown: Boolean);
begin
 MessageDlg('Выключаете',mtWarning,[mbOk],0)
end;

procedure TFirst.mrAddClick(Sender: TObject);
begin
  if MessageDlg('Запускать при загрузке',mtInformation,[mbYes,mbNo],0)= mrYes then
  AppAutoRun.AutoRun:=True;
end;

procedure TFirst.mrDelClick(Sender: TObject);
begin
  if MessageDlg('Не запускать при загрузке',mtInformation,[mbYes,mbNo],0)= mrYes then
  AppAutoRun.AutoRun:=False;
end;

end.
