object fPostav: TfPostav
  Left = 192
  Top = 107
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1089#1087#1080#1089#1086#1082' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1086#1074
  ClientHeight = 334
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 256
    Width = 49
    Height = 24
    Caption = #1060#1048#1054':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 0
    Top = 304
    Width = 85
    Height = 24
    Caption = #1058#1077#1083#1077#1092#1086#1085':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 280
    Width = 97
    Height = 24
    Caption = #1070#1088'. '#1072#1076#1088#1077#1089':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DBGPostavshik: TDBGrid
    Left = 1
    Top = 32
    Width = 309
    Height = 172
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGPostavshikCellClick
  end
  object edtFIO: TEdit
    Left = 61
    Top = 256
    Width = 244
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 30
    ParentFont = False
    TabOrder = 1
    Text = 'edtFIO'
  end
  object Panel1: TPanel
    Left = 4
    Top = 211
    Width = 305
    Height = 41
    BevelInner = bvLowered
    TabOrder = 2
    object btnAddPredmet: TButton
      Left = 8
      Top = 8
      Width = 89
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btnAddPredmetClick
    end
    object btnUpPredmet: TButton
      Left = 104
      Top = 8
      Width = 89
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btnUpPredmetClick
    end
    object btnDelPredmet: TButton
      Left = 200
      Top = 8
      Width = 97
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnDelPredmetClick
    end
  end
  object btnMenu: TButton
    Left = 192
    Top = 305
    Width = 121
    Height = 25
    Caption = #1054#1089#1085#1086#1074#1085#1086#1077' '#1084#1077#1085#1102
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = btnMenuClick
  end
  object RGPostavshik: TRadioGroup
    Left = 1
    Top = -4
    Width = 305
    Height = 33
    Columns = 4
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      'ID'
      #1060#1048#1054
      #1058#1077#1083#1077#1092#1086#1085
      #1070#1088'. '#1040#1076#1088#1077#1089)
    ParentFont = False
    TabOrder = 4
    OnClick = RGPostavshikClick
  end
  object edtTel: TEdit
    Left = 88
    Top = 304
    Width = 97
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = 'edtTel'
  end
  object edtAddr: TEdit
    Left = 112
    Top = 280
    Width = 193
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = 'edtAddr'
  end
  object FormRoller1: TFormRoller
    CaptionButton.BtnOrder = 0
    CaptionButton.Cursor = crDefault
    CaptionButton.CursorDown = crDefault
    CaptionButton.Enabled = True
    CaptionButton.Hint = 'Roll up'
    CaptionButton.SeparatorWidth = 0
    CaptionButton.ShowHint = True
    CaptionButton.Visible = True
    SystemMenu.ApplyToMenu = False
    SystemMenu.Caption = 'Roll up'
    SystemMenu.Position = 7
    SystemMenu.Separators = [seBefore]
    Animate = True
    AnimateSpeed = 8
    HintRollUp = 'Roll up'
    HintRollDown = 'Roll down'
    RemainHeight = 0
    RollUp = False
    Left = 280
  end
end
