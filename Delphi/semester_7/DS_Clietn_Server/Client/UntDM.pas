unit UntDM;

interface

uses
  SysUtils, Classes, DB, DBClient, MConnect, Provider, IBDatabase, DBLocal,
  DBLocalI, DBTables;

type
  TdmClient = class(TDataModule)
    CDSTovar: TClientDataSet;
    CDSPostavshik: TClientDataSet;
    DCOMConnection: TDCOMConnection;
    CDSSchet: TClientDataSet;
    CDSNakladnaya: TClientDataSet;
    CDSPay: TClientDataSet;
    CDSQuery: TClientDataSet;
    DSTovar: TDataSource;
    DSPostavshik: TDataSource;
    DSNakladnaya: TDataSource;
    DSQuery: TDataSource;
    DSPay: TDataSource;
    DSSchet: TDataSource;
    CDSQuery2: TClientDataSet;
    DSQuery2: TDataSource;
    CDSTovarID_T: TIntegerField;
    CDSTovarNAME: TStringField;
    CDSTovarNDS: TSmallintField;
    CDSPostavshikID_P: TIntegerField;
    CDSPostavshikFIO: TStringField;
    CDSPostavshikTEL: TStringField;
    CDSPostavshikADDR: TStringField;
    CDSNakladnayaID_N: TIntegerField;
    CDSNakladnayaID_P: TIntegerField;
    CDSNakladnayaSHIFR: TStringField;
    CDSNakladnayaDATA: TDateField;
    CDSNakladnayaSUM_NDS: TBCDField;
    CDSNakladnayaPREDOPLATA: TStringField;
    CDSNakladnayaOTGRUZKA: TStringField;
    CDSSchetID_S: TIntegerField;
    CDSSchetID_N: TIntegerField;
    CDSSchetID_T: TIntegerField;
    CDSSchetKOL_VO: TIntegerField;
    CDSSchetNDS_SUM: TBCDField;
    CDSPayID_P: TIntegerField;
    CDSPayID_N: TIntegerField;
    CDSPayDATA: TDateField;
    CDSPayNDS_SUM: TBCDField;
    CDSNakladnayaOSTATOK: TBCDField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    EventString : String;
    procedure GetEventString;
    { Public declarations }
  end;

var
  dmClient: TdmClient;

implementation

uses UntFirst;

{$R *.dfm}
procedure TdmClient.GetEventString;
begin
 EventString:=DCOMConnection.AppServer.sm_UpdateEvents(EventString);
end;

procedure TdmClient.DataModuleCreate(Sender: TObject);
begin
   DCOMConnection.Connected:=True;
   EventString:='';
   CDSTovar.Open;
   CDSPostavshik.Open;
   CDSSchet.Open;
   CDSNakladnaya.Open;
   CDSPay.Open;
   DecimalSeparator:='.';
//   CDSTeacher.AddIndex('IxF_Tch','DeF;ID_T',[],'','',0);
end;

procedure TdmClient.DataModuleDestroy(Sender: TObject);
begin
   CDSTovar.Close;
   CDSPostavshik.Close;
   CDSSchet.Close;
   CDSNakladnaya.Close;
   CDSPay.Close;
   dmClient.DCOMConnection.Connected:=False;
end;

end.
