unit UntTovar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, ExtCtrls, cbClasses, CaptionButton, FormRoller;

type
  TfTovar = class(TForm)
    DBGTovar: TDBGrid;
    edtName: TEdit;
    Panel1: TPanel;
    FormRoller1: TFormRoller;
    Label1: TLabel;
    btnAddPredmet: TButton;
    btnUpPredmet: TButton;
    btnDelPredmet: TButton;
    btnMenu: TButton;
    RGTovar: TRadioGroup;
    RGNds: TRadioGroup;
    procedure btnAddPredmetClick(Sender: TObject);
    procedure btnUpPredmetClick(Sender: TObject);
    procedure btnDelPredmetClick(Sender: TObject);
    procedure btnMenuClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBGTovarCellClick(Column: TColumn);
    procedure RGTovarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fTovar: TfTovar;

implementation

uses UntDM, UntFirst;

{$R *.dfm}

procedure TfTovar.btnAddPredmetClick(Sender: TObject);
 var NDS : byte;
begin
 Case RGNds.ItemIndex of
  0: NDS:=20;
  1: NDS:=10;
  2: NDS:=0;
 end;
 if dmClient.DCOMConnection.AppServer.sm_AddTovar(0,edtName.Text,NDS) > 0
  then dmClient.CDSTovar.Refresh;
end;

procedure TfTovar.btnUpPredmetClick(Sender: TObject);
 var NDS : byte;
begin
 if MessageDlg('’отите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
    Case RGNds.ItemIndex of
     0: NDS:=20;
     1: NDS:=10;
     2: NDS:=0;
    end;
   if dmClient.DCOMConnection.AppServer.sm_AddTovar(dmClient.CDSTovarID_T.Value,edtName.Text, NDS) > 0
     then dmClient.CDSTovar.Refresh;
  end;
end;

procedure TfTovar.btnDelPredmetClick(Sender: TObject);
begin
 if MessageDlg('’отите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmClient.DCOMConnection.AppServer.sm_DelTovar(dmClient.CDSTovarID_T.Value);
   dmClient.CDSTovar.Refresh;
  end;
end;

procedure TfTovar.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fTovar.Visible:=False;
  fTovar.DBGTovar.DataSource:= nil;
end;

procedure TfTovar.FormActivate(Sender: TObject);
begin
 edtName.Text:=dmClient.CDSTovarNAME.Value;
end;

procedure TfTovar.DBGTovarCellClick(Column: TColumn);
begin
 edtName.Text:=dmClient.CDSTovarNAME.Value;
 Case dmClient.CDSTovarNDS.Value of
   0 : RGNds.ItemIndex:=0;
   10: RGNds.ItemIndex:=1;
   20: RGNds.ItemIndex:=2;
 end;
end;

procedure TfTovar.RGTovarClick(Sender: TObject);
begin
 Case RGTovar.ItemIndex of
   0: dmClient.CDSTovar.IndexFieldNames := 'ID_T';
   1: dmClient.CDSTovar.IndexFieldNames := 'Name;ID_T';
   2: dmClient.CDSTovar.IndexFieldNames := 'NDS;ID_T';
  end;
end;

end.
