unit UntFirst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, Grids, DBGrids, ComCtrls, ExtCtrls, Buttons, Mask,
  fcButton, fcImgBtn, fcClearPanel, fcButtonGroup, cbClasses,
  CaptionButton, FormRoller, FormTrayIcon, fcLabel;

type
  TFirst = class(TForm)
    pnlSlovar: TPanel;
    Label1: TLabel;
    FormRoller1: TFormRoller;
    FormTrayIcon: TFormTrayIcon;
    btnTovar: TButton;
    btnPostav: TButton;
    btnAll: TButton;
    procedure btnTovarClick(Sender: TObject);
    procedure btnPostavClick(Sender: TObject);
    procedure btnAllClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  First: TFirst;
  Password : string='371';
implementation

uses UntDM, UntPostav, UntTovar, UntAll;

{$R *.dfm}

procedure TFirst.btnTovarClick(Sender: TObject);
begin
  fTovar.DBGTovar.DataSource:=dmClient.DSTovar;
  fTovar.Visible:=True;
  First.Visible:=false;
end;

procedure TFirst.btnPostavClick(Sender: TObject);
begin
  fPostav.DBGPostavshik.DataSource:=dmClient.DSPostavshik;
  fPostav.Visible:=True;
  First.Visible:=false;
end;

procedure TFirst.btnAllClick(Sender: TObject);
begin
  fAll.DBGPostavshik.DataSource:=dmClient.DSPostavshik;
  fAll.DBGNakladn.DataSource:=dmClient.DSNakladnaya;
  fAll.DBGQuery.DataSource:=dmClient.DSQuery;
  fAll.DBGTovar.DataSource:=dmClient.DSTovar;
  fAll.DBGQuery2.DataSource:=dmClient.DSQuery;
  fAll.DBGA.DataSource:=dmClient.DSQuery2;
  fAll.DBGB.DataSource:=dmClient.DSQuery2;
  fAll.PageControl.ActivePageIndex:=0;
  fAll.Visible:=True;
  First.Visible:=false;
end;

end.
