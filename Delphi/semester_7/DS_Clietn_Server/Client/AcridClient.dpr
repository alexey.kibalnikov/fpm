program AcridClient;

uses
  Forms,
  UntFirst in 'UntFirst.pas' {First},
  UntDM in 'UntDM.pas' {dmClient: TDataModule},
  UntTovar in 'UntTovar.pas' {fTovar},
  UntPostav in 'UntPostav.pas' {fPostav},
  UntAll in 'UntAll.pas' {fAll};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFirst, First);
  Application.CreateForm(TdmClient, dmClient);
  Application.CreateForm(TfPostav, fPostav);
  Application.CreateForm(TfTovar, fTovar);
  Application.CreateForm(TfAll, fAll);
  Application.Run;
end.
