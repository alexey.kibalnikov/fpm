unit UntAll;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cbClasses, CaptionButton, FormRoller, StdCtrls,
  ExtCtrls, Grids, DBGrids, DBCtrls, CheckLst,   DBClient;

type
  TfAll = class(TForm)
    btnMenu: TButton;
    FormRoller1: TFormRoller;
    pgNacladn: TPageControl;
    tabNakladn: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBGNakladn: TDBGrid;
    edtShifr: TEdit;
    Panel1: TPanel;
    btnAddNakladn: TButton;
    btnUpNakladn: TButton;
    btnDelNakladn: TButton;
    RGNakladn: TRadioGroup;
    DTPData: TDateTimePicker;
    RGOtgruzka: TRadioGroup;
    edtSum: TEdit;
    DBGPostavshik: TDBGrid;
    PageControl: TPageControl;
    tabSchet: TTabSheet;
    tabPay: TTabSheet;
    DBGQuery: TDBGrid;
    DBGTovar: TDBGrid;
    Panel2: TPanel;
    btnAddSchet: TButton;
    btnUpSchet: TButton;
    btnDelSchet: TButton;
    Label5: TLabel;
    edtKol_vo: TEdit;
    Label6: TLabel;
    edtNDS_Sum: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    DBGQuery2: TDBGrid;
    Panel3: TPanel;
    btnAddPay: TButton;
    btnUpPay: TButton;
    btnDelPay: TButton;
    Label9: TLabel;
    Label11: TLabel;
    edtPay: TEdit;
    Label12: TLabel;
    DTPDataPay: TDateTimePicker;
    Label10: TLabel;
    edtOstatok: TEdit;
    tabA: TTabSheet;
    tabB: TTabSheet;
    CLBPostavshik: TCheckListBox;
    CLBid: TCheckListBox;
    Label13: TLabel;
    DTPA1: TDateTimePicker;
    DTPA2: TDateTimePicker;
    DBGA: TDBGrid;
    btnA: TButton;
    Label14: TLabel;
    DBGB: TDBGrid;
    DTPB2: TDateTimePicker;
    DTPB1: TDateTimePicker;
    btnB: TButton;
    btnUpDate: TButton;
    procedure RGNakladnClick(Sender: TObject);
    procedure btnMenuClick(Sender: TObject);
    procedure btnAddNakladnClick(Sender: TObject);
    procedure btnUpNakladnClick(Sender: TObject);
    procedure btnDelNakladnClick(Sender: TObject);
    procedure DBGNakladnCellClick(Column: TColumn);
    procedure btnAddSchetClick(Sender: TObject);
    procedure DBGQueryCellClick(Column: TColumn);
    procedure btnUpSchetClick(Sender: TObject);
    procedure btnDelSchetClick(Sender: TObject);
    procedure btnAddPayClick(Sender: TObject);
    procedure btnUpPayClick(Sender: TObject);
    procedure btnDelPayClick(Sender: TObject);
    procedure DBGQuery2CellClick(Column: TColumn);
    procedure PageControlChange(Sender: TObject);
    procedure btnAClick(Sender: TObject);
    procedure btnBClick(Sender: TObject);
    procedure btnUpDateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fAll: TfAll;

implementation

uses UntDM, UntFirst, UntPostav, DB;

{$R *.dfm}

procedure TfAll.RGNakladnClick(Sender: TObject);
begin
 case RGNakladn.ItemIndex of
   0: dmClient.CDSNakladnaya.IndexFieldNames := 'ID_N';
   1: dmClient.CDSNakladnaya.IndexFieldNames := 'Shifr;ID_N';
   2: dmClient.CDSNakladnaya.IndexFieldNames := 'data;ID_N';
   3: dmClient.CDSNakladnaya.IndexFieldNames := 'Sum_NDS;ID_N';
   4: dmClient.CDSNakladnaya.IndexFieldNames := 'Predoplata;ID_N';
   5: dmClient.CDSNakladnaya.IndexFieldNames := 'Otgruzka;ID_N';
 end;
end;

procedure TfAll.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fAll.Visible:=False;
  fAll.DBGPostavshik.DataSource:= nil;
  fAll.DBGNakladn.DataSource:=nil;
  fAll.DBGQuery.DataSource:=nil;
  fAll.DBGTovar.DataSource:=nil;
  fAll.DBGQuery2.DataSource:=nil;
  fAll.DBGA.DataSource:=nil;
  fAll.DBGB.DataSource:=nil;
end;

procedure TfAll.btnAddNakladnClick(Sender: TObject);
 var Otgruzka : char;
begin
 if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
 if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(0,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,0,'F',Otgruzka,0) > 0
  then dmClient.CDSNakladnaya.Refresh;
end;

procedure TfAll.btnUpNakladnClick(Sender: TObject);
 var Otgruzka : char;
begin
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
   if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),dmClient.CDSNakladnayaPREDOPLATA.Value,Otgruzka,dmClient.CDSNakladnayaOSTATOK.Value) > 0
    then dmClient.CDSNakladnaya.Refresh;
  end;
end;

procedure TfAll.btnDelNakladnClick(Sender: TObject);
begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmClient.DCOMConnection.AppServer.sm_DelNakladnaya(dmClient.CDSNakladnayaID_N.Value);
   dmClient.CDSNakladnaya.Refresh;
  end;
end;

procedure TfAll.DBGNakladnCellClick(Column: TColumn);
 var StrSQL : string;
     Sum : Currency;
begin
 edtShifr.Text:=dmClient.CDSNakladnayaSHIFR.Value;
 edtSum.Text:=CurrToStr(dmClient.CDSNakladnayaSUM_NDS.Value);
 DTPData.Date:=dmClient.CDSNakladnayaDATA.Value;
 if dmClient.CDSNakladnayaOTGRUZKA.Value='T' then RGOtgruzka.ItemIndex:=0 else RGOtgruzka.ItemIndex:=1;

 if not dmClient.CDSNakladnaya.Eof then
  begin
  if PageControl.TabIndex=0 then
   begin
    dmClient.CDSPostavshik.First;
    while dmClient.CDSPostavshikID_P.Value<>dmClient.CDSNakladnayaID_P.Value do dmClient.CDSPostavshik.Next;

    StrSQL:='SELECT T.ID_T,T.Name AS "Название",S.Kol_vo AS "Кол-во",S.NDS_Sum AS "Cумма (с НДС)",S.ID_S '
           +'FROM schet_faktura S INNER JOIN Nakladnaya N ON S.ID_N=N.ID_N INNER JOIN Tovar T ON S.ID_T=T.ID_T '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY T.Name;';
    dmClient.DSQuery.DataSet := nil;
    dmClient.CDSQuery.Close;
    dmClient.DCOMConnection.AppServer.sm_QueryClear;
    dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
    dmClient.DCOMConnection.AppServer.sm_QueryExec;
    dmClient.CDSQuery.Open;
    dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
    DBGQuery.Columns[0].Visible:=False;
    DBGQuery.Columns[4].Visible:=False;
   end // tabSchet
    else begin
     StrSQL:='SELECT P.ID_P,P.Data AS "Дата",P.NDS_Sum AS "Оплата" '
           +'FROM Nakladnaya N INNER JOIN Pay P ON N.ID_N=P.ID_N '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY P.Data;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery2.Columns[0].Visible:=False;

     dmClient.CDSQuery.First; Sum:=0;
     while not dmClient.CDSQuery.Eof do
      begin
       Sum:=Sum+dmClient.CDSQuery.Fields[2].Value;
       dmClient.CDSQuery.Next;
      end;
     dmClient.CDSQuery.First;
     edtOstatok.Text:=CurrToStr(Sum);
    end;// tabPay
  end;
end;

procedure TfAll.btnAddSchetClick(Sender: TObject);
 var StrSQL : string;
     Sum : Currency;
     Otgruzka : char;
begin
 if dmClient.DCOMConnection.AppServer.sm_AddSchet(0,dmClient.CDSNakladnayaID_N.Value,dmClient.CDSTovarID_T.Value,StrToInt(edtKol_vo.Text),StrToFloat(edtNDS_Sum.Text)) > 0
  then begin dmClient.CDSSchet.Refresh;

     StrSQL:='SELECT T.ID_T,T.Name AS "Название",S.Kol_vo AS "Кол-во",S.NDS_Sum AS "Cумма (с НДС)",S.ID_S '
           +'FROM schet_faktura S INNER JOIN Nakladnaya N ON S.ID_N=N.ID_N INNER JOIN Tovar T ON S.ID_T=T.ID_T '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY T.Name;';
   dmClient.DSQuery.DataSet := nil;
   dmClient.CDSQuery.Close;
   dmClient.DCOMConnection.AppServer.sm_QueryClear;
   dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
   dmClient.DCOMConnection.AppServer.sm_QueryExec;
   dmClient.CDSQuery.Open;
   dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
   DBGQuery.Columns[0].Visible:=False;
   DBGQuery.Columns[4].Visible:=False;

   dmClient.CDSQuery.First; Sum:=0;
   while not dmClient.CDSQuery.Eof do
    begin
     Sum:=Sum+dmClient.CDSQuery.Fields[3].Value;
     dmClient.CDSQuery.Next;
    end;
   dmClient.CDSQuery.First;
   edtSum.Text:=CurrToStr(Sum);

   if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
   if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),dmClient.CDSNakladnayaPREDOPLATA.Value,Otgruzka,dmClient.CDSNakladnayaOSTATOK.Value) > 0
    then dmClient.CDSNakladnaya.Refresh;

  end;
end;

procedure TfAll.DBGQueryCellClick(Column: TColumn);
begin
 if dmClient.CDSQuery.Fields[0].Value>0 then
  begin
   dmClient.CDSTovar.First;
   while dmClient.CDSTovarID_T.Value<> dmClient.CDSQuery.Fields[0].Value do dmClient.CDSTovar.Next;
   edtKol_vo.Text:=IntToStr(dmClient.CDSQuery.Fields[2].Value);
   edtNDS_Sum.Text:=FloatToStr(dmClient.CDSQuery.Fields[3].Value);
  end;
end;

procedure TfAll.btnUpSchetClick(Sender: TObject);
 var StrSQL : string;
     Sum : Currency;
     Otgruzka : char;
begin
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmClient.DCOMConnection.AppServer.sm_AddSchet(dmClient.CDSQuery.Fields[4].Value,dmClient.CDSNakladnayaID_N.Value,dmClient.CDSTovarID_T.Value,StrToInt(edtKol_vo.Text),StrToFloat(edtNDS_Sum.Text)) > 0
    then begin dmClient.CDSSchet.Refresh;

     StrSQL:='SELECT T.ID_T,T.Name AS "Название",S.Kol_vo AS "Кол-во",S.NDS_Sum AS "Cумма (с НДС)",S.ID_S '
           +'FROM schet_faktura S INNER JOIN Nakladnaya N ON S.ID_N=N.ID_N INNER JOIN Tovar T ON S.ID_T=T.ID_T '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY T.Name;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery.Columns[0].Visible:=False;
     DBGQuery.Columns[4].Visible:=False;

     dmClient.CDSQuery.First; Sum:=0;
     while not dmClient.CDSQuery.Eof do
      begin
       Sum:=Sum+dmClient.CDSQuery.Fields[3].Value;
       dmClient.CDSQuery.Next;
      end;
     dmClient.CDSQuery.First;
     edtSum.Text:=CurrToStr(Sum);

     if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
     if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),dmClient.CDSNakladnayaPREDOPLATA.Value,Otgruzka,dmClient.CDSNakladnayaOSTATOK.Value) > 0
      then dmClient.CDSNakladnaya.Refresh;
  end;
 end;
end;

procedure TfAll.btnDelSchetClick(Sender: TObject);
 var StrSQL : string;
     Sum : Currency;
     Otgruzka : char;
begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmClient.DCOMConnection.AppServer.sm_DelSchet(dmClient.CDSQuery.Fields[4].Value);
   dmClient.CDSSchet.Refresh;

     StrSQL:='SELECT T.ID_T,T.Name AS "Название",S.Kol_vo AS "Кол-во",S.NDS_Sum AS "Cумма (с НДС)",S.ID_S '
           +'FROM schet_faktura S INNER JOIN Nakladnaya N ON S.ID_N=N.ID_N INNER JOIN Tovar T ON S.ID_T=T.ID_T '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY T.Name;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery.Columns[0].Visible:=False;
     DBGQuery.Columns[4].Visible:=False;

     dmClient.CDSQuery.First; Sum:=0;
     while not dmClient.CDSQuery.Eof do
      begin
       Sum:=Sum+dmClient.CDSQuery.Fields[3].Value;
       dmClient.CDSQuery.Next;
      end;
     dmClient.CDSQuery.First;
     edtSum.Text:=CurrToStr(Sum);

     if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
     if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),dmClient.CDSNakladnayaPREDOPLATA.Value,Otgruzka,dmClient.CDSNakladnayaOSTATOK.Value) > 0
      then dmClient.CDSNakladnaya.Refresh;
  end;
end;

procedure TfAll.btnAddPayClick(Sender: TObject);
 var StrSQL : string;
 Sum : Currency;
 Otgruzka,Predoplata : char;
begin
 if dmClient.DCOMConnection.AppServer.sm_AddPay(0,dmClient.CDSNakladnayaID_N.Value,DTPDataPay.Date,StrToCurr(edtPay.Text)) > 0
  then begin
     dmClient.CDSPay.Refresh;
        StrSQL:='SELECT P.ID_P,P.Data AS "Дата",P.NDS_Sum AS "Оплата" '
           +'FROM Nakladnaya N INNER JOIN Pay P ON N.ID_N=P.ID_N '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY P.Data;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery2.Columns[0].Visible:=False;

   dmClient.CDSQuery.First; Sum:=0;
   while not dmClient.CDSQuery.Eof do
    begin
     Sum:=Sum+dmClient.CDSQuery.Fields[2].Value;
     dmClient.CDSQuery.Next;
    end;
   dmClient.CDSQuery.First;
   edtOstatok.Text:=CurrToStr(Sum);

   if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
   if DTPDataPay.Date<dmClient.CDSNakladnayaDATA.Value then Predoplata:='T' else Predoplata:='F';
   if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),Predoplata,Otgruzka,Sum) > 0
    then dmClient.CDSNakladnaya.Refresh;
  end;
end;

procedure TfAll.btnUpPayClick(Sender: TObject);
 var StrSQL : string;
     Sum : Currency;
     Otgruzka,Predoplata : char;
begin
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmClient.DCOMConnection.AppServer.sm_AddPay(dmClient.CDSQuery.Fields[0].Value,dmClient.CDSNakladnayaID_N.Value,DTPDataPay.Date,StrToCurr(edtPay.Text)) > 0
    then begin
     dmClient.CDSPay.Refresh;
     StrSQL:='SELECT P.ID_P,P.Data AS "Дата",P.NDS_Sum AS "Оплата" '
           +'FROM Nakladnaya N INNER JOIN Pay P ON N.ID_N=P.ID_N '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY P.Data;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery2.Columns[0].Visible:=False;

   dmClient.CDSQuery.First; Sum:=0;
   while not dmClient.CDSQuery.Eof do
    begin
     Sum:=Sum+dmClient.CDSQuery.Fields[2].Value;
     dmClient.CDSQuery.Next;
    end;
   dmClient.CDSQuery.First;
   edtOstatok.Text:=CurrToStr(Sum);

   if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
   if DTPDataPay.Date<dmClient.CDSNakladnayaDATA.Value then Predoplata:='T' else Predoplata:='F';
   if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),Predoplata,Otgruzka,Sum) > 0
    then dmClient.CDSNakladnaya.Refresh;
   end;
  end;
end;

procedure TfAll.btnDelPayClick(Sender: TObject);
 var StrSQL : string;
     Sum : Currency;
     Otgruzka,Predoplata : char;
begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmClient.DCOMConnection.AppServer.sm_DelPay(dmClient.CDSQuery.Fields[0].Value);
   dmClient.CDSPay.Refresh;
     dmClient.CDSPay.Refresh;
     StrSQL:='SELECT P.ID_P,P.Data AS "Дата",P.NDS_Sum AS "Оплата" '
           +'FROM Nakladnaya N INNER JOIN Pay P ON N.ID_N=P.ID_N '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY P.Data;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery2.Columns[0].Visible:=False;

   dmClient.CDSQuery.First; Sum:=0;
   while not dmClient.CDSQuery.Eof do
    begin
     Sum:=Sum+dmClient.CDSQuery.Fields[2].Value;
     dmClient.CDSQuery.Next;
    end;
   dmClient.CDSQuery.First;
   edtOstatok.Text:=CurrToStr(Sum);

   if RGOtgruzka.ItemIndex=0 then Otgruzka:='T' else Otgruzka:='F';
   if DTPDataPay.Date<dmClient.CDSNakladnayaDATA.Value then Predoplata:='T' else Predoplata:='F';
   if dmClient.DCOMConnection.AppServer.sm_AddNakladnaya(dmClient.CDSNakladnayaID_N.Value,dmClient.CDSPostavshikID_P.Value,edtShifr.Text,DTPData.Date,StrToCurr(edtSum.Text),Predoplata,Otgruzka,Sum) > 0
    then dmClient.CDSNakladnaya.Refresh;
  end;
end;

procedure TfAll.DBGQuery2CellClick(Column: TColumn);
begin
  edtPay.Text:=CurrToStr(dmClient.CDSQuery.Fields[2].Value);
  DTPDataPay.Date:=dmClient.CDSQuery.Fields[1].Value;
end;

procedure TfAll.PageControlChange(Sender: TObject);
 var StrSQL : string;
     Sum : Currency;
begin
 if not dmClient.CDSNakladnaya.Eof then
  begin
  if PageControl.TabIndex=0 then
   begin
    dmClient.CDSPostavshik.First;
    while dmClient.CDSPostavshikID_P.Value<>dmClient.CDSNakladnayaID_P.Value do dmClient.CDSPostavshik.Next;

    StrSQL:='SELECT T.ID_T,T.Name AS "Название",S.Kol_vo AS "Кол-во",S.NDS_Sum AS "Cумма (с НДС)",S.ID_S '
           +'FROM schet_faktura S INNER JOIN Nakladnaya N ON S.ID_N=N.ID_N INNER JOIN Tovar T ON S.ID_T=T.ID_T '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY T.Name;';
    dmClient.DSQuery.DataSet := nil;
    dmClient.CDSQuery.Close;
    dmClient.DCOMConnection.AppServer.sm_QueryClear;
    dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
    dmClient.DCOMConnection.AppServer.sm_QueryExec;
    dmClient.CDSQuery.Open;
    dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
    DBGQuery.Columns[0].Visible:=False;
    DBGQuery.Columns[4].Visible:=False;
   end; // tabSchet
  if PageControl.TabIndex=1 then
    begin
     StrSQL:='SELECT P.ID_P,P.Data AS "Дата",P.NDS_Sum AS "Оплата" '
           +'FROM Nakladnaya N INNER JOIN Pay P ON N.ID_N=P.ID_N '
           +'WHERE N.ID_N='+IntToStr(dmClient.CDSNakladnayaID_N.Value)+' '
           +'ORDER BY P.Data;';
     dmClient.DSQuery.DataSet := nil;
     dmClient.CDSQuery.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec;
     dmClient.CDSQuery.Open;
     dmClient.DSQuery.DataSet :=dmClient.CDSQuery;
     DBGQuery2.Columns[0].Visible:=False;

     dmClient.CDSQuery.First; Sum:=0;
     while not dmClient.CDSQuery.Eof do
      begin
       Sum:=Sum+dmClient.CDSQuery.Fields[2].Value;
       dmClient.CDSQuery.Next;
      end;
     dmClient.CDSQuery.First;
     edtOstatok.Text:=CurrToStr(Sum);
    end;// tabPay
  if PageControl.TabIndex=2 then
   begin
    dmClient.CDSPostavshik.First;
    CLBPostavshik.Clear;
    CLBid.Clear;
    while not dmClient.CDSPostavshik.Eof do
     begin
      CLBPostavshik.Items.Add(dmClient.CDSPostavshikFIO.Value);
      CLBid.Items.Add(IntToStr(dmClient.CDSPostavshikID_P.Value));
      dmClient.CDSPostavshik.Next;
     end;
   end; // tabA
  if PageControl.TabIndex=3 then
   begin

   end; // tabB
  end;
end;

procedure TfAll.btnAClick(Sender: TObject);
 var StrSQL,ID : string;
     i: integer;
begin
   ID:='-1,';
   for i:=0 to CLBPostavshik.Count-1 do
    begin
     if CLBPostavshik.Checked[i]
       then ID:=ID+CLBid.Items.Strings[i]+',';
    end;
   for i:=1 to length(ID)-1 do StrSQL:=StrSQL+ID[i];
   ID:=StrSQL;
   StrSQL:='SELECT N.Shifr AS "Шифр",P.FIO AS "ФИО",N.Data "Дата",N.Sum_NDS AS "Сумма (с НДС)" '
           +'FROM Nakladnaya N INNER JOIN Postavshik P ON N.ID_P=P.ID_P '
           +'WHERE N.ID_P in ('+ID+') '
           +'AND N.Data>='''+DateToStr(DTPA1.Date)+''' AND N.Data<='''+DateToStr(DTPA2.Date)+''' '
           +'AND N.Sum_NDS<=N.Ostatok '
           +'AND N.Otgruzka=''F'' '
           +'ORDER BY N.Data;';
     dmClient.DSQuery2.DataSet := nil;
     dmClient.CDSQuery2.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear2;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd2(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec2;
     dmClient.CDSQuery2.Open;
     dmClient.DSQuery2.DataSet :=dmClient.CDSQuery2;
end;

procedure TfAll.btnBClick(Sender: TObject);
 var StrSQL : string;
begin
   StrSQL:='SELECT N.Shifr AS "Шифр",T.Name AS "Товар",N.Data "Дата",S.Kol_vo AS "Кол-во",S.NDS_Sum AS "Цена (с НДС)" '
           +'FROM Nakladnaya N INNER JOIN Schet_faktura S ON N.ID_N=S.ID_N INNER JOIN Tovar T ON S.ID_T=T.ID_T '
           +'WHERE N.Data>='''+DateToStr(DTPB1.Date)+''' AND N.Data<='''+DateToStr(DTPB2.Date)+''' '
           +'AND N.Otgruzka=''T'' '
           +'AND N.Predoplata=''T'' '
           +'ORDER BY N.Data;';
     dmClient.DSQuery2.DataSet := nil;
     dmClient.CDSQuery2.Close;
     dmClient.DCOMConnection.AppServer.sm_QueryClear2;
     dmClient.DCOMConnection.AppServer.sm_QueryAdd2(StrSQL);
     dmClient.DCOMConnection.AppServer.sm_QueryExec2;
     dmClient.CDSQuery2.Open;
     dmClient.DSQuery2.DataSet :=dmClient.CDSQuery2;
end;

procedure TfAll.btnUpDateClick(Sender: TObject);
 var i : Integer;
begin
 dmClient.GetEventString;
 MessageDlg(dmClient.EventString,mtInformation,[mbOk],0);
 for i:=0 to dmClient.ComponentCount-1 do
  begin
   if (dmClient.Components[i] is TClientDataSet) and
    ((dmClient.Components[i] as TClientDataSet).Tag>0) then
    if dmClient.EventString[(dmClient.Components[i] as TClientDataSet).Tag]='1' then
     begin
      (dmClient.Components[i] as TClientDataSet).Close;
      (dmClient.Components[i] as TClientDataSet).Open;
      dmClient.EventString[(dmClient.Components[i] as TClientDataSet).Tag]:='0';
     end;
  end;
end;

end.
