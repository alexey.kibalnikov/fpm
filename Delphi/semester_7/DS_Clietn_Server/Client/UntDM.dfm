object dmClient: TdmClient
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 217
  Top = 141
  Height = 189
  Width = 291
  object CDSTovar: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPTovar'
    RemoteServer = DCOMConnection
    Left = 24
    Top = 64
    object CDSTovarID_T: TIntegerField
      FieldName = 'ID_T'
      Visible = False
    end
    object CDSTovarNAME: TStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077
      FieldName = 'NAME'
      Size = 30
    end
    object CDSTovarNDS: TSmallintField
      DisplayLabel = #1053#1044#1057
      DisplayWidth = 3
      FieldName = 'NDS'
    end
  end
  object CDSPostavshik: TClientDataSet
    Tag = 2
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPPostavshik'
    RemoteServer = DCOMConnection
    Left = 80
    Top = 48
    object CDSPostavshikID_P: TIntegerField
      FieldName = 'ID_P'
      Visible = False
    end
    object CDSPostavshikFIO: TStringField
      FieldName = 'FIO'
      Size = 30
    end
    object CDSPostavshikTEL: TStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      DisplayWidth = 12
      FieldName = 'TEL'
      Size = 30
    end
    object CDSPostavshikADDR: TStringField
      DisplayLabel = #1070#1088'. '#1072#1076#1088#1077#1089
      FieldName = 'ADDR'
      Size = 30
    end
  end
  object DCOMConnection: TDCOMConnection
    Connected = True
    ServerGUID = '{F512D3C3-7D7F-11D8-B0EB-BD9930084878}'
    ServerName = 'AcridServer.CoClassAcridServer'
    Left = 32
    Top = 16
  end
  object CDSSchet: TClientDataSet
    Tag = 4
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPSchet'
    ReadOnly = True
    RemoteServer = DCOMConnection
    Left = 192
    Top = 48
    object CDSSchetID_S: TIntegerField
      FieldName = 'ID_S'
      Visible = False
    end
    object CDSSchetID_N: TIntegerField
      FieldName = 'ID_N'
      Visible = False
    end
    object CDSSchetID_T: TIntegerField
      FieldName = 'ID_T'
      Visible = False
    end
    object CDSSchetKOL_VO: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'KOL_VO'
    end
    object CDSSchetNDS_SUM: TBCDField
      DisplayLabel = #1057#1091#1084#1084#1072' ('#1089' '#1053#1044#1057')'
      FieldName = 'NDS_SUM'
      Precision = 18
    end
  end
  object CDSNakladnaya: TClientDataSet
    Tag = 3
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPNakladnaya'
    RemoteServer = DCOMConnection
    Left = 136
    Top = 64
    object CDSNakladnayaID_N: TIntegerField
      FieldName = 'ID_N'
      Visible = False
    end
    object CDSNakladnayaID_P: TIntegerField
      FieldName = 'ID_P'
      Visible = False
    end
    object CDSNakladnayaSHIFR: TStringField
      DisplayLabel = #1064#1080#1092#1088
      DisplayWidth = 7
      FieldName = 'SHIFR'
      Size = 30
    end
    object CDSNakladnayaDATA: TDateField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DATA'
    end
    object CDSNakladnayaSUM_NDS: TBCDField
      DisplayLabel = #1057#1091#1084#1084#1072' ('#1089' '#1053#1044#1057')'
      FieldName = 'SUM_NDS'
      Precision = 18
    end
    object CDSNakladnayaPREDOPLATA: TStringField
      DisplayLabel = #1055#1088#1077#1076#1086#1087#1083#1072#1090#1072
      FieldName = 'PREDOPLATA'
      Size = 1
    end
    object CDSNakladnayaOTGRUZKA: TStringField
      DisplayLabel = #1054#1090#1075#1088#1091#1079#1082#1072
      FieldName = 'OTGRUZKA'
      Size = 1
    end
    object CDSNakladnayaOSTATOK: TBCDField
      DisplayLabel = #1054#1087#1083#1072#1095#1077#1085#1085#1086
      FieldName = 'OSTATOK'
      Precision = 18
    end
  end
  object CDSPay: TClientDataSet
    Tag = 5
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPPay'
    RemoteServer = DCOMConnection
    Left = 240
    Top = 64
    object CDSPayID_P: TIntegerField
      FieldName = 'ID_P'
    end
    object CDSPayID_N: TIntegerField
      FieldName = 'ID_N'
    end
    object CDSPayDATA: TDateField
      FieldName = 'DATA'
    end
    object CDSPayNDS_SUM: TBCDField
      FieldName = 'NDS_SUM'
      Precision = 18
    end
  end
  object CDSQuery: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPQuery'
    RemoteServer = DCOMConnection
    Left = 80
  end
  object DSTovar: TDataSource
    Tag = 1
    DataSet = CDSTovar
    Left = 24
    Top = 112
  end
  object DSPostavshik: TDataSource
    Tag = 2
    DataSet = CDSPostavshik
    Left = 80
    Top = 96
  end
  object DSNakladnaya: TDataSource
    Tag = 3
    DataSet = CDSNakladnaya
    Left = 136
    Top = 112
  end
  object DSQuery: TDataSource
    DataSet = CDSQuery
    Left = 136
    Top = 16
  end
  object DSPay: TDataSource
    Tag = 5
    DataSet = CDSPay
    Left = 240
    Top = 112
  end
  object DSSchet: TDataSource
    Tag = 4
    DataSet = CDSSchet
    Left = 192
    Top = 96
  end
  object CDSQuery2: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPQuery2'
    RemoteServer = DCOMConnection
    Left = 184
  end
  object DSQuery2: TDataSource
    DataSet = CDSQuery2
    Left = 240
    Top = 16
  end
end
