unit UntPostav;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cbClasses, CaptionButton, FormRoller, StdCtrls, ExtCtrls, Grids,
  DBGrids;

type
  TfPostav = class(TForm)
    Label1: TLabel;
    DBGPostavshik: TDBGrid;
    edtFIO: TEdit;
    Panel1: TPanel;
    btnAddPredmet: TButton;
    btnUpPredmet: TButton;
    btnMenu: TButton;
    RGPostavshik: TRadioGroup;
    FormRoller1: TFormRoller;
    Label2: TLabel;
    Label3: TLabel;
    edtTel: TEdit;
    edtAddr: TEdit;
    btnDelPredmet: TButton;
    procedure RGPostavshikClick(Sender: TObject);
    procedure DBGPostavshikCellClick(Column: TColumn);
    procedure btnAddPredmetClick(Sender: TObject);
    procedure btnUpPredmetClick(Sender: TObject);
    procedure btnDelPredmetClick(Sender: TObject);
    procedure btnMenuClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPostav: TfPostav;

implementation

uses UntDM, DB, UntFirst;

{$R *.dfm}

procedure TfPostav.RGPostavshikClick(Sender: TObject);
begin
 case RGPostavshik.ItemIndex of
  0: dmClient.CDSPostavshik.IndexFieldNames := 'ID_P';
  1: dmClient.CDSPostavshik.IndexFieldNames := 'FIO;ID_P';
  2: dmClient.CDSPostavshik.IndexFieldNames := 'Tel;ID_P';
  3: dmClient.CDSPostavshik.IndexFieldNames := 'Addr;ID_P';
 end;
end;

procedure TfPostav.DBGPostavshikCellClick(Column: TColumn);
begin
 edtFIO.Text:=dmClient.CDSPostavshikFIO.Value;
 edtAddr.Text:=dmClient.CDSPostavshikADDR.Value;
 edtTel.Text:=dmClient.CDSPostavshikTEL.Value;
end;

procedure TfPostav.btnAddPredmetClick(Sender: TObject);
begin
 if dmClient.DCOMConnection.AppServer.sm_AddPostavshik(0,edtFIO.Text,edtTel.Text,edtAddr.Text) > 0
  then dmClient.CDSPostavshik.Refresh;
end;

procedure TfPostav.btnUpPredmetClick(Sender: TObject);
begin
if MessageDlg('’отите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
 if dmClient.DCOMConnection.AppServer.sm_AddPostavshik(dmClient.CDSPostavshikID_P.Value,edtFIO.Text,edtTel.Text,edtAddr.Text) > 0
  then dmClient.CDSPostavshik.Refresh;
end;

procedure TfPostav.btnDelPredmetClick(Sender: TObject);
begin
 if MessageDlg('’отите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmClient.DCOMConnection.AppServer.sm_DelPostavshik(dmClient.CDSPostavshikID_P.Value);
   dmClient.CDSPostavshik.Refresh;
  end;
end;

procedure TfPostav.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fPostav.Visible:=False;
  fPostav.DBGPostavshik.DataSource:= nil;
end;

end.
