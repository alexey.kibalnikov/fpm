object fAll: TfAll
  Left = 113
  Top = 80
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1053#1072#1082#1083#1072#1076#1085#1099#1077', '#1089#1095#1077#1090#1072'-'#1092#1072#1082#1090#1091#1088#1099' '#1080' '#1087#1083#1072#1090#1077#1078
  ClientHeight = 435
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnMenu: TButton
    Left = 479
    Top = 406
    Width = 133
    Height = 25
    Caption = #1054#1089#1085#1086#1074#1085#1086#1077' '#1084#1077#1085#1102
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = btnMenuClick
  end
  object pgNacladn: TPageControl
    Left = -1
    Top = 0
    Width = 322
    Height = 433
    ActivePage = tabNakladn
    TabIndex = 0
    TabOrder = 1
    object tabNakladn: TTabSheet
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      object Label1: TLabel
        Left = 128
        Top = 285
        Width = 55
        Height = 24
        Caption = #1064#1080#1092#1088':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 136
        Top = 309
        Width = 49
        Height = 24
        Caption = #1044#1072#1090#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 128
        Top = 333
        Width = 63
        Height = 24
        Caption = #1057#1091#1084#1084#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 8
        Top = 269
        Width = 98
        Height = 24
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBGNakladn: TDBGrid
        Left = 1
        Top = 51
        Width = 309
        Height = 172
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGNakladnCellClick
      end
      object edtShifr: TEdit
        Left = 197
        Top = 280
        Width = 100
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 1
        Text = 'xxxx1234'
      end
      object Panel1: TPanel
        Left = 4
        Top = 228
        Width = 305
        Height = 41
        BevelInner = bvLowered
        TabOrder = 2
        object btnAddNakladn: TButton
          Left = 8
          Top = 8
          Width = 89
          Height = 25
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = btnAddNakladnClick
        end
        object btnUpNakladn: TButton
          Left = 104
          Top = 8
          Width = 89
          Height = 25
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnUpNakladnClick
        end
        object btnDelNakladn: TButton
          Left = 200
          Top = 8
          Width = 97
          Height = 25
          Caption = #1059#1076#1072#1083#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = btnDelNakladnClick
        end
      end
      object RGNakladn: TRadioGroup
        Left = 1
        Top = -4
        Width = 305
        Height = 53
        Columns = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          'ID'
          #1064#1080#1092#1088' '
          #1044#1072#1090#1072
          #1057#1091#1084#1084#1072
          #1055#1088#1077#1076#1086#1087#1083#1072#1090#1072
          #1054#1090#1075#1088#1091#1079#1082#1072)
        ParentFont = False
        TabOrder = 3
        OnClick = RGNakladnClick
      end
      object DTPData: TDateTimePicker
        Left = 197
        Top = 304
        Width = 101
        Height = 24
        CalAlignment = dtaLeft
        Date = 38141.0878043982
        Time = 38141.0878043982
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 4
      end
      object RGOtgruzka: TRadioGroup
        Left = 200
        Top = 356
        Width = 97
        Height = 49
        Caption = #1054#1090#1075#1088#1091#1079#1082#1072' '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          #1044#1072
          #1053#1077#1090)
        ParentFont = False
        TabOrder = 5
      end
      object edtSum: TEdit
        Left = 197
        Top = 328
        Width = 100
        Height = 24
        Color = clScrollBar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        Text = '0'
      end
      object DBGPostavshik: TDBGrid
        Left = 0
        Top = 292
        Width = 121
        Height = 113
        TabOrder = 7
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PageControl: TPageControl
    Left = 323
    Top = 2
    Width = 289
    Height = 399
    ActivePage = tabB
    TabIndex = 3
    TabOrder = 2
    OnChange = PageControlChange
    object tabSchet: TTabSheet
      Caption = #1057#1095#1077#1090'-'#1060#1072#1082#1090#1091#1088#1072
      object Label5: TLabel
        Left = 64
        Top = 317
        Width = 66
        Height = 24
        Caption = #1050#1086#1083'-'#1074#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 0
        Top = 341
        Width = 136
        Height = 16
        Caption = #1062#1077#1085#1072' ('#1079#1072' '#1042#1057#1045' '#1089' '#1053#1044#1057'):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 104
        Top = 153
        Width = 68
        Height = 24
        Caption = #1058#1086#1074#1072#1088#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 0
        Top = 1
        Width = 271
        Height = 24
        Caption = #1058#1086#1074#1072#1088#1099' '#1087#1086' '#1082#1086#1085#1082#1088#1077#1090#1085#1086#1084#1091' '#1089#1095#1077#1090#1091
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBGQuery: TDBGrid
        Left = 2
        Top = 24
        Width = 277
        Height = 129
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGQueryCellClick
      end
      object DBGTovar: TDBGrid
        Left = 2
        Top = 176
        Width = 277
        Height = 97
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGNakladnCellClick
      end
      object Panel2: TPanel
        Left = 1
        Top = 276
        Width = 280
        Height = 41
        BevelInner = bvLowered
        TabOrder = 2
        object btnAddSchet: TButton
          Left = 3
          Top = 8
          Width = 89
          Height = 25
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = btnAddSchetClick
        end
        object btnUpSchet: TButton
          Left = 94
          Top = 8
          Width = 89
          Height = 25
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnUpSchetClick
        end
        object btnDelSchet: TButton
          Left = 185
          Top = 8
          Width = 92
          Height = 25
          Caption = #1059#1076#1072#1083#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = btnDelSchetClick
        end
      end
      object edtKol_vo: TEdit
        Left = 144
        Top = 320
        Width = 121
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Text = '0'
      end
      object edtNDS_Sum: TEdit
        Left = 144
        Top = 344
        Width = 121
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Text = '0'
      end
    end
    object tabPay: TTabSheet
      Caption = #1055#1083#1072#1090#1077#1078#1080
      ImageIndex = 1
      object Label9: TLabel
        Left = 0
        Top = 1
        Width = 271
        Height = 24
        Caption = #1055#1083#1072#1090#1077#1078' '#1087#1086' '#1082#1086#1085#1082#1088#1077#1090#1085#1086#1084#1091' '#1089#1095#1077#1090#1091
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 64
        Top = 317
        Width = 73
        Height = 24
        Caption = #1055#1083#1072#1090#1077#1078':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 88
        Top = 341
        Width = 49
        Height = 24
        Caption = #1044#1072#1090#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 0
        Top = 245
        Width = 156
        Height = 24
        Caption = #1057#1091#1084#1084#1072' '#1087#1083#1072#1090#1077#1078#1077#1081':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBGQuery2: TDBGrid
        Left = 1
        Top = 24
        Width = 277
        Height = 217
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGQuery2CellClick
      end
      object Panel3: TPanel
        Left = 1
        Top = 276
        Width = 280
        Height = 41
        BevelInner = bvLowered
        TabOrder = 1
        object btnAddPay: TButton
          Left = 3
          Top = 8
          Width = 89
          Height = 25
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = btnAddPayClick
        end
        object btnUpPay: TButton
          Left = 94
          Top = 8
          Width = 89
          Height = 25
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnUpPayClick
        end
        object btnDelPay: TButton
          Left = 185
          Top = 8
          Width = 92
          Height = 25
          Caption = #1059#1076#1072#1083#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = btnDelPayClick
        end
      end
      object edtPay: TEdit
        Left = 144
        Top = 320
        Width = 121
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '0'
      end
      object DTPDataPay: TDateTimePicker
        Left = 144
        Top = 344
        Width = 121
        Height = 24
        CalAlignment = dtaLeft
        Date = 38141.2236439815
        Time = 38141.2236439815
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 3
      end
      object edtOstatok: TEdit
        Left = 158
        Top = 248
        Width = 119
        Height = 21
        Color = clScrollBar
        ReadOnly = True
        TabOrder = 4
        Text = 'edtOstatok'
      end
    end
    object tabA: TTabSheet
      Caption = #1054#1090#1095#1077#1090' '#1040
      ImageIndex = 2
      object Label13: TLabel
        Left = 8
        Top = 5
        Width = 273
        Height = 68
        AutoSize = False
        Caption = 
          #1047#1072' '#1087#1077#1088#1080#1086#1076' '#1074#1099#1074#1077#1089#1090#1080' '#1089#1087#1080#1089#1086#1082' '#1086#1087#1083#1072#1095#1077#1085#1085#1099#1093', '#1085#1086' '#1085#1077' '#1086#1090#1075#1088#1091#1078#1077#1085#1085#1099#1093' '#1087#1086' '#1085#1072#1082#1083#1072#1076 +
          #1085#1099#1084' '#1087#1086' '#1091#1082#1072#1079#1072#1085#1099#1084' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084' '#1089' '#1091#1082#1072#1079#1072#1085#1080#1077#1084' '#1089#1091#1084#1084' :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        WordWrap = True
      end
      object CLBPostavshik: TCheckListBox
        Left = 2
        Top = 240
        Width = 113
        Height = 126
        ItemHeight = 13
        TabOrder = 0
      end
      object CLBid: TCheckListBox
        Left = 80
        Top = 248
        Width = 25
        Height = 113
        ItemHeight = 13
        TabOrder = 1
        Visible = False
      end
      object DTPA1: TDateTimePicker
        Left = 136
        Top = 240
        Width = 89
        Height = 21
        CalAlignment = dtaLeft
        Date = 38141.8753857639
        Time = 38141.8753857639
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 2
      end
      object DTPA2: TDateTimePicker
        Left = 136
        Top = 272
        Width = 89
        Height = 21
        CalAlignment = dtaLeft
        Date = 38141.8754175926
        Time = 38141.8754175926
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 3
      end
      object DBGA: TDBGrid
        Left = 1
        Top = 72
        Width = 278
        Height = 164
        ReadOnly = True
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGNakladnCellClick
      end
      object btnA: TButton
        Left = 136
        Top = 304
        Width = 89
        Height = 25
        Caption = #1054#1050' '
        TabOrder = 5
        OnClick = btnAClick
      end
    end
    object tabB: TTabSheet
      Caption = #1054#1090#1095#1077#1090' '#1042
      ImageIndex = 3
      object Label14: TLabel
        Left = 8
        Top = 5
        Width = 273
        Height = 68
        AutoSize = False
        Caption = 
          #1047#1072' '#1087#1077#1088#1080#1086#1076' '#1074#1099#1074#1077#1089#1090#1080' '#1089#1087#1080#1089#1086#1082' '#1087#1086#1083#1091#1095#1077#1085#1085#1099#1093' ('#1086#1090#1075#1088#1091#1078#1077#1085#1085#1099#1093') '#1090#1086#1074#1072#1088#1086#1074', '#1087#1086' '#1082#1086 +
          #1090#1086#1088#1099#1084' '#1087#1088#1086#1080#1079#1074#1077#1076#1077#1085#1072' '#1087#1088#1077#1076#1086#1087#1083#1072#1090#1072', '#1089' '#1091#1082#1072#1079#1072#1085#1080#1077#1084' '#1082#1086#1083'-'#1074#1072' '#1080' '#1089#1091#1084#1084#1099' :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        WordWrap = True
      end
      object DBGB: TDBGrid
        Left = 1
        Top = 72
        Width = 278
        Height = 265
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGNakladnCellClick
      end
      object DTPB2: TDateTimePicker
        Left = 96
        Top = 344
        Width = 89
        Height = 21
        CalAlignment = dtaLeft
        Date = 38141.8754175926
        Time = 38141.8754175926
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 1
      end
      object DTPB1: TDateTimePicker
        Left = 0
        Top = 344
        Width = 89
        Height = 21
        CalAlignment = dtaLeft
        Date = 38141.8753857639
        Time = 38141.8753857639
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 2
      end
      object btnB: TButton
        Left = 191
        Top = 344
        Width = 89
        Height = 25
        Caption = #1054#1050' '
        TabOrder = 3
        OnClick = btnBClick
      end
    end
  end
  object btnUpDate: TButton
    Left = 352
    Top = 408
    Width = 75
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1083#1077#1085#1080#1077
    TabOrder = 3
    OnClick = btnUpDateClick
  end
  object FormRoller1: TFormRoller
    CaptionButton.BtnOrder = 0
    CaptionButton.Cursor = crDefault
    CaptionButton.CursorDown = crDefault
    CaptionButton.Enabled = True
    CaptionButton.Hint = 'Roll up'
    CaptionButton.SeparatorWidth = 0
    CaptionButton.ShowHint = True
    CaptionButton.Visible = True
    SystemMenu.ApplyToMenu = False
    SystemMenu.Caption = 'Roll up'
    SystemMenu.Position = 7
    SystemMenu.Separators = [seBefore]
    Animate = True
    AnimateSpeed = 8
    HintRollUp = 'Roll up'
    HintRollDown = 'Roll down'
    RemainHeight = 0
    RollUp = False
    Left = 584
  end
end
