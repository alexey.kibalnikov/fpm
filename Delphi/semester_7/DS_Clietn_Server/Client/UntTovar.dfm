object fTovar: TfTovar
  Left = 234
  Top = 119
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1057#1087#1080#1089#1086#1082' '#1090#1086#1074#1072#1088#1086#1074
  ClientHeight = 316
  ClientWidth = 312
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010002002020100000000000E80200002600000020200200000000003001
    00000E0300002800000020000000400000000100040000000000800200000000
    0000000000000000000000000000000000000000800000800000008080008000
    0000800080008080000080808000C0C0C0000000FF0000FF000000FFFF00FF00
    0000FF00FF00FFFF0000FFFFFF009999999FFFFFF99999999999999999999999
    9999FFFFFF999999999999999999999999999FFFFFF999999999999999999999
    999999FFFFFF99999999999999999999999FFFFFFFFFFFFFF999999999999999
    99FFFFFFFFFFFFFFFFF99999999999999FFFFFFFFFFFFFFFFFFF999999999999
    FFFFFFFFFFFFFFFFFFFFF99999999999FFFFFFF999999FFFFFFFFF999999999F
    FFFFFF99999999FFFFFFFF999999999FFFFFFF99999999FFFFFFFF999999999F
    FFFFFF99999999FFFFFFFF9999999999999999999999FFFFFFFFF99999999999
    999999999FFFFFFFFFFFF99999999999999999FFFFFFFFFFFFFF999999999999
    99999FFFFFFFFFFFFFF99999999999999999FFFFFFFFFFFFF999999999999999
    999FFFFFFFFFFFF9999999999999999999FFFFFFFFFF99999999999999999999
    99FFFFFFFF99999999FFFFFFF999999999FFFFFFF999999999FFFFFFF9999999
    99FFFFFFF999999999FFFFFFF999999999FFFFFFFF9999999FFFFFFF99999999
    999FFFFFFF99999FFFFFFFFF999999999999FFFFFFFFFFFFFFFFFFF999999999
    99999FFFFFFFFFFFFFFFFF99999999999999999FFFFFFFFFFFFFF99999999999
    999999999FFFFFFFFFF999999999999999999999999999FFFFFF999999999999
    99999999999999FFFFFFF99999999999999999999999999FFFFFFF9999999999
    9999999999999999FFFFFFF99999000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000280000002000000040000000010001000000
    0000000100000000000000000000000000000000000000000000FFFFFF0001F8
    000000FC0000007E0000003F000001FFF80003FFFE0007FFFF000FFFFF800FE0
    7FC01FC03FC01FC03FC01FC03FC00000FF800007FF80003FFF00007FFE0000FF
    F80001FFE00003FF000003FC03F803F803F803F803F803FC07F001FC1FF000FF
    FFE0007FFFC0001FFF800007FE0000003F0000003F8000001FC000000FE00000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 256
    Width = 92
    Height = 24
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DBGTovar: TDBGrid
    Left = 1
    Top = 32
    Width = 309
    Height = 172
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGTovarCellClick
  end
  object edtName: TEdit
    Left = 101
    Top = 256
    Width = 208
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 30
    ParentFont = False
    TabOrder = 1
    Text = 'edtName'
  end
  object Panel1: TPanel
    Left = 4
    Top = 211
    Width = 305
    Height = 41
    BevelInner = bvLowered
    TabOrder = 2
    object btnAddPredmet: TButton
      Left = 8
      Top = 8
      Width = 89
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btnAddPredmetClick
    end
    object btnUpPredmet: TButton
      Left = 104
      Top = 8
      Width = 89
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btnUpPredmetClick
    end
    object btnDelPredmet: TButton
      Left = 200
      Top = 8
      Width = 97
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnDelPredmetClick
    end
  end
  object btnMenu: TButton
    Left = 168
    Top = 286
    Width = 142
    Height = 25
    Caption = #1054#1089#1085#1086#1074#1085#1086#1077' '#1084#1077#1085#1102
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = btnMenuClick
  end
  object RGTovar: TRadioGroup
    Left = 1
    Top = -4
    Width = 305
    Height = 33
    Columns = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      'ID'
      #1053#1072#1079#1074#1072#1085#1080#1077
      #1053#1044#1057)
    ParentFont = False
    TabOrder = 4
    OnClick = RGTovarClick
  end
  object RGNds: TRadioGroup
    Left = 8
    Top = 280
    Width = 153
    Height = 33
    Caption = #1053#1044#1057' '
    Columns = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      '20 %'
      '10 %'
      ' 0  %')
    ParentFont = False
    TabOrder = 5
  end
  object FormRoller1: TFormRoller
    CaptionButton.BtnOrder = 0
    CaptionButton.Cursor = crDefault
    CaptionButton.CursorDown = crDefault
    CaptionButton.Enabled = True
    CaptionButton.Hint = 'Roll up'
    CaptionButton.SeparatorWidth = 0
    CaptionButton.ShowHint = True
    CaptionButton.Visible = True
    SystemMenu.ApplyToMenu = False
    SystemMenu.Caption = 'Roll up'
    SystemMenu.Position = 7
    SystemMenu.Separators = [seBefore]
    Animate = True
    AnimateSpeed = 8
    HintRollUp = 'Roll up'
    HintRollDown = 'Roll down'
    RemainHeight = 0
    RollUp = False
    Left = 280
  end
end
