unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, CheckLst;

type
  TFirst = class(TForm)
    btnStart: TButton;
    edtParams: TEdit;
    edtInFile: TEdit;
    edtTable: TEdit;
    edtOutFile: TEdit;
    OpenDialog: TOpenDialog;
    pnlParams: TPanel;
    btnOpen: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CLBString: TCheckListBox;
    Label6: TLabel;
    procedure btnStartClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure edtParamsChange(Sender: TObject);
    procedure edtTableChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  First: TFirst;

implementation

{$R *.dfm}

procedure TFirst.btnStartClick(Sender: TObject);
var i,j,p,ID:integer;
      inF,outF:textfile;
      S,S2:string;
begin
  ID:=0;
  AssignFile(inF,edtInFile.Text);
  Reset(inF);
  AssignFile(outF,edtOutFile.Text);
  ReWrite(outF);
  //writeln(outF,'connect "C:\Students.gdb"');
  //writeln(outF,'user "SYSDBA" password "masterkey"');
  writeln(outF,'set generator gen'+edtTable.text+' to ',0,'++');
  while not EOF(inF) do
   begin
    Readln(inF,S);
    Write(outF,'insert into '+edtTable.text+' values(',ID,',');
    inc(ID);
    for i:=0 to StrToInt(edtParams.Text)-1 do
     begin
      S2:='';
      p:=Pos('$',S);
      if p<>0 then
       begin
        for j:=1 to p-1 do S2:=S2+S[j];
        if (CLBString.Checked[i])and(S2<>'')then write(outF,'''');
        write(outF,S2);
        if (CLBString.Checked[i])and(S2<>'')then write(outF,'''');
       end;
      if (i<>StrToInt(edtParams.Text)-1)then write(outF,',')
       else writeln(outF,')++');
      S2:=Copy(S,p+1,length(S)-p);
      S:=S2
     end
  end;
  writeln(outF,'set generator gen'+edtTable.text+' to ',ID,'++');
  write(outF,'Commit++');
  MessageDlg('��������� Script ����� ������� ���������',mtCustom,[mbOK],i);
  CloseFile(inF);
  CloseFile(outF);
end;

procedure TFirst.btnOpenClick(Sender: TObject);
begin
 OpenDialog.Execute;
 edtInFile.Text:=OpenDialog.FileName;
 if OpenDialog.FileName<>'' then pnlParams.Visible:=True;
end;

procedure TFirst.edtParamsChange(Sender: TObject);
var i: integer;
begin
  if (edtParams.Text<>'')and(edtTable.Text<>'') then btnStart.Visible:=True;
  for i:=0 to CLBString.Items.Count-1 do CLBString.Items.Delete(i);
  for i:=0 to StrToInt(edtParams.Text)-1 do CLBString.Items.Add(IntToStr(i));
end;

procedure TFirst.edtTableChange(Sender: TObject);
begin
  if (edtParams.Text<>'')and(edtTable.Text<>'') then btnStart.Visible:=True;
end;

end.
