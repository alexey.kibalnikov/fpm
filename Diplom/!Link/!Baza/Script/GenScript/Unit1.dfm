object First: TFirst
  Left = 247
  Top = 140
  Width = 364
  Height = 217
  Caption = 'AcridGenScript'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 292
    Height = 24
    Caption = #1054#1090#1082#1088#1086#1081#1090#1077' '#1092#1072#1081#1083' '#1084#1077#1090#1086#1076#1072#1085#1085#1099#1093' '#1076#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 40
    Top = 24
    Width = 197
    Height = 24
    Caption = ' '#1089#1086#1089#1090#1072#1074#1083#1077#1085#1080#1103' '#1089#1082#1088#1080#1087#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 307
    Top = 0
    Width = 39
    Height = 20
    Caption = 'string'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edtInFile: TEdit
    Left = 0
    Top = 48
    Width = 209
    Height = 21
    ReadOnly = True
    TabOrder = 0
  end
  object pnlParams: TPanel
    Left = 1
    Top = 78
    Width = 293
    Height = 110
    TabOrder = 1
    Visible = False
    object Label2: TLabel
      Left = 6
      Top = 4
      Width = 180
      Height = 24
      Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 30
      Width = 122
      Height = 24
      Caption = #1048#1084#1103' '#1090#1072#1073#1083#1080#1094#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 57
      Width = 267
      Height = 24
      Caption = #1048#1084#1103' '#1088#1077#1079#1091#1083#1100#1090#1080#1088#1091#1102#1097#1077#1075#1086' '#1092#1072#1081#1083#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object btnStart: TButton
      Left = 214
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Start'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = btnStartClick
    end
    object edtOutFile: TEdit
      Left = 8
      Top = 84
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Rez.sql'
    end
    object edtParams: TEdit
      Left = 192
      Top = 8
      Width = 49
      Height = 21
      TabOrder = 2
      OnChange = edtParamsChange
    end
    object edtTable: TEdit
      Left = 136
      Top = 32
      Width = 153
      Height = 21
      TabOrder = 3
      OnChange = edtTableChange
    end
  end
  object btnOpen: TButton
    Left = 214
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Open'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = btnOpenClick
  end
  object CLBString: TCheckListBox
    Left = 299
    Top = 24
    Width = 54
    Height = 161
    ItemHeight = 13
    TabOrder = 3
  end
  object OpenDialog: TOpenDialog
    Top = 16
  end
end
