CREATE DOMAIN "DMN_KEY" AS integer not null++
CREATE DOMAIN "DMN_CODE_MARK" AS SMALLINT
	 check (value in (-2,-1,0,1,2,3,4,5)) NOT NULL++
CREATE DOMAIN "DMN_FALSE" AS CHAR(1) default 'F'
	 check (value in ('F','T')) NOT NULL++
CREATE DOMAIN "DMN_KURS" AS SMALLINT
	 check (value in (1,2,3,4,5)) NOT NULL++
CREATE DOMAIN "DMN_N_GR" AS SMALLINT
	 check (value in (1,2,3,4,6,7,8) or value is NULL)++
CREATE DOMAIN "DMN_PROF" AS CHAR(3) 
	 check (value in ('���','���','���','���','') or value is NULL)++
CREATE DOMAIN "DMN_RANG" AS CHAR(3) 
	 check (value in ('���','���')) NOT NULL++
CREATE DOMAIN "DMN_SEX" AS CHAR(1) default '�'
	 check (value in ('�','�')) NOT NULL++
CREATE DOMAIN "DMN_STR" AS VARCHAR(30)++
CREATE DOMAIN "DMN_TRUE" AS CHAR(1) default 'T'
	 check (value in ('F','T')) NOT NULL++

CREATE TABLE "CODE_MARK"("ID_C" dmn_Key,"CODE" dmn_Str,
PRIMARY KEY ("ID_C"))++
INSERT INTO "CODE_MARK" values (-2, '�� ������')++
INSERT INTO "CODE_MARK" values (-1, '�� �������')++
INSERT INTO "CODE_MARK" values ( 0, '�� �����')++
INSERT INTO "CODE_MARK" values ( 1, '�����')++
INSERT INTO "CODE_MARK" values ( 2, '�������.')++
INSERT INTO "CODE_MARK" values ( 3, '�����.')++
INSERT INTO "CODE_MARK" values ( 4, '������')++
INSERT INTO "CODE_MARK" values ( 5, '�������')++

CREATE TABLE "HISTORY" ("ID_H"	dmn_Key,"ID_S"	dmn_Key,
"HDATA"	DATE NOT NULL,"ACADEM" dmn_False,"OTCHISL" dmn_False,
"PEREVOD" dmn_False,"BUDGET" dmn_True,
PRIMARY KEY ("ID_H"))++

CREATE TABLE "LECTURE"("ID_L" dmn_Key,"ID_T" dmn_Key,"ID_P" dmn_Key,
"KURS" dmn_Kurs,"DS" dmn_False,"RANG" dmn_Rang,"WINER" dmn_True,
PRIMARY KEY ("ID_L"))++

CREATE TABLE "PREDMET"("ID_P" dmn_Key,"PNAME" dmn_Str,
PRIMARY KEY ("ID_P"))++

CREATE TABLE "SESSION"("ID" dmn_Key,"ID_S" dmn_Key,"ID_L" dmn_Key,
"CODE_MARK" dmn_Code_mark,"S_YEAR" DATE NOT NULL,
PRIMARY KEY ("ID"))++

CREATE TABLE "STUDENT"("ID_S" dmn_Key,"F" dmn_Str,"I" dmn_Str,
"O" dmn_Str,"N_GR" dmn_N_gr,"PROF" dmn_Prof,"PDATA" DATE,
"RDATA"	DATE, "SCHOOL"	SMALLINT,"CITY"	dmn_False,"BUDGET" dmn_True,
"SEX" dmn_Sex,
PRIMARY KEY ("ID_S"))++

CREATE TABLE "TEACHER"("ID_T" dmn_Key,"F" dmn_Str,"I" dmn_Str,
"O" dmn_Str,"JOB" dmn_Str,"RANG" dmn_Str,
PRIMARY KEY ("ID_T"))++

CREATE INDEX "IXI_STUD" ON "STUDENT"("I")++
CREATE INDEX "IXN_GR_STUD" ON "STUDENT"("N_GR")++
CREATE INDEX "IXPROF_STUD" ON "STUDENT"("PROF")++
CREATE INDEX "IXSCHOOL_STUD" ON "STUDENT"("SCHOOL")++
CREATE INDEX "IXF_TEACH" ON "TEACHER"("F")++

ALTER TABLE "HISTORY" ADD FOREIGN KEY ("ID_S") REFERENCES STUDENT ("ID_S")++
ALTER TABLE "LECTURE" ADD FOREIGN KEY ("ID_P") REFERENCES PREDMET ("ID_P")++
ALTER TABLE "LECTURE" ADD FOREIGN KEY ("ID_T") REFERENCES TEACHER ("ID_T")++
ALTER TABLE "SESSION" ADD FOREIGN KEY ("ID_S") REFERENCES STUDENT ("ID_S")++
ALTER TABLE "SESSION" ADD FOREIGN KEY ("ID_L") REFERENCES LECTURE ("ID_L")++

CREATE GENERATOR "GENHISTORY"++
CREATE GENERATOR "GENLECTURE"++
CREATE GENERATOR "GENPREDMET"++
CREATE GENERATOR "GENSESSION"++
CREATE GENERATOR "GENSTUDENT"++
CREATE GENERATOR "GENTEACHER"++

CREATE PROCEDURE "ADDHISTORY"("INID_H" INTEGER,
  "INID_S" INTEGER,
  "INHDATA" DATE,
  "INACADEM" CHAR(1),
  "INOTCHISL" CHAR(1),
  "INPEREVOD" CHAR(1),
  "INBUDGET" CHAR(1))
RETURNS("OUTID" INTEGER)
AS
Begin
 if (:inID_h=0) then
  begin
   inId_S=Gen_id(GenHistory,1);
   insert into History
   values(:inID_H,:inID_S,:inhDATA,:inACADEM,:inOTCHISL,:inPEREVOD,:inBudget);
  end
   else
    begin
    update History
     set ID_H=:inID_H,
         ID_S=:inID_S,
         hDATA=:inhDATA,
         ACADEM=:inACADEM,
         OTCHISL=:inOTCHISL,
         PEREVOD=:inPEREVOD,
         BUDGET=:inBUDGET
     where id_h=:inID_h;
    end
    OUTID=:inID_h;
End++

CREATE PROCEDURE "ADDLECTURE"("INID_L" INTEGER,
  "INID_T" INTEGER,
  "INID_P" INTEGER,
  "INKURS" SMALLINT,
  "INDS" CHAR(1),
  "INRANG" CHAR(3),
  "INWINER" CHAR(1))
RETURNS("OUTID" INTEGER)
AS
Begin
 if (:inID_l=0) then
  begin
   inId_l=Gen_id(Genlecture,1);
   insert into lecture
   values(:inID_L,:inID_T,:inID_P,:inKURS,:inDS,:inRANG,:inWINER);
  end
   else
    begin
    update Lecture
     set id_l=:inID_L,id_t=:inID_T,
     id_p=:inID_P,kurs=:inKURS,DS=:inDS,
     RANG=:inRANG,winer=:inWINER
     where id_l=:inID_l;
    end
  OUTID=:inID_l;
End++

CREATE PROCEDURE "ADDPREDMET"("INID_P" INTEGER,
  "INPNAME" VARCHAR(30))
RETURNS("OUTID" INTEGER)
AS
Begin
 if (:inID_p=0) then
  begin
   inId_p=Gen_id(GenPredmet,1);
   insert into Predmet
   values(:inId_p,:inPNAME);
  end
   else
    begin
    update Predmet
     set ID_p=:inid_p,
         PNAME=:inPNAME
     where id_p=:inID_p;
    end
  OUTID=:inID_p;
End++

CREATE PROCEDURE "ADDSESSION"("INID" INTEGER,
  "INID_S" INTEGER,
  "INID_L" INTEGER,
  "INCODE_MARK" SMALLINT,
  "INS_YEAR" DATE)
RETURNS("OUTID" INTEGER)
AS
Begin
 if (:inID=0) then
  begin
   inId=Gen_id(GenSession,1);
   insert into Session
   values(:inID,:inID_S,:inID_L,:inCODE_MARK,:inS_YEAR);
  end
   else
    begin
    update Session
     set
     id=:inID,id_s=:inID_S,
     id_l=:inID_L,
     code_mark=:inCODE_MARK,
     s_year=:inS_YEAR
     where id=:inID;
    end
  OUTID=:inID;
End++

CREATE PROCEDURE "ADDSTUDENT"("INID_S" INTEGER,
  "INF" VARCHAR(30),
  "INI" VARCHAR(30),
  "INO" VARCHAR(30),
  "INN_GR" SMALLINT,
  "INPROF" CHAR(3),
  "INPDATA" DATE,
  "INRDATA" DATE,
  "INSCHOOL" SMALLINT,
  "INCITY" CHAR(1),
  "INBUDGET" CHAR(1),
  "INSEX" CHAR(1))
RETURNS("OUTID" INTEGER)
AS
Begin
 if (:inID_S=0) then
  begin
   inId_S=Gen_id(GenStudent,1);
   insert into Student
   values(:inId_S,:inF,:inI,:inO,:inN_GR,:inPROF,:inPDATA,
          :inRDATA,:inSCHOOL,:inCITY,:inBUDGET,:inSEX);
  end
   else
    begin
    update Student
     set ID_S=:inid_s,
         F=:inF,
         I=:inI,
         O=:inO,
         N_GR=:inN_gr,
         PROF=:inProf,
         PDATA=:inPdata,
         RDATA=:inRdata,
         SCHOOL=:inSchool,
         CITY=:inCity,
         BUDGET=:inBudget,
         SEX=:inSEX
     where id_s=:inID_s;
    end
    OUTID=:inID_s;
End++

CREATE PROCEDURE "ADDTEACHER"("INID_T" INTEGER,
  "INF" VARCHAR(30),
  "INI" VARCHAR(30),
  "INO" VARCHAR(30),
  "INJOB" VARCHAR(30),
  "INRANG" VARCHAR(30))
RETURNS("OUTID" INTEGER)
AS
Begin
 if (:inID_t=0) then
  begin
   inId_t=Gen_id(Genteacher,1);
   insert into Teacher
   values(:inID_T,:inF,:inI,:inO,:inJOB,:inRANG);
  end
   else
    begin
    update Teacher
     set id_t=:inID_T,
         F=:inF,I=:inI,O=:inO,Job=:inJOB,Rang=:inRANG
     where id_t=:inID_t;
    end
   OUTID=:inID_t;
End++

CREATE PROCEDURE "DELFROMHISTORY"("INID_H" INTEGER)
AS
begin
delete from history
where id_h=:inID_h;
end++

CREATE PROCEDURE "DELFROMLECTURE"("INID_L" INTEGER)
AS
begin
delete from lecture
where id_l=:inID_l;
end++

CREATE PROCEDURE "DELFROMPREDMET"("INID_P" INTEGER)
AS
begin
delete from predmet
where id_p=:inID_p;
end++

CREATE PROCEDURE "DELFROMSESSION"("INID" INTEGER)
AS
begin
delete from session
where id=:inID;
end++

CREATE PROCEDURE "DELFROMSTUDENT"("INID_S" INTEGER)
AS
begin
delete from student
where id_s=:inID_s;
end++

CREATE PROCEDURE "DELFROMTEACHER"("INID_T" INTEGER)
AS
begin
delete from teacher
where id_t=:inID_t;
end++

CREATE TRIGGER "TRDELLECTURE" FOR "LECTURE" 
ACTIVE BEFORE DELETE POSITION 0
as begin delete from Session where Session.id_l=Old.id_l; end++
CREATE TRIGGER "TRDELPREDMET" FOR "PREDMET" 
ACTIVE BEFORE DELETE POSITION 0
as begin delete from Lecture where Lecture.id_p=Old.id_p; end++
CREATE TRIGGER "TRDELSTUDENT" FOR "STUDENT" 
ACTIVE BEFORE DELETE POSITION 0
as begin
  delete from History where Old.id_s=History.id_s;
  delete from Session where Old.id_s=Session.id_s; 
end++
CREATE TRIGGER "TRDELTEACHER" FOR "TEACHER" 
ACTIVE BEFORE DELETE POSITION 0
as begin delete from Lecture where Lecture.id_t=Old.id_t; end++

create view wLecture as
 select P.pName,T.F,L.Kurs,L.DS,L.Rang,L.Winer
  from Teacher T,Predmet P,Lecture L
   where T.ID_T=L.ID_T and P.ID_P=L.ID_P++
create view wHistory as
 select S.F,S.i,S.O,H.hData,H.Academ,H.Otchisl,H.Perevod,H.Budget
  from History H,Student S
   where H.ID_S=S.ID_S++
create view wSession as
 select S.F,S.i,S.O,P.pName,C.Code,Ss.S_year
  from Student S,Lecture L,Session Ss,Code_Mark C,Predmet P
   where Ss.ID_S=S.ID_S and Ss.ID_L=L.ID_L
    and C.ID_C=Ss.Code_mark and L.ID_P=P.ID_P++

COMMIT WORK++
