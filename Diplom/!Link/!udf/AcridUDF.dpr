library AcridUDF;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes;

{$R *.res}
type Vector = array[0..7] of byte;
const IxBit : Vector=(128,64,32,16,8,4,2,1);
    IxCoder : Vector=(3,5,4,7,6,0,2,1);
    Baza = 8;

function Coder(var Str : string) : string; cdecl; export;
 var Len,i : integer;
     k,j,Cod: byte;
     Ch : char;
  begin
   Len:=length(Str); k:=0; Result:='';
   for i:=1 to Len do
    begin
      Ch:=Str[i]; Cod:=0;
      for j:=0 to 7 do Cod:=Cod+(Ord(Ch) and IxBit[j] shr(7-j))*IxBit[(IxCoder[j]+k) mod 8];
      Result:=Result+Chr(Cod);
      k:=(k+1) mod baza;
    end;
end;

function DeCoder(var Str : string) : string; cdecl; export;
 var Len,i : integer;
     k,j,Cod: byte;
     Ch : char;
     ixDeCoder : Vector;
  begin
   Len:=length(Str); k:=0; Result:='';
   for i:=1 to Len do
    begin
      for j:=0 to 7 do ixDeCoder[(ixCoder[j]+k)mod 8]:=j;
      Ch:=Str[i]; Cod:=0;
      for j:=0 to 7 do Cod:=Cod+(Ord(Ch) and IxBit[j] shr(7-j))*IxBit[IxDeCoder[j]];
      Result:=Result+Chr(Cod);
      k:=(k+1) mod baza;
    end;
end;
{-----------------------------------------}
exports
  Coder,DeCoder;

begin
end.
