unit UntMatrixFrequency;
{==========================================================================}
interface
{==========================================================================}
uses
  UntInterface;

type
  tIndex = integer;
  tCount = integer;
  tVector = array of tCount;
  tStrokA = record // Groupe Param
    A: tDataAndResult;
    ptrVector: tVector;
  end;
  tStolB = record // Variable Param
    B: tDataAndResult;
    ix_B: tIndex;
  end;
// -> ��� function DihatSearch
  ixType = (ix_A, ix_B);
  tSearch = record
    ix: tIndex;
    Found: boolean;
  end;
  tDihatSearch = array [ixType] of tSearch;
// ->
  tGroupParam = (_A_, _B_, _not_);
  tDivParam = (_divA_, _divB_, _divALL_);
  tStepen = (_one_, _two_);

  TMatrixFrequency = class
  private
// -> property read method
    TypeGroupParam_A,
    TypeVariableParam_B: tType;
    CountAllElement,
    CountStrokA,
    CountStolB: tCount;
    function DihatSearchForGetMatrElement(A, B: tIndex): tDihatSearch;
    function GetLabelStrokA(ix_A: tIndex): tDataAndResult;
    function GetLabelStolB(ix_B: tIndex): tDataAndResult;
  public
    property CountA:   tCount read CountStrokA;
    property CountB:   tCount read CountStolB;
    property CountAll: tCount read CountAllElement;
    property LabelStrokA[ix_A: tIndex]: tDataAndResult read GetLabelStrokA;
    property LabelStolB[ix_B: tIndex]: tDataAndResult read GetLabelStolB;

    function CumulateFrequency(A, B: tIndex; GroupParametr: tGroupParam): tResultType;
    function CumulateProbability(A, B: tIndex; GroupParametr: tGroupParam; DivParam: tDivParam): tResultType;
    function Mx(index: tIndex; Stepen: tStepen = _one_): tResultType;
    function My: tResultType;
    function Dx(index: tIndex): tResultType;
    function Sx(ix_A: tIndex): tResultType;
    function Sy: tResultType;
    function LineCorrelation: tResultType;
// -> method
    constructor Create(_ClientID_: longint; TGP_A, TVP_B: tType);
    destructor Destroy; override;
    procedure AddElement(A, B: tDataAndResult);
  private
    ClientID: longint;
    ptrStrokA: array of tStrokA;
    ptrStolB: array of tStolB;
    procedure AddStrocA_Sort(A: tDataAndResult; SearchA: tSearch);
    procedure AddStolB_Sort(B: tDataAndResult; SearchB: tSearch);
    function DihatSearch(A, B: tDataAndResult): tDihatSearch;
  end;//TMatrixFrequency

{==========================================================================}
implementation
{==========================================================================}
uses
  SysUtils;

constructor TMatrixFrequency.Create(_ClientID_: longint; TGP_A, TVP_B: tType);
var
  DiscriptorIN: TextFile;
begin
  inherited Create;
  TypeGroupParam_A := TGP_A;
  TypeVariableParam_B  := TVP_B;
  ptrStrokA := nil;
  ptrStolB := nil;
  CountALLElement := 0;
  CountStrokA := 0;
  CountStolB := 0;

  ClientID := _ClientID_;
//  ChDir(First.DataPath);
  AssignFile(DiscriptorIN, 'DataClient_'+IntToStr(ClientID)+'.dar');
{$I-} Reset(DiscriptorIN); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                           +'[TMatrixFrequency.Create;]'#13#10
                           +'EXCEPTION: �� ������ ���� ������ SQL.dar (�������),'+#13#10
                           +'����������: �������� ���� SQL.dar �� ������ ������� �� ������,'+#13#10
                           +'   �������: 1. sm_NewDataFile(ClientID: Integer);'+#13#10
                           +'            2. sm_AddDataFile(ClientID: Integer; const Str: WideString);');
  UntInterface.FoundBlok(DiscriptorIN, system_blok, _first_);
  UntInterface.FoundBlok(DiscriptorIN, data_blok, _first_);
  UntInterface.FoundBlok(DiscriptorIN, end_file, _first_);
  CloseFile(DiscriptorIN);
end;//TMatrixFrequency.Create

destructor TMatrixFrequency.Destroy;
var
  i: integer;
begin
  for i := 0 to Length(ptrStrokA) - 1 do
    SetLength(ptrStrokA[i].ptrVector, 0);
  SetLength(ptrStrokA, 0);
  SetLength(ptrStolB, 0);
  inherited Destroy;
end;//TMatrixFrequency.Destroy

// -> ��������������� ������ ��� ���� property ----------------------------

function TMatrixFrequency.DihatSearchForGetMatrElement(A, B: tIndex): tDihatSearch;
begin
  Result[ix_A].ix := 0;
  Result[ix_A].Found := False;
  Result[ix_B].ix := 0;
  Result[ix_B].Found := False;

  if (0 <= A) and (A <= Length(ptrStrokA)) and (0 <= B) and (B <= Length(ptrStolB)) then
    begin
      Result[ix_A].ix := round(A);
      Result[ix_A].Found := True;
      Result[ix_B].ix := round(B);
      Result[ix_B].Found := True;
    end
  else
    raise Exception.Create('������ ������� ['+FloatToStr(A)+','+FloatToStr(B)+'] � ������� �� ����������');
end;//TMatrixFrequency.DihatSearchForGetMatrElement

// -> property BEGIN ------------------------------------------------------

function TMatrixFrequency.CumulateFrequency(A, B: tIndex; GroupParametr: tGroupParam): tResultType;
var
  Search: tDihatSearch;
  i, k: tIndex;
begin
  if (A = CountStrokA) and (B = CountStolB) then
    begin
      Result := CountAll;
      Exit;
    end;
  if A = CountStrokA then // B <> CountStolB
    begin
      Result := 0;
      Case GroupParametr of
        _A_: for k := 0 to B do
               for i := 0 to CountStrokA - 1 do
                 Result := Result + ptrStrokA[i].ptrVector[ptrStolB[k].ix_B];
        _B_:   raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                       +'[TMatrixFrequency.CumulateFrequency;]'#13#10
                       +'EXCEPTION: �������� �� ����� ������');
        _not_: for i := 0 to CountStrokA - 1 do
                 Result := Result + ptrStrokA[i].ptrVector[ptrStolB[B].ix_B];
      end;//case
      Exit;
    end;
  if B = CountStolB then // A <> CountStrokA
    begin
      Result := 0;
      Case GroupParametr of
        _A_: raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                       +'[TMatrixFrequency.CumulateFrequency;]'#13#10
                       +'EXCEPTION: �������� �� ����� ������');
        _B_: for k := 0 to A do
               for i := 0 to CountStolB - 1 do
                 Result := Result + ptrStrokA[k].ptrVector[ptrStolB[i].ix_B];
        _not_: for i := 0 to CountStolB - 1 do
                 Result := Result + ptrStrokA[A].ptrVector[ptrStolB[i].ix_B];
      end;//case
      Exit;
    end;
  Search := DihatSearchForGetMatrElement(A, B);
// -> ���� ������� �� ������, �� raise -> Search[_x_].found = TRUE
  Result := 0;
  Case GroupParametr of
    _A_: for i := 0 to Search[ix_B].ix do
           Result := Result + ptrStrokA[Search[ix_A].ix].ptrVector[ptrStolB[i].ix_B];
    _B_: for i := 0 to Search[ix_A].ix do
           Result := Result + ptrStrokA[i].ptrVector[ptrStolB[Search[ix_B].ix].ix_B];
    _not_: Result := ptrStrokA[Search[ix_A].ix].ptrVector[ptrStolB[Search[ix_B].ix].ix_B];
  end;//case
end;//TMatrixFrequency.GetMatrIndex

function TMatrixFrequency.CumulateProbability(A, B: tIndex; GroupParametr: tGroupParam; DivParam: tDivParam): tResultType;
var
  Count: tResultType;
begin
  if B = CountStolB then
    begin
      Result := 1;
      Exit;
    end;
  Result := CumulateFrequency(A, B, GroupParametr);
  case DivParam of
    _divA_:   Count := CumulateFrequency(A, CountStolB, _not_);
    _divB_:   Count := CumulateFrequency(CountStrokA, B, _not_);
    _divALL_: Count := CountAll;
  end;//Case
  Result := Result/Count;
  UntInterface.RoundResulToEpsilon(Result, UntInterface.cEpsilon);
end;//TMatrixFrequency.CumulateProbability

function TMatrixFrequency.GetLabelStrokA(ix_A: tIndex): tDataAndResult;
begin
  if (0 <= ix_A) and (ix_A < Length(ptrStrokA)) then
    Result := ptrStrokA[ix_A].A
  else
    raise Exception.Create('������ ������� ['+IntToStr(ix_A)+'] �� ����������');
end;//TMatrixFrequency.GetLabelStrokA

function TMatrixFrequency.GetLabelStolB(ix_B: tIndex): tDataAndResult;
begin
  if (0 <= ix_B) and (ix_B < Length(ptrStolB)) then
    Result := ptrStolB[ix_B].B
  else
    raise Exception.Create('������ ������� ['+IntToStr(ix_B)+'] �� ����������');
end;//TMatrixFrequency.GetLabelStolB

function TMatrixFrequency.Mx(index: tIndex; Stepen: tStepen =_one_): tResultType;
var
  x: tResultType;
  i: tIndex;
begin
  if TypeVariableParam_B = _string_ then
    raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                           +'[TMatrixFrequency.Mx;]'#13#10
                           +'EXCEPTION: ���. �������� �� ���������� �������� Variable �� ������������'+#13#10
                           +'����������: �������� ��� Variable ��������� ����������');
  if (0 <= index) and (index <= Length(ptrStrokA)) then
    begin
      Result := 0;
      for i := 0 to CountStolB - 1 do
        begin
          x := LabelStolB[i].realParam;
          if Stepen = _two_ then
            x := sqr(x);
          Result := Result + x*CumulateProbability(index, i, _not_, _divA_);
        end;
      UntInterface.RoundResulToEpsilon(Result, UntInterface.cEpsilon);
    end
  else
    raise Exception.Create('������ ������� ['+IntToStr(index)+'] �� ����������');
end;//TMatrixFrequency.GetMx

function TMatrixFrequency.My: tResultType;
var
  i: tIndex;
begin
  Result := 0;
  for i := 0 to CountStrokA - 1 do
    Result := Result + LabelStrokA[i].realParam*CumulateFrequency(i, CountStolB, _not_)/CountAll;
end;//TMatrixFrequency.My

function TMatrixFrequency.Dx(index: tIndex): tResultType;
begin
  Result := Mx(index, _two_) - sqr(Mx(index));
  UntInterface.RoundResulToEpsilon(Result, UntInterface.cEpsilon);
end;//TMatrixFrequency.Dx

function TMatrixFrequency.Sx(ix_A: tIndex): tResultType;
var
  n: tResultType;
begin
  Result := Dx(ix_A); // ix_A ����� ����� ��������� �� ����� (exception)
  if (0 <= ix_A) and (ix_A < Length(ptrStrokA)) then
    n := CumulateFrequency(ix_A, CountStolB - 1, _A_);
  if ix_A = Length(ptrStrokA) then
    n := CountAll;
  if n <> 1 then // ����� �� ������ �� ����
    Result := n/(n-1)*Result;
  UntInterface.RoundResulToEpsilon(Result, UntInterface.cEpsilon);
end;//TMatrixFrequency.Sx

function TMatrixFrequency.Sy: tResultType;
var
  i: tIndex;
begin
  Result := 0;
  for i := 0 to CountStrokA - 1 do
    Result := Result + sqr(LabelStrokA[i].realParam)*CumulateFrequency(i, CountStolB, _not_)/CountAll;
  Result := Result - sqr(My);
  if CountAll <> 1 then
    Result := CountAll/(CountAll-1)*Result;
end;//TMatrixFrequency.Sy

function TMatrixFrequency.LineCorrelation: tResultType;
var
  Sxx, Syy: tResultType;
  i, j: tIndex;
begin
  if (TypeGroupParam_A = _string_) or (TypeVariableParam_B = _string_) then
    raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                           +'[TMatrixFrequency.Correlation;]'#13#10
                           +'EXCEPTION: ���������� �� ���������� �������� �� ������������'+#13#10
                           +'����������: �������� ��� Variable � Var2 ��������� ����������');
  Result := 0;
  for i := 0 to CountStrokA - 1 do
    for j := 0 to CountStolB - 1 do
{Mxy} Result := Result + LabelStrokA[i].realParam*LabelStolB[j].realParam*CumulateProbability(i, j, _not_, _divALL_);
  Result := Result - Mx(CountStrokA)*My; {Mxy - Mx*My}
  Sxx := sqrt(Sx(CountStrokA));
  Syy := sqrt(Sy);
  if (Result = 0) and ((Sxx = 0)or(Syy = 0)) then
    begin
      Result := 1;
      Exit;
    end;
  if Sxx <> 0 then
    Result := Result/Sxx;
  if Syy <> 0 then
    Result := Result/Syy;
  UntInterface.RoundResulToEpsilon(Result, UntInterface.cEpsilon);
end;//TMatrixFrequency.LineCorrelation

// -> property END --------------------------------------------------------

procedure TMatrixFrequency.AddElement(A, B: tDataAndResult);
var
  Search: tDihatSearch;
begin
  Search := DihatSearch(A, B);
  AddStrocA_Sort(A, Search[ix_A]);
  AddStolB_Sort(B, Search[ix_B]);
  inc(ptrStrokA[Search[ix_A].ix].ptrVector[ptrStolB[Search[ix_B].ix].ix_B]);
  inc(CountAllElement);
end;//TMatrixFrequency.AddElement

procedure TMatrixFrequency.AddStrocA_Sort(A: tDataAndResult; SearchA: tSearch);
var
  i: tIndex;
begin
  if not SearchA.Found then
    begin
      SetLength(ptrStrokA, Length(ptrStrokA) + 1);
// -> MoveTo
      for i := Length(ptrStrokA) - 1 downto SearchA.ix + 1 do
        ptrStrokA[i] := ptrStrokA[i-1];

      ptrStrokA[SearchA.ix].A := A;
      SetLength(ptrStrokA[SearchA.ix].ptrVector, Length(ptrStolB) + 1);
      for i := 0 to Length(ptrStolB) - 1 do
        ptrStrokA[SearchA.ix].ptrVector[i] := 0;
      inc(CountStrokA);
    end;
end;//TMatrixFrequency.AddStrocA_Sort

procedure TMatrixFrequency.AddStolB_Sort(B: tDataAndResult; SearchB: tSearch);
var
  i: tIndex;
begin
  if not SearchB.Found then
    begin
      SetLength(ptrStolB, Length(ptrStolB) + 1);
// -> MoveTo
      for i := Length(ptrStolB) - 1 downto SearchB.ix + 1 do
        ptrStolB[i] := ptrStolB[i-1];

      ptrStolB[SearchB.ix].B := B;
      ptrStolB[SearchB.ix].ix_B := Length(ptrStolB) - 1;
      for i := 0 to Length(ptrStrokA) - 1 do
        begin
          SetLength(ptrStrokA[i].ptrVector, Length(ptrStolB) + 1);
          ptrStrokA[i].ptrVector[Length(ptrStolB) - 1] := 0;
        end;
      inc(CountStolB);
    end;
end;//TMatrixFrequency.AddStolB_Sort

function TMatrixFrequency.DihatSearch(A, B: tDataAndResult): tDihatSearch;
var
  i: tIndex;
begin
  Result[ix_A].ix := 0;
  Result[ix_A].Found := False;
  Result[ix_B].ix := 0;
  Result[ix_B].Found := False;

  if ptrStrokA <> nil then
    begin
      for i := 0 to Length(ptrStrokA) - 1 do
        begin
          Case TypeGroupParam_A of
            _numeric_:
              begin
                if ptrStrokA[i].A.realParam < A.realParam then
                  Continue;
                if ptrStrokA[i].A.realParam = A.realParam then
                  Result[ix_A].Found := True;
                Break;
              end;
            _string_:
              begin
                if ptrStrokA[i].A.strParam < A.strParam then
                  Continue;
                if ptrStrokA[i].A.strParam = A.strParam then
                  Result[ix_A].Found := True;
                Break;
              end;
          end;//Case
        end;//for i
      Result[ix_A].ix := i;
    end;

  if ptrStolB <> nil then
    begin
      for i := 0 to Length(ptrStolB) - 1 do
        begin
          Case TypeVariableParam_B of
            _numeric_:
              begin
                if ptrStolB[i].B.realParam < B.realParam then
                  Continue;
                if ptrStolB[i].B.realParam = B.realParam then
                  Result[ix_B].Found := True;
                Break;
              end;
            _string_:
              begin
                if ptrStolB[i].B.strParam < B.strParam then
                  Continue;
                if ptrStolB[i].B.strParam = B.strParam then
                  Result[ix_B].Found := True;
                Break;
              end;
          end;//Case
        end;// for i
      Result[ix_B].ix := i;
    end;
end;//TMatrixFrequency.DihatSearch

END.
