unit UntInterface;
                        INTERFACE

Const
// ���������� � ���������� ������ � ������ Data And Result, *.dar
  system_blok = '// -> system information';
    GraphResult = 'GraphResult';
    DataResult = 'DataResult';
  data_blok = '// -> data information';
  graph_blok = '// ---> graphics element information';
  rem_blok ='// ---> rem element';
  comment_blok = '// -> comment information';
  end_file = '// -> end file';
  DARSeparator = ';'; // ����������� ����� �������
// ����������� ����������
  cEpsilon = 0.00001;
  cAlpha = 0.05;

Type
  tResultType = Real;
  tType = (_numeric_, _string_);
  tFlag = (_first_, _next_);

  tDataAndResult = record
                     Case tType of
                       _numeric_: (realParam: tResultType);
                       _string_:  (strParam:  string[20]);
                   end;
  tSupplementID = (
// -> class TSupplements (�������� ����������)
            _fGaus_                  {���������� 1},
            _xGaus_                  {���������� 1},
            _fLaplas_                {���������� 2},
            _xLaplas_                {���������� 2},
            _t_                      {���������� 3},
            _q_                      {���������� 4},
            _xPirson_criticle_point_ {���������� 5},
            _xStudetn_criticle_point_{���������� 6}
  );

  tStatMethodID = (
// -> class TMatrixFrequency (�������� ����������)
{01}         _MatrixFrequency_{���������� ������},
{02}         _MatrixProbability_{������������ �����������},
{--}{c. 229}//      _Hypothesis_SUPPOSED_Probability_{������������� �� ���. ������������},
{03}{�. 157} _Mx_{�� ���������},
{--}{c. 218}//      _Hypothesis_SUPPOSED_Mx_normal_sharing_{������������� �� ���. ������������},
{!!}{c. 251}{!}   _CRITERION_Pirson_hypothesis_NORMAL_sharing_{���������� �������������},
{04}{�. 158} _Dx_{c�������� ������},
{04}{�. 158}// _Sx_{������������ ��������� (����������� ������)},
{--}{c. 210}//      _Hypothesis_SUPPOSED_Sx_{������������� �� ���. ������������},
{05}{�. 190} _LineCorrelation_coef_{�����������},
{--}{�. 239}//      _Hypothesis_SIGNIFICANT_LineCorrelation_{����������},
{06}{�. 201} _SpirmenRangCorrelation_coef_{�������� ����������},
{--}{�. 244}//      _Hypothesis_SIGNIFICANT_SpirmentRangCorrelation_{����������},
{07}{�. 204} _KendallRangCorrelation_coef_,
{--}{�. 246}//      _Hypothesis_SIGNIFICANT_KendallRangCorrelation_{����������}
// �������
{08}         _Poligon_,
{09}         _Fx_,//������������ �-�� �������������
{10}         _Px_//������������ ���������
);

function FoundBlok(var Discriptor: TextFile; BlokName: string; Flag: tFlag): boolean;
procedure RoundResultoEpsilon(var Result: tResultType; Epsilon: real);

                        IMPLEMENTATION
uses
  SysUtils;

function FoundBlok(var Discriptor: TextFile; BlokName: string; Flag: tFlag): boolean;
var
  Str: string;
begin
  Result := False;
  if Flag = _first_ then
    Reset(Discriptor);
  while not EOF(Discriptor) do
    begin
      ReadLn(Discriptor, Str);
      if Str = BlokName then
        begin
          Result := True;
          Exit;
        end;
    end;
  if not Result then
    begin
      CloseFile(Discriptor);
      raise Exception.Create('����� �������������: UntInterface'+#13#10
                             +'[FoundBlok;]'#13#10
                             +'EXCEPTION: �� ������ ���� ������ '+BlokName+' � SQL.dar (�������),'+#13#10
                             +'����������: ����������� ���������� SQL.dar ���� (����������� �������� ����������)');
    end;
end;//FoundBlok

procedure RoundResultoEpsilon(var Result: tResultType; Epsilon: real);
begin
  Result :=       Result /Epsilon/10;
  Result := round(Result)*Epsilon*10;
end;//RoundResultoEpsilon

END.

