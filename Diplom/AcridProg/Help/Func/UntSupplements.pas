unit UntSupplements;

                        interface
uses
  UntInterface,
  SysUtils, Dialogs;

const
  cEpsilonForGaus = 0.0001;
  cEpsilonForGammaFunc = 0.001;

type
  tFunc = function (x: tResultType; param: tResultType = 0): tResultType of object;

  THelpMethod = class
  public
    function GausMethod (F: tFunc;    a, b: tResultType; ParamF: tResultType = 0): tResultType;
    function DihatMethod(F: tFunc; Y, a, b: tResultType; ParamF: tResultType = 0): tResultType;
// -> ������ ����� !!!
    function fGamma(x: tResultType): tResultType;
    function fFactorial(x: tResultType): tResultType;
  private
    function FuncForGamma(x, p: tResultType): tResultType;
  end;//THelpMethod

  TDensityDistribution = class {��������� �������������}
  public
    constructor Create;
    destructor  Destroy; virtual;
    function Density_Binomial(k, n, p: tResultType): tResultType;
    function Density_Exponential(r, x: tResultType): tResultType;
    function Density_Geometric(k, p: tResultType): tResultType;
    function Density_Log_Normal(x, a, s: tResultType): tResultType;
    function Density_Normal(x, a, s: tResultType): tResultType;
    function Density_Poisson(k, l: tResultType): tResultType;
    function Density_CHI_Squared_Pirson(x, d: tResultType): tResultType;
    function Density_T_Students(x, d: tResultType): tResultType;
  private
    clHelpMethod: THelpMethod;
  end;//TDensityDistribution

  TSupplements = class
  public
    constructor Create;
    destructor Destroy; virtual;
    function GetResult(SupplementMethod: tSupplementID; x: tResultType; DegreesOfFreedom: integer=0): tResultType;
    function HypothesisT_LineCorrelation(R: tResultType; Count: Integer): tResultType;
  private
    clHelpMethod: THelpMethod;
    function fGaus(x: tResultType; param: tResultType=0): tResultType;
    function fLaplas(x: tResultType; param: tResultType=0): tResultType;
    function xPirsonCriticlePoint(alpha: tResultType; DegreesOfFreedom: Integer): tResultType;
      function FuncForGaus_PirsonCriticlePoint (x, p: tResultType): tResultType;
      function FuncForDihat_PirsonCriticlePoint(x, p: tResultType): tResultType;
    function xStudentCriticlePoint(alpha: tResultType; DegreesOfFreedom: Integer): tResultType;
      function FuncForGaus_StudentCriticlePoint (x, p: tResultType): tResultType;
      function FuncForDihat_StudentCriticlePoint(x, p: tResultType): tResultType;
  end;//TSupplements
{ ----------------------------------------------------------------------- }
                        implementation
{ ----------------------------------------------------------------------- }

function THelpMethod.GausMethod(F: tFunc; a, b: tResultType; ParamF: tResultType=0): tResultType;
const
  q: array [1..4] of tResultType = ( 0.3478548451, 0.6521451549,0.6521451549,0.3478548451);
  t: array [1..4] of tResultType = (-0.8611363116,-0.3399810436,0.3399810436,0.8611363116);
var
  s, s2, h, baza, s_save: tResultType;
  i, j, n, CountIteration: longint;
begin
  s := 0;
  n := 1;
  CountIteration := 0;
  repeat
    s_save := s;
    h := (b-a)/n;
    baza := a;
    s := 0;
    for i := 1 to n do
      begin s2 := 0;
        for j := 1 to 4 do
          s2 := s2 + q[j] * F((2*baza+h)/2 + t[j]*h/2, paramF);
        s := s + s2*h/2;
        baza := baza + h
      end;{for i}
    n := n*2;
    inc(CountIteration);
    if CountIteration = 1000 then
      begin
        raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                               +'[THelpMethod.GausMethod;]'#13#10
                               +'EXCEPTION: �� �������� �������� (������ ������� ����������)');
        Exit;
      end;
  until abs(s - s_save) <= cEpsilonForGaus;
  Result := s;
end;//THelpMethod.GausMethod

function THelpMethod.DihatMethod(F: tFunc; Y, a, b: tResultType; ParamF: tResultType=0): tResultType;
var
  x, Frez: tResultType;
  CountIteration: longint;
begin
  CountIteration := 0;
  repeat
    x := (a+b)/2;
    Frez := F(x, ParamF);
    if (Frez - Y) >= 0 then b := x else a := x;
    inc(CountIteration);
    if CountIteration = 1000 then
      begin
        raise Exception.Create('����� �������������: AcridStatSoftServer'+#13#10
                               +'[THelpMethod.DihatMethod;]'#13#10
                               +'EXCEPTION: �� �������� �������� (������ ������� ����������)');
        Exit;
      end;
  until (b-a) < cEpsilon;
  Result := x;
end;//THelpMethod.DihatMethod

function THelpMethod.FuncForGamma(x, p: tResultType): tResultType;
begin
  Result := exp((p-1)*ln(x))*exp(-x); //(t^(z-1))*e^(-t);
end;//THelpMethod.FuncForGamma

function THelpMethod.fGamma(x: tResultType): tResultType;
const
// -> ������ �����
  infinityLage = 100;
  infinitySmal = 10;
begin
  if x >= 1 then
    Result := GausMethod(FuncForGamma, 0, infinityLage, x)
  else
    Result := GausMethod(FuncForGamma, 0, infinitySmal, x)
end;//THelpMethod.fGamma

function THelpMethod.fFactorial(x: tResultType): tResultType;
begin
  if (x = 0) or (x = 1) then
    Result := 1
  else
    Result := x * fFactorial(x-1);
end;//THelpMethod.fFactorial
{ ----------------------------------------------------------------------- }

constructor TDensityDistribution.Create;
begin
  inherited Create;
  clHelpMethod := THelpMethod.Create;
end;

destructor TDensityDistribution.Destroy;
begin
  clHelpMethod.Destroy;
  inherited Destroy;
end;

function TDensityDistribution.Density_Binomial(k, n, p: tResultType): tResultType;
begin with clHelpMethod do Result := fFactorial(n)/fFactorial(k)/fFactorial(n-k)*exp(k*ln(p))*exp((n-k)*ln(1-p)); end;

function TDensityDistribution.Density_Exponential(r, x: tResultType): tResultType;
begin Result := r*exp(-r*x); end;

function TDensityDistribution.Density_Geometric(k, p: tResultType): tResultType;
begin Result := p*exp(k*ln(1-p)); end;

function TDensityDistribution.Density_Log_Normal(x, a, s: tResultType): tResultType;
begin Result := 1/sqrt(2*Pi)/s/x*exp((-1)/2/sqr(s)*sqr(ln(x)-a)); end;

function TDensityDistribution.Density_Normal(x, a, s: tResultType): tResultType;
begin Result := 1/sqrt(2*Pi)/s*exp((-1)/2/sqr(s)*sqr(x-a)); end;

function TDensityDistribution.Density_Poisson(k, l: tResultType): tResultType;
begin Result := exp(k*ln(l)/clHelpMethod.fFactorial(k)*exp(-l)); end;

function TDensityDistribution.Density_CHI_Squared_Pirson(x, d: tResultType): tResultType;
begin Result := exp(-x/2)/2/clHelpMethod.fGamma(d/2)*exp((d/2-1)*ln(x/2)); end;

function TDensityDistribution.Density_T_Students(x, d: tResultType): tResultType;
begin with clHelpMethod do Result := fGamma((d+1)/2)/fGamma(d/2)/sqrt(Pi*d)*exp((-(d+1)/2)*ln(1+sqr(x)/d)); end;

{ ----------------------------------------------------------------------- }

function TSupplements.HypothesisT_LineCorrelation(R: tResultType; Count: Integer): tResultType;
var
  T: tResultType;
  DegreesOfFreedom: Integer;
begin
  Result := - 1;
  DegreesOfFreedom := Count - 2;
  if (DegreesOfFreedom > 0) and (abs(R) <> 1) then
    begin
      Result := abs(R)*sqrt(DegreesOfFreedom)/sqrt(1 - sqr(R));
      T := xStudentCriticlePoint(UntInterface.cAlpha, DegreesOfFreedom);
      Result := Result - T;
   end;
  UntInterface.RoundResultoEpsilon(Result, UntInterface.cEpsilon);
end;//TSupplements.HypothesisT_LineCorrelation

constructor TSupplements.Create;
begin
  inherited Create;
  clHelpMethod := THelpMethod.Create;
end;//function TSupplements.Create

destructor TSupplements.Destroy;
begin
  clHelpMethod.Destroy;
  inherited Destroy;
end;//function TSupplements.Destroy

function TSupplements.fGaus (x: tResultType; param: tResultType=0): tResultType;
begin // e, param  ����� ��� ���������� � ���� tFunc
  Result := exp(-sqr(x)/2)/sqrt(2*Pi);
end;//TSupplements.fGaus

function TSupplements.fLaplas (x: tResultType; param: tResultType=0): tResultType;
begin // param  ����� ��� ���������� � ���� tFunc
  Result := clHelpMethod.GausMethod(fGaus, 0, x);
end;//TSupplements.fLaplas

function TSupplements.FuncForGaus_PirsonCriticlePoint(x, p: tResultType): tResultType;
begin
  Result := 1/2*exp(-1/2*x)*exp((p/2-1)*ln(x/2));
end;//TSupplements.FuncForGaus_PirsonCriticlePoint

function TSupplements.FuncForDihat_PirsonCriticlePoint(x, p: tResultType): tResultType;
begin
  with clHelpMethod do
    Result := GausMethod(FuncForGaus_PirsonCriticlePoint, 0, x, p)/fGamma(p/2);
end;//TSupplements.FuncForDihat_PirsonCriticlePoint

function TSupplements.xPirsonCriticlePoint(alpha: tResultType; DegreesOfFreedom: Integer): tResultType;
const
  a = 0;
var
  b: tResultType; // ��������, �� ������� ���� ����������� �����
begin
  b := 2*DegreesOfFreedom + 5; // �� ����� ���!!!
  Result := clHelpMethod.DihatMethod(FuncForDihat_PirsonCriticlePoint, 1-alpha, a, b, DegreesOfFreedom)
end;//TSupplements.xPirsonCriticlePoint

function TSupplements.FuncForGaus_StudentCriticlePoint(x, p: tResultType): tResultType;
begin
  Result := exp((-1/2*(p+1))*ln(1+sqr(x)/p));
end;//TSupplements.FuncForGaus_StudentCriticlePoint

function TSupplements.FuncForDihat_StudentCriticlePoint(x, p: tResultType): tResultType;
const
  infinity = -100;
begin
  with clHelpMethod do
    Result := GausMethod(FuncForGaus_StudentCriticlePoint, infinity, x, p)*fGamma((p+1)/2)/fGamma(p/2)/sqrt(Pi*p);
end;//TSupplements.FuncForDihat_StudentCriticlePoint

function TSupplements.xStudentCriticlePoint(alpha: tResultType; DegreesOfFreedom: Integer): tResultType;
const
  a = 0; b = 15; // ��������, �� ������� ���� ����������� �����
var
  StrAlpha: String;
begin
  StrAlpha := FloatToStr(alpha); // � if alpha = 0.05 �� �������� !!!
  if DegreesOfFreedom = 1 then
    begin
      Result := 12.7;
      if StrAlpha = '0,01' then
        Result := 6.31;
      if StrAlpha = '0,02' then
        Result := 31.82;
      if StrAlpha = '0,01' then
        Result := 63.7;
      Exit;
    end;
  if DegreesOfFreedom <= 30 then
    begin
      Result := clHelpMethod.DihatMethod(FuncForDihat_StudentCriticlePoint, 1-alpha/2, a, b, DegreesOfFreedom);
      Exit;
    end;

  if DegreesOfFreedom <= 60 then
    begin
      Result := 2;
      if StrAlpha = '0,01' then
        Result := 1.67;
      if StrAlpha = '0,02' then
        Result := 2.39;
      if StrAlpha = '0,01' then
        Result := 2.66;
    end
  else
    begin
      Result := 1.96;
      if StrAlpha = '0,01' then
        Result := 1.64;
      if StrAlpha = '0,02' then
        Result := 2.33;
      if StrAlpha = '0,01' then
        Result := 2.58;
    end;
end;//TSupplements.xStudentCriticlePoint

function TSupplements.GetResult(SupplementMethod: tSupplementID; x: tResultType; DegreesOfFreedom: integer=0): tResultType;
begin
  Result := 0;
  Case SupplementMethod of
    _fGaus_                  {���������� 1}: Result := fGaus(x);
    _xGaus_                  {���������� 1}: Result := sqrt(-2*ln(x*sqrt(2*Pi)));
    _fLaplas_                {���������� 2}: Result := fLaplas(x);
    _xLaplas_                {���������� 2}: Result := clHelpMethod.DihatMethod(fLaplas, x, 0, 10);
    _t_                      {���������� 3}: Result := 0;
    _q_                      {���������� 4}: Result := 0;
    _xPirson_criticle_point_ {���������� 5}: Result := xPirsonCriticlePoint(x, DegreesOfFreedom);
    _xStudetn_criticle_point_{���������� 6}: Result := xStudentCriticlePoint(x, DegreesOfFreedom);
  end;//Case
  Result :=        Result/cEpsilon/10;
  Result := round(Result)*cEpsilon*10;
end;//TSupplements.GetResult

END.

