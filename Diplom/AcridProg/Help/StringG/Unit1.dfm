object Form1: TForm1
  Left = 192
  Top = 107
  Width = 544
  Height = 375
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 124
    Height = 13
    Caption = 'RowCount ('#1082#1086#1083'-'#1074#1086' '#1089#1090#1088#1086#1082')'
  end
  object Label2: TLabel
    Left = 24
    Top = 32
    Width = 96
    Height = 13
    Caption = 'ColCount('#1089#1090#1086#1083#1073#1094#1086#1074')'
  end
  object Label3: TLabel
    Left = 24
    Top = 56
    Width = 122
    Height = 13
    Caption = 'DefaultRowHeith('#1089#1090#1088#1086#1082#1080')'
  end
  object Label4: TLabel
    Left = 24
    Top = 80
    Width = 124
    Height = 13
    Caption = 'DefaultColWidth('#1089#1090#1086#1083#1073#1094#1072')'
  end
  object edtRowCount: TEdit
    Left = 160
    Top = 0
    Width = 65
    Height = 21
    TabOrder = 0
    Text = '2'
  end
  object edtColCount: TEdit
    Left = 160
    Top = 24
    Width = 65
    Height = 21
    TabOrder = 1
    Text = '3'
  end
  object edtDefaultRowHeith: TEdit
    Left = 160
    Top = 48
    Width = 65
    Height = 21
    TabOrder = 2
    Text = '10'
  end
  object edtDefaultCalWidth: TEdit
    Left = 160
    Top = 72
    Width = 65
    Height = 21
    TabOrder = 3
    Text = '20'
  end
  object Button1: TButton
    Left = 232
    Top = 8
    Width = 75
    Height = 25
    Caption = #1056#1072#1079#1084#1077#1088#1099
    TabOrder = 4
    OnClick = Button1Click
  end
  object sgDemo: TStringGrid
    Left = 16
    Top = 103
    Width = 193
    Height = 226
    ColCount = 3
    DefaultColWidth = 60
    DefaultRowHeight = 20
    RowCount = 10
    TabOrder = 5
    RowHeights = (
      20
      20
      20
      20
      20
      20
      20
      20
      20
      20)
  end
  object Button2: TButton
    Left = 376
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 6
    OnClick = Button2Click
  end
  object edtIXstr: TEdit
    Left = 304
    Top = 56
    Width = 49
    Height = 21
    TabOrder = 7
    Text = '1'
  end
  object edtIXStolb: TEdit
    Left = 304
    Top = 80
    Width = 49
    Height = 21
    TabOrder = 8
    Text = '2'
  end
end
