unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edtRowCount: TEdit;
    edtColCount: TEdit;
    edtDefaultRowHeith: TEdit;
    edtDefaultCalWidth: TEdit;
    Button1: TButton;
    sgDemo: TStringGrid;
    Label3: TLabel;
    Label4: TLabel;
    Button2: TButton;
    edtIXstr: TEdit;
    edtIXStolb: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  with sgDemo do
    begin
      ColCount := StrToInt(edtColCount.Text);
      RowCount := StrToInt(edtRowCount.Text);
      DefaultRowHeight := StrToInt(edtDefaultRowHeith.Text);
      DefaultColWidth := StrToInt(edtDefaultCalWidth.Text);
      Height := RowCount*(DefaultRowHeight + GridLineWidth) + 3;
      Width := ColCount*(DefaultColWidth + GridLineWidth) + 3;
   end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  ixStr, ixStolb: Integer;
  X1 ,Y1, X2, Y2: Integer;
begin
  ixStr := StrToInt(edtIXstr.Text);
  ixStolb := StrToInt(edtIXStolb.Text);

  with sgDemo do
    with Canvas do
      begin
        Y1 := ixStr*(DefaultRowHeight + GridLineWidth);
        X1 := ixStolb*(DefaultColWidth + GridLineWidth);
        Y2 := (ixStr+1)*(DefaultRowHeight + GridLineWidth);
        X2 := (ixStolb+1)*(DefaultColWidth + GridLineWidth);

        Pen.Color := clRed;
        Brush.Color := clRed;
        Rectangle(X1, Y1, X2, Y2);
      end;

end;

end.
