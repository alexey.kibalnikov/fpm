unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IniFiles;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  FileINI: TIniFile;
  Str, Path: string;

begin
  GetDir(0, Path);
  Edit1.Text := Path;

  FileINI := TIniFile.Create(Path+'\ConfigMainClient.ini');

  Str := FileINI.ReadString('AcridMainClient','Found','');
  if Str <> 'OK' then
    begin
      raise Exception.Create('����� �������������: AcridMainClient'+#13#10
                            +'[TdmBazaClient.DataModuleCreate;]'+#13#10
                            +'EXCEPTION: ��� �������������,'+#13#10
                            +'����������: ��������� ���� "ConfigMainClient.ini"'#13#10
                            +'� ���������� "..\Windows" �� �������');
    end;

    Edit1.Text := FileINI.ReadString('AcridBazaServer','ComputerName','');
    FileINI.Free;
end;

end.
