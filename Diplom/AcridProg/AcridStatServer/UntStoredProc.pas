unit UntStoredProc;

{$WARN SYMBOL_PLATFORM OFF}
{=========================================================================}
interface
{=========================================================================}
uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, AcridStatSoftServer_TLB, StdVcl, Provider, IBCustomDataSet, IBTable, IBDatabase, IBEvents, Dialogs,
  UntInterface;

type
  TCoClassAcridStatSoftServer = class(TRemoteDataModule, ICoClassAcridStatSoftServer)
    procedure RemoteDataModuleCreate(Sender: TObject);
    procedure RemoteDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  protected
    class
      procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
      procedure sm_NewDataFile(ClientID: Integer); safecall;
      procedure sm_AddDataFile(ClientID: Integer; const Str: WideString); safecall;
      procedure sm_Execute(ClientID: Integer; StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer); safecall;
      function  sm_ClientID: Integer; safecall;
      procedure sm_DestroyClient(ClientID: Integer); safecall;
      procedure sm_MoveResultToClient(ClientID: Integer; StrN: Integer; out StrResult: WideString); safecall;
    public
    { Public declarations }
    end;
{=========================================================================}
implementation
{=========================================================================}

uses
  UntFirst,
  UntExecute;

{$R *.DFM}

class procedure TCoClassAcridStatSoftServer.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TCoClassAcridStatSoftServer.RemoteDataModuleCreate(Sender: TObject);
begin
  inc(First.ClientCount);
  inc(First.ClientID);
  if First.ClientID = 2147483646 then // А вдруг пригодится :o) !!!
    First.ClientID := 0;
  First.TrayIcon.Hint:='AcridStatSoftServer Подключено клиентов: '+IntToStr(First.ClientCount);
end;

procedure TCoClassAcridStatSoftServer.RemoteDataModuleDestroy(Sender: TObject);
begin
  dec(First.ClientCount);
  First.TrayIcon.Hint:='AcridStatSoftServer Подключено клиентов: '+IntToStr(First.ClientCount);
end;

procedure TCoClassAcridStatSoftServer.sm_NewDataFile(ClientID: Integer);
var
  DiscriptorIN: TextFile;
begin
  ChDir(First.DataPath);
  AssignFile(DiscriptorIN, 'DataClient_'+IntToStr(ClientID)+'.dar');
{$I-} ReWrite(DiscriptorIN); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[TCoClassAcridStatSoftServer.sm_NewDataFile;]'#13#10
                           +'EXCEPTION: Блокирован файл DataClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  CloseFile(DiscriptorIN);
end;//TCoClassAcridStatSoftServer.sm_NewDataFile

procedure TCoClassAcridStatSoftServer.sm_AddDataFile(ClientID: Integer; const Str: WideString);
var
  DiscriptorIN: TextFile;
begin
  ChDir(First.DataPath);
  AssignFile(DiscriptorIN, 'DataClient_'+IntToStr(ClientID)+'.dar');
{$I-} Append(DiscriptorIN); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[TCoClassAcridStatSoftServer.sm_AddDataFile;]'#13#10
                           +'EXCEPTION: Блокирован файл DataClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorIN, Str);
  finally
    CloseFile(DiscriptorIN);
  end;
end;//TCoClassAcridStatSoftServer.sm_AddDataFile

procedure TCoClassAcridStatSoftServer.sm_Execute(ClientID: Integer; StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer);
begin
  UntExecute.Execute(ClientID, StatMethodID, TypeGroupParam, ixGroupParam, TypeVariableParam, ixVariableParam, TypeVariableParam2, ixVariableParam2);
end;//TCoClassAcridStatSoftServer.sm_Execute

procedure TCoClassAcridStatSoftServer.sm_DestroyClient(ClientID: Integer);
var
  DiscriptorFile: TextFile;
begin
{$I-}
  ChDir(First.DataPath);
  AssignFile(DiscriptorFile, 'DataClient_'+IntToStr(ClientID)+'.dar');
  Erase(DiscriptorFile);
  ChDir(First.ResultPath);
  AssignFile(DiscriptorFile, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  Erase(DiscriptorFile);
{$I+} // IOResult = 0
end;//TCoClassAcridStatSoftServer.sm_DestroyClient

function  TCoClassAcridStatSoftServer.sm_ClientID: Integer;
begin
  Result := First.ClientID;
end;//TCoClassAcridStatSoftServer.sm_ClientID

procedure TCoClassAcridStatSoftServer.sm_MoveResultToClient(ClientID: Integer; StrN: Integer; out StrResult: WideString);
var
  DiscriptorOUT: Text;
  i: integer;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
{$I-} Reset(DiscriptorOUT); {$I+}
  if IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[TCoClassAcridStatSoftServer.sm_MoveResultToClient;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: 1. Закройте приложение, которое его использует'+#13#10
                           +'            2. ВЫ не отослали запрос на ВЫЧИСЛЕНИЕ'+#13#10
                           +'   команда: sm_Execute(ClientID: Integer; StatMethodID: Integer);');
  try
    for i := 0 to StrN do
      ReadLn(DiscriptorOUT, StrResult);
  finally
    CloseFile(DiscriptorOUT);
  end;
end;//TCoClassAcridStatSoftServer.sm_MoveResultToClient

initialization
  TComponentFactory.Create(ComServer, TCoClassAcridStatSoftServer,
    Class_CoClassAcridStatSoftServer, ciMultiInstance, tmApartment);
end.
