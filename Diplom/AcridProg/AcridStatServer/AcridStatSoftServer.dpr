program AcridStatSoftServer;

uses
  Forms,
  AcridStatSoftServer_TLB in 'AcridStatSoftServer_TLB.pas',
  UntStoredProc in 'UntStoredProc.pas' {CoClassAcridStatSoftServer: TRemoteDataModule},
  UntFirst in 'UntFirst.pas' {First},
  UntInterface in 'UntInterface.pas',
  UntMatrixFrequency in 'UntMatrixFrequency.pas',
  UntSupplements in 'UntSupplements.pas',
  UntExecute in 'UntExecute.pas',
  UntLineCorrelation in 'UntLineCorrelation.pas',
  UntReadWriteInterfaceFile in 'UntReadWriteInterfaceFile.pas';

{$R *.TLB}

{$R *.res}

begin
  Application.Initialize;
  Application.ShowMainForm:=False; 
  Application.CreateForm(TFirst, First);
  Application.Run;
end.
