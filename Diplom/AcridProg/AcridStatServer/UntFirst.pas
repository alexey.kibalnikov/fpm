unit UntFirst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cbClasses, CaptionButton, FormTrayIcon, Menus,
  AppAutoRun,
  IniFiles, ExtCtrls;

type
  TFirst = class(TForm)
    TrayIcon: TFormTrayIcon;
    PMServer: TPopupMenu;
    mrClose: TMenuItem;
    mrRun: TMenuItem;
    AppAutoRun: TAppAutoRun;
    mrAdd: TMenuItem;
    mrDel: TMenuItem;
    mrConfig: TMenuItem;
    ImageAcridBrend: TImage;
    Label1: TLabel;
    Label2: TLabel;
    mrAbaut: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure mrCloseClick(Sender: TObject);
    procedure TrayIconMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure abfShutdown1QueryShutdown(Sender: TObject;
      var CanShutdown: Boolean);
    procedure mrAddClick(Sender: TObject);
    procedure mrDelClick(Sender: TObject);
    procedure mrConfigClick(Sender: TObject);
    procedure mrAbautClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     ClientCount: Integer;
     ClientID: longint;

     DataPath: string; // *.ini
     ResultPath: string;
  end;

VAR
  First: TFirst;

implementation
{$R *.dfm}

procedure TFirst.FormCreate(Sender: TObject);
var
  FileINI: TIniFile;
  ServerPath: string;
begin
  ClientCount := 0;
  ClientID := 0;

  FileINI := TIniFile.Create('ConfigStatSoftServer.ini');
  ServerPath := FileINI.ReadString('AcridStatSoftServer','ServerPath','');
  DataPath := FileINI.ReadString('AcridStatSoftServer','NameDirClientDataFile','TempClientDatFaile');
  ResultPath := FileINI.ReadString('AcridStatSoftServer','NameDirClientResultFile','TempClientResultFile');
  DataPath := ServerPath+'\'+DataPath;
  ResultPath := ServerPath+'\'+ResultPath;
  FileINI.Free;
end;

procedure TFirst.mrCloseClick(Sender: TObject);
begin Close; end;

procedure TFirst.TrayIconMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ClientCount <> 0 then
    First.mrClose.Visible:=False
  else
    First.mrClose.Visible:=True;
end;

procedure TFirst.abfShutdown1QueryShutdown(Sender: TObject; var CanShutdown: Boolean);
begin MessageDlg('Выключаете',mtWarning,[mbOk],0) end;

procedure TFirst.mrAddClick(Sender: TObject);
begin
  if MessageDlg('Запускать при загрузке',mtInformation,[mbYes,mbNo],0)= mrYes then
    AppAutoRun.AutoRun:=True;
end;

procedure TFirst.mrDelClick(Sender: TObject);
begin
  if MessageDlg('Не запускать при загрузке',mtInformation,[mbYes,mbNo],0)= mrYes then
    AppAutoRun.AutoRun:=False;
end;

procedure TFirst.mrConfigClick(Sender: TObject);
var
  FileINI: tIniFile;
  Str: string;
begin
  if ClientCount <> 0 then
    MessageDlg('запустите AcridStatSoftServer.exe АВТОНОМНО',mtInformation,[mbOK],0)
  else
    begin
      FileINI := TIniFile.Create('ConfigStatSoftServer.ini');
      GetDir(0,Str);
      FileINI.WriteString('AcridStatSoftServer','ServerPath', Str);
      FileINI.WriteString('AcridStatSoftServer','NameDirClientDataFile','TempClientDatFaile');
      FileINI.WriteString('AcridStatSoftServer','NameDirClientResultFile','TempClientResultFile');

      if not DirectoryExists(Str+'\TempClientDatFaile') then
        if not CreateDir(Str+'\TempClientDatFaile') then
          raise Exception.Create('НЕ могу создать директорию '+Str+'\TempClientDatFaile');

      if not DirectoryExists(Str+'\TempClientResultFile') then
        if not CreateDir(Str+'\TempClientResultFile') then
          raise Exception.Create('НЕ могу создать директорию '+Str+'\TempClientResultFile');

      MessageDlg('Настройка УСПЕШНО завершена',mtInformation,[mbOK],0);
      FileINI.Free;
    end;
end;

procedure TFirst.mrAbautClick(Sender: TObject);
begin First.Visible := True; end;

end.
