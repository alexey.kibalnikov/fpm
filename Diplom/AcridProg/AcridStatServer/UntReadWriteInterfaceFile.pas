unit UntReadWriteInterfaceFile;

interface
uses
  UntInterface,
  UntMatrixFrequency,
  UntSupplements,
  UntLineCorrelation;

  procedure ReadSystemBlok(var DiscriptorIN: TextFile; var countDataStr: Integer; var countDataStolb: Integer);

  procedure WriteMatrixFrequency(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
  procedure WriteMatrixProbability(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
  procedure WriteMx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam: tType);
  procedure WriteDx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam: tType);
  procedure WriteLineCorrelation(ClientID: Integer; StatMethodID: tStatMethodID; clLineCorrelation: TLineCorrelation; TypeGroupParam, TypeVariableParam, TypeVariableParam2: tType; countDataStr, countDataStolb: Integer; ixGroupParam, ixVariableParam, ixVariableParam2: Integer);
  procedure WritePoligon(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
  procedure WriteFx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
  procedure WritePx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);

implementation
uses
  SysUtils,
  UntFirst;

procedure ReadSystemBlok(var DiscriptorIN: TextFile; var countDataStr: Integer; var countDataStolb: Integer);
var
  Str, Lecsema: string;
  s: Integer;
begin
  UntInterface.FoundBlok(DiscriptorIN, system_blok, _first_);
  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  countDataStr := StrToInt(Lecsema);
  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  countDataStolb := StrToInt(Lecsema);
end;//ReadSystemBlock

procedure WriteMatrixFrequency(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
var
  DiscriptorOUT: TextFile;
  i, j: integer;
  Str: string;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.DataResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во строк');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountB + 2)+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//кол-во элементов в выборке');

    WriteLn(DiscriptorOUT, UntInterface.data_blok);
    for i := 0 to clMatrixFrequency.CountA do // ResultMatr
      begin
        Write(DiscriptorOUT, clMatrixFrequency.Moda(i)+DARSeparator);
        for j := 0 to clMatrixFrequency.CountB do
          Write(DiscriptorOUT, FloatToStr(clMatrixFrequency.CumulateFrequency(i, j, _not_))+DARSeparator);
        WriteLn(DiscriptorOUT);
      end;

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Матрица абсолютных частот');
    for i := 0 to clMatrixFrequency.CountA - 1 do
      begin
        Case TypeGroupParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
        end;//Case
        Write(DiscriptorOUT, Str + DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'Gr*'+DARSeparator+'//Label строк');

    Write(DiscriptorOUT, 'Moda' + DARSeparator);
    for i := 0 to clMatrixFrequency.CountB - 1 do
      begin
        Case TypeVariableParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStolB[i].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStolB[i].strParam;
        end;//Case
        Write(DiscriptorOUT, Str + DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'Var*'+DARSeparator+'//Label столбцов');
    WriteLn(DiscriptorOUT, DARSeparator+'//Rem1');
    WriteLn(DiscriptorOUT, 'Moda - это наиболее часто встречаемое значение'+DARSeparator+'//Rem2');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WriteMatrixFrequency

procedure WriteMatrixProbability(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
var
  DiscriptorOUT: TextFile;
  i, j: integer;
  Str: string;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.DataResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во строк');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountB + 2)+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//кол-во элементов в выборке');

    WriteLn(DiscriptorOUT, UntInterface.data_blok);
    for i := 0 to clMatrixFrequency.CountA do // ResultMatr
      begin
        Write(DiscriptorOUT, FloatToStr(clMatrixFrequency.CumulateFrequency(i, clMatrixFrequency.CountB - 1, _A_))+DARSeparator);
        for j := 0 to clMatrixFrequency.CountB  do
          Write(DiscriptorOUT, FloatToStr(clMatrixFrequency.CumulateProbability(i, j, _not_, _divA_))+DARSeparator);
        WriteLn(DiscriptorOUT);
      end;

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Матрица эмпирических вероятностей');
    for i := 0 to clMatrixFrequency.CountA - 1 do
      begin
        Case TypeGroupParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
        end;//Case
        Write(DiscriptorOUT, Str + DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'Gr*'+DARSeparator+'//Label строк');

    Write(DiscriptorOUT, 'Count' + DARSeparator);
    for i := 0 to clMatrixFrequency.CountB - 1 do
      begin
        Case TypeVariableParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStolB[i].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStolB[i].strParam;
        end;//Case
        Write(DiscriptorOUT, Str + DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'Var*'+DARSeparator+'//Label столбцов');
    WriteLn(DiscriptorOUT, DARSeparator+'//Rem1');
    WriteLn(DiscriptorOUT, DARSeparator+'//Rem2');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WriteMatrixProbability

procedure WriteMx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam: tType);
var
  DiscriptorOUT: TextFile;
  i: integer;
  Str: string;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.DataResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во строк');
    WriteLn(DiscriptorOUT, '2'+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//кол-во элементов в выборке');

    WriteLn(DiscriptorOUT, UntInterface.data_blok);
    for i := 0 to clMatrixFrequency.CountA  do // ResultMatr
      begin
        Str := FloatToStr(clMatrixFrequency.CumulateFrequency(i, clMatrixFrequency.CountB, _not_));
        WriteLn(DiscriptorOUT, Str+DARSeparator+FloatToStr(clMatrixFrequency.Mx(i))+DARSeparator);
      end;

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Матрица математических ожиданий');
    for i := 0 to clMatrixFrequency.CountA - 1 do
      begin
        Case TypeGroupParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
        end;//Case
        Write(DiscriptorOUT, Str + DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'Gr*'+DARSeparator+'//Label строк');
    WriteLn(DiscriptorOUT, 'Count'+DARSeparator+'Mx'+DARSeparator+'//Label столбцов');
    WriteLn(DiscriptorOUT, DARSeparator+'//Rem1');
    WriteLn(DiscriptorOUT, 'Mx- абсолютно корректная оценка'+DARSeparator+'//Rem2');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WriteMx

procedure WriteDx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam: tType);
var
  DiscriptorOUT: TextFile;
  i: integer;
  Str: string;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.DataResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во строк');
    WriteLn(DiscriptorOUT, '3'+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//кол-во элементов в выборке');

    WriteLn(DiscriptorOUT, UntInterface.data_blok);
    for i := 0 to clMatrixFrequency.CountA  do // ResultMatr
      begin
        Str := FloatToStr(clMatrixFrequency.CumulateFrequency(i, clMatrixFrequency.CountB, _not_));
        WriteLn(DiscriptorOUT, Str+DARSeparator+FloatToStr(clMatrixFrequency.Dx(i))+DARSeparator+FloatToStr(clMatrixFrequency.Sx(i))+DARSeparator);
      end;
    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Матрица дисперсий');
    for i := 0 to clMatrixFrequency.CountA - 1 do
      begin
        Case TypeGroupParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
        end;//Case
        Write(DiscriptorOUT, Str + DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'Gr*'+DARSeparator+'//Label строк');
    WriteLn(DiscriptorOUT, 'Count'+DARSeparator+'Dx'+DARSeparator+'Sx'+DARSeparator+'//Label столбцов');
    WriteLn(DiscriptorOUT, 'Dx- корректная оценка генеральной дисперсии'+DARSeparator+'//Rem1');
    WriteLn(DiscriptorOUT, 'Sx- абсолютно корректная оценка ...'+DARSeparator+'//Rem2');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WriteDx

procedure WritePoligon(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
var
  DiscriptorOUT: TextFile;
  i,j: Integer;
  Str: string;
  X1, Y1, X2, Y2, Y: tResultType;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.GraphResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во графических элементов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountB + 1)+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//объем выборки');

    for i := 0 to clMatrixFrequency.CountA do // ResultMatr
      begin
        WriteLn(DiscriptorOUT, UntInterface.graph_blok);
        if i = clMatrixFrequency.CountA then
          Str := 'Gr*'
        else
          Case TypeGroupParam of
            UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
            UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
          end;//Case
        WriteLn(DiscriptorOUT, Str + DARSeparator);

        X1 := 0; Y1 := 1;
        for j := 0 to clMatrixFrequency.CountB - 1 do
          begin
            X2 := (j+1)/(clMatrixFrequency.CountB + 1);
            Y := clMatrixFrequency.CumulateProbability(i, j, _not_, _divA_);
            Y2 := 1 - Y;
            WriteLn(DiscriptorOUT, FloatToStr(X1)+DARSeparator+FloatToStr(Y1)+DARSeparator+FloatToStr(X2)+DARSeparator+FloatToStr(Y2)+DARSeparator);
            X1 := X2;
            Y1 := Y2;
          end;//for j
        WriteLn(DiscriptorOUT, FloatToStr(X1)+DARSeparator+FloatToStr(Y1)+DARSeparator+'1'+DARSeparator+'1'+DARSeparator);

        WriteLn(DiscriptorOUT, UntInterface.rem_blok);
        for j := 0 to clMatrixFrequency.CountB - 1 do
          begin
            Y := clMatrixFrequency.CumulateProbability(i, j, _not_, _divA_);
            Y2 := 1 - Y;
            Write(DiscriptorOUT, FloatToStr(Y)+DARSeparator+FloatToStr(Y2)+DARSeparator);
          end;
        WriteLn(DiscriptorOUT, '// Label Y; Cord Y;');
      end;//for i

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Полигон относительных частот');
    Write(DiscriptorOUT, 'до'+ DARSeparator + '0' + DARSeparator);
    for j := 0 to clMatrixFrequency.CountB - 1 do
      begin
        X2 := (j+1)/(clMatrixFrequency.CountB + 1);
        Case TypeVariableParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStolB[j].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStolB[j].strParam;
        end;//Case
        Write(DiscriptorOUT, Str+DARSeparator+FloatToStr(X2)+DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'после'+ DARSeparator + '1' + DARSeparator + '// Label X; cord X;');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WritePoligon

procedure WriteFx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
var
  DiscriptorOUT: TextFile;
  i,j: Integer;
  Str: string;
  X1, X2, Y2, Y: tResultType;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.GraphResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во графических элементов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountB + 1)+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//объем выборки');

    for i := 0 to clMatrixFrequency.CountA do // ResultMatr
      begin
        WriteLn(DiscriptorOUT, UntInterface.graph_blok);
        if i = clMatrixFrequency.CountA then
          Str := 'Gr*'
        else
          Case TypeGroupParam of
            UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
            UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
          end;//Case
        WriteLn(DiscriptorOUT, Str + DARSeparator);

        X1 := 1/(clMatrixFrequency.CountB+1);
        WriteLn(DiscriptorOUT, '0'+DARSeparator+'1'+DARSeparator+FloatToStr(X1)+DARSeparator+'1'+DARSeparator);
        for j := 0 to clMatrixFrequency.CountB - 1 do
          begin
            X2 := (j+2)/(clMatrixFrequency.CountB+1);
            Y := clMatrixFrequency.CumulateProbability(i, j, _A_, _divA_);
            Y2 := 1 - Y;
            WriteLn(DiscriptorOUT, FloatToStr(X1)+DARSeparator+FloatToStr(Y2)+DARSeparator+FloatToStr(X2)+DARSeparator+FloatToStr(Y2)+DARSeparator);
            X1 := X2;
          end;//for j

        WriteLn(DiscriptorOUT, UntInterface.rem_blok);
        for j := 0 to clMatrixFrequency.CountB - 1 do
          begin
            Y := clMatrixFrequency.CumulateProbability(i, j, _A_, _divA_);
            Y2 := 1 - Y;
            Write(DiscriptorOUT, FloatToStr(Y)+DARSeparator+FloatToStr(Y2)+DARSeparator);
          end;//for j
        WriteLn(DiscriptorOUT,'// Label Y; cord Y;');
      end;//for i

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Эмпирическая функция распределения');
    Write(DiscriptorOUT, 'до'+ DARSeparator + '0' + DARSeparator);
    for j := 0 to clMatrixFrequency.CountB - 1 do
      begin
        X2 := (j+1)/(clMatrixFrequency.CountB+1);
        Case TypeVariableParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStolB[j].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStolB[j].strParam;
        end;//Case
        Write(DiscriptorOUT, Str+DARSeparator+FloatToStr(X2)+DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'после'+ DARSeparator + '1' + DARSeparator + '// Label X; cord X;');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WriteFx

procedure WritePx(ClientID: Integer; StatMethodID: tStatMethodID; clMatrixFrequency: TMatrixFrequency; TypeGroupParam, TypeVariableParam: tType);
var
  DiscriptorOUT: TextFile;
  i,j: Integer;
  Str: string;
  X1, Y1, X2, Y2, Y: tResultType;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.GraphResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountA + 1)+DARSeparator+'//кол-во графических элементов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountB + 1)+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clMatrixFrequency.CountALL)+DARSeparator+'//объем выборки');

    for i := 0 to clMatrixFrequency.CountA do // ResultMatr
      begin
        WriteLn(DiscriptorOUT, UntInterface.graph_blok);
        if i = clMatrixFrequency.CountA then
          Str := 'Gr*'
        else
          Case TypeGroupParam of
            UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStrokA[i].realParam);
            UntInterface._string_:  Str := clMatrixFrequency.LabelStrokA[i].strParam;
          end;//Case
        WriteLn(DiscriptorOUT, Str + DARSeparator);

        X1 := 0; Y1 := 1;
        for j := 0 to clMatrixFrequency.CountB - 1 do
          begin
            X2 := (j+1)/clMatrixFrequency.CountB;
            Y := clMatrixFrequency.CumulateProbability(i, j, _not_, _divA_);
            Y2 := 1 - Y;
            WriteLn(DiscriptorOUT, FloatToStr(X1)+DARSeparator+FloatToStr(Y2)+DARSeparator+FloatToStr(X2)+DARSeparator+FloatToStr(Y2)+DARSeparator);
            WriteLn(DiscriptorOUT, FloatToStr(X1)+DARSeparator+FloatToStr(Y1)+DARSeparator+FloatToStr(X1)+DARSeparator+FloatToStr(Y2)+DARSeparator);
            X1 := X2;
            Y1 := Y2;
          end;//for j
        WriteLn(DiscriptorOUT, FloatToStr(X1)+DARSeparator+FloatToStr(Y1)+DARSeparator+'1'+DARSeparator+'1'+DARSeparator);

        WriteLn(DiscriptorOUT, UntInterface.rem_blok);
        for j := 0 to clMatrixFrequency.CountB - 1 do
          begin
            Y := clMatrixFrequency.CumulateProbability(i, j, _not_, _divA_);
            Y2 := 1 - Y;
            Write(DiscriptorOUT, FloatToStr(Y)+DARSeparator+FloatToStr(Y2)+DARSeparator);
          end;//for j
        WriteLn(DiscriptorOUT,'// Label Y; cord Y;');
      end;//for i

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Эмпирическая плотность распределения');
    Write(DiscriptorOUT, 'до'+DARSeparator+'0'+DARSeparator);
    for j := 0 to clMatrixFrequency.CountB - 1 do
      begin
        X2 := (j+1)/clMatrixFrequency.CountB;
        Case TypeVariableParam of
          UntInterface._numeric_: Str := FloatToStr(clMatrixFrequency.LabelStolB[j].realParam);
          UntInterface._string_:  Str := clMatrixFrequency.LabelStolB[j].strParam;
        end;//Case
        Write(DiscriptorOUT, Str+DARSeparator+FloatToStr(X2)+DARSeparator);
      end;
    WriteLn(DiscriptorOUT, 'после'+DARSeparator+'1'+DARSeparator);
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WritePx

procedure WriteLineCorrelation(ClientID: Integer; StatMethodID: tStatMethodID; clLineCorrelation: TLineCorrelation; TypeGroupParam, TypeVariableParam, TypeVariableParam2: tType; countDataStr, countDataStolb: Integer; ixGroupParam, ixVariableParam, ixVariableParam2: Integer);
var
  DiscriptorOUT: TextFile;
  clSupplements: TSupplements;
  i: Integer;
  Count: tCount;
  Correlation, CriterionT: tResultType;
begin
  ChDir(First.ResultPath);
  AssignFile(DiscriptorOUT, 'ResultClient_'+IntToStr(ClientID)+'.dar');
  {$I-} ReWrite(DiscriptorOUT); {$I+}
  If IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                           +'[Write...;]'#13#10
                           +'EXCEPTION: Блокирован файл ResultClient.dar (на сервере),'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  try
    WriteLn(DiscriptorOUT, UntInterface.system_blok);
    WriteLn(DiscriptorOUT, IntToStr(INTEGER(StatMethodID))+DARSeparator+'//идентификатор вычисленного метода');
    WriteLn(DiscriptorOUT, UntInterface.DataResult+DARSeparator+'//тип результата');
    WriteLn(DiscriptorOUT, IntToStr(clLineCorrelation.CountGroup)+DARSeparator+'//кол-во строк');
    WriteLn(DiscriptorOUT, '3'+DARSeparator+'//кол-во столбцов');
    WriteLn(DiscriptorOUT, IntToStr(clLineCorrelation.CountALL)+DARSeparator+'//кол-во элементов в выборке');

    clSupplements := TSupplements.Create;
    try
      WriteLn(DiscriptorOUT, UntInterface.data_blok);
      for  i := 0 to clLineCorrelation.CountGroup - 1 do // ResultMatr
        begin
          Count := clLineCorrelation.CountInGroup[i];
          Correlation := clLineCorrelation.GetLineCorrelation(i);
          CriterionT := clSupplements.HypothesisT_LineCorrelation(Correlation, Count);
          WriteLn(DiscriptorOUT, FloatToStr(Count)+DARSeparator+FloatToStr(CriterionT)+DARSeparator+FloatToStr(Correlation)+DARSeparator);
        end;// for i
    finally
      clSupplements.Destroy;
    end;//try

    WriteLn(DiscriptorOUT, UntInterface.comment_blok);
    WriteLn(DiscriptorOUT, 'Матрица линейной корреляции Присона');
    for i := 0 to clLineCorrelation.CountGroup - 1 do
      Write(DiscriptorOUT, clLineCorrelation.LabelGroup[i] + DARSeparator);
    WriteLn(DiscriptorOUT, '//Label строк');
    WriteLn(DiscriptorOUT, 'Count'+DARSeparator+'T'+DARSeparator+'Rв'+DARSeparator+'//Label столбцов');
    WriteLn(DiscriptorOUT, 'T- критерий Стьюдента (T<0, то Rв - НЕ значимый)'+DARSeparator+'//Rem1');
    WriteLn(DiscriptorOUT, 'Rв- выборочный коэффициент линейной корреляции'+DARSeparator+'//Rem2');
    WriteLn(DiscriptorOUT, UntInterface.end_file);
  finally
    CloseFile(DiscriptorOUT);
  end;//try
end;//WriteLineCorrelation

end.
