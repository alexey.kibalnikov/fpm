unit UntInterface;
                        INTERFACE

Const
// информация о заголовках блоков в файлах Data And Result, *.dar
  system_blok = '// -> system information';
    GraphResult = 'GraphResult';
    DataResult = 'DataResult';
  data_blok = '// -> data information';
  graph_blok = '// ---> graphics element information';
  rem_blok ='// ---> rem element';
  comment_blok = '// -> comment information';
  end_file = '// -> end file';
  DARSeparator = ';'; // разделитель между числами
// погрешность вычислений
  cEpsilon = 0.00001;
  cAlpha = 0.05;

Type
  tResultType = Real;
  tType = (_numeric_, _string_);
  tFlag = (_first_, _next_);

  tDataAndResult = record
                     Case tType of
                       _numeric_: (realParam: tResultType);
                       _string_:  (strParam:  string[20]);
                   end;
  tSupplementID = (
// -> class TSupplements (содержит реализацию)
            _fGaus_                  {приложение 1},
            _xGaus_                  {приложение 1},
            _fLaplas_                {приложение 2},
            _xLaplas_                {приложение 2},
            _t_                      {приложение 3},
            _q_                      {приложение 4},
            _xPirson_criticle_point_ {приложение 5},
            _xStudetn_criticle_point_{приложение 6}
  );

  tStatMethodID = (
// -> class TMatrixFrequency (содержит реализацию)
{01}         _MatrixFrequency_{абсолютных частот},
{02}         _MatrixProbability_{эмпирической вероятности},
{--}{c. 229}//      _Hypothesis_SUPPOSED_Probability_{предположение по ген. совокупности},
{03}{с. 157} _Mx_{не смещенная},
{--}{c. 218}//      _Hypothesis_SUPPOSED_Mx_normal_sharing_{предположение по ген. совокупности},
{!!}{c. 251}{!}   _CRITERION_Pirson_hypothesis_NORMAL_sharing_{нормальное распределение},
{04}{с. 158} _Dx_{cмещенная оценка},
{04}{с. 158}// _Sx_{исправленная дисперсия (несмещенная оценка)},
{--}{c. 210}//      _Hypothesis_SUPPOSED_Sx_{предположение по ген. совокупности},
{05}{с. 190} _LineCorrelation_coef_{коэффициент},
{--}{с. 239}//      _Hypothesis_SIGNIFICANT_LineCorrelation_{значимость},
{06}{с. 201} _SpirmenRangCorrelation_coef_{ранговая корреляция},
{--}{с. 244}//      _Hypothesis_SIGNIFICANT_SpirmentRangCorrelation_{значимость},
{07}{с. 204} _KendallRangCorrelation_coef_,
{--}{с. 246}//      _Hypothesis_SIGNIFICANT_KendallRangCorrelation_{значимость}
// графика
{08}         _Poligon_,
{09}         _Fx_,//эмпирическая ф-ия распределения
{10}         _Px_//эмпирическая плотность
);

function FoundBlok(var Discriptor: TextFile; BlokName: string; Flag: tFlag): boolean;
procedure RoundResultoEpsilon(var Result: tResultType; Epsilon: real);

                        IMPLEMENTATION
uses
  SysUtils;

function FoundBlok(var Discriptor: TextFile; BlokName: string; Flag: tFlag): boolean;
var
  Str: string;
begin
  Result := False;
  if Flag = _first_ then
    Reset(Discriptor);
  while not EOF(Discriptor) do
    begin
      ReadLn(Discriptor, Str);
      if Str = BlokName then
        begin
          Result := True;
          Exit;
        end;
    end;
  if not Result then
    begin
      CloseFile(Discriptor);
      raise Exception.Create('Место возникновения: UntInterface'+#13#10
                             +'[FoundBlok;]'#13#10
                             +'EXCEPTION: НЕ найден блок данных '+BlokName+' в SQL.dar (клиента),'+#13#10
                             +'УСТРАНЕНИЕ: Сформируйте ПРАВИЛЬНЫЙ SQL.dar файл (используйте описание интерфейса)');
    end;
end;//FoundBlok

procedure RoundResultoEpsilon(var Result: tResultType; Epsilon: real);
begin
  Result :=       Result /Epsilon/10;
  Result := round(Result)*Epsilon*10;
end;//RoundResultoEpsilon

END.

