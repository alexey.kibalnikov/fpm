unit UntLineCorrelation;

interface
uses
  UntInterface,
  UntMatrixFrequency;

type
  tElement = record
    Group: tDataAndResult; // значение групперующей переменной
    Count: Integer; // кол-во элементов
    clMatrixFrequency: TMatrixFrequency;
  end;

  TLineCorrelation = class
  private
    TypeGroupParam,
    TypeVariableParam,
    TypeVariableParam2: tType;
    ClientID: longint;
    CountAllElement,
    CountGroupElement: tCount;
    function GetLabelGroup(ix: tIndex): string;
    function GetCountInGroup(ix: tIndex): tCount;
  public
    property CountAll: tCount read CountAllElement;
    property CountGroup: tCount read CountGroupElement;
    property CountInGroup[ix: tIndex]: tCount read GetCountInGroup;
    property LabelGroup[ix: tIndex]: string read GetLabelGroup;


    constructor Create(_ClientID_: longint; TGP, TVP, TVP2: tType);
    destructor Destroy; virtual;
    procedure AddElement(Variable, Variable2, Group: tDataAndResult);
    function GetLineCorrelation(ixCoef: tIndex): tResultType;

  private
    ListClass: array of tElement;
    clMatrixFrequencyALL: TMatrixFrequency;
    function DihatSearch(Group: tDataAndResult): tSearch;

  end;//TLineCorrelation

implementation
uses
  SysUtils;

constructor TLineCorrelation.Create(_ClientID_: longint; TGP, TVP, TVP2: tType);
begin
  inherited Create;
  clMatrixFrequencyALL := TMatrixFrequency.Create(_ClientID_, TVP, TVP2);
  ClientID := _ClientID_;
  TypeGroupParam := TGP;
  TypeVariableParam := TVP;
  TypeVariableParam2 := TVP2;
  SetLength(ListClass, 0);
  CountAllElement := 0;
  CountGroupElement := 0;
end;//TLineCorrelation.Create

destructor TLineCorrelation.Destroy;
var
  i: integer;
begin
  clMatrixFrequencyALL.Destroy;
  for i := 0 to Length(ListClass) - 1 do
    ListClass[i].clMatrixFrequency.Destroy;
  SetLength(ListClass, 0);
  inherited Destroy;
end;//TLineCorrelation.Destroy

function TLineCorrelation.GetLabelGroup(ix: tIndex): string;
begin
  if (ix < 0) or (ix > CountGroupElement - 1) then
      raise Exception.Create('Такого индекса ['+FloatToStr(ix)+'] НЕ существует');

  if (0 <= ix) and (ix < CountGroupElement - 1) then
    Case TypeGroupParam of
      UntInterface._numeric_: Result := FloatToStr(ListClass[ix].Group.realParam);
      UntInterface._string_:  Result := ListClass[ix].Group.strParam;
    end;//Case
  if ix = CountGroupElement - 1 then
    Result := 'Gr*';
end;//TLineCorrelation.GetLabelGroup

function TLineCorrelation.GetCountInGroup(ix: tIndex): tCount;
begin
  if (ix < 0) or (ix > CountGroupElement - 1) then
      raise Exception.Create('Такого индекса ['+FloatToStr(ix)+'] НЕ существует');
  if (0 <= ix) and (ix < CountGroupElement - 1) then
    Result := ListClass[ix].Count;
  if ix = CountGroupElement - 1 then
    Result := CountAllElement;
end;//TLineCorrelation.GetCountInGroup

procedure TLineCorrelation.AddElement(Variable, Variable2, Group: tDataAndResult);
var
  Search: tSearch;
  i: Integer;
begin
  clMatrixFrequencyALL.AddElement(Variable, Variable2);
  inc(CountAllElement);
  if Length(ListClass) = 0 then
    begin
      CountGroupElement := 2;
      SetLength(ListClass, 1);
      ListClass[0].clMatrixFrequency := TMatrixFrequency.Create(ClientID, TypeVariableParam, TypeVariableParam2);
      ListClass[0].Group := Group;
      ListClass[0].Count := 1;
      ListClass[0].clMatrixFrequency.AddElement(Variable, Variable2);
    end
  else
    begin
      Search := DihatSearch(Group); // поиск
      if not Search.Found then
        begin // НЕ найден
          inc(CountGroupElement);
          SetLength(ListClass, Length(ListClass) + 1);
          for i := Length(ListClass) - 1 downto Search.ix + 1 do
            ListClass[i] := ListClass[i-1];

          ListClass[Search.ix].Group := Group;
          ListClass[Search.ix].Count := 1;
          ListClass[Search.ix].clMatrixFrequency := TMatrixFrequency.Create(ClientID, TypeVariableParam, TypeVariableParam2);
          ListClass[Search.ix].clMatrixFrequency.AddElement(Variable, Variable2);
        end
      else // если найден
        begin
          inc(ListClass[Search.ix].Count);
          ListClass[Search.ix].clMatrixFrequency.AddElement(Variable, Variable2);
        end;
    end;
end;//TLineCorrelation.AddElement

function TLineCorrelation.GetLineCorrelation(ixCoef: tIndex): tResultType;
begin
  if (ixCoef < 0) or (ixCoef > CountGroupElement - 1) then
      raise Exception.Create('Такого индекса ['+FloatToStr(ixCoef)+'] НЕ существует');
  if (0 <= ixCoef) and (ixCoef < CountGroupElement - 1) then
    Result := ListClass[ixCoef].clMatrixFrequency.LineCorrelation;
  if ixCoef = CountGroupElement - 1 then
    Result := clMatrixFrequencyALL.LineCorrelation;
end;//TLineCorrelation.GetLineCorrelation

function TLineCorrelation.DihatSearch(Group: tDataAndResult): tSearch;
var
  i: tIndex;
begin
  Result.ix := 0;
  Result.Found := False;
  if ListClass <> nil then
    begin
      for i := 0 to Length(ListClass) - 1 do
        begin
          Case TypeGroupParam of
            _numeric_:
              begin
                if ListClass[i].Group.realParam < Group.realParam then
                  Continue;
                if ListClass[i].Group.realParam = Group.realParam then
                  Result.Found := True;
                Break;
              end;
            _string_:
              begin
                if ListClass[i].Group.strParam < Group.strParam then
                  Continue;
                if ListClass[i].Group.strParam = Group.strParam then
                  Result.Found := True;
                Break;
              end;
          end;//Case
        end;//for i
      Result.ix := i;
    end;
end;//TLineCorrelation.DihatSearch

end.
