unit UntExecute;
{=========================================================================}
interface
{=========================================================================}
uses
  UntInterface;

procedure Execute(ClientID: Integer; StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer);
{=========================================================================}
implementation
{=========================================================================}
uses
  UntFirst,
  SysUtils,
  UntReadWriteInterfaceFile,
  UntMatrixFrequency,
  UntLineCorrelation;

{ NB!!! main part method -------------------------------------------------}

procedure Execute(ClientID: Integer; StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer);
var
  DiscriptorIN: TextFile;
  clMatrixFrequency: TMatrixFrequency;
  clLineCorrelation: TLineCorrelation;
  countDataStr, countDataStolb: Integer;
  Vector: array of tDataAndResult;
  Group, Variable, Variable2: tDataAndResult;
  Str, Lecsema: string;
  s, i, j: Integer;
begin
  if StatMethodID = UntInterface._LineCorrelation_coef_ then
    clLineCorrelation := TLineCorrelation.Create(ClientID, TypeGroupParam, TypeVariableParam, TypeVariableParam2)
  else
    clMatrixFrequency := TMatrixFrequency.Create(ClientID, TypeGroupParam, TypeVariableParam);
// -> BEGIN ReadDataFileParsing
  try
    ChDir(First.DataPath);
    AssignFile(DiscriptorIN, 'DataClient_'+IntToStr(ClientID)+'.dar');
    {$I-} Reset(DiscriptorIN); {$I+}
    If IOResult <> 0 then
      raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                             +'[Execute;]'#13#10
                             +'EXCEPTION: Блокирован файл DataClient.dar (на сервере),'+#13#10
                             +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
    try
      ReadSystemBlok(DiscriptorIN, countDataStr, countDataStolb);

      UntInterface.FoundBlok(DiscriptorIN, data_blok, _first_);
      SetLength(Vector, countDataStolb); // GetMem
      try
        for i := 0 to countDataStr - 1 do
          begin
            Readln(DiscriptorIN, Str);
            s := 1;
            for j := 0 to countDataStolb - 1 do
              begin
                Lecsema := '';
                while Str[s] <> DARSeparator do
                  begin
                    Lecsema := Lecsema + Str[s];
                    inc(s);
                  end;
                Vector[j].strParam := Lecsema;
                inc(s);
              end; // for j
              Case TypeGroupParam of
                UntInterface._numeric_: Group.realParam := StrToFloat(Vector[ixGroupParam].strParam);
                UntInterface._string_:  Group.strParam := Vector[ixGroupParam].strParam;
              end;// Case
              Case TypeVariableParam of
                UntInterface._numeric_: Variable.realParam := StrToFloat(Vector[ixVariableParam].strParam);
                UntInterface._string_:  Variable.strParam := Vector[ixVariableParam].strParam;
              end;//Case
              Case TypeVariableParam2 of
                UntInterface._numeric_: Variable2.realParam := StrToFloat(Vector[ixVariableParam2].strParam);
                UntInterface._string_:  Variable2.strParam := Vector[ixVariableParam2].strParam;
              end;//Case

              if StatMethodID = UntInterface._LineCorrelation_coef_ then
                clLineCorrelation.AddElement(Variable, Variable2, Group)
              else
                clMatrixFrequency.AddElement(Group, Variable);
          end; // for i
      finally
        SetLength(Vector, 0);
      end; // try
    finally
      CloseFile(DiscriptorIN);
    end;// try

  // -> применение (вычисление) соответствующего ID метода
    Case StatMethodID of
      _MatrixFrequency_: // частота
        UntReadWriteInterfaceFile.WriteMatrixFrequency(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam, TypeVariableParam);
      _MatrixProbability_: //  эмпирическая вероятность
        UntReadWriteInterfaceFile.WriteMatrixProbability(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam, TypeVariableParam);
      _Mx_: // Мат. ожидание
        UntReadWriteInterfaceFile.WriteMx(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam);
      _Dx_: // Дисперсия
        UntReadWriteInterfaceFile.WriteDx(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam);
      _LineCorrelation_coef_:{линейная корреляция}
        UntReadWriteInterfaceFile.WriteLineCorrelation(ClientID, StatMethodID, clLineCorrelation, TypeGroupParam, TypeVariableParam, TypeVariableParam2, countDataStr, countDataStolb, ixGroupParam, ixVariableParam, ixVariableParam2);

     _SpirmenRangCorrelation_coef_:{ранговая корреляция}
  //????????????????????
    ;
     _KendallRangCorrelation_coef_:{ранговая корреляция}
  //????????????????????
    ;
// -> ГРАФИКА -------------------------------------------------------------
      _Poligon_: // Полигон относительных частот
        UntReadWriteInterfaceFile.WritePoligon(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam, TypeVariableParam);
      _Fx_: // Функция распределения
        UntReadWriteInterfaceFile.WriteFx(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam, TypeVariableParam);
      _Px_:// Плотность распределения
        UntReadWriteInterfaceFile.WritePx(ClientID, StatMethodID, clMatrixFrequency, TypeGroupParam, TypeVariableParam);
    else//case
      raise Exception.Create('Место возникновения: AcridStatSoftServer'+#13#10
                             +'[Execute;]'#13#10
                             +'EXCEPTION: Данный метод НЕ вычисляется AcridStatSoftServer-ом,'+#13#10
                             +'УСТРАНЕНИЕ: реализуйте метод на сервере');
    end;//case
  finally
    if StatMethodID = UntInterface._LineCorrelation_coef_ then
      clLineCorrelation.Destroy
    else
      clMatrixFrequency.Destroy
  end;// try
end;//Execute

END.




