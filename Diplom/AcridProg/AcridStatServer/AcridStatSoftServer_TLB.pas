unit AcridStatSoftServer_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 30.03.05 11:23:08 from Type Library described below.

// ************************************************************************  //
// Type Lib: J:\User\My_Doc\Diplom\AcridProg\AcridStatServer\AcridStatSoftServer.tlb (1)
// LIBID: {F512D3C0-7D7F-11D8-B0EB-BD99300848A8}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\W_98\SYSTEM\midas.dll)
//   (2) v2.0 stdole, (C:\W_98\SYSTEM\stdole2.tlb)
//   (3) v4.0 StdVCL, (C:\W_98\SYSTEM\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, Midas, StdVCL, Variants, Windows, UntInterface;
// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AcridStatSoftServerMajorVersion = 1;
  AcridStatSoftServerMinorVersion = 0;

  LIBID_AcridStatSoftServer: TGUID = '{F512D3C0-7D7F-11D8-B0EB-BD99300848A8}';

  IID_ICoClassAcridStatSoftServer: TGUID = '{F512D3C1-7D7F-11D8-B0EB-BD99300848A8}';
  CLASS_CoClassAcridStatSoftServer: TGUID = '{F512D3C3-7D7F-11D8-B0EB-BD99300848A8}';
type
// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  ICoClassAcridStatSoftServer = interface;
  ICoClassAcridStatSoftServerDisp = dispinterface;
// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  CoClassAcridStatSoftServer = ICoClassAcridStatSoftServer;
// *********************************************************************//
// Interface: ICoClassAcridStatSoftServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F512D3C1-7D7F-11D8-B0EB-BD99300848A8}
// *********************************************************************//
  ICoClassAcridStatSoftServer = interface(IAppServer)
    ['{F512D3C1-7D7F-11D8-B0EB-BD99300848A8}']
    procedure sm_AddDataFile(ClientID: Integer; const Str: WideString); safecall;
    procedure sm_Execute(ClientID: Integer; StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer); safecall;
    function  sm_ClientID: Integer; safecall;
    procedure sm_DestroyClient(ClientID: Integer); safecall;
    procedure sm_NewDataFile(ClientID: Integer); safecall;
    procedure sm_MoveResultToClient(ClientID: Integer; StrN: Integer; out StrResult: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  ICoClassAcridStatSoftServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F512D3C1-7D7F-11D8-B0EB-BD99300848A8}
// *********************************************************************//
  ICoClassAcridStatSoftServerDisp = dispinterface
    ['{F512D3C1-7D7F-11D8-B0EB-BD99300848A8}']
    procedure sm_AddDataFile(ClientID: Integer; const Str: WideString); dispid 2;
    procedure sm_Execute(ClientID: Integer; StatMethodID: Integer; TypeGroupParam: Integer; 
                         ixGroupParam: Integer; TypeVariableParam: Integer; 
                         ixVariableParam: Integer; TypeVariableParam2: Integer; 
                         ixVariableParam2: Integer); dispid 3;
    function  sm_ClientID: Integer; dispid 4;
    procedure sm_DestroyClient(ClientID: Integer); dispid 5;
    procedure sm_NewDataFile(ClientID: Integer); dispid 1;
    procedure sm_MoveResultToClient(ClientID: Integer; StrN: Integer; out StrResult: WideString); dispid 7;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CoCoClassAcridStatSoftServer provides a Create and CreateRemote method to          
// create instances of the default interface ICoClassAcridStatSoftServer exposed by              
// the CoClass CoClassAcridStatSoftServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCoClassAcridStatSoftServer = class
    class function Create: ICoClassAcridStatSoftServer;
    class function CreateRemote(const MachineName: string): ICoClassAcridStatSoftServer;
  end;

implementation

uses ComObj;

class function CoCoClassAcridStatSoftServer.Create: ICoClassAcridStatSoftServer;
begin
  Result := CreateComObject(CLASS_CoClassAcridStatSoftServer) as ICoClassAcridStatSoftServer;
end;

class function CoCoClassAcridStatSoftServer.CreateRemote(const MachineName: string): ICoClassAcridStatSoftServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CoClassAcridStatSoftServer) as ICoClassAcridStatSoftServer;
end;

end.
