unit UntStoredProc;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, AcridBazaServer_TLB, StdVcl, Provider, IBStoredProc, IBQuery, DB,
  IBCustomDataSet, IBTable, IBDatabase, IBEvents, Dialogs;

type
  TCoClassAcridBazaServer = class(TRemoteDataModule, ICoClassAcridBazaServer)
    IBDBaza: TIBDatabase;
    IBTransaction: TIBTransaction;
    IBTHistory: TIBTable;
    IBTHistoryID_H: TIntegerField;
    IBTHistoryID_S: TIntegerField;
    IBTHistoryHDATA: TDateField;
    IBTHistoryCODE_MARK: TSmallintField;
    IBQuery: TIBQuery;
    IBSPAddHistory: TIBStoredProc;
    IBSPAddHistoryOUTID: TIntegerField;
    DSPStudent: TDataSetProvider;
    DSPHistory: TDataSetProvider;
    IBTLecture: TIBTable;
    IBTLectureID_L: TIntegerField;
    IBTLectureID_T: TIntegerField;
    IBTLectureID_P: TIntegerField;
    IBTLectureKURS: TSmallintField;
    IBTLectureDS: TIBStringField;
    IBTLectureRANG: TIBStringField;
    IBTLectureWINER: TIBStringField;
    IBTPredmet: TIBTable;
    IBTPredmetID_P: TIntegerField;
    IBTPredmetPNAME: TIBStringField;
    IBTSession: TIBTable;
    IBTSessionID: TIntegerField;
    IBTSessionID_S: TIntegerField;
    IBTSessionID_L: TIntegerField;
    IBTSessionCODE_MARK: TSmallintField;
    IBTSessionS_YEAR: TDateField;
    IBTStudent: TIBTable;
    IBTStudentID_S: TIntegerField;
    IBTStudentF: TIBStringField;
    IBTStudentI: TIBStringField;
    IBTStudentO: TIBStringField;
    IBTStudentN_GR: TSmallintField;
    IBTStudentPROF: TIBStringField;
    IBTStudentPDATA: TDateField;
    IBTStudentRDATA: TDateField;
    IBTStudentSCHOOL: TSmallintField;
    IBTStudentCITY: TIBStringField;
    IBTStudentBUDGET: TIBStringField;
    IBTStudentSEX: TIBStringField;
    IBTStudentDeF: TStringField;
    IBTStudentDeI: TStringField;
    IBTStudentDeO: TStringField;
    IBTTeacher: TIBTable;
    IBTTeacherID_T: TIntegerField;
    IBTTeacherF: TIBStringField;
    IBTTeacherI: TIBStringField;
    IBTTeacherO: TIBStringField;
    IBTTeacherJOB: TIBStringField;
    IBTTeacherRANG: TIBStringField;
    IBTTeacherDeF: TStringField;
    IBTTeacherDeI: TStringField;
    IBTTeacherDeO: TStringField;
    IBSPAddStudent: TIBStoredProc;
    IBSPAddStudentOUTID: TIntegerField;
    IBSPDelTeacher: TIBStoredProc;
    IBSPAddLecture: TIBStoredProc;
    IBSPAddLectureOUTID: TIntegerField;
    IBSPAddSession: TIBStoredProc;
    IBSPAddSessionOUTID: TIntegerField;
    IBSPAddPredmet: TIBStoredProc;
    IBSPAddPredmetOUTID: TIntegerField;
    IBSPAddTeacher: TIBStoredProc;
    IBSPAddTeacherOUTID: TIntegerField;
    IBSPDelSession: TIBStoredProc;
    IBSPDelHistory: TIBStoredProc;
    IBSPDelStudent: TIBStoredProc;
    IBSPDelLecture: TIBStoredProc;
    IBSPDelPredmet: TIBStoredProc;
    DSPLecture: TDataSetProvider;
    DSPSsesion: TDataSetProvider;
    DSPPredmet: TDataSetProvider;
    DSPTeacher: TDataSetProvider;
    DSPQuery: TDataSetProvider;
    IBCode: TIBTable;
    IBCodeID_C: TIntegerField;
    IBCodeCODE: TIBStringField;
    DSPCode: TDataSetProvider;
    IBEvents: TIBEvents;
    DSPQuery2: TDataSetProvider;
    IBQuery2: TIBQuery;
    procedure RemoteDataModuleCreate(Sender: TObject);
    procedure RemoteDataModuleDestroy(Sender: TObject);
    procedure IBTStudentCalcFields(DataSet: TDataSet);
    procedure IBTTeacherCalcFields(DataSet: TDataSet);
    procedure IBEventsEventAlert(Sender: TObject; EventName: String;
      EventCount: Integer; var CancelAlerts: Boolean);
  private
    { Private declarations }
    EventString : String;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    procedure sm_DelStudent(ID_S: Integer); safecall;
    procedure sm_DelPredmet(ID_P: Integer); safecall;
    procedure sm_DelHistory(ID_H: Integer); safecall;
    procedure sm_DelTeacher(ID_T: Integer); safecall;
    procedure sm_DelSession(ID: Integer); safecall;
    procedure sm_DelLecture(ID_L: Integer); safecall;
    function sm_AddStudent(ID_S: Integer; const F, I, O: WideString;
      N_gr: Smallint; const Prof: WideString; pData, rData: TDateTime;
      School: Smallint; const City, Budget, Sex: WideString): Integer;
      safecall;
    function sm_AddPredmet(id_p: Integer; const pName: WideString): Integer;
      safecall;
    function sm_AddHistory(ID_H: Integer; ID_S: Integer; hData: TDateTime;
                            code_mark: Smallint): Integer; safecall;
    function sm_AddLecture(ID_L, ID_T, ID_P: Integer; Kurs: Smallint; const DS,
      Rang, Winer: WideString): Integer; safecall;
    function sm_AddSession(ID, ID_S, ID_L: Integer; code_mark: Smallint;
      S_Year: TDateTime): Integer; safecall;
    function sm_AddTeacher(ID_T: Integer; const F, I, O, Job,
      Rang: WideString): Integer; safecall;
    procedure sm_QueryAdd(const S: WideString); safecall;
    procedure sm_QueryClear; safecall;
    procedure sm_QueryExec; safecall;
    procedure sm_QueryAdd2(const S: WideString); safecall;
    procedure sm_QueryClear2; safecall;
    procedure sm_QueryExec2; safecall;
    function  sm_UpdateEvenst(const OldStr: WideString): WideString; safecall;
  public
    { Public declarations }
  end;

VAR
  BazaPath: string;

implementation
uses
  UntFirst,
  UntCoder,
  IniFiles; // *.ini;

{$R *.DFM}

class procedure TCoClassAcridBazaServer.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function  TCoClassAcridBazaServer.sm_UpdateEvenst(const OldStr: WideString): WideString; safecall;
var
 i: Integer;
begin
 if OldStr='' then Result:= EventString
 else
  begin
   Result:=OldStr;
   for i:=1 to length(EventString) do
    if EventString[i]='1' then
     begin
      Result[i]:='1';
      EventString[i]:='0';
     end;
  end;
end;

procedure TCoClassAcridBazaServer.sm_DelStudent(ID_S: Integer);
begin
  if IBSPDelStudent.Transaction.InTransaction then IBSPDelStudent.Transaction.Commit;
  IBSPDelStudent.Params[0].Value :=ID_S;
  IBSPDelStudent.ExecProc;
  if IBSPDelStudent.Transaction.InTransaction then IBSPDelStudent.Transaction.Commit;
end;

procedure TCoClassAcridBazaServer.sm_DelPredmet(ID_P: Integer);
begin
  if IBSPDelPredmet.Transaction.InTransaction then IBSPDelPredmet.Transaction.Commit;
  IBSPDelPredmet.Params[0].Value :=ID_P;
  IBSPDelPredmet.ExecProc;
  if IBSPDelPredmet.Transaction.InTransaction then IBSPDelPredmet.Transaction.Commit;
end;

procedure TCoClassAcridBazaServer.sm_DelHistory(ID_H: Integer);
begin
  if IBSPDelHistory.Transaction.InTransaction then IBSPDelHistory.Transaction.Commit;
  IBSPDelHistory.Params[0].Value :=ID_H;
  IBSPDelHistory.ExecProc;
  if IBSPDelHistory.Transaction.InTransaction then IBSPDelHistory.Transaction.Commit;
end;

procedure TCoClassAcridBazaServer.sm_DelTeacher(ID_T: Integer);
begin
  if IBSPDelTeacher.Transaction.InTransaction then IBSPDelTeacher.Transaction.Commit;
  IBSPDelTeacher.Params[0].Value :=ID_T;
  IBSPDelTeacher.ExecProc;
  if IBSPDelTeacher.Transaction.InTransaction then IBSPDelTeacher.Transaction.Commit;
end;

procedure TCoClassAcridBazaServer.sm_DelSession(ID: Integer);
begin
  if IBSPDelSession.Transaction.InTransaction then IBSPDelSession.Transaction.Commit;
  IBSPDelSession.Params[0].Value :=ID;
  IBSPDelSession.ExecProc;
  if IBSPDelSession.Transaction.InTransaction then IBSPDelSession.Transaction.Commit;
end;

procedure TCoClassAcridBazaServer.sm_DelLecture(ID_L: Integer);
begin
  if IBSPDelLecture.Transaction.InTransaction then IBSPDelLecture.Transaction.Commit;
  IBSPDelLecture.Params[0].Value :=ID_L;
  IBSPDelLecture.ExecProc;
  if IBSPDelLecture.Transaction.InTransaction then IBSPDelLecture.Transaction.Commit;
end;

function TCoClassAcridBazaServer.sm_AddStudent(ID_S: Integer; const F, I,
  O: WideString; N_gr: Smallint; const Prof: WideString; pData,
  rData: TDateTime; School: Smallint; const City, Budget,
  Sex: WideString): Integer;
begin
  if IBSPAddStudent.Transaction.InTransaction then IBSPAddStudent.Transaction.Commit;
  IBSPAddStudent.Params[1].Value :=ID_S;
  IBSPAddStudent.Params[2].Value :=Coder(F);
  IBSPAddStudent.Params[3].Value :=Coder(I);
  IBSPAddStudent.Params[4].Value :=Coder(O);
  IBSPAddStudent.Params[5].Value :=N_gr;
  IBSPAddStudent.Params[6].Value :=Prof;
  IBSPAddStudent.Params[7].Value :=pData;
  IBSPAddStudent.Params[8].Value :=rData;
  IBSPAddStudent.Params[9].Value :=School;
  IBSPAddStudent.Params[10].Value :=City;
  IBSPAddStudent.Params[11].Value :=Budget;
  IBSPAddStudent.Params[12].Value :=Sex;
  IBSPAddStudent.ExecProc;
  if IBSPAddStudent.Transaction.InTransaction then IBSPAddStudent.Transaction.Commit;
  Result := IBSPAddStudent.Params[0].Value;
end;

function TCoClassAcridBazaServer.sm_AddPredmet(id_p: Integer;
  const pName: WideString): Integer;
begin
  if IBSPAddPredmet.Transaction.InTransaction then IBSPAddPredmet.Transaction.Commit;
  IBSPAddPredmet.Params[1].Value :=ID_P;
  IBSPAddPredmet.Params[2].Value :=pName;
  IBSPAddPredmet.ExecProc;
  if IBSPAddPredmet.Transaction.InTransaction then IBSPAddPredmet.Transaction.Commit;
  Result := IBSPAddPredmet.Params[0].Value;
end;

function TCoClassAcridBazaServer.sm_AddHistory(ID_H: Integer; ID_S: Integer; hData: TDateTime; code_mark: Smallint): Integer;
begin
  if IBSPAddHistory.Transaction.InTransaction then IBSPAddHistory.Transaction.Commit;
  IBSPAddHistory.Params[1].Value :=ID_H;
  IBSPAddHistory.Params[2].Value :=ID_S;
  IBSPAddHistory.Params[3].Value :=hData;
  IBSPAddHistory.Params[4].Value :=Code_mark;
  IBSPAddHistory.ExecProc;
  if IBSPAddHistory.Transaction.InTransaction then IBSPAddHistory.Transaction.Commit;
  Result := IBSPAddHistory.Params[0].Value;
end;

function TCoClassAcridBazaServer.sm_AddLecture(ID_L, ID_T, ID_P: Integer;
  Kurs: Smallint; const DS, Rang, Winer: WideString): Integer;
begin
  if IBSPAddLecture.Transaction.InTransaction then IBSPAddPredmet.Transaction.Commit;
  IBSPAddLecture.Params[1].Value :=ID_L;
  IBSPAddLecture.Params[2].Value :=ID_T;
  IBSPAddLecture.Params[3].Value :=ID_P;
  IBSPAddLecture.Params[4].Value :=Kurs;
  IBSPAddLecture.Params[5].Value :=DS;
  IBSPAddLecture.Params[6].Value :=Rang;
  IBSPAddLecture.Params[7].Value :=Winer;
  IBSPAddLecture.ExecProc;
  if IBSPAddLecture.Transaction.InTransaction then IBSPAddLecture.Transaction.Commit;
  Result := IBSPAddLecture.Params[0].Value;
end;

function TCoClassAcridBazaServer.sm_AddSession(ID, ID_S, ID_L: Integer;
  code_mark: Smallint; S_Year: TDateTime): Integer;
begin
 if IBSPAddSession.Transaction.InTransaction then IBSPAddSession.Transaction.Commit;
  IBSPAddSession.Params[1].Value :=ID;
  IBSPAddSession.Params[2].Value :=ID_S;
  IBSPAddSession.Params[3].Value :=ID_L;
  IBSPAddSession.Params[4].Value :=Code_mark;
  IBSPAddSession.Params[5].Value :=S_year;
  IBSPAddSession.ExecProc;
  if IBSPAddSession.Transaction.InTransaction then IBSPAddSession.Transaction.Commit;
  Result := IBSPAddSession.Params[0].Value;
end;

function TCoClassAcridBazaServer.sm_AddTeacher(ID_T: Integer; const F, I, O,
  Job, Rang: WideString): Integer;
begin
 if IBSPAddTeacher.Transaction.InTransaction then IBSPAddTeacher.Transaction.Commit;
  IBSPAddTeacher.Params[1].Value :=ID_T;
  IBSPAddTeacher.Params[2].Value :=Coder(F);
  IBSPAddTeacher.Params[3].Value :=Coder(I);
  IBSPAddTeacher.Params[4].Value :=Coder(O);
  IBSPAddTeacher.Params[5].Value :=Job;
  IBSPAddTeacher.Params[6].Value :=Rang;
  IBSPAddTeacher.ExecProc;
  if IBSPAddTeacher.Transaction.InTransaction then IBSPAddTeacher.Transaction.Commit;
  Result := IBSPAddTeacher.Params[0].Value;
end;

procedure TCoClassAcridBazaServer.sm_QueryAdd(const S: WideString);
begin IBQuery.SQL.Add(s); end;

procedure TCoClassAcridBazaServer.sm_QueryClear;
begin
  if IBQuery.Transaction.InTransaction then IBQuery.Transaction.Commit;
  IBQuery.SQL.Clear;
end;

procedure TCoClassAcridBazaServer.sm_QueryExec;
begin
  if (pos('SELECT',IBQuery.SQL[0])>0) or (pos('select',IBQuery.SQL[0])>0) then
    IBQuery.Open
  else
    IBQuery.ExecSQL;
end;

procedure TCoClassAcridBazaServer.sm_QueryAdd2(const S: WideString);
begin IBQuery2.SQL.Add(s); end;

procedure TCoClassAcridBazaServer.sm_QueryClear2;
begin
  if IBQuery2.Transaction.InTransaction then IBQuery.Transaction.Commit;
  IBQuery2.SQL.Clear;
end;

procedure TCoClassAcridBazaServer.sm_QueryExec2;
begin
  if (pos('SELECT',IBQuery.SQL[0])>0) or (pos('select',IBQuery.SQL[0])>0) then
    IBQuery2.Open
  else
    IBQuery2.ExecSQL;
end;

procedure TCoClassAcridBazaServer.RemoteDataModuleCreate(Sender: TObject);
var
  FileINI: tIniFile;
begin
  FileINI := TIniFile.Create('ConfigBazaServer.ini');
  BazaPath := FileINI.ReadString('AcridBazaServer','BazaPath','');
  IBDBaza.DatabaseName := BazaPath;
  FileINI.Free;

  EventString:='000000';
  inc(First.ClientCount);
  First.TrayIcon.Hint:='AcridBazaServer Подключено клиентов: '+IntToStr(First.ClientCount);
end;

procedure TCoClassAcridBazaServer.RemoteDataModuleDestroy(Sender: TObject);
begin
  dec(First.ClientCount);
  First.TrayIcon.Hint:='AcridBazaServer Подключено клиентов: '+IntToStr(First.ClientCount);
end;

procedure TCoClassAcridBazaServer.IBTStudentCalcFields(DataSet: TDataSet);
begin
 IBTStudentDeF.Value:=DeCoder(IBTStudentF.Value);
 IBTStudentDeI.Value:=DeCoder(IBTStudentI.Value);
 IBTStudentDeO.Value:=DeCoder(IBTStudentO.Value);
end;

procedure TCoClassAcridBazaServer.IBTTeacherCalcFields(DataSet: TDataSet);
begin
 IBTTeacherDeF.Value:=DeCoder(IBTTeacherF.Value);
 IBTTeacherDeI.Value:=DeCoder(IBTTeacherI.Value);
 IBTTeacherDeO.Value:=DeCoder(IBTTeacherO.Value);
end;

procedure TCoClassAcridBazaServer.IBEventsEventAlert(Sender: TObject;
  EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
begin
 if EventName='STUDENT' then EventString[1]:='1';
 if EventName='PREDMET' then EventString[2]:='1';
 if EventName='TEACHER' then EventString[3]:='1';
 if EventName='HISTORY' then EventString[4]:='1';
 if EventName='LECTURE' then EventString[5]:='1';
 if EventName='SESSION' then EventString[6]:='1';
end;

initialization
  TComponentFactory.Create(ComServer, TCoClassAcridBazaServer,
    Class_CoClassAcridBazaServer, ciMultiInstance, tmApartment);
end.
