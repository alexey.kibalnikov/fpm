object CoClassAcridBazaServer: TCoClassAcridBazaServer
  OldCreateOrder = False
  OnCreate = RemoteDataModuleCreate
  OnDestroy = RemoteDataModuleDestroy
  Left = 204
  Top = 105
  Height = 303
  Width = 430
  object IBDBaza: TIBDatabase
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey'
      'lc_ctype=WIN1251')
    LoginPrompt = False
    DefaultTransaction = IBTransaction
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 88
    Top = 8
  end
  object IBTransaction: TIBTransaction
    Active = False
    DefaultDatabase = IBDBaza
    AutoStopAction = saNone
    Left = 32
    Top = 24
  end
  object IBTHistory: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'ID_H'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'ID_S'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'HDATA'
        DataType = ftDate
      end
      item
        Name = 'CODE_MARK'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'RDB$PRIMARY2'
        Fields = 'ID_H'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'RDB$FOREIGN8'
        Fields = 'ID_S'
      end>
    StoreDefs = True
    TableName = 'HISTORY'
    Left = 88
    Top = 104
    object IBTHistoryID_H: TIntegerField
      FieldName = 'ID_H'
      Required = True
    end
    object IBTHistoryID_S: TIntegerField
      FieldName = 'ID_S'
      Required = True
    end
    object IBTHistoryHDATA: TDateField
      FieldName = 'HDATA'
    end
    object IBTHistoryCODE_MARK: TSmallintField
      FieldName = 'CODE_MARK'
    end
  end
  object IBQuery: TIBQuery
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    Left = 368
    Top = 120
  end
  object IBSPAddHistory: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDHISTORY'
    Left = 88
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_H'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_S'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INHDATA'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'INCODE_MARK'
        ParamType = ptInput
      end>
    object IBSPAddHistoryOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDHISTORY.OUTID'
    end
  end
  object DSPStudent: TDataSetProvider
    DataSet = IBTStudent
    Constraints = True
    Left = 32
    Top = 72
  end
  object DSPHistory: TDataSetProvider
    DataSet = IBTHistory
    Constraints = True
    Left = 88
    Top = 56
  end
  object IBTLecture: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'LECTURE'
    Left = 144
    Top = 120
    object IBTLectureID_L: TIntegerField
      FieldName = 'ID_L'
    end
    object IBTLectureID_T: TIntegerField
      FieldName = 'ID_T'
    end
    object IBTLectureID_P: TIntegerField
      FieldName = 'ID_P'
    end
    object IBTLectureKURS: TSmallintField
      FieldName = 'KURS'
    end
    object IBTLectureDS: TIBStringField
      FieldName = 'DS'
      Size = 1
    end
    object IBTLectureRANG: TIBStringField
      FieldName = 'RANG'
      Size = 3
    end
    object IBTLectureWINER: TIBStringField
      FieldName = 'WINER'
      Size = 1
    end
  end
  object IBTPredmet: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'PREDMET'
    Left = 256
    Top = 120
    object IBTPredmetID_P: TIntegerField
      FieldName = 'ID_P'
    end
    object IBTPredmetPNAME: TIBStringField
      FieldName = 'PNAME'
      Size = 30
    end
  end
  object IBTSession: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    ObjectView = True
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'SESSION'
    Left = 200
    Top = 104
    object IBTSessionID: TIntegerField
      FieldName = 'ID'
    end
    object IBTSessionID_S: TIntegerField
      FieldName = 'ID_S'
    end
    object IBTSessionID_L: TIntegerField
      FieldName = 'ID_L'
    end
    object IBTSessionCODE_MARK: TSmallintField
      FieldName = 'CODE_MARK'
    end
    object IBTSessionS_YEAR: TDateField
      FieldName = 'S_YEAR'
    end
  end
  object IBTStudent: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    OnCalcFields = IBTStudentCalcFields
    BufferChunks = 1000
    CachedUpdates = False
    StoreDefs = True
    TableName = 'STUDENT'
    Left = 32
    Top = 120
    object IBTStudentID_S: TIntegerField
      FieldName = 'ID_S'
      Required = True
    end
    object IBTStudentF: TIBStringField
      FieldName = 'F'
      Visible = False
      Size = 30
    end
    object IBTStudentI: TIBStringField
      FieldName = 'I'
      Visible = False
      Size = 30
    end
    object IBTStudentO: TIBStringField
      FieldName = 'O'
      Visible = False
      Size = 30
    end
    object IBTStudentN_GR: TSmallintField
      FieldName = 'N_GR'
    end
    object IBTStudentPROF: TIBStringField
      FieldName = 'PROF'
      Size = 3
    end
    object IBTStudentPDATA: TDateField
      FieldName = 'PDATA'
    end
    object IBTStudentRDATA: TDateField
      FieldName = 'RDATA'
    end
    object IBTStudentSCHOOL: TSmallintField
      FieldName = 'SCHOOL'
    end
    object IBTStudentCITY: TIBStringField
      FieldName = 'CITY'
      Size = 1
    end
    object IBTStudentBUDGET: TIBStringField
      FieldName = 'BUDGET'
      Size = 1
    end
    object IBTStudentSEX: TIBStringField
      FieldName = 'SEX'
      Size = 1
    end
    object IBTStudentDeF: TStringField
      FieldKind = fkCalculated
      FieldName = 'DeF'
      Calculated = True
    end
    object IBTStudentDeI: TStringField
      FieldKind = fkCalculated
      FieldName = 'DeI'
      Calculated = True
    end
    object IBTStudentDeO: TStringField
      FieldKind = fkCalculated
      FieldName = 'DeO'
      Calculated = True
    end
  end
  object IBTTeacher: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    OnCalcFields = IBTTeacherCalcFields
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'TEACHER'
    Left = 312
    Top = 104
    object IBTTeacherID_T: TIntegerField
      FieldName = 'ID_T'
    end
    object IBTTeacherF: TIBStringField
      FieldName = 'F'
      Visible = False
      Size = 30
    end
    object IBTTeacherI: TIBStringField
      FieldName = 'I'
      Visible = False
      Size = 30
    end
    object IBTTeacherO: TIBStringField
      FieldName = 'O'
      Visible = False
      Size = 30
    end
    object IBTTeacherJOB: TIBStringField
      FieldName = 'JOB'
      Size = 30
    end
    object IBTTeacherRANG: TIBStringField
      FieldName = 'RANG'
    end
    object IBTTeacherDeF: TStringField
      FieldKind = fkCalculated
      FieldName = 'DeF'
      Size = 30
      Calculated = True
    end
    object IBTTeacherDeI: TStringField
      FieldKind = fkCalculated
      FieldName = 'DeI'
      Size = 30
      Calculated = True
    end
    object IBTTeacherDeO: TStringField
      FieldKind = fkCalculated
      FieldName = 'DeO'
      Size = 30
      Calculated = True
    end
  end
  object IBSPAddStudent: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDSTUDENT'
    Left = 32
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_S'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INF'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INI'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INO'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'INN_GR'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INPROF'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INPDATA'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INRDATA'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'INSCHOOL'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INCITY'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INBUDGET'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INSEX'
        ParamType = ptInput
      end>
    object IBSPAddStudentOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDSTUDENT.OUTID'
    end
  end
  object IBSPDelTeacher: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELFROMTEACHER'
    Left = 312
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_T'
        ParamType = ptInput
      end>
  end
  object IBSPAddLecture: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDLECTURE'
    Left = 144
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_L'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_T'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'INKURS'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INDS'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INRANG'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INWINER'
        ParamType = ptInput
      end>
    object IBSPAddLectureOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDLECTURE.OUTID'
    end
  end
  object IBSPAddSession: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDSESSION'
    Left = 200
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_S'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'INID_L'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'INCODE_MARK'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INS_YEAR'
        ParamType = ptInput
      end>
    object IBSPAddSessionOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDSESSION.OUTID'
    end
  end
  object IBSPAddPredmet: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDPREDMET'
    Left = 256
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INPNAME'
        ParamType = ptInput
      end>
    object IBSPAddPredmetOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDPREDMET.OUTID'
    end
  end
  object IBSPAddTeacher: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'ADDTEACHER'
    Left = 312
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OUTID'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'INID_T'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INF'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INI'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INJOB'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INRANG'
        ParamType = ptInput
      end>
    object IBSPAddTeacherOUTID: TIntegerField
      FieldName = 'OUTID'
      Origin = 'ADDTEACHER.OUTID'
    end
  end
  object IBSPDelSession: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELFROMSESSION'
    Left = 200
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID'
        ParamType = ptInput
      end>
  end
  object IBSPDelHistory: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELFROMHISTORY'
    Left = 88
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_H'
        ParamType = ptInput
      end>
  end
  object IBSPDelStudent: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELFROMSTUDENT'
    Left = 32
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_S'
        ParamType = ptInput
      end>
  end
  object IBSPDelLecture: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELFROMLECTURE'
    Left = 144
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_L'
        ParamType = ptInput
      end>
  end
  object IBSPDelPredmet: TIBStoredProc
    Database = IBDBaza
    Transaction = IBTransaction
    StoredProcName = 'DELFROMPREDMET'
    Left = 256
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'INID_P'
        ParamType = ptInput
      end>
  end
  object DSPLecture: TDataSetProvider
    DataSet = IBTLecture
    Constraints = True
    Left = 144
    Top = 72
  end
  object DSPSsesion: TDataSetProvider
    DataSet = IBTSession
    Constraints = True
    Left = 200
    Top = 56
  end
  object DSPPredmet: TDataSetProvider
    DataSet = IBTPredmet
    Constraints = True
    Left = 256
    Top = 72
  end
  object DSPTeacher: TDataSetProvider
    DataSet = IBTTeacher
    Constraints = True
    Left = 312
    Top = 56
  end
  object DSPQuery: TDataSetProvider
    DataSet = IBQuery
    Constraints = True
    Left = 368
    Top = 72
  end
  object IBCode: TIBTable
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    ReadOnly = True
    TableName = 'CODE_MARK'
    Left = 368
    Top = 216
    object IBCodeID_C: TIntegerField
      FieldName = 'ID_C'
      Required = True
    end
    object IBCodeCODE: TIBStringField
      FieldName = 'CODE'
      Size = 30
    end
  end
  object DSPCode: TDataSetProvider
    DataSet = IBCode
    Constraints = True
    Left = 368
    Top = 168
  end
  object IBEvents: TIBEvents
    AutoRegister = False
    Database = IBDBaza
    Events.Strings = (
      'HISTORY'
      'LECTURE'
      'PREDMET'
      'SESSION'
      'STUDENT'
      'TEACHER')
    Registered = False
    OnEventAlert = IBEventsEventAlert
    Left = 144
    Top = 16
  end
  object DSPQuery2: TDataSetProvider
    DataSet = IBQuery2
    Constraints = True
    Left = 312
    Top = 8
  end
  object IBQuery2: TIBQuery
    Database = IBDBaza
    Transaction = IBTransaction
    BufferChunks = 1000
    CachedUpdates = False
    Left = 368
    Top = 24
  end
end
