unit UntFirst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cbClasses, CaptionButton, FormTrayIcon, Menus,
  AppAutoRun, ExtCtrls;

type

  TFirst = class(TForm)
    TrayIcon: TFormTrayIcon;
    PMServer: TPopupMenu;
    mrClose: TMenuItem;
    mrRun: TMenuItem;
    AppAutoRun: TAppAutoRun;
    mrAdd: TMenuItem;
    mrDel: TMenuItem;
    OpenDialog: TOpenDialog;
    mrConfig: TMenuItem;
    ImageAcridBrend: TImage;
    Label1: TLabel;
    Label2: TLabel;
    mrAbotu: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure mrCloseClick(Sender: TObject);
    procedure TrayIconMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure abfShutdown1QueryShutdown(Sender: TObject;
      var CanShutdown: Boolean);
    procedure mrAddClick(Sender: TObject);
    procedure mrDelClick(Sender: TObject);
    procedure mrConfigClick(Sender: TObject);
    procedure mrAbotuClick(Sender: TObject);
  private
    { Private declarations }
  public
     ClientCount: Integer;
    { Public declarations }
  end;

var
  First: TFirst;

implementation

uses
  UntStoredProc,
  IniFiles; // *.ini;
{$R *.dfm}

procedure TFirst.FormCreate(Sender: TObject);
begin ClientCount := 0; end;

procedure TFirst.mrCloseClick(Sender: TObject);
begin Close; end;
procedure TFirst.TrayIconMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if ClientCount<>0 then First.mrClose.Visible:=False
   else First.mrClose.Visible:=True;
end;

procedure TFirst.abfShutdown1QueryShutdown(Sender: TObject;
  var CanShutdown: Boolean);
begin
 MessageDlg('����������',mtWarning,[mbOk],0)
end;

procedure TFirst.mrAddClick(Sender: TObject);
begin
  if MessageDlg('��������� ��� ��������',mtInformation,[mbYes,mbNo],0)= mrYes then
  AppAutoRun.AutoRun:=True;
end;

procedure TFirst.mrDelClick(Sender: TObject);
begin
  if MessageDlg('�� ��������� ��� ��������',mtInformation,[mbYes,mbNo],0)= mrYes then
  AppAutoRun.AutoRun:=False;
end;

function FoundFile(FileName: string):boolean;
var
  Discriptor: TextFile;
begin
  Result := False;
  if FileName <> '' then
    begin
      AssignFile(Discriptor, FileName);
{$I-} // ���������� ��������
      Reset(Discriptor);
{$I+} // ��������� ��������
      if IOResult = 0 then
        begin
          Result := True;
          CloseFile(Discriptor);
        end;
    end;
end;//FoundFile

procedure TFirst.mrConfigClick(Sender: TObject);
var
  FileINI: tIniFile;
  Str: string;
  OK: boolean;
begin
  if ClientCount <> 0 then
    MessageDlg('��������� AcridBazaServer.exe ���������',mtInformation,[mbOK],0)
  else
    begin
      FileINI := TIniFile.Create('ConfigBazaServer.ini');
      GetDir(0,Str);
      FileINI.WriteString('AcridBazaServer','ServerPath', Str);

      OK := True;
      BazaPath := FileINI.ReadString('AcridBazaServer','BazaPath','');
      if not FoundFile(BazaPath) then
        repeat
          OpenDialog.Filter := 'InterBase Files (*.gdb)|*.gdb';
          OpenDialog.InitialDir := 'C:\';
          OpenDialog.FileName := 'Students.gdb';
          OpenDialog.Execute;
          BazaPath := OpenDialog.FileName;
          FileINI.WriteString('AcridBazaServer','BazaPath', BazaPath);

          if not FoundFile(BazaPath) then
            begin
              if MessageDlg('���� �� �������, ������ ���������� �����',mtWarning,[mbYes,mbNo],0) = mrNo then
                begin
                  MessageDlg('��������� �� ���������',mtError,[mbOK],0);
                  OK := False;
                  Exit;
                end
            end
          else
            begin
              OK := False;
              MessageDlg('��������� ������� ���������',mtInformation,[mbOK],0);
            end;
        until not OK
      else
        MessageDlg('��������� ������� ���������',mtInformation,[mbOK],0);
      FileINI.Free;
    end;
end;

procedure TFirst.mrAbotuClick(Sender: TObject);
begin First.Visible := True; end;

end.
