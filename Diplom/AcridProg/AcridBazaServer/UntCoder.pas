unit UntCoder;
interface

 function Coder(Str : string) : string;
 function DeCoder(Str : string) : string;

implementation

type Vector = array[0..7] of byte;
const IxBit : Vector=(128,64,32,16,8,4,2,1);
    IxCoder : Vector=(3,5,4,7,6,0,2,1);
    Baza = 8;

function Coder;
 var Len,i : integer;
     k,j,Cod: byte;
     Ch : char;
  begin
   Len:=length(Str); k:=0; Result:='';
   for i:=1 to Len do
    begin
      Ch:=Str[i]; Cod:=0;
      for j:=0 to 7 do Cod:=Cod+(Ord(Ch) and IxBit[j] shr(7-j))*IxBit[(IxCoder[j]+k) mod 8];
      Result:=Result+Chr(Cod);
      k:=(k+1) mod baza;
    end;
end;

function DeCoder;
 var Len,i : integer;
     k,j,Cod: byte;
     Ch : char;
     ixDeCoder : Vector;
  begin
   Len:=length(Str); k:=0; Result:='';
   for i:=1 to Len do
    begin
      for j:=0 to 7 do ixDeCoder[(ixCoder[j]+k)mod 8]:=j;
      Ch:=Str[i]; Cod:=0;
      for j:=0 to 7 do Cod:=Cod+(Ord(Ch) and IxBit[j] shr(7-j))*IxBit[IxDeCoder[j]];
      Result:=Result+Chr(Cod);
      k:=(k+1) mod baza;
    end;
end;

end.
