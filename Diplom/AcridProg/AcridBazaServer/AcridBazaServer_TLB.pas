unit AcridBazaServer_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 09.01.05 15:46:52 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\User\My_Doc\Kursovie\AcridProg\AcridBazaServer\Server\AcridBazaServer.tlb (1)
// LIBID: {F512D3C0-7D7F-11D8-B0EB-BD9930084878}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v1.0 Midas, (C:\W\SYSTEM\midas.dll)
//   (2) v2.0 stdole, (C:\W\SYSTEM\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, Midas, StdVCL, Variants, Windows;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AcridBazaServerMajorVersion = 1;
  AcridBazaServerMinorVersion = 0;

  LIBID_AcridBazaServer: TGUID = '{F512D3C0-7D7F-11D8-B0EB-BD9930084878}';

  IID_ICoClassAcridBazaServer: TGUID = '{F512D3C1-7D7F-11D8-B0EB-BD9930084878}';
  CLASS_CoClassAcridBazaServer: TGUID = '{F512D3C3-7D7F-11D8-B0EB-BD9930084878}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICoClassAcridBazaServer = interface;
  ICoClassAcridBazaServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CoClassAcridBazaServer = ICoClassAcridBazaServer;


// *********************************************************************//
// Interface: ICoClassAcridBazaServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F512D3C1-7D7F-11D8-B0EB-BD9930084878}
// *********************************************************************//
  ICoClassAcridBazaServer = interface(IAppServer)
    ['{F512D3C1-7D7F-11D8-B0EB-BD9930084878}']
    procedure sm_DelStudent(ID_S: Integer); safecall;
    procedure sm_DelPredmet(ID_P: Integer); safecall;
    procedure sm_DelHistory(ID_H: Integer); safecall;
    procedure sm_DelTeacher(ID_T: Integer); safecall;
    procedure sm_DelSession(ID: Integer); safecall;
    procedure sm_DelLecture(ID_L: Integer); safecall;
    function  sm_AddStudent(ID_S: Integer; const F: WideString; const I: WideString; 
                            const O: WideString; N_gr: Smallint; const Prof: WideString; 
                            pData: TDateTime; rData: TDateTime; School: Smallint; 
                            const City: WideString; const Budget: WideString; const Sex: WideString): Integer; safecall;
    function  sm_AddPredmet(ID_P: Integer; const pName: WideString): Integer; safecall;
    function  sm_AddHistory(ID_H: Integer; ID_S: Integer; hData: TDateTime; Code_mark: Smallint): Integer; safecall;
    function  sm_AddTeacher(ID_T: Integer; const F: WideString; const I: WideString; 
                            const O: WideString; const Job: WideString; const Rang: WideString): Integer; safecall;
    function  sm_AddSession(ID: Integer; ID_S: Integer; ID_L: Integer; Code_mark: Smallint; 
                            S_Year: TDateTime): Integer; safecall;
    function  sm_AddLecture(ID_L: Integer; ID_T: Integer; ID_P: Integer; Kurs: Smallint; 
                            const DS: WideString; const Rang: WideString; const Winer: WideString): Integer; safecall;
    procedure sm_QueryClear; safecall;
    procedure sm_QueryExec; safecall;
    procedure sm_QueryAdd(const S: WideString); safecall;
    procedure sm_QueryClear2; safecall;
    procedure sm_QueryExec2; safecall;
    procedure sm_QueryAdd2(const S: WideString); safecall;
    function  sm_UpdateEvenst(const OldStr: WideString): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  ICoClassAcridBazaServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F512D3C1-7D7F-11D8-B0EB-BD9930084878}
// *********************************************************************//
  ICoClassAcridBazaServerDisp = dispinterface
    ['{F512D3C1-7D7F-11D8-B0EB-BD9930084878}']
    procedure sm_DelStudent(ID_S: Integer); dispid 1;
    procedure sm_DelPredmet(ID_P: Integer); dispid 7;
    procedure sm_DelHistory(ID_H: Integer); dispid 8;
    procedure sm_DelTeacher(ID_T: Integer); dispid 9;
    procedure sm_DelSession(ID: Integer); dispid 10;
    procedure sm_DelLecture(ID_L: Integer); dispid 11;
    function  sm_AddStudent(ID_S: Integer; const F: WideString; const I: WideString; 
                            const O: WideString; N_gr: Smallint; const Prof: WideString; 
                            pData: TDateTime; rData: TDateTime; School: Smallint; 
                            const City: WideString; const Budget: WideString; const Sex: WideString): Integer; dispid 12;
    function  sm_AddPredmet(ID_P: Integer; const pName: WideString): Integer; dispid 2;
    function  sm_AddHistory(ID_H: Integer; ID_S: Integer; hData: TDateTime; Code_mark: Smallint): Integer; dispid 3;
    function  sm_AddTeacher(ID_T: Integer; const F: WideString; const I: WideString; 
                            const O: WideString; const Job: WideString; const Rang: WideString): Integer; dispid 4;
    function  sm_AddSession(ID: Integer; ID_S: Integer; ID_L: Integer; Code_mark: Smallint; 
                            S_Year: TDateTime): Integer; dispid 5;
    function  sm_AddLecture(ID_L: Integer; ID_T: Integer; ID_P: Integer; Kurs: Smallint; 
                            const DS: WideString; const Rang: WideString; const Winer: WideString): Integer; dispid 6;
    procedure sm_QueryClear; dispid 13;
    procedure sm_QueryExec; dispid 14;
    procedure sm_QueryAdd(const S: WideString); dispid 15;
    procedure sm_QueryClear2; dispid 17;
    procedure sm_QueryExec2; dispid 18;
    procedure sm_QueryAdd2(const S: WideString); dispid 19;
    function  sm_UpdateEvenst(const OldStr: WideString): WideString; dispid 16;
    function  AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; 
                              MaxErrors: Integer; out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function  AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                            Options: Integer; const CommandText: WideString; 
                            var Params: OleVariant; var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function  AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function  AS_GetProviderNames: OleVariant; dispid 20000003;
    function  AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function  AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                            var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CoCoClassAcridBazaServer provides a Create and CreateRemote method to          
// create instances of the default interface ICoClassAcridBazaServer exposed by              
// the CoClass CoClassAcridBazaServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCoClassAcridBazaServer = class
    class function Create: ICoClassAcridBazaServer;
    class function CreateRemote(const MachineName: string): ICoClassAcridBazaServer;
  end;

implementation

uses ComObj;

class function CoCoClassAcridBazaServer.Create: ICoClassAcridBazaServer;
begin
  Result := CreateComObject(CLASS_CoClassAcridBazaServer) as ICoClassAcridBazaServer;
end;

class function CoCoClassAcridBazaServer.CreateRemote(const MachineName: string): ICoClassAcridBazaServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CoClassAcridBazaServer) as ICoClassAcridBazaServer;
end;

end.
