program AcridBazaServer;

uses
  Forms,
  AcridBazaServer_TLB in 'AcridBazaServer_TLB.pas',
  UntStoredProc in 'UntStoredProc.pas' {CoClassAcridBazaServer: TRemoteDataModule},
  UntFirst in 'UntFirst.pas' {First},
  UntCoder in 'UntCoder.pas';

{$R *.TLB}

{$R *.res}

begin
  Application.Initialize;
  Application.ShowMainForm:=False;
  Application.CreateForm(TFirst, First);
  Application.Run;
end.
