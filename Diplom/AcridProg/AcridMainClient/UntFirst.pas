unit UntFirst;

interface

uses
  Windows, Messages, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, Grids, DBGrids, ComCtrls, ExtCtrls, Buttons, Mask,
  fcButton, fcImgBtn, fcClearPanel, fcButtonGroup, cbClasses,
  CaptionButton, FormRoller, FormTrayIcon, fcLabel;

type
  TFirst = class(TForm)
    pnlSlovar: TPanel;
    Label1: TLabel;
    pnlSecond: TPanel;
    Label2: TLabel;
    edtPassword: TMaskEdit;
    btnKGB: TBitBtn;
    btnStudent: TfcImageBtn;
    btnPredmet: TfcImageBtn;
    btnTeacher: TfcImageBtn;
    btnSession: TfcImageBtn;
    btnLecture: TfcImageBtn;
    btnHistory: TfcImageBtn;
    FormRoller1: TFormRoller;
    FormTrayIcon: TFormTrayIcon;
    pnlLook: TPanel;
    Label3: TLabel;
    btnQuery: TfcImageBtn;
    btnLook: TfcImageBtn;
    btnStatSoft: TfcImageBtn;
    Label4: TLabel;
    procedure btnStudentClick(Sender: TObject);
    procedure btnTeacherClick(Sender: TObject);
    procedure btnPredmetClick(Sender: TObject);
    procedure btnQueryClick(Sender: TObject);
    procedure btnLectureClick(Sender: TObject);
    procedure btnHistoryClick(Sender: TObject);
    procedure btnSessionClick(Sender: TObject);
    procedure btnKGBClick(Sender: TObject);
    procedure btnLookClick(Sender: TObject);
    procedure btnStatSoftClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ClientDir: string;
  end;

var
  First: TFirst;
  Password: string = '371';
  ExceptFlag: boolean = False;

implementation

uses UntDMBaza, UntPredmet, UntTeacher, UntStudent, UntQuery, UntHistory, UntLecture, UntSession,
  UntLook, UntSQLconstr;

{$R *.dfm}

procedure TFirst.btnStudentClick(Sender: TObject);
begin
 if edtPassword.Tag=0 then
  MessageDlg('¬ы находитесь в режиме просмотра и не облабаете правом доступа',mtInformation,[mbOK],0)
  else begin
   fStudent.DBGStudent.DataSource:=dmBazaClient.DSStudent;
   fStudent.Visible:=True;
   First.Visible:=false;
  end;
end;

procedure TFirst.btnTeacherClick(Sender: TObject);
begin
 if edtPassword.Tag=0 then
  MessageDlg('¬ы находитесь в режиме просмотра и не облабаете правом доступа',mtInformation,[mbOK],0)
  else begin
   fTeacher.DBGTeacher.DataSource:=dmBazaClient.DSTeacher;
   fTeacher.Visible:=True;
   First.Visible:=false;
  end;
end;

procedure TFirst.btnPredmetClick(Sender: TObject);
begin
  fPredmet.DBGPredmet.DataSource:=dmBazaClient.DSPredmet;
  fPredmet.Visible:=True;
  First.Visible:=false;
end;

procedure TFirst.btnQueryClick(Sender: TObject);
begin
 if edtPassword.Tag=0 then
 MessageDlg('¬ы находитесь в режиме просмотра и не облабаете правом доступа',mtInformation,[mbOK],0)
  else begin
   dmBazaClient.DSQuery.DataSet := nil;
   fQuery.DBGQuery.DataSource:=dmBazaClient.DSQuery;
   fQuery.Visible:=True;
   First.Visible:=False;
  end;
end;

procedure TFirst.btnLectureClick(Sender: TObject);
begin
  fLecture.Visible:=True;
  First.Visible:=False;
  fLecture.DBGLecture.DataSource:=dmBazaClient.DSLecture;
  fLecture.DBGTeacher.DataSource:=dmBazaClient.DSTeacher;
  fLecture.DBGPredmet.DataSource:=dmBazaClient.DSPredmet;
end;

procedure TFirst.btnHistoryClick(Sender: TObject);
begin
  fHistory.Visible:=True;
  First.Visible:=False;
  fHistory.frmStudent.DBGStudent.DataSource:=dmBazaClient.DSStudent;
  fHistory.DBGHistory.DataSource:=dmBazaClient.DSHistory;
end;

procedure TFirst.btnSessionClick(Sender: TObject);
begin
  fSession.Visible:=True;
  fSession.DBGSession.DataSource:=dmBazaClient.DSSession;
  fSession.frmStudent.DBGStudent.DataSource:=dmBazaClient.DSStudent;
  fSession.DBGLecture.DataSource:=dmBazaClient.DSLecture;
  First.Visible:=false;
end;

procedure TFirst.btnKGBClick(Sender: TObject);
begin
 if (edtPassword.Text=Password) or (edtPassword.Tag=1) then
  begin
   MessageDlg('ѕароль прин¤т, у ¬ас полное право доступа',mtInformation,[mbOK],0);
   edtPassword.Tag:=1;

   dmBazaClient.CDSStudentDeF.Visible:=True;
   dmBazaClient.CDSStudentDeI.Visible:=True;
   dmBazaClient.CDSStudentDeO.Visible:=True;
   dmBazaClient.CDSStudentSAlias.Visible:=False;
   dmBazaClient.CDSTeacherDeF.Visible:=True;
   dmBazaClient.CDSTeacherDeI.Visible:=True;
   dmBazaClient.CDSTeacherDeO.Visible:=True;
   dmBazaClient.CDSTeacherTAlias.Visible:=False;
   dmBazaClient.CDSHistoryXF.Visible:=True;
   dmBazaClient.CDSHistoryXI.Visible:=True;
   dmBazaClient.CDSHistorySAlias.Visible:=False;
   dmBazaClient.CDSLectureXT.Visible:=True;
   dmBazaClient.CDSLectureTAlias.Visible:=False;
   dmBazaClient.CDSSessionXF.Visible:=True;
   dmBazaClient.CDSSessionXI.Visible:=True;
   dmBazaClient.CDSSessionSAlias.Visible:=False;
   edtPassword.Visible:=False;
   btnKGB.Visible:=False;
 end else
   MessageDlg('Ќе верный пароль',mtInformation,[mbOK],0);
end;

procedure TFirst.btnLookClick(Sender: TObject);
begin
  fLook.Visible:=True;
  if fLook.btnStudent.Down=True then fLook.DBGFirst.DataSource:=dmBazaClient.DSStudent else
  if fLook.btnTeacher.Down=True then fLook.DBGFirst.DataSource:=dmBazaClient.DSTeacher
    else fLook.DBGFirst.DataSource:=dmBazaClient.DSPredmet;
  fLook.DBGQuery.DataSource:=dmBazaClient.DSQuery;
end;

procedure TFirst.btnStatSoftClick(Sender: TObject);
 var StrSQL : string;
begin
   dmBazaClient.DSQuery.DataSet := nil;
   StrSQL:='SELECT P.ID_P,P.pName AS "Ќазвание" FROM Predmet P '
           +'ORDER BY P.ID_P;';
   dmBazaClient.CDSQuery.Close;
   dmBazaClient.DCOMConnection.AppServer.sm_QueryClear;
   dmBazaClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
   dmBazaClient.DCOMConnection.AppServer.sm_QueryExec;
   dmBazaClient.CDSQuery.Open;
   dmBazaClient.DSQuery.DataSet :=dmBazaClient.CDSQuery;
   fSQLconstructor.DBGp1.DataSource:=dmBazaClient.DSPredmet;
   fSQLconstructor.DBGp2.DataSource:=dmBazaClient.DSQuery;
   fSQLconstructor.DBGp2.Columns[0].Visible:=False;
   fSQLconstructor.DBGQuery.DataSource:=dmBazaClient.DSQuery2;
   fSQLconstructor.Visible:=True;
   First.Visible:=false;
end;

end.
