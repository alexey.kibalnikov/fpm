unit UntLook;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cbClasses, CaptionButton, FormRoller, Grids, DBGrids, StdCtrls,
  fcButton, fcImgBtn, fcClearPanel, fcButtonGroup, ExtCtrls, fcImager;

type
  TfLook = class(TForm)
    FormRoller1: TFormRoller;
    DBGQuery: TDBGrid;
    DBGFirst: TDBGrid;
    btnMenu: TfcImageBtn;
    Panel1: TPanel;
    fcButtonGroup1: TfcButtonGroup;
    btnStudent: TfcImageBtn;
    btnTeacher: TfcImageBtn;
    btnPredmet: TfcImageBtn;
    fcImager3: TfcImager;
    SortSQL: TfcButtonGroup;
    btnSortID: TfcImageBtn;
    btnSortAlfavit: TfcImageBtn;
    procedure DBGFirstCellClick(Column: TColumn);
    procedure btnMenuClick(Sender: TObject);
    procedure btnStudentClick(Sender: TObject);
    procedure btnTeacherClick(Sender: TObject);
    procedure btnPredmetClick(Sender: TObject);
    procedure SortSQLChange(ButtonGroup: TfcCustomButtonGroup;
      OldSelected, Selected: TfcButtonGroupItem);
  private
    procedure SQLQuery;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fLook: TfLook;

implementation

uses
  UntDMBaza,
  UntFirst;

{$R *.dfm}

procedure TfLook.SQLQuery;
 var StrSQL: string;
begin
  if btnStudent.Down=True then
    begin
      StrSQL:='SELECT P.pName AS "Предмет",C.Code AS "Оценка",SS.S_Year AS "Дата" '
          +'FROM Session Ss INNER JOIN Code_mark C ON Ss.code_mark=C.ID_C INNER JOIN Lecture L ON Ss.ID_L=L.ID_L INNER JOIN Predmet P ON L.ID_P=P.ID_P '
          +'WHERE Ss.ID_S='+IntToStr(dmBazaClient.CDSStudentID_S.Value)+' ';

      if btnSortID.Down = True then
        StrSQL := StrSQL + 'ORDER BY L.Kurs';
      if btnSortAlfavit.Down = True then
        StrSQL := StrSQL + 'ORDER BY P.pName';
    end;
  if btnTeacher.Down=True then
    begin
      StrSQL:='SELECT P.pName AS "Предмет",L.Kurs AS "Курс",L.DS AS "ДС/Общий",L.Rang AS "Зач/Экз",L.Winer AS "Зимой" '
          +' FROM Predmet P INNER JOIN Lecture L ON P.ID_P=L.ID_P INNER JOIN Teacher T ON L.ID_T=T.ID_T '
          +'WHERE L.ID_T='+IntToStr(dmBazaClient.CDSTeacherID_T.Value)+' ';

      if btnSortID.Down = True then
        StrSQL := StrSQL + 'ORDER BY L.Kurs';
      if btnSortAlfavit.Down = True then
        StrSQL := StrSQL + 'ORDER BY P.pName';
    end;
  if btnPredmet.Down=True then
    begin
      StrSQL:='SELECT DISTINCT T.F AS "Фамилия",T.I AS "Имя",T.O AS "Отчество" '
          +'FROM Predmet P INNER JOIN Lecture L ON L.ID_P=P.ID_P INNER JOIN Teacher T ON L.ID_T=T.ID_T '
          +'WHERE L.ID_P='+IntToStr(dmBazaClient.CDSPredmetID_P.Value)+' ';

      if btnSortID.Down = True then
        StrSQL := StrSQL + 'ORDER BY L.Kurs';
      if btnSortAlfavit.Down = True then
        StrSQL := StrSQL + 'ORDER BY T.F';
    end;
// DeCoder
  dmBazaClient.DSQuery.DataSet := nil;
  dmBazaClient.CDSQuery.Close;
  dmBazaClient.DCOMConnection.AppServer.sm_QueryClear;
  dmBazaClient.DCOMConnection.AppServer.sm_QueryAdd(StrSQL);
  dmBazaClient.DCOMConnection.AppServer.sm_QueryExec;
  dmBazaClient.CDSQuery.Open;
  dmBazaClient.DSQuery.DataSet :=dmBazaClient.CDSQuery;
end;// TfLook.SQLQuery

procedure TfLook.DBGFirstCellClick(Column: TColumn);
begin SQLQuery; end;

procedure TfLook.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fLook.Visible:=False;
  fLook.DBGFirst.DataSource:=nil;
  fLook.DBGQuery.DataSource:=nil;
end;

procedure TfLook.btnStudentClick(Sender: TObject);
begin DBGFirst.DataSource := dmBazaClient.DSStudent; end;

procedure TfLook.btnTeacherClick(Sender: TObject);
begin DBGFirst.DataSource := dmBazaClient.DSTeacher; end;

procedure TfLook.btnPredmetClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
   MessageDlg('Вы находитесь в режиме просмотра и не облабаете правом доступа',mtInformation,[mbOK],0)
  else DBGFirst.DataSource:=dmBazaClient.DSPredmet;
end;

procedure TfLook.SortSQLChange(ButtonGroup: TfcCustomButtonGroup;
  OldSelected, Selected: TfcButtonGroupItem);
begin SQLQuery; end;

end.
