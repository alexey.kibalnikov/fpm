unit UntPredmet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, ExtCtrls, fcButton, fcImgBtn,
  fcClearPanel, fcButtonGroup, cbClasses, CaptionButton, FormRoller,
  fcImager, UntFrameModify;

type
  TfPredmet = class(TForm)
    DBGPredmet: TDBGrid;
    edtP: TEdit;
    btnMenu: TfcImageBtn;
    FormRoller1: TFormRoller;
    fcImager3: TfcImager;
    HorizontalImageBtnGroup: TfcButtonGroup;
    btnSortID: TfcImageBtn;
    btnSortPname: TfcImageBtn;
    Label1: TLabel;
    frmModify: TfrmModify;
    procedure btnMenuClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBGPredmetCellClick(Column: TColumn);
    procedure btnSortIDClick(Sender: TObject);
    procedure btnSortPNameClick(Sender: TObject);
    procedure frmModifybtnAddClick(Sender: TObject);
    procedure frmModifybtnUpClick(Sender: TObject);
    procedure frmModifybtnDelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPredmet: TfPredmet;

implementation

uses
  UntDMBaza,
  UntFirst;

{$R *.dfm}

procedure TfPredmet.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fPredmet.Visible:=False;
  fPredmet.DBGPredmet.DataSource:= nil;
end;

procedure TfPredmet.FormActivate(Sender: TObject);
begin
 edtP.Text:=dmBazaClient.CDSPredmetPNAME.Value;
end;

procedure TfPredmet.DBGPredmetCellClick(Column: TColumn);
begin
 edtP.Text:=dmBazaClient.CDSPredmetPNAME.Value;
end;

procedure TfPredmet.btnSortIDClick(Sender: TObject);
begin dmBazaClient.CDSPredmet.IndexFieldNames := 'ID_P';end;

procedure TfPredmet.btnSortPNameClick(Sender: TObject);
begin dmBazaClient.CDSPredmet.IndexFieldNames := 'pName;ID_P';end;

procedure TfPredmet.frmModifybtnAddClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
    if dmBazaClient.DCOMConnection.AppServer.sm_AddPredmet(0,edtP.Text) > 0
     then dmBazaClient.CDSPredmet.Refresh;
  end;
end;

procedure TfPredmet.frmModifybtnUpClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmBazaClient.DCOMConnection.AppServer.sm_AddPredmet(dmBazaClient.CDSPredmetID_P.Value,edtP.Text) > 0
     then dmBazaClient.CDSPredmet.Refresh;
  end;
 end;
end;

procedure TfPredmet.frmModifybtnDelClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmBazaClient.DCOMConnection.AppServer.sm_DelPredmet(dmBazaClient.CDSPredmetID_P.Value);
   dmBazaClient.CDSPredmet.Refresh;
  end;
 end;
end;

end.
