program AcridClient;

uses
  Forms,
  SysUtils,
  Dialogs,
  UntFirst in 'UntFirst.pas' {First},
  UntSkinAcrid in 'UntSkinAcrid.pas' {fSkinAcrid},
  UntPredmet in 'UntPredmet.pas' {fPredmet},
  UntTeacher in 'UntTeacher.pas' {fTeacher},
  UntStudent in 'UntStudent.pas' {fStudent},
  UntQuery in 'UntQuery.pas' {fQuery},
  UntHistory in 'UntHistory.pas' {fHistory},
  UntLecture in 'UntLecture.pas' {fLecture},
  UntSession in 'UntSession.pas' {fSession},
  UntLook in 'UntLook.pas' {fLook},
  UntSQLconstr in 'UntSQLconstr.pas' {fSQLconstructor},
  UntFrameModify in 'UntFrameModify.pas' {frmModify: TFrame},
  UntFrameStudent in 'UntFrameStudent.pas' {frmStudent: TFrame},
  UntDMBaza in 'UntDMBaza.pas' {dmBazaClient: TDataModule},
  UntDMStatSoft in 'UntDMStatSoft.pas' {dmStatSoftClient: TDataModule},
  UntConnectStatSoft in 'UntConnectStatSoft.pas',
  UntInterface in 'UntInterface.pas',
  UntResultFileParser in 'UntResultFileParser.pas';

{$R *.res}

begin
  try
    Application.Initialize;
    Application.CreateForm(TFirst, First);
  Application.CreateForm(TfSkinAcrid, fSkinAcrid);
  fSkinAcrid.Visible := True; // ������������ ��� ��������
    fSkinAcrid.Repaint;

    Application.CreateForm(TdmBazaClient, dmBazaClient);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TdmStatSoftClient, dmStatSoftClient);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfHistory, fHistory);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfPredmet, fPredmet);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfTeacher, fTeacher);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfStudent, fStudent);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfQuery, fQuery);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfLecture, fLecture);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfSession, fSession);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfLook, fLook);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    Application.CreateForm(TfSQLconstructor, fSQLconstructor);
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    if ExceptFlag then
        begin // ������ �������������
          MessageDlg('��� �������� �������� ������,'#10#13
                     +'������� ������ �����������'#13#10
                     +'���������� ������, ��������� ��'#13#10
                     +'� ��������� ���������� ��� ���'
                     ,mtWarning, [mbOk], 0);
          First.Close;
        end
    else
      fSkinAcrid.Close;

    Application.Run;
  except
    on E: Exception do
      MessageDlg(E.Message, mtError, [mbOk], 0);
  end;//try
end.
