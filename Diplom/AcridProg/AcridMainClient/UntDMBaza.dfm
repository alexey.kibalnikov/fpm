object dmBazaClient: TdmBazaClient
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 217
  Top = 141
  Height = 189
  Width = 370
  object CDSStudent: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPStudent'
    RemoteServer = DCOMConnection
    OnCalcFields = CDSStudentCalcFields
    Left = 24
    Top = 64
    object CDSStudentID_S: TIntegerField
      FieldName = 'ID_S'
      ReadOnly = True
      Required = True
      Visible = False
    end
    object CDSStudentSAlias: TStringField
      DisplayLabel = #1057#1090#1091#1076#1077#1085#1090' '
      DisplayWidth = 6
      FieldKind = fkCalculated
      FieldName = 'SAlias'
      ReadOnly = True
      Calculated = True
    end
    object CDSStudentDeF: TStringField
      DisplayLabel = #1060#1072#1084#1080#1083#1080#1103
      FieldName = 'DeF'
      ReadOnly = True
      Visible = False
    end
    object CDSStudentDeI: TStringField
      DisplayLabel = #1048#1084#1103
      DisplayWidth = 6
      FieldName = 'DeI'
      ReadOnly = True
      Visible = False
    end
    object CDSStudentDeO: TStringField
      DisplayLabel = #1054#1090#1095#1077#1089#1090#1074#1086' '
      DisplayWidth = 6
      FieldName = 'DeO'
      ReadOnly = True
      Visible = False
    end
    object CDSStudentRDATA: TDateField
      DisplayLabel = #1056#1086#1076#1080#1083#1089#1103
      DisplayWidth = 10
      FieldName = 'RDATA'
    end
    object CDSStudentN_GR: TSmallintField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      DisplayWidth = 2
      FieldName = 'N_GR'
    end
    object CDSStudentPROF: TStringField
      DisplayLabel = #1050#1072#1092#1077#1076#1088#1072
      DisplayWidth = 4
      FieldName = 'PROF'
      Size = 3
    end
    object CDSStudentPDATA: TDateField
      DisplayLabel = #1055#1086#1089#1090#1091#1087#1080#1083
      FieldName = 'PDATA'
    end
    object CDSStudentCITY: TStringField
      DisplayLabel = #1055#1088#1086#1087#1080#1089#1082#1072
      FieldName = 'CITY'
      Size = 1
    end
    object CDSStudentSCHOOL: TSmallintField
      DisplayLabel = #1064#1082#1086#1083#1072
      DisplayWidth = 3
      FieldName = 'SCHOOL'
    end
    object CDSStudentBUDGET: TStringField
      DisplayLabel = #1060#1086#1088#1084#1072
      FieldName = 'BUDGET'
      Size = 1
    end
    object CDSStudentSEX: TStringField
      FieldName = 'SEX'
      Size = 1
    end
    object CDSStudentF: TStringField
      FieldName = 'F'
      Visible = False
      Size = 30
    end
    object CDSStudentI: TStringField
      FieldName = 'I'
      Visible = False
      Size = 30
    end
    object CDSStudentO: TStringField
      FieldName = 'O'
      Visible = False
      Size = 30
    end
  end
  object CDSPredmet: TClientDataSet
    Tag = 2
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPPredmet'
    RemoteServer = DCOMConnection
    Left = 72
    Top = 48
    object CDSPredmetID_P: TIntegerField
      FieldName = 'ID_P'
      Visible = False
    end
    object CDSPredmetPNAME: TStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077
      FieldName = 'PNAME'
      Size = 30
    end
  end
  object DCOMConnection: TDCOMConnection
    ServerGUID = '{F512D3C3-7D7F-11D8-B0EB-BD9930084878}'
    ServerName = 'AcridBazaServer.CoClassAcridBazaServer'
    Left = 32
  end
  object CDSHistory: TClientDataSet
    Tag = 4
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPHistory'
    ReadOnly = True
    RemoteServer = DCOMConnection
    OnCalcFields = CDSHistoryCalcFields
    Left = 168
    Top = 48
    object CDSHistoryID_H: TIntegerField
      FieldName = 'ID_H'
      Visible = False
    end
    object CDSHistoryID_S: TIntegerField
      FieldName = 'ID_S'
      Visible = False
    end
    object CDSHistorySAlias: TStringField
      DisplayLabel = #1057#1090#1091#1076#1077#1085#1090' '
      FieldKind = fkCalculated
      FieldName = 'SAlias '
      ReadOnly = True
      Size = 6
      Calculated = True
    end
    object CDSHistoryXF: TStringField
      DisplayLabel = #1060#1072#1084#1080#1083#1080#1103' '
      FieldKind = fkLookup
      FieldName = 'XF'
      LookupDataSet = CDSStudent
      LookupKeyFields = 'ID_S'
      LookupResultField = 'DeF'
      KeyFields = 'ID_S'
      ReadOnly = True
      Visible = False
      Lookup = True
    end
    object CDSHistoryXI: TStringField
      DisplayLabel = #1048#1084#1103
      DisplayWidth = 8
      FieldKind = fkLookup
      FieldName = 'XI'
      LookupDataSet = CDSStudent
      LookupKeyFields = 'ID_S'
      LookupResultField = 'DeI'
      KeyFields = 'ID_S'
      ReadOnly = True
      Visible = False
      Size = 10
      Lookup = True
    end
    object CDSHistoryHDATA: TDateField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'HDATA'
    end
    object CDSHistoryCODE_MARK: TSmallintField
      FieldName = 'CODE_MARK'
      Visible = False
    end
    object CDSHistoryXCode: TStringField
      DisplayLabel = #1057#1086#1073#1099#1090#1080#1077
      FieldKind = fkLookup
      FieldName = 'XCode'
      LookupDataSet = CDSCode
      LookupKeyFields = 'ID_C'
      LookupResultField = 'CODE'
      KeyFields = 'CODE_MARK'
      Lookup = True
    end
  end
  object CDSTeacher: TClientDataSet
    Tag = 3
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPTeacher'
    RemoteServer = DCOMConnection
    OnCalcFields = CDSTeacherCalcFields
    Left = 120
    Top = 64
    object CDSTeacherID_T: TIntegerField
      FieldName = 'ID_T'
      Visible = False
    end
    object CDSTeacherTAlias: TStringField
      DisplayLabel = #1055#1088#1077#1087#1086#1076'.'
      DisplayWidth = 6
      FieldKind = fkCalculated
      FieldName = 'TAlias'
      ReadOnly = True
      Size = 7
      Calculated = True
    end
    object CDSTeacherDeF: TStringField
      DisplayLabel = #1060#1072#1084#1080#1083#1080#1103' '
      DisplayWidth = 6
      FieldName = 'DeF'
      ReadOnly = True
      Visible = False
      Size = 30
    end
    object CDSTeacherDeI: TStringField
      DisplayLabel = #1048#1084#1103
      DisplayWidth = 6
      FieldName = 'DeI'
      ReadOnly = True
      Visible = False
      Size = 30
    end
    object CDSTeacherDeO: TStringField
      DisplayLabel = #1054#1090#1095#1077#1089#1090#1074#1086
      DisplayWidth = 6
      FieldName = 'DeO'
      ReadOnly = True
      Visible = False
    end
    object CDSTeacherJOB: TStringField
      DisplayLabel = #1044#1086#1083#1078#1085#1086#1089#1090#1100
      FieldName = 'JOB'
      Size = 30
    end
    object CDSTeacherRANG: TStringField
      DisplayLabel = #1053#1072#1091#1095#1085#1086#1077' '#1079#1074#1072#1085#1080#1077
      FieldName = 'RANG'
    end
    object CDSTeacherF: TStringField
      DisplayWidth = 30
      FieldName = 'F'
      Visible = False
    end
    object CDSTeacherI: TStringField
      FieldName = 'I'
      Visible = False
      Size = 15
    end
    object CDSTeacherO: TStringField
      FieldName = 'O'
      Visible = False
      Size = 15
    end
  end
  object CDSSession: TClientDataSet
    Tag = 6
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPSsesion'
    RemoteServer = DCOMConnection
    OnCalcFields = CDSSessionCalcFields
    Left = 264
    Top = 48
    object CDSSessionID: TIntegerField
      FieldName = 'ID'
      Visible = False
    end
    object CDSSessionID_S: TIntegerField
      FieldName = 'ID_S'
      Visible = False
    end
    object CDSSessionID_L: TIntegerField
      FieldName = 'ID_L'
      Visible = False
    end
    object CDSSessionSAlias: TStringField
      DisplayLabel = #1057#1090#1091#1076#1077#1085#1090
      FieldKind = fkCalculated
      FieldName = 'SAlias'
      ReadOnly = True
      Size = 6
      Calculated = True
    end
    object CDSSessionXF: TStringField
      DisplayLabel = #1060#1072#1084#1080#1083#1080#1103
      FieldKind = fkLookup
      FieldName = 'XF'
      LookupDataSet = CDSStudent
      LookupKeyFields = 'ID_S'
      LookupResultField = 'DeF'
      KeyFields = 'ID_S'
      ReadOnly = True
      Visible = False
      Lookup = True
    end
    object CDSSessionXI: TStringField
      DisplayLabel = #1048#1084#1103
      DisplayWidth = 8
      FieldKind = fkLookup
      FieldName = 'XI'
      LookupDataSet = CDSStudent
      LookupKeyFields = 'ID_S'
      LookupResultField = 'DeI'
      KeyFields = 'ID_S'
      ReadOnly = True
      Visible = False
      Lookup = True
    end
    object CDSSessionXP: TStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '
      FieldKind = fkLookup
      FieldName = 'XP'
      LookupDataSet = CDSLecture
      LookupKeyFields = 'ID_L'
      LookupResultField = 'XP'
      KeyFields = 'ID_L'
      ReadOnly = True
      Lookup = True
    end
    object CDSSessionCODE_MARK: TSmallintField
      DisplayWidth = 8
      FieldName = 'CODE_MARK'
      Visible = False
    end
    object CDSSessionXC: TStringField
      DisplayLabel = #1054#1094#1077#1085#1082#1072' '
      DisplayWidth = 8
      FieldKind = fkLookup
      FieldName = 'XC'
      LookupDataSet = CDSCode
      LookupKeyFields = 'ID_C'
      LookupResultField = 'CODE'
      KeyFields = 'CODE_MARK'
      ReadOnly = True
      Lookup = True
    end
    object CDSSessionS_YEAR: TDateField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'S_YEAR'
    end
  end
  object CDSLecture: TClientDataSet
    Tag = 5
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPLecture'
    RemoteServer = DCOMConnection
    OnCalcFields = CDSLectureCalcFields
    Left = 216
    Top = 64
    object CDSLectureID_L: TIntegerField
      FieldName = 'ID_L'
      Visible = False
    end
    object CDSLectureID_T: TIntegerField
      FieldName = 'ID_T'
      Visible = False
    end
    object CDSLectureID_P: TIntegerField
      FieldName = 'ID_P'
      Visible = False
    end
    object CDSLectureXP: TStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'XP'
      LookupDataSet = CDSPredmet
      LookupKeyFields = 'ID_P'
      LookupResultField = 'PNAME'
      KeyFields = 'ID_P'
      ReadOnly = True
      Size = 30
      Lookup = True
    end
    object CDSLectureXT: TStringField
      DisplayLabel = #1055#1088#1077#1087#1086#1076#1072#1074#1072#1090#1077#1083#1100' '
      FieldKind = fkLookup
      FieldName = 'XT'
      LookupDataSet = CDSTeacher
      LookupKeyFields = 'ID_T'
      LookupResultField = 'DeF'
      KeyFields = 'ID_T'
      ReadOnly = True
      Visible = False
      Size = 15
      Lookup = True
    end
    object CDSLectureTAlias: TStringField
      DisplayLabel = #1055#1088#1077#1087#1086#1076'.'
      FieldKind = fkCalculated
      FieldName = 'TAlias'
      ReadOnly = True
      Size = 6
      Calculated = True
    end
    object CDSLectureKURS: TSmallintField
      DisplayLabel = #1050#1091#1088#1089
      DisplayWidth = 5
      FieldName = 'KURS'
    end
    object CDSLectureDS: TStringField
      DisplayLabel = #1044'/'#1057
      DisplayWidth = 3
      FieldName = 'DS'
      Size = 1
    end
    object CDSLectureRANG: TStringField
      DisplayLabel = #1069#1082#1079'/'#1047#1072#1095
      DisplayWidth = 5
      FieldName = 'RANG'
      Size = 3
    end
    object CDSLectureWINER: TStringField
      DisplayLabel = #1047#1080#1084#1086#1081
      DisplayWidth = 6
      FieldName = 'WINER'
      Size = 1
    end
  end
  object CDSQuery: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPQuery'
    RemoteServer = DCOMConnection
    Left = 264
  end
  object DSStudent: TDataSource
    DataSet = CDSStudent
    Left = 24
    Top = 112
  end
  object DSPredmet: TDataSource
    DataSet = CDSPredmet
    Left = 72
    Top = 96
  end
  object DSTeacher: TDataSource
    DataSet = CDSTeacher
    Left = 120
    Top = 112
  end
  object DSQuery: TDataSource
    DataSet = CDSQuery
    Left = 312
    Top = 16
  end
  object DSLecture: TDataSource
    DataSet = CDSLecture
    Left = 216
    Top = 112
  end
  object DSHistory: TDataSource
    DataSet = CDSHistory
    Left = 168
    Top = 96
  end
  object DSSession: TDataSource
    DataSet = CDSSession
    Left = 264
    Top = 96
  end
  object CDSCode: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPCode'
    RemoteServer = DCOMConnection
    Left = 120
    Top = 16
    object CDSCodeID_C: TIntegerField
      FieldName = 'ID_C'
      Required = True
    end
    object CDSCodeCODE: TStringField
      FieldName = 'CODE'
      Size = 30
    end
  end
  object CDSQuery2: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPQuery2'
    RemoteServer = DCOMConnection
    Left = 312
    Top = 64
  end
  object DSQuery2: TDataSource
    DataSet = CDSQuery2
    Left = 312
    Top = 112
  end
end
