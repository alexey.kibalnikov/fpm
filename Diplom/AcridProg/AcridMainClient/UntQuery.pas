unit UntQuery;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, fcButton, fcImgBtn, cbClasses,
  CaptionButton, FormRoller, Buttons;

type
  TfQuery = class(TForm)
    memoSQL: TMemo;
    DBGQuery: TDBGrid;
    btnMenu: TfcImageBtn;
    FormRoller1: TFormRoller;
    btnSQL: TSpeedButton;
    procedure btnSQLClick(Sender: TObject);
    procedure btnMenuClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fQuery: TfQuery;

implementation

uses
  UntDMBaza,
  UntFirst;

{$R *.dfm}

procedure TfQuery.btnSQLClick(Sender: TObject);
 var i:integer;
begin
  dmBazaClient.DSQuery.DataSet := nil;
  dmBazaClient.CDSQuery.Close;
  dmBazaClient.DCOMConnection.AppServer.sm_QueryClear;
  for i:=0 to MemoSQL.Lines.Count -1 do
      dmBazaClient.DCOMConnection.AppServer.sm_QueryAdd(MemoSQL.Lines[i]);
  dmBazaClient.DCOMConnection.AppServer.sm_QueryExec;
  dmBazaClient.CDSQuery.Open;
  dmBazaClient.DSQuery.DataSet :=dmBazaClient.CDSQuery;
end;

procedure TfQuery.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fQuery.Visible:=False;
  fQuery.DBGQuery.DataSource:= nil;
end;

end.
