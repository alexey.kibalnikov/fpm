unit UntSQLconstr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fcButton, fcImgBtn, Grids, DBGrids, cbClasses,
  CaptionButton, FormRoller, StdCtrls, ExtCtrls, fcClearPanel,
  fcButtonGroup, ComCtrls, fcLabel, Buttons, CheckLst;

type
  arN_gr = array[1..8] of boolean;
  arProf = array[1..4] of boolean;
  tParam = (_P1_, _P2_, _T1_, _T2_, _K1_, _K2_, _Prof_);

  TfSQLconstructor = class(TForm)
    FormRoller1: TFormRoller;
    DBGQuery: TDBGrid;
    btnMenu: TfcImageBtn;
    memoSQL: TMemo;
    pnlWhwre: TPanel;
    Label1: TLabel;
    pnlCity: TPanel;
    RGCity: TfcButtonGroup;
    btnCityT: TfcImageBtn;
    btnCityF: TfcImageBtn;
    pnlSex: TPanel;
    pnlBudget: TPanel;
    RGBudget: TfcButtonGroup;
    btnBudgetT: TfcImageBtn;
    btnBudgetF: TfcImageBtn;
    pnlN_gr: TPanel;
    RGN_gr: TfcButtonGroup;
    btnN_gr_1: TfcImageBtn;
    btnN_gr_2: TfcImageBtn;
    btnN_gr_3: TfcImageBtn;
    btnN_gr_4: TfcImageBtn;
    btnN_gr_5: TfcImageBtn;
    btnN_gr_6: TfcImageBtn;
    btnN_gr_7: TfcImageBtn;
    btnN_gr_8: TfcImageBtn;
    pnlProf: TPanel;
    RGProf: TfcButtonGroup;
    btnProfKPM: TfcImageBtn;
    btnProfK4A: TfcImageBtn;
    btnProfKMM: TfcImageBtn;
    btnProfKIT: TfcImageBtn;
    Label2: TLabel;
    Label3: TLabel;
    DBGp1: TDBGrid;
    btnRun: TfcImageBtn;
    pnlSelect: TPanel;
    RGSex: TfcButtonGroup;
    btnSexM: TfcImageBtn;
    btnSexW: TfcImageBtn;
    DBGp2: TDBGrid;
    Label5: TLabel;
    pnlStatMethodID: TPanel;
    Label4: TLabel;
    pnlResult: TPanel;
    bntClosePnlResult: TButton;
    pnlTime: TPanel;
    ProgressBarSQL: TProgressBar;
    lbTimeComment: TLabel;
    sgFileResultData: TStringGrid;
    lbCommentMethodID: TLabel;
    btnSQL: TfcImageBtn;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edtCountAll: TEdit;
    rgStatMethodID: TfcButtonGroup;
    btnFrequency: TfcImageBtn;
    btnProbability: TfcImageBtn;
    btnMx: TfcImageBtn;
    btnDx: TfcImageBtn;
    btnLineCorrelation: TfcImageBtn;
    btnPoligon: TfcImageBtn;
    btnFx: TfcImageBtn;
    btnPx: TfcImageBtn;
    pnlExecuteParams: TPanel;
    rgGroupParam: TfcButtonGroup;
    btnGrP1: TfcImageBtn;
    btnGrP2: TfcImageBtn;
    btnGrK1: TfcImageBtn;
    btnGrK2: TfcImageBtn;
    btnGrProf: TfcImageBtn;
    btnGrT2: TfcImageBtn;
    btnGrT1: TfcImageBtn;
    pnlGroup: TPanel;
    pnlVariable: TPanel;
    rgVariableParam: TfcButtonGroup;
    btnVarP2: TfcImageBtn;
    btnVarT1: TfcImageBtn;
    btnVarT2: TfcImageBtn;
    btnVarK1: TfcImageBtn;
    btnVarK2: TfcImageBtn;
    btnVarProf: TfcImageBtn;
    Label9: TLabel;
    Label10: TLabel;
    pnlVar2: TPanel;
    rgVar2: TfcButtonGroup;
    btnVar2P1: TfcImageBtn;
    btnVar2P2: TfcImageBtn;
    btnVar2T1: TfcImageBtn;
    btnVar2T2: TfcImageBtn;
    btnVar2K1: TfcImageBtn;
    btnVar2K2: TfcImageBtn;
    btnVar2Prof: TfcImageBtn;
    Label11: TLabel;
    Label12: TLabel;
    lbVar2: TLabel;
    lbRemVar2: TLabel;
    btnVarP1: TfcImageBtn;
    lbRem1: TLabel;
    lbRem2: TLabel;
    Pole: TImage;
    clbLook: TCheckListBox;
    btnGraph: TBitBtn;
    cbClear: TCheckBox;
    pnlLegenda: TPanel;
    Label15: TLabel;
    pnlViborResult: TPanel;
    rgViborResult: TfcButtonGroup;
    btnViborGraph: TfcImageBtn;
    btnViborTabel: TfcImageBtn;
    Label16: TLabel;
    Label17: TLabel;
    edtAlpha: TEdit;
    edtEpsilon: TEdit;
    btnHelp: TfcImageBtn;
    pnlTwo: TPanel;
    RGTwo: TfcButtonGroup;
    btnTwoYes: TfcImageBtn;
    btnTwoNo: TfcImageBtn;
    Label13: TLabel;
    Label14: TLabel;
    procedure btnMenuClick(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure btnSQLClick(Sender: TObject);
    procedure bntClosePnlResultClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFrequencyClick(Sender: TObject);
    procedure btnProbabilityClick(Sender: TObject);
    procedure btnMxClick(Sender: TObject);
    procedure btnDxClick(Sender: TObject);
    procedure btnLineCorrelationClick(Sender: TObject);
    procedure btnSpirmenRangCorrelationClick(Sender: TObject);
    procedure btnKendallRangCorrelationClick(Sender: TObject);
    procedure btnGrP1Click(Sender: TObject);
    procedure btnGrP2Click(Sender: TObject);
    procedure btnGrT1Click(Sender: TObject);
    procedure btnGrT2Click(Sender: TObject);
    procedure btnGrK1Click(Sender: TObject);
    procedure btnGrK2Click(Sender: TObject);
    procedure btnGrProfClick(Sender: TObject);
    procedure btnVarP1Click(Sender: TObject);
    procedure btnVarP2Click(Sender: TObject);
    procedure btnVarT1Click(Sender: TObject);
    procedure btnVarT2Click(Sender: TObject);
    procedure btnVarK1Click(Sender: TObject);
    procedure btnVarK2Click(Sender: TObject);
    procedure btnVarProfClick(Sender: TObject);
    procedure btnVar2P1Click(Sender: TObject);
    procedure btnVar2P2Click(Sender: TObject);
    procedure btnVar2T1Click(Sender: TObject);
    procedure btnVar2T2Click(Sender: TObject);
    procedure btnVar2K1Click(Sender: TObject);
    procedure btnVar2K2Click(Sender: TObject);
    procedure btnVar2ProfClick(Sender: TObject);
    procedure btnPoligonClick(Sender: TObject);
    procedure btnFxClick(Sender: TObject);
    procedure btnPxClick(Sender: TObject);
    procedure cbClearClick(Sender: TObject);
    procedure btnGraphClick(Sender: TObject);
    procedure btnViborGraphClick(Sender: TObject);
    procedure btnViborTabelClick(Sender: TObject);
    procedure rgStatMethodIDChange(ButtonGroup: TfcCustomButtonGroup;
      OldSelected, Selected: TfcButtonGroupItem);
    procedure btnHelpClick(Sender: TObject);
  private
    { Private declarations }
    countStolb: longint;
    function Test_Where(var N_gr : arN_gr;var Prof : arProf; var N_grFlag, ProfFlag, KursFlag : boolean):string;
    procedure CreateDataFileFromCDSQuery2(countStolb: longint; var countStr: longint);
  public
    { Public declarations }
  end;

var
  fSQLconstructor: TfSQLconstructor;

implementation

uses
  ShellApi,
  UntFirst,
  UntDMBaza,
  UntConnectStatSoft,
  UntDMStatSoft,
  UntInterface,
  UntResultFileParser;

const
  nullSQL = -1;
  newSQL = 2; // оптимизация пересылок
  oldSQL = 3;

{$R *.dfm}
procedure TfSQLconstructor.btnMenuClick(Sender: TObject);
begin
  First.Visible := True;
  fSQLconstructor.Visible := False;
  fSQLconstructor.DBGQuery.DataSource := nil;
  fSQLconstructor.DBGp1.DataSource := nil;
  fSQLconstructor.DBGp2.DataSource := nil;
  btnRun.Down := False;
  btnSQL.Down := False;
end;

function TfSQLconstructor.Test_Where(var N_gr : arN_gr;var Prof : arProf; var N_grFlag, ProfFlag, KursFlag : boolean):string;
 var i : integer;
     StrWhere : string;
begin
//Test part
  N_gr[1]:=btnN_gr_1.Down;
  N_gr[2]:=btnN_gr_2.Down;
  N_gr[3]:=btnN_gr_3.Down;
  N_gr[4]:=btnN_gr_4.Down;
  N_gr[5]:=btnN_gr_5.Down;
  N_gr[6]:=btnN_gr_6.Down;
  N_gr[7]:=btnN_gr_7.Down;
  N_gr[8]:=btnN_gr_8.Down;
  Prof[1]:=btnProfKPM.Down;
  Prof[2]:=btnProfK4A.Down;
  Prof[3]:=btnProfKMM.Down;
  Prof[4]:=btnProfKIT.Down;
  N_grFlag:=False;   RGN_gr.Tag:=0;
  ProfFlag:=False;   RGProf.Tag:=0;
  KursFlag:=False;
                     RGSex.Tag:=1;
                     RGBudget.Tag:=1;
                     RGCity.Tag:=1;
  for i:=1 to 8 do
   if N_gr[i] then
    begin
     N_grFlag:=True;
     RGN_gr.Tag:=RGN_gr.Tag+1;
    end;
  for i:=1 to 4 do
   if Prof[i] then
    begin
     ProfFlag:=True;
     RGProf.Tag:=RGProf.Tag+1;
    end;
  if not N_grFlag then
   begin
    RGN_gr.Tag:=8;
    btnN_gr_1.Down:=True;
    btnN_gr_2.Down:=True;
    btnN_gr_3.Down:=True;
    btnN_gr_4.Down:=True;
    btnN_gr_5.Down:=True;
    btnN_gr_6.Down:=True;
    btnN_gr_7.Down:=True;
    btnN_gr_8.Down:=True;
    MessageDlg('Не выбрано ни одной группы, будут выбраны ВСЕ',mtInformation,[mbOK],0);
   end;
  if not ProfFlag then
   begin
    RGProf.Tag:=4;
    btnProfKPM.Down:=True;
    btnProfK4A.Down:=True;
    btnProfKMM.Down:=True;
    btnProfKIT.Down:=True;
    MessageDlg('Не выбрано ни одного профиля, будут выбраны ВСЕ',mtInformation,[mbOK],0);
   end;
  if not (btnSexM.Down xor btnSexW.Down) then
   begin
    if not (btnSexM.Down and btnSexW.Down) then
      begin
       MessageDlg('Не выбрано ни одного из полов, будут выбраны ВСЕ',mtInformation,[mbOK],0);
      end;
    RGSex.Tag:=2;
    btnSexM.Down:=True;
    btnSexW.Down:=True;
   end;
  if not (btnBudgetT.Down xor btnBudgetF.Down) then
   begin
    if not (btnBudgetT.Down and btnBudgetF.Down) then
      begin
       MessageDlg('Не выбрано ни одной из форм обучения, будут выбраны ВСЕ',mtInformation,[mbOK],0);
      end;
    RGBudget.Tag:=2;
    btnBudgetT.Down:=True;
    btnBudgetF.Down:=True;
   end;
  if not (btnCityT.Down xor btnCityF.Down) then
   begin
    if not (btnCityT.Down and btnCityF.Down) then
      begin
       MessageDlg('Не выбрано ни одной из прописок, будут выбраны ВСЕ',mtInformation,[mbOK],0);
      end;
    RGCity.Tag:=2;
    btnCityT.Down:=True;
    btnCityF.Down:=True;
   end;  // даты ????
//WHERE part
    StrWhere:='';
    if RGN_gr.Tag <> 8 then
     begin
      N_grFlag:=False;
      StrWhere:='AND S1.N_gr in (';
      for i:=1 to 8 do
       begin
        if N_gr[i] then
         begin
          if N_grFlag then StrWhere:=StrWhere+',';
          StrWhere:=StrWhere+IntToStr(i);
          N_grFlag:=True;
         end;
       end;
      StrWhere:=StrWhere+')';
     end;
    if RGProf.Tag<>4 then
     begin
      ProfFlag:=False;
      StrWhere:= StrWhere+' AND S1.Prof in (';
      if btnProfKPM.Down then
       begin
        if ProfFlag then StrWhere:=StrWhere+',';
        StrWhere:=StrWhere+'''КПМ''';
        ProfFlag:=True;
       end;
      if btnProfK4A.Down then
       begin
        if ProfFlag then StrWhere:=StrWhere+',';
        StrWhere:=StrWhere+'''КЧА''';
        ProfFlag:=True;
       end;
      if btnProfKMM.Down then
       begin
        if ProfFlag then StrWhere:=StrWhere+',';
        StrWhere:=StrWhere+'''КММ''';
        ProfFlag:=True;
       end;
      if btnProfKIT.Down then
       begin
        if ProfFlag then StrWhere:=StrWhere+',';
        StrWhere:=StrWhere+'''КИТ''';
        ProfFlag:=True;
       end;
      StrWhere:=StrWhere+')';
     end;
   if RGBudget.Tag<>2 then
    if btnBudgetT.Down
     then StrWhere:=StrWhere+' AND S1.Budget=''T'''
     else StrWhere:=StrWhere+' AND S1.Budget=''F''';
   if RGSex.Tag<>2 then
    if btnSexM.Down
     then StrWhere:=StrWhere+' AND S1.Sex=''М'''
     else StrWhere:=StrWhere+' AND S1.Sex=''Ж''';
   if RGCity.Tag<>2 then
    if btnCityT.Down
     then StrWhere:=StrWhere+' AND S1.City=''T'''
     else StrWhere:=StrWhere+' AND S1.City=''F''';
// для более точной корреляции!!!
  if btnTwoNo.Down then StrWhere := StrWhere+'AND Ss1.Code_mark>2 AND Ss2.Code_mark>2';
  StrWhere:=StrWhere+';';
  Result:=StrWhere;
end;

procedure TfSQLconstructor.CreateDataFileFromCDSQuery2;
var
  Discriptor: TextFile;
  AddStr: string;
  i: longint;
begin
  ChDir(First.ClientDir);
  AssignFile(Discriptor,'SQL_'+IntToStr(dmStatSoftClient.ClientID)+'.dar');

{$I-} ReWrite(Discriptor); {$I+}
  if IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                           +'[TfSQLconstructor.CreateDataFileFromCDSQuery2;]'#13#10
                           +'EXCEPTION: Блокирован файл SQL.dar,'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует');
  countStr := 0;
  WriteLn(Discriptor, UntInterface.data_blok);
  dmBazaClient.CDSQuery2.First;
  while not dmBazaClient.CDSQuery2.Eof do
    begin
      inc(countStr);
      AddStr := '';
      for i := 0 to countStolb - 1 do
        AddStr := AddStr + String(dmBazaClient.CDSQuery2.Fields[i].Value) + UntInterface.DARSeparator;

      ProgressBarSQL.Position := (ProgressBarSQL.Position + 1) mod ProgressBarSQL.Max;
      WriteLn(Discriptor, AddStr);
      dmBazaClient.CDSQuery2.Next;
    end;

  WriteLn(Discriptor, UntInterface.system_blok);
  AddStr := IntToStr(countStr)+UntInterface.DARSeparator+'//кол-во строк в матрице';
  WriteLn(Discriptor, AddStr);
  AddStr := IntToStr(countStolb)+UntInterface.DARSeparator+'//кол-во столбцов в матрице';
  WriteLn(Discriptor, AddStr);

  WriteLn(Discriptor, UntInterface.end_file);
  CloseFile(Discriptor);
  dmBazaClient.CDSQuery2.First;
end;//TfSQLconstructor.CreateDataFileFromCDSQuery2

procedure TfSQLconstructor.btnSQLClick(Sender: TObject);
var
  StrWhere : string;
  N_gr : arN_gr;
  Prof : arProf;
  N_grFlag, ProfFlag, KursFlag : boolean;
  i : integer;
begin
  btnSQL.Repaint;
  pnlExecuteParams.Visible := False;
  pnlExecuteParams.Repaint;

  btnSQL.Tag := newSQL;
  lbTimeComment.Caption := 'По результатам выбраных параметров формируетря выборка (AcridBazaServer)';
  ProgressBarSQL.Position := 0;
  pnlTime.Visible := True;
  pnlTime.Repaint;

  StrWhere:=Test_Where(N_gr, Prof, N_grFlag, ProfFlag, KursFlag);
  memoSQL.Clear;
  memoSQL.Lines.Add('SELECT Ss1.Code_mark AS "P1", Ss2.Code_mark AS "P2", L1.ID_T AS "T1", L2.ID_T AS "T2", L1.Kurs AS "K1", L2.Kurs AS "K2", S1.Prof AS "Prof"');//{, S1.Prof AS "спец. (Prof)"}
  memoSQL.Lines.Add('FROM Session Ss1 INNER JOIN Session Ss2 ON Ss1.ID_S=Ss2.ID_S INNER JOIN Lecture L1 ON Ss1.ID_L=L1.ID_L INNER JOIN Lecture L2 ON Ss2.ID_L=L2.ID_L INNER JOIN Student S1 ON Ss1.ID_S=S1.ID_S INNER JOIN Student S2 ON Ss2.ID_S=S2.ID_S');
  memoSQL.Lines.Add('WHERE L1.ID_P='+IntToStr(dmBazaClient.CDSPredmetID_P.Value)+' AND L2.ID_P='+IntToStr(dmBazaClient.CDSQuery.Fields[0].AsInteger));
  memoSQL.Lines.Add('AND Ss1.Code_mark>1 AND Ss2.Code_mark>1');
// >1 т.к. имеет смысл только оценка, а не кол-во перездач
// -> where SQL
  memoSQL.Lines.Add(StrWhere);
  dmBazaClient.DSQuery2.DataSet:= nil;
  dmBazaClient.CDSQuery2.Close;
  dmBazaClient.DCOMConnection.AppServer.sm_QueryClear2;
  for i:=0 to memoSQL.Lines.Count-1 do
   dmBazaClient.DCOMConnection.AppServer.
   sm_QueryAdd2(memoSQL.Lines[i]);
  dmBazaClient.DCOMConnection.AppServer.sm_QueryExec2;
  dmBazaClient.CDSQuery2.Open;
  dmBazaClient.DSQuery2.DataSet:=dmBazaClient.CDSQuery2;

  countStolb := dmBazaClient.CDSQuery2.Fields.Count;// NB move to SQl.dar file
  btnSQL.Down := False;
  pnlTime.Visible := False;
  pnlExecuteParams.Visible := True;
end;

procedure TfSQLconstructor.btnRunClick(Sender: TObject);
var
  countStr: longint;
  TypeGroupParam, TypeVariableParam, TypeVar2: tType;
begin
  btnRun.Repaint;

  if btnSQL.Tag = nullSQL then
    begin
      MessageDlg('Создайте ВЫБОРКУ (нажмите SQL)',mtInformation,[mbOK],0);
      btnRun.Down := False;
      Exit;
    end;
// ->   CreateResultDataFile -----------------------------
  try
    if btnSQL.Tag = newSQL then
      begin
        lbTimeComment.Caption := 'По результату выборки формируетря файл SQL.dar (на клиенте)';
        ProgressBarSQL.Position := 0;
        pnlTime.Visible := True;
        pnlTime.Repaint;
        CreateDataFileFromCDSQuery2(countStolb, countStr);
        pnlTime.Visible := False;

        lbTimeComment.Caption := 'Сформироавнный файл SQL.dar передается на AcridStatSoftServer';
        ProgressBarSQL.Position := 0;
        countStolb := ProgressBarSQL.Max; // это просто SAVE
        ProgressBarSQL.Max := countStr;
        ProgressBarSQL.Smooth := True;
        pnlTime.Visible := True;
        pnlTime.Repaint;
        UntConnectStatSoft.MoveDataToServer;
        pnlTime.Visible := False;
        ProgressBarSQL.Smooth := False;
        ProgressBarSQL.Max := countStolb; // возврат

        btnSQL.Tag := oldSQL;
      end;//if ntnSQL.Tag = newSQL

{    lbTimeComment.Caption := 'Ваш запрос обрабатывается AcridStatSoftServer-ом';
    ProgressBarSQL.Position := 0;
    pnlTime.Visible := True;
    pnlTime.Repaint; }
// -> NB !!! при групперующем переменном (string)
    TypeGroupParam := _numeric_;
    if (rgGroupParam.Tag = Integer(_T1_)) or (rgGroupParam.Tag = Integer(_T2_)) or (rgGroupParam.Tag = Integer(_Prof_)) then
      TypeGroupParam := _string_;
    TypeVariableParam := _numeric_;
    if (rgVariableParam.Tag = Integer(_T1_)) or (rgVariableParam.Tag = Integer(_T2_)) or (rgVariableParam.Tag = Integer(_Prof_)) then
      TypeVariableParam := _string_;
    TypeVar2 := _numeric_;
    if (rgVar2.Tag = Integer(_T1_)) or (rgVar2.Tag = Integer(_T2_)) or (rgVar2.Tag = Integer(_Prof_)) then
      TypeVar2 := _string_;
// -> непосредственно САМ запрос
    UntConnectStatSoft.QuaryStatSoftServer(tStatMethodID(btnRun.Tag), TypeGroupParam, rgGroupParam.Tag, TypeVariableParam, rgVariableParam.Tag, TypeVar2, rgVar2.Tag);
    pnlTime.Visible := False;

{    lbTimeComment.Caption := 'Возврат вычисленного server-ом результата на client-a';
    ProgressBarSQL.Position := 0;
    pnlTime.Visible := True;
    pnlTime.Repaint; }
    UntConnectStatSoft.GetResultFromServer;
//    pnlTime.Visible := False;

{    lbTimeComment.Caption := 'Интерпритация (parsing) результата на Сlient-е';
    ProgressBarSQL.Position := 0;
    pnlTime.Visible := True;
    pnlTime.Repaint; }
    UntResultFileParser.ResultFileParser(_first_);
  finally
    pnlTime.Visible := False;
    btnRun.Down := False;
  end;//try
end;

procedure TfSQLconstructor.bntClosePnlResultClick(Sender: TObject);
begin pnlResult.Visible := False; end;

procedure TfSQLconstructor.FormCreate(Sender: TObject);
begin
  btnSQL.Tag := nullSQL;
  btnRun.Tag := Integer(UntInterface._MatrixFrequency_);
  rgGroupParam.Tag := Integer(_Prof_);
  rgVariableParam.Tag := Integer(_P1_);
  rgVar2.Tag := Integer(_P2_);
  edtEpsilon.Text := FloatToStr(UntInterface.cEpsilon);
  edtAlpha.Text := FloatToStr(UntInterface.cAlpha);
end;

procedure TfSQLconstructor.btnFrequencyClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._MatrixFrequency_); end;

procedure TfSQLconstructor.btnProbabilityClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._MatrixProbability_); end;

procedure TfSQLconstructor.btnMxClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._Mx_); end;

procedure TfSQLconstructor.btnDxClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._Dx_); end;

procedure TfSQLconstructor.btnLineCorrelationClick(Sender: TObject);
begin
  if btnTwoYes.Down then MessageDlg('Неуспешные сдачи (двойки) могут сильно повлеять на корреляцию',mtInformation,[mbOK],0);
  btnRun.Tag := Integer(UntInterface._LineCorrelation_coef_);
end;

procedure TfSQLconstructor.btnSpirmenRangCorrelationClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._SpirmenRangCorrelation_coef_); end;

procedure TfSQLconstructor.btnKendallRangCorrelationClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._KendallRangCorrelation_coef_); end;

procedure TfSQLconstructor.btnPoligonClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._Poligon_); end;

procedure TfSQLconstructor.btnFxClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._Fx_); end;

procedure TfSQLconstructor.btnPxClick(Sender: TObject);
begin btnRun.Tag := Integer(UntInterface._Px_); end;

procedure TfSQLconstructor.btnGrP1Click(Sender: TObject);
begin rgGroupParam.Tag := Integer(_P1_); end;

procedure TfSQLconstructor.btnGrP2Click(Sender: TObject);
begin rgGroupParam.Tag := Integer(_P2_); end;

procedure TfSQLconstructor.btnGrT1Click(Sender: TObject);
begin rgGroupParam.Tag := Integer(_T1_); end;

procedure TfSQLconstructor.btnGrT2Click(Sender: TObject);
begin rgGroupParam.Tag := Integer(_T2_); end;

procedure TfSQLconstructor.btnGrK1Click(Sender: TObject);
begin rgGroupParam.Tag := Integer(_K1_); end;

procedure TfSQLconstructor.btnGrK2Click(Sender: TObject);
begin rgGroupParam.Tag := Integer(_K2_); end;

procedure TfSQLconstructor.btnGrProfClick(Sender: TObject);
begin rgGroupParam.Tag := Integer(_Prof_); end;

procedure TfSQLconstructor.btnVarP1Click(Sender: TObject);
begin rgVariableParam.Tag := Integer(_P1_); end;

procedure TfSQLconstructor.btnVarP2Click(Sender: TObject);
begin rgVariableParam.Tag := Integer(_P2_); end;

procedure TfSQLconstructor.btnVarT1Click(Sender: TObject);
begin rgVariableParam.Tag := Integer(_T1_); end;

procedure TfSQLconstructor.btnVarT2Click(Sender: TObject);
begin rgVariableParam.Tag := Integer(_T2_); end;

procedure TfSQLconstructor.btnVarK1Click(Sender: TObject);
begin rgVariableParam.Tag := Integer(_K1_); end;

procedure TfSQLconstructor.btnVarK2Click(Sender: TObject);
begin rgVariableParam.Tag := Integer(_K2_); end;

procedure TfSQLconstructor.btnVarProfClick(Sender: TObject);
begin rgVariableParam.Tag := Integer(_Prof_); end;

procedure TfSQLconstructor.btnVar2P1Click(Sender: TObject);
begin rgVar2.Tag := Integer(_P1_); end;

procedure TfSQLconstructor.btnVar2P2Click(Sender: TObject);
begin rgVar2.Tag := Integer(_P2_); end;

procedure TfSQLconstructor.btnVar2T1Click(Sender: TObject);
begin rgVar2.Tag := Integer(_T1_); end;

procedure TfSQLconstructor.btnVar2T2Click(Sender: TObject);
begin rgVar2.Tag := Integer(_T2_); end;

procedure TfSQLconstructor.btnVar2K1Click(Sender: TObject);
begin rgVar2.Tag := Integer(_K1_); end;

procedure TfSQLconstructor.btnVar2K2Click(Sender: TObject);
begin rgVar2.Tag := Integer(_K2_); end;

procedure TfSQLconstructor.btnVar2ProfClick(Sender: TObject);
begin rgVar2.Tag := Integer(_Prof_); end;

procedure TfSQLconstructor.cbClearClick(Sender: TObject);
var
  i: Integer;
begin
 if cbClear.Checked = True then
   begin
     cbClear.Checked := False;
     cbClear.Visible := False;
     for i := 0 to clbLook.Items.Count - 1 do
       clbLook.Checked[i] := False;
   end;
end;

procedure TfSQLconstructor.btnGraphClick(Sender: TObject);
begin ResultFileParser(_next_); end;

procedure TfSQLconstructor.btnViborGraphClick(Sender: TObject);
var
  i, clr_count: Integer;
begin
// -> это надо при визуализации GraphFile
  Pole.Visible := True;
  pnlLegenda.Visible := True;
  pnlLegenda.Repaint;
  clr_count := 0;
  for i := 0 to clbLook.Items.Count - 1 do
    begin
      if clbLook.Checked[i] = True then
        begin
          clbLook.Canvas.Brush.Color := Col[clr_count];
          clbLook.Canvas.Rectangle(2, 1+13*i, 13, 12+13*i);
          cbClear.Visible := True;
        end;
      clr_count := (clr_count + 1) mod 7;
    end;
// -> это НЕ надо при визуализации GraphFile
  sgFileResultData.Visible := False;
//  lbRem1.Visible := False;
//  lbRem2.Visible := False;
end;

procedure TfSQLconstructor.btnViborTabelClick(Sender: TObject);
begin
// -> это надо при визуализации DataFile
  sgFileResultData.Visible := True;
//  lbRem1.Visible := True;
//  lbRem2.Visible := True;
// -> это НЕ надо при визуализации DataFile
  Pole.Visible := False;
  pnlLegenda.Visible := False;
end;

procedure TfSQLconstructor.rgStatMethodIDChange(
  ButtonGroup: TfcCustomButtonGroup; OldSelected,
  Selected: TfcButtonGroupItem);
begin
  if btnLineCorrelation.Down then
    begin
      lbVar2.Visible := True;
      lbRemVar2.Visible := True;
      pnlVar2.Visible := True;
    end
  else
    begin
      lbVar2.Visible := False;
      lbRemVar2.Visible := False;
      pnlVar2.Visible := False;
    end;
end;

procedure TfSQLconstructor.btnHelpClick(Sender: TObject);
var
  Str: string;
begin
  Str := First.ClientDir+'\Help';
  ChDir(Str);
  ShellExecute(Application.Handle,'open','!default.html',nil,nil,SW_SHOWNORMAL);
end;

end.
