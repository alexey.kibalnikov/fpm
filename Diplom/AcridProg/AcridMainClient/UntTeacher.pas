unit UntTeacher;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, ExtCtrls, fcButton, fcImgBtn,
  fcClearPanel, fcButtonGroup, cbClasses, CaptionButton, FormRoller,
  fcImager, UntFrameModify;

type
  TfTeacher = class(TForm)
    DBGTeacher: TDBGrid;
    edtF: TEdit;
    edtI: TEdit;
    edtO: TEdit;
    edtJob: TEdit;
    EdtRang: TEdit;
    btnMenu: TfcImageBtn;
    btnReset: TfcImageBtn;
    FormRoller2: TFormRoller;
    fcImager3: TfcImager;
    HorizontalImageBtnGroup: TfcButtonGroup;
    btnSortID: TfcImageBtn;
    btnSortF: TfcImageBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    frmModify: TfrmModify;
    procedure btnMenuClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBGTeacherCellClick(Column: TColumn);
    procedure btnSortIDClick(Sender: TObject);
    procedure btnSortFClick(Sender: TObject);
    procedure frmModify1btnDelClick(Sender: TObject);
    procedure frmModify1btnAddClick(Sender: TObject);
    procedure frmModifybtnUpClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fTeacher: TfTeacher;

implementation

uses
  UntDMBaza,
  UntFirst;

{$R *.dfm}

procedure TfTeacher.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fTeacher.Visible:=False;
  fTeacher.DBGTeacher.DataSource:= nil;
end;

procedure TfTeacher.btnResetClick(Sender: TObject);
begin
 edtF.Text:='';
 edtI.Text:='';
 edtO.Text:='';
 edtJob.Text:='';
 edtRang.Text:='';
end;

procedure TfTeacher.FormActivate(Sender: TObject);
begin
  edtF.Text:=dmBazaClient.CDSTeacherDeF.Value;
  edtI.Text:=dmBazaClient.CDSTeacherDeI.Value;
  edtO.Text:=dmBazaClient.CDSTeacherDeO.Value;
  edtJob.Text:=dmBazaClient.CDSTeacherJOB.Value;
  EdtRang.Text:=dmBazaClient.CDSTeacherRANG.Value;
end;

procedure TfTeacher.DBGTeacherCellClick(Column: TColumn);
begin
  edtF.Text:=dmBazaClient.CDSTeacherDeF.Value;
  edtI.Text:=dmBazaClient.CDSTeacherDeI.Value;
  edtO.Text:=dmBazaClient.CDSTeacherDeO.Value;
  edtJob.Text:=dmBazaClient.CDSTeacherJOB.Value;
  EdtRang.Text:=dmBazaClient.CDSTeacherRANG.Value;
end;

procedure TfTeacher.btnSortIDClick(Sender: TObject);
begin dmBazaClient.CDSTeacher.IndexFieldNames := 'ID_T';end;

procedure TfTeacher.btnSortFClick(Sender: TObject);
begin dmBazaClient.CDSTeacher.IndexFieldNames := 'DeF;ID_T';end;

procedure TfTeacher.frmModify1btnDelClick(Sender: TObject);
begin
 if MessageDlg('’отите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmBazaClient.DCOMConnection.AppServer.sm_DelTeacher(dmBazaClient.CDSTeacherID_T.Value);
   dmBazaClient.CDSTeacher.Refresh;
  end;
end;

procedure TfTeacher.frmModify1btnAddClick(Sender: TObject);
begin
 if dmBazaClient.DCOMConnection.AppServer.sm_AddTeacher(0,edtF.Text,edtI.Text,edtO.Text,edtJOB.Text,edtRANG.Text) > 0
     then dmBazaClient.CDSTeacher.Refresh;
end;

procedure TfTeacher.frmModifybtnUpClick(Sender: TObject);
begin
 if MessageDlg('’отите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmBazaClient.DCOMConnection.AppServer.sm_AddTeacher(dmBazaClient.CDSTeacherID_T.Value,edtF.Text,edtI.Text,edtO.Text,edtJOB.Text,edtRANG.Text) > 0
     then dmBazaClient.CDSTeacher.Refresh;
  end;
end;

end.
