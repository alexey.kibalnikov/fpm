unit UntConnectStatSoft;
{=========================================================================}
interface
{=========================================================================}
uses
  UntInterface;

procedure MoveDataToServer;
procedure QuaryStatSoftServer(StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer);
procedure GetResultFromServer;

{=========================================================================}
implementation
{=========================================================================}
uses
  UntDMStatSoft,
  UntFirst,
  SysUtils,
  UntSQLConstr;

procedure MoveDataToServer;
var
  DiscriptorIN: TextFile;
  Str: string;
begin
  ChDir(First.ClientDir);
  AssignFile(DiscriptorIN, 'SQL_'+IntToStr(dmStatSoftClient.ClientID)+'.dar');
{$I-} Reset(DiscriptorIN); {$I+}
  if IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                           +'[MoveDataToServer;]'#13#10
                           +'EXCEPTION: НЕ найден файл данных SQL.dar,'+#13#10
                           +'УСТРАНЕНИЕ: Cоздайте выборку SQL конструктором')
  else
    try
      dmStatSoftClient.DCOMConnection.AppServer.sm_NewDataFile(dmStatSoftClient.ClientID);
      while not EOF(DiscriptorIN) do
        begin
          ReadLn(DiscriptorIN, Str);
          dmStatSoftClient.DCOMConnection.AppServer.sm_AddDataFile(dmStatSoftClient.ClientID, Str);
          with fSQLconstructor.ProgressBarSQL do
            Position := (Position + 1) mod Max;
        end;
    finally // IOResult = 0
      CloseFile(DiscriptorIN);
    end;
end;//MoveDataToServer

procedure QuaryStatSoftServer(StatMethodID: tStatMethodID; TypeGroupParam: tType; ixGroupParam: Integer; TypeVariableParam: tType; ixVariableParam: Integer; TypeVariableParam2: tType; ixVariableParam2: Integer);
begin
  dmStatSoftClient.DCOMConnection.AppServer.sm_Execute(dmStatSoftClient.ClientID, StatMethodID, TypeGroupParam, ixGroupParam, TypeVariableParam, ixVariableParam, TypeVariableParam2, ixVariableParam2);
end;//QuaryStatSoftServer

procedure GetResultFromServer;
var
  Discriptor: Text;
  StrResult: string;
  StrN: integer;
begin
  ChDir(First.ClientDir);
  AssignFile(Discriptor,'Result_'+IntToStr(dmStatSoftClient.ClientID)+'.dar');

{$I-} ReWrite(Discriptor); {$I+}
  if IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                           +'[GetResultFromServer;]'#13#10
                           +'EXCEPTION: Блокирован файл Result.dar,'+#13#10
                           +'УСТРАНЕНИЕ: Закройте приложение, которое его использует')
  else
    try
      StrN := 0;
      while StrResult <> UntInterface.end_file do
        begin
          dmStatSoftClient.DCOMConnection.AppServer.sm_MoveResultToClient(dmStatSoftClient.ClientID, StrN, StrResult);
          WriteLn(Discriptor, StrResult);
          inc(StrN);
          with fSQLconstructor.ProgressBarSQL do
            Position := (Position + 1) mod Max;
        end;
    finally
      CloseFile(Discriptor);
    end;
end;//GetResultFromServer

end.
