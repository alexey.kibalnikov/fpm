unit UntSession;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, ExtCtrls, ComCtrls, fcButton,
  fcImgBtn, fcClearPanel, fcButtonGroup, cbClasses, CaptionButton,
  FormRoller,
  DBClient, fcImager, UntFrameModify, UntFrameStudent; // For TClientDataSet

type
  TfSession = class(TForm)
    DBGSession: TDBGrid;
    DBGLecture: TDBGrid;
    DTPS_year: TDateTimePicker;
    btnMenu: TfcImageBtn;
    FormRoller1: TFormRoller;
    fcImager1: TfcImager;
    btnSortP: TfcImageBtn;
    btnSortKurs: TfcImageBtn;
    fcImager2: TfcImager;
    btnSortWiner: TfcImageBtn;
    fcImager4: TfcButtonGroup;
    pnlCode: TPanel;
    RGCode: TfcButtonGroup;
    btnCodeNeJavka: TfcImageBtn;
    btnCodeNeDopysk: TfcImageBtn;
    btnCodeNeZ: TfcImageBtn;
    btnCodeZ: TfcImageBtn;
    btnCode2: TfcImageBtn;
    btnCode3: TfcImageBtn;
    btnCode4: TfcImageBtn;
    btnCode5: TfcImageBtn;
    Label1: TLabel;
    frmModify1: TfrmModify;
    frmStudent: TfrmStudent;
    procedure btnMenuClick(Sender: TObject);
    procedure DBGSessionCellClick(Column: TColumn);
    procedure btnSortPClick(Sender: TObject);
    procedure btnSortKursClick(Sender: TObject);
    procedure btnSortWinerClick(Sender: TObject);
    procedure btnCodeNeJavkaClick(Sender: TObject);
    procedure btnCodeNeDopyskClick(Sender: TObject);
    procedure btnCodeNeZClick(Sender: TObject);
    procedure btnCodeZClick(Sender: TObject);
    procedure btnCode2Click(Sender: TObject);
    procedure btnCode3Click(Sender: TObject);
    procedure btnCode4Click(Sender: TObject);
    procedure btnCode5Click(Sender: TObject);
    procedure RGCodeChange(ButtonGroup: TfcCustomButtonGroup; OldSelected,
      Selected: TfcButtonGroupItem);
    procedure frmModify1btnAddClick(Sender: TObject);
    procedure frmModify1btnUpClick(Sender: TObject);
    procedure frmModify1btnDelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSession: TfSession;

implementation

uses
  UntFirst,
  UntDMBaza;

{$R *.dfm}

procedure TfSession.btnMenuClick(Sender: TObject);
begin
 First.Visible:=True;
 fSession.Visible:=False;
 DBGSession.DataSource:=nil;
 frmStudent.DBGStudent.DataSource:=nil;
 DBGLecture.DataSource:=nil;
end;

procedure TfSession.DBGSessionCellClick(Column: TColumn);
begin
  dmBazaClient.CDSStudent.First;
  while dmBazaClient.CDSStudentID_S.Value<>dmBazaClient.CDSSessionID_S.Value
    do dmBazaClient.CDSStudent.Next;
  dmBazaClient.CDSLecture.First;
  while dmBazaClient.CDSLectureID_L.Value<>dmBazaClient.CDSSessionID_L.Value
    do dmBazaClient.CDSLecture.Next;
  Case dmBazaClient.CDSSessionCODE_MARK.Value of
    -2: btnCodeNeJavka.Down:=True;
    -1: btnCodeNeDopysk.Down:=True;
     0: btnCodeNeZ.Down:=True;
     1: btnCodeZ.Down:=True;
     2: btnCode2.Down:=True;
     3: btnCode3.Down:=True;
     4: btnCode4.Down:=True;
     5: btnCode5.Down:=True;
  end; RGCode.Tag:=dmBazaClient.CDSSessionCODE_MARK.Value;
  DTPS_year.DateTime:=dmBazaClient.CDSSessionS_YEAR.Value;
end;

procedure TfSession.btnSortPClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames:='ID_L';end;

procedure TfSession.btnSortKursClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames:='Kurs;ID_L';end;

procedure TfSession.btnSortWinerClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames:='Winer;ID_L';end;

procedure TfSession.btnCodeNeJavkaClick(Sender: TObject);
begin RGCode.Tag:=-2; end;

procedure TfSession.btnCodeNeDopyskClick(Sender: TObject);
begin RGCode.Tag:=-1;end;

procedure TfSession.btnCodeNeZClick(Sender: TObject);
begin RGCode.Tag:=0; end;

procedure TfSession.btnCodeZClick(Sender: TObject);
begin RGCode.Tag:=1; end;

procedure TfSession.btnCode2Click(Sender: TObject);
begin RGCode.Tag:=2;end;

procedure TfSession.btnCode3Click(Sender: TObject);
begin RGCode.Tag:=3;end;

procedure TfSession.btnCode4Click(Sender: TObject);
begin RGCode.Tag:=4; end;

procedure TfSession.btnCode5Click(Sender: TObject);
begin RGCode.Tag:=5; end;

procedure TfSession.RGCodeChange(ButtonGroup: TfcCustomButtonGroup;
  OldSelected, Selected: TfcButtonGroupItem);
begin
 if (RGCode.Tag=-1) and (btnCode5.Down=True) then First.edtPassword.Tag:=1;
end;

procedure TfSession.frmModify1btnAddClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
   if dmBazaClient.DCOMConnection.AppServer.sm_AddSession(0,dmBazaClient.CDSStudentID_S.Value,dmBazaClient.CDSLectureID_L.Value,RGCode.Tag,DTPS_year.DateTime) > 0
     then dmBazaClient.CDSSession.Refresh;
  end;
end;

procedure TfSession.frmModify1btnUpClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmBazaClient.DCOMConnection.AppServer.sm_AddSession(dmBazaClient.CDSSessionID.Value,dmBazaClient.CDSStudentID_S.Value,dmBazaClient.CDSLectureID_L.Value,RGCode.Tag,DTPS_year.DateTime) > 0
     then dmBazaClient.CDSSession.Refresh;
  end;
 end;
end;

procedure TfSession.frmModify1btnDelClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmBazaClient.DCOMConnection.AppServer.sm_DelSession(dmBazaClient.CDSSessionID.Value);
   dmBazaClient.CDSSession.Refresh;
  end;
 end;
end;

(*
procedure ОБНОВЛЕНИЕ;
 var
 i : Integer;
begin
 dmBazaClient.GetEventString;
 MessageDlg(dmBazaClient.EventString,mtInformation,[mbOk],0);
 for i:=0 to dmBazaClient.ComponentCount-1 do
  begin
   if (dmBazaClient.Components[i] is TClientDataSet) and
    ((dmBazaClient.Components[i] as TClientDataSet).Tag>0) then
    if dmBazaClient.EventString[(dmBazaClient.Components[i] as TClientDataSet).Tag]='1' then
     begin
      (dmBazaClient.Components[i] as TClientDataSet).Close;
      (dmBazaClient.Components[i] as TClientDataSet).Open;
      dmBazaClient.EventString[(dmBazaClient.Components[i] as TClientDataSet).Tag]:='0';
     end;
  end;
end;
*)
end.
