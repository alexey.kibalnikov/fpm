unit UntHistory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, Grids, DBGrids, dbcgrids, ComCtrls, ExtCtrls,
  fcButton, fcImgBtn, fcClearPanel, fcButtonGroup, cbClasses,
  CaptionButton, FormRoller, fcImager, UntFrameModify, UntFrameStudent;

type
  TfHistory = class(TForm)
    DBGHistory: TDBGrid;
    DTPhData: TDateTimePicker;
    btnMenu: TfcImageBtn;
    FormRoller1: TFormRoller;
    pnlCode_mark: TPanel;
    RGCode_mark: TfcButtonGroup;
    btnCodeOtch: TfcImageBtn;
    btnCodeAcadem: TfcImageBtn;
    btnCodePerevod: TfcImageBtn;
    btnCodePovtor: TfcImageBtn;
    btnCodeBudget: TfcImageBtn;
    btnCodeDogovor: TfcImageBtn;
    fcImager1: TfcImager;
    Label1: TLabel;
    frmModify: TfrmModify;
    frmStudent: TfrmStudent;
    procedure btnMenuClick(Sender: TObject);
    procedure DBGHistoryCellClick(Column: TColumn);
    procedure btnCodeOtchClick(Sender: TObject);
    procedure btnCodeAcademClick(Sender: TObject);
    procedure btnCodePerevodClick(Sender: TObject);
    procedure btnCodePovtorClick(Sender: TObject);
    procedure btnCodeBudgetClick(Sender: TObject);
    procedure btnCodeDogovorClick(Sender: TObject);
    procedure frmModifybtnAddClick(Sender: TObject);
    procedure frmModifybtnUpClick(Sender: TObject);
    procedure frmModifybtnDelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fHistory: TfHistory;

implementation

uses
  UntDMBaza,
  UntFirst;

{$R *.dfm}

procedure TfHistory.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fHistory.Visible:=False;
  frmStudent.DBGStudent.DataSource:=nil;
  DBGHistory.DataSource:=nil;
end;

procedure TfHistory.DBGHistoryCellClick(Column: TColumn);
begin
  Case dmBazaClient.CDSHistoryCODE_MARK.Value of
   10: btnCodeOtch.Down:=True;
   11: btnCodeAcadem.Down:=True;
   12: btnCodePerevod.Down:=True;
   13: btnCodePovtor.Down:=True;
   14: btnCodeBudget.Down:=True;
   15: btnCodeDogovor.Down:=True;
  end; RGCode_mark.Tag:=dmBazaClient.CDSHistoryCODE_MARK.Value;
  DTPhData.DateTime:=dmBazaClient.CDSHistoryHDATA.Value;
  dmBazaClient.CDSStudent.First;
  while dmBazaClient.CDSStudentID_S.Value<>dmBazaClient.CDSHistoryID_S.Value do dmBazaClient.CDSStudent.Next;
end;

procedure TfHistory.btnCodeOtchClick(Sender: TObject);
begin RGCode_mark.Tag:=10; end;

procedure TfHistory.btnCodeAcademClick(Sender: TObject);
begin RGCode_mark.Tag:=11; end;

procedure TfHistory.btnCodePerevodClick(Sender: TObject);
begin RGCode_mark.Tag:=12; end;

procedure TfHistory.btnCodePovtorClick(Sender: TObject);
begin RGCode_mark.Tag:=13;end;

procedure TfHistory.btnCodeBudgetClick(Sender: TObject);
begin RGCode_mark.Tag:=14; end;

procedure TfHistory.btnCodeDogovorClick(Sender: TObject);
begin RGCode_mark.Tag:=15;end;

procedure TfHistory.frmModifybtnAddClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
    MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else
    begin
      if dmBazaClient.DCOMConnection.AppServer.sm_AddHistory(0,dmBazaClient.CDSStudentID_S.Value,DTPhData.DateTime,RGCode_mark.Tag) > 0 then
        dmBazaClient.CDSHistory.Refresh;
    end;
end;

procedure TfHistory.frmModifybtnUpClick(Sender: TObject);
begin
  if First.edtPassword.Tag=0 then
   MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
   if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
    begin
     if dmBazaClient.DCOMConnection.AppServer.sm_AddHistory(dmBazaClient.CDSHistoryID_H.Value,dmBazaClient.CDSStudentID_S.Value,DTPhData.DateTime,RGCode_mark.Tag) > 0
      then dmBazaClient.CDSHistory.Refresh;
    end;
   end;
end;

procedure TfHistory.frmModifybtnDelClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
   if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
    begin
     dmBazaClient.DCOMConnection.AppServer.sm_DelHistory(dmBazaClient.CDSHistoryID_H.Value);
     dmBazaClient.CDSHistory.Refresh;
    end;
  end;
end;

end.
