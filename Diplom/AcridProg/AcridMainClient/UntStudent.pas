unit UntStudent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Grids, DBGrids, fcButton,
  fcImgBtn, fcClearPanel, fcButtonGroup, cbClasses, CaptionButton,
  FormRoller, fcImager, UntFrameModify;

type
  TfStudent = class(TForm)
    DBGStudent: TDBGrid;
    edtF: TEdit;
    DTPrData: TDateTimePicker;
    edtI: TEdit;
    edtO: TEdit;
    DTPpData: TDateTimePicker;
    edtSchool: TEdit;
    btnMenu: TfcImageBtn;
    btnReset: TfcImageBtn;
    FormRoller1: TFormRoller;
    fcImager3: TfcImager;
    HorizontalImageBtnGroup: TfcButtonGroup;
    btnSortID: TfcImageBtn;
    btnSortF: TfcImageBtn;
    btnSortI: TfcImageBtn;
    btnSortN_gr: TfcImageBtn;
    btnSortProf: TfcImageBtn;
    pnlCity: TPanel;
    RGCity: TfcButtonGroup;
    btnCityT: TfcImageBtn;
    btnCityF: TfcImageBtn;
    pnlSex: TPanel;
    RGSex: TfcButtonGroup;
    btnSexM: TfcImageBtn;
    btnSexW: TfcImageBtn;
    pnlBudget: TPanel;
    fcButtonGroup3: TfcButtonGroup;
    btnBudgetT: TfcImageBtn;
    btnBudgetF: TfcImageBtn;
    pnlN_gr: TPanel;
    RGN_gr: TfcButtonGroup;
    btnN_gr_1: TfcImageBtn;
    btnN_gr_2: TfcImageBtn;
    btnN_gr_3: TfcImageBtn;
    btnN_gr_4: TfcImageBtn;
    btnN_gr_5: TfcImageBtn;
    btnN_gr_6: TfcImageBtn;
    btnN_gr_7: TfcImageBtn;
    btnN_gr_8: TfcImageBtn;
    pnlProf: TPanel;
    RGProf: TfcButtonGroup;
    btnProfNot: TfcImageBtn;
    btnProfKPM: TfcImageBtn;
    btnProfK4A: TfcImageBtn;
    btnProfKMM: TfcImageBtn;
    btnProfKIT: TfcImageBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    frmModify: TfrmModify;
    procedure btnMenuClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure DBGStudentCellClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
    procedure btnSortIDClick(Sender: TObject);
    procedure btnSortFClick(Sender: TObject);
    procedure btnSortIClick(Sender: TObject);
    procedure btnSortN_grClick(Sender: TObject);
    procedure btnSortProfClick(Sender: TObject);
    procedure btnCityTClick(Sender: TObject);
    procedure btnCityFClick(Sender: TObject);
    procedure btnN_gr_1Click(Sender: TObject);
    procedure btnN_gr_2Click(Sender: TObject);
    procedure btnN_gr_3Click(Sender: TObject);
    procedure btnN_gr_4Click(Sender: TObject);
    procedure btnN_gr_5Click(Sender: TObject);
    procedure btnN_gr_6Click(Sender: TObject);
    procedure btnN_gr_7Click(Sender: TObject);
    procedure btnN_gr_8Click(Sender: TObject);
    procedure btnProfNotClick(Sender: TObject);
    procedure btnProfKPMClick(Sender: TObject);
    procedure btnProfK4AClick(Sender: TObject);
    procedure btnProfKMMClick(Sender: TObject);
    procedure btnProfKITClick(Sender: TObject);
    procedure frmModify1btnAddClick(Sender: TObject);
    procedure frmModify1btnDelClick(Sender: TObject);
    procedure frmModify1btnUpDataClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fStudent: TfStudent;

implementation

uses
  UntDMBaza,
  UntFirst;

{$R *.dfm}

procedure TfStudent.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fStudent.Visible:=False;
  fStudent.DBGStudent.DataSource:= nil;
end;

procedure TfStudent.btnResetClick(Sender: TObject);
begin
  edtF.Text:='';
  edtI.Text:='';
  edtO.Text:='';
  btnN_gr_1.Down:=True; RGN_gr.Tag:=1;
  btnProfNot.Down:=True;
  edtSchool.Text:='';
  btnCityT.Down:=True;
  btnBudgetT.Down:=True;
  btnSexM.Down:=True;
end;

procedure TfStudent.DBGStudentCellClick(Column: TColumn);
begin
  edtF.Text:=dmBazaClient.CDSStudentDeF.Value;
  edtI.Text:=dmBazaClient.CDSStudentDeI.Value;
  edtO.Text:=dmBazaClient.CDSStudentDeO.Value;
  Case dmBazaClient.CDSStudentN_GR.Value of
   1: btnN_gr_1.Down:=True;
   2: btnN_gr_2.Down:=True;
   3: btnN_gr_3.Down:=True;
   4: btnN_gr_4.Down:=True;
   5: btnN_gr_5.Down:=True;
   6: btnN_gr_6.Down:=True;
   7: btnN_gr_7.Down:=True;
   8: btnN_gr_8.Down:=True;
  end; RGN_gr.Tag:=dmBazaClient.CDSStudentN_GR.Value;
  if dmBazaClient.CDSStudentPROF.Value='КПМ'then btnProfKPM.Down:=True else
  if dmBazaClient.CDSStudentPROF.Value='КЧА'then btnProfK4A.Down:=True else
  if dmBazaClient.CDSStudentPROF.Value='КММ'then btnProfKMM.Down:=True else
  if dmBazaClient.CDSStudentPROF.Value='КИТ'then btnProfKIT.Down:=True
   else btnProfNot.Down:=True;
  DTPpData.DateTime:=dmBazaClient.CDSStudentPDATA.Value;
  DTPrData.DateTime:=dmBazaClient.CDSStudentRDATA.Value;
  edtSchool.Text:=IntToStr(dmBazaClient.CDSStudentSCHOOL.Value);
  if dmBazaClient.CDSStudentCITY.Value='T' then btnCityT.Down:=True
    else btnCityF.Down:=True;
  if dmBazaClient.CDSStudentBUDGET.Value='T' then btnBudgetT.Down:=True
    else btnBudgetF.Down:=True;
  if dmBazaClient.CDSStudentSEX.Value='М' then btnSexM.Down:=True
    else btnSexW.Down:=True;
end;

procedure TfStudent.FormActivate(Sender: TObject);
begin
  edtF.Text:=dmBazaClient.CDSStudentDeF.Value;
  edtI.Text:=dmBazaClient.CDSStudentDeI.Value;
  edtO.Text:=dmBazaClient.CDSStudentDeO.Value;
  Case dmBazaClient.CDSStudentN_GR.Value of
   1: btnN_gr_1.Down:=True;
   2: btnN_gr_2.Down:=True;
   3: btnN_gr_3.Down:=True;
   4: btnN_gr_4.Down:=True;
   5: btnN_gr_5.Down:=True;
   6: btnN_gr_6.Down:=True;
   7: btnN_gr_7.Down:=True;
   8: btnN_gr_8.Down:=True;
  end; RGN_gr.Tag:=dmBazaClient.CDSStudentN_GR.Value;
  if dmBazaClient.CDSStudentPROF.Value='КПМ'then btnProfKPM.Down:=True else
  if dmBazaClient.CDSStudentPROF.Value='КЧА'then btnProfK4A.Down:=True else
  if dmBazaClient.CDSStudentPROF.Value='КММ'then btnProfKMM.Down:=True else
  if dmBazaClient.CDSStudentPROF.Value='КИТ'then btnProfKIT.Down:=True
    else btnProfNot.Down:=True;
  DTPpData.DateTime:=dmBazaClient.CDSStudentPDATA.Value;
  DTPrData.DateTime:=dmBazaClient.CDSStudentRDATA.Value;
  edtSchool.Text:=IntToStr(dmBazaClient.CDSStudentSCHOOL.Value);
  if dmBazaClient.CDSStudentCITY.Value='T' then btnCityT.Down:=True
    else btnCityF.Down:=True;
  if dmBazaClient.CDSStudentBUDGET.Value='T' then btnBudgetT.Down:=True
    else btnBudgetF.Down:=True;
  if dmBazaClient.CDSStudentSEX.Value='М' then btnSexM.Down:=True
    else btnSexW.Down:=True;
end;

procedure TfStudent.btnSortIDClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames := 'ID_S'; end;

procedure TfStudent.btnSortFClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames := 'DeF;ID_S';end;

procedure TfStudent.btnSortIClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames := 'DeI;ID_S';end;

procedure TfStudent.btnSortN_grClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames := 'N_gr;ID_S';end;

procedure TfStudent.btnSortProfClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames := 'Prof;ID_S';end;

procedure TfStudent.btnCityTClick(Sender: TObject);
begin RGCity.Tag:=0; end;

procedure TfStudent.btnCityFClick(Sender: TObject);
begin RGCity.Tag:=1;end;

procedure TfStudent.btnN_gr_1Click(Sender: TObject);
begin RGN_gr.Tag:=1;end;

procedure TfStudent.btnN_gr_2Click(Sender: TObject);
begin RGN_gr.Tag:=2;end;

procedure TfStudent.btnN_gr_3Click(Sender: TObject);
begin RGN_gr.Tag:=3;end;

procedure TfStudent.btnN_gr_4Click(Sender: TObject);
begin RGN_gr.Tag:=4;end;

procedure TfStudent.btnN_gr_5Click(Sender: TObject);
begin RGN_gr.Tag:=5;end;

procedure TfStudent.btnN_gr_6Click(Sender: TObject);
begin RGN_gr.Tag:=6;end;

procedure TfStudent.btnN_gr_7Click(Sender: TObject);
begin RGN_gr.Tag:=7;end;

procedure TfStudent.btnN_gr_8Click(Sender: TObject);
begin RGN_gr.Tag:=8;end;

procedure TfStudent.btnProfNotClick(Sender: TObject);
begin RGProf.Tag:=0;end;

procedure TfStudent.btnProfKPMClick(Sender: TObject);
begin RGProf.Tag:=1;end;

procedure TfStudent.btnProfK4AClick(Sender: TObject);
begin RGProf.Tag:=2;end;

procedure TfStudent.btnProfKMMClick(Sender: TObject);
begin RGProf.Tag:=3;end;

procedure TfStudent.btnProfKITClick(Sender: TObject);
begin RGProf.Tag:=4;end;

procedure TfStudent.frmModify1btnAddClick(Sender: TObject);
var
  N_gr:integer;
  Prof:string;
  Sex,Budget,City:char;
  School:shortint;
begin
  N_gr:=RGN_gr.Tag;
  Case RGProf.Tag of
    0: Prof:='';
    1: Prof:='КПМ';
    2: Prof:='КЧА';
    3: Prof:='КММ';
    4: Prof:='КИТ';
  end;
  if edtSchool.Text = '' then School := 0 else School:=StrToInt(edtSchool.Text);
  if RGCity.Tag = 0 then City:='T' else City:='F';
  if btnBudgetT.Down then Budget:='T' else Budget:='F';
  if btnSexM.Down then Sex:='М' else Sex:='Ж';
  if dmBazaClient.DCOMConnection.AppServer.sm_AddStudent(0,edtF.text,edtI.Text,edtO.Text,N_gr,Prof,DTPpData.Date,DTPrData.Date,School,City,Budget,Sex) > 0 then
    dmBazaClient.CDSStudent.Refresh;
end;

procedure TfStudent.frmModify1btnDelClick(Sender: TObject);
begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmBazaClient.DCOMConnection.AppServer.sm_DelStudent(dmBazaClient.CDSStudentID_S.Value);
   dmBazaClient.CDSStudent.Refresh;
  end;
end;

procedure TfStudent.frmModify1btnUpDataClick(Sender: TObject);
var
  N_gr:integer;
  Prof:string;
  Sex,Budget,City:char;
  School:shortint;
begin
 N_gr:=RGN_gr.Tag;
 Case RGProf.Tag of
   0: Prof:='';
   1: Prof:='КПМ';
   2: Prof:='КЧА';
   3: Prof:='КММ';
   4: Prof:='КИТ';
 end;
 if edtSchool.Text='' then School:=0 else School:=StrToInt(edtSchool.Text);
 if RGCity.Tag=0 then City:='T' else City:='F';
 if btnBudgetT.Down then Budget:='T' else Budget:='F';
 if btnSexM.Down then Sex:='М' else Sex:='Ж';
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmBazaClient.DCOMConnection.AppServer.sm_AddStudent(dmBazaClient.CDSStudentID_S.Value,edtF.text,edtI.Text,edtO.Text,N_gr,Prof,DTPpData.Date,DTPrData.Date,School,City,Budget,Sex) > 0
     then dmBazaClient.CDSStudent.Refresh;
  end;
end;

end.
