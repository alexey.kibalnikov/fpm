unit UntDMBaza;

interface

uses
  SysUtils, Classes, DB, DBClient, MConnect, Provider, IBDatabase, DBLocal,
  DBLocalI, DBTables, Dialogs;

type

  TdmBazaClient = class(TDataModule)
    CDSStudent: TClientDataSet;
    CDSStudentID_S: TIntegerField;
    CDSStudentSAlias: TStringField;
    CDSStudentDeF: TStringField;
    CDSStudentDeI: TStringField;
    CDSStudentDeO: TStringField;
    CDSStudentRDATA: TDateField;
    CDSStudentN_GR: TSmallintField;
    CDSStudentPROF: TStringField;
    CDSStudentPDATA: TDateField;
    CDSStudentCITY: TStringField;
    CDSStudentSCHOOL: TSmallintField;
    CDSStudentBUDGET: TStringField;
    CDSStudentSEX: TStringField;
    CDSStudentF: TStringField;
    CDSStudentI: TStringField;
    CDSStudentO: TStringField;
    CDSPredmet: TClientDataSet;
    CDSPredmetID_P: TIntegerField;
    CDSPredmetPNAME: TStringField;
    DCOMConnection: TDCOMConnection;
    CDSHistory: TClientDataSet;
    CDSHistoryID_H: TIntegerField;
    CDSHistoryID_S: TIntegerField;
    CDSHistorySAlias: TStringField;
    CDSHistoryXF: TStringField;
    CDSHistoryXI: TStringField;
    CDSHistoryHDATA: TDateField;
    CDSHistoryCODE_MARK: TSmallintField;
    CDSHistoryXCode: TStringField;
    CDSTeacher: TClientDataSet;
    CDSTeacherID_T: TIntegerField;
    CDSTeacherTAlias: TStringField;
    CDSTeacherDeF: TStringField;
    CDSTeacherDeI: TStringField;
    CDSTeacherDeO: TStringField;
    CDSTeacherJOB: TStringField;
    CDSTeacherRANG: TStringField;
    CDSTeacherF: TStringField;
    CDSTeacherI: TStringField;
    CDSTeacherO: TStringField;
    CDSSession: TClientDataSet;
    CDSSessionID: TIntegerField;
    CDSSessionID_S: TIntegerField;
    CDSSessionID_L: TIntegerField;
    CDSSessionSAlias: TStringField;
    CDSSessionXF: TStringField;
    CDSSessionXI: TStringField;
    CDSSessionXP: TStringField;
    CDSSessionCODE_MARK: TSmallintField;
    CDSSessionXC: TStringField;
    CDSSessionS_YEAR: TDateField;
    CDSLecture: TClientDataSet;
    CDSLectureID_L: TIntegerField;
    CDSLectureID_T: TIntegerField;
    CDSLectureID_P: TIntegerField;
    CDSLectureXP: TStringField;
    CDSLectureXT: TStringField;
    CDSLectureTAlias: TStringField;
    CDSLectureKURS: TSmallintField;
    CDSLectureDS: TStringField;
    CDSLectureRANG: TStringField;
    CDSLectureWINER: TStringField;
    CDSQuery: TClientDataSet;
    DSStudent: TDataSource;
    DSPredmet: TDataSource;
    DSTeacher: TDataSource;
    DSQuery: TDataSource;
    DSLecture: TDataSource;
    DSHistory: TDataSource;
    DSSession: TDataSource;
    CDSCode: TClientDataSet;
    CDSCodeID_C: TIntegerField;
    CDSCodeCODE: TStringField;
    CDSQuery2: TClientDataSet;
    DSQuery2: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure CDSStudentCalcFields(DataSet: TDataSet);
    procedure CDSTeacherCalcFields(DataSet: TDataSet);
    procedure CDSLectureCalcFields(DataSet: TDataSet);
    procedure CDSSessionCalcFields(DataSet: TDataSet);
    procedure CDSHistoryCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    EventString : String;
    procedure GetEventString;
    { Public declarations }
  end;

var
  dmBazaClient: TdmBazaClient;

implementation

uses
  UntFirst,
  IniFiles,
  UntSkinAcrid;

{$R *.dfm}
procedure TdmBazaClient.GetEventString;
begin
  EventString:=DCOMConnection.AppServer.sm_UpdateEvents(EventString);
end;

procedure TdmBazaClient.DataModuleCreate(Sender: TObject);
var
  FileINI: TIniFile;
  Str: string;
begin
  GetDir(0, Str);
  FileINI := TIniFile.Create(Str+'\ConfigMainClient.ini');

  Str := FileINI.ReadString('AcridMainClient','Found','');
  if Str <> 'OK' then
    begin
      ExceptFlag := True;
      raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                            +'[TdmBazaClient.DataModuleCreate;]'+#13#10
                            +'EXCEPTION: при инициализации,'+#13#10
                            +'УСТРАНЕНИЕ: Поместите файл "ConfigMainClient.ini"'#13#10
                            +'в директорию c ArcidMainClient.exe');
    end;

  try
    DCOMConnection.ComputerName := FileINI.ReadString('AcridBazaServer','ComputerName','localhost');
    FileINI.Free;

    DCOMConnection.Connected:=True;
    EventString:='';
    CDSStudent.Open;
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 3;
    CDSPredmet.Open;
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 2;
    CDSHistory.Open;
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    CDSTeacher.Open;
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 1;
    CDSSession.Open;
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 5;
    CDSLecture.Open;
with fSkinAcrid.ProgressBarInstall do
  Position := Position + 2;

    CDSStudent.AddIndex('IxN_gr_Std','N_gr;ID_S',[],'','',0);
    CDSStudent.AddIndex('IxProf_Std','Prof;ID_S',[],'','',0);
    CDSPredmet.AddIndex('IxID_Prd','ID_P',[],'','',0);
    CDSPredmet.AddIndex('IxName_Prd','pName;ID_P',[],'','',0);
    CDSLecture.AddIndex('IxKurs_Lec','Kurs;ID_L',[],'','',0);
    CDSLecture.AddIndex('IxDS_Lec','DS;ID_L',[],'','',0);
    CDSLecture.AddIndex('IxRang_Lec','Rang;ID_L',[],'','',0);
    CDSLecture.AddIndex('IxWiner_Lec','Winer;ID_L',[],'','',0);
  except
   on E: Exception do
     begin
       ExceptFlag := True;
       raise Exception.Create('Место возникновения: AcridBazaServer'+#13#10
                             +'[TdmBazaClient.DataModuleCreate;]'+#13#10
                             +'EXCEPTION: при инициализации,'+#13#10
                             +'УСТРАНЕНИЕ: 1. включите InterBase'#13#10
                             +'2. выполните "Настройка(Avto)" на сервере');
     end;
  end;//try
end;

procedure TdmBazaClient.DataModuleDestroy(Sender: TObject);
begin
   CDSStudent.Close;
   CDSPredmet.Close;
   CDSHistory.Close;
   CDSTeacher.Close;
   CDSSession.Close;
   CDSLecture.Close;
   dmBazaClient.DCOMConnection.Connected:=False;
end;

procedure TdmBazaClient.CDSStudentCalcFields(DataSet: TDataSet);
begin
 if First.edtPassword.Tag=0 then
   CDSStudentSAlias.Value:='S@'+IntToStr(CDSStudentID_S.Value)
// else
;//  begin
//   dmBazaClient.CDSStudentDeF.Value:=DeCoder(dmBazaClient.CDSStudentF.Value);
//   dmBazaClient.CDSStudentDeI.Value:=DeCoder(dmBazaClient.CDSStudentI.Value);
//   dmBazaClient.CDSStudentDeO.Value:=DeCoder(dmBazaClient.CDSStudentO.Value);
//  end;
end;

procedure TdmBazaClient.CDSTeacherCalcFields(DataSet: TDataSet);
begin
 if First.edtPassword.Tag=0 then
   CDSTeacherTAlias.Value:='T@'+IntToStr(CDSTeacherID_T.Value)
// else
;//  begin
//   dmBazaClient.CDSTeacherDeF.Value:=DeCoder(dmBazaClient.CDSTeacherF.Value);
//   dmBazaClient.CDSTeacherDeI.Value:=DeCoder(dmBazaClient.CDSTeacherI.Value);
//   dmBazaClient.CDSTeacherDeO.Value:=DeCoder(dmBazaClient.CDSTeacherO.Value);
//  end;
end;

procedure TdmBazaClient.CDSLectureCalcFields(DataSet: TDataSet);
begin
 if First.edtPassword.Tag=0 then
   CDSLectureTAlias.Value:='T@'+IntToStr(CDSLectureID_T.Value);
end;

procedure TdmBazaClient.CDSSessionCalcFields(DataSet: TDataSet);
begin
 if First.edtPassword.Tag=0 then
   CDSSessionSAlias.Value:='S@'+IntToStr(CDSSessionID_S.Value);
end;

procedure TdmBazaClient.CDSHistoryCalcFields(DataSet: TDataSet);
begin
 if First.edtPassword.Tag=0 then
   CDSHistorySAlias.Value:='S@'+IntToStr(CDSHistoryID_S.Value);
end;

end.
