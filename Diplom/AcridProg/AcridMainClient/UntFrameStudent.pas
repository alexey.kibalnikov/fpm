unit UntFrameStudent;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  fcButton, fcImgBtn, ExtCtrls, fcClearPanel, fcButtonGroup, Grids,
  DBGrids, fcImager;

type
  TfrmStudent = class(TFrame)
    fcImager3: TfcImager;
    DBGStudent: TDBGrid;
    fcImager5: TfcButtonGroup;
    btnSortID: TfcImageBtn;
    btnSortF: TfcImageBtn;
    procedure btnSortFClick(Sender: TObject);
    procedure btnSortIDClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  UntDMBaza;

{$R *.dfm}

procedure TfrmStudent.btnSortFClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames:='DeF;ID_S'; end;

procedure TfrmStudent.btnSortIDClick(Sender: TObject);
begin dmBazaClient.CDSStudent.IndexFieldNames:='ID_S'; end;

end.
