unit UntDMStatSoft;

interface

uses
  SysUtils,
  Classes,
  MConnect,
  Provider,
  DBClient,
  DB;

type
  TdmStatSoftClient = class(TDataModule)
    DCOMConnection: TDCOMConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ClientID: longint;
  end;

var
  dmStatSoftClient: TdmStatSoftClient;

implementation

uses
  UntFirst,
  Inifiles;

{$R *.dfm}

procedure TdmStatSoftClient.DataModuleCreate(Sender: TObject);
var
  FileINI: TIniFile;
  Str: string;
begin
  GetDir(0, Str);
  FileINI := TIniFile.Create(Str+'\ConfigMainClient.ini');

  Str := FileINI.ReadString('AcridMainClient','Found','');
  if Str <> 'OK' then
    begin
      ExceptFlag := True;
      raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                            +'[TdmBazaClient.DataModuleCreate;]'+#13#10
                            +'EXCEPTION: при инициализации,'+#13#10
                            +'УСТРАНЕНИЕ: Поместите файл "ConfigMainClient.ini"'#13#10
                            +'в директорию c ArcidMainClient.exe');
    end;

  DCOMConnection.ComputerName := FileINI.ReadString('AcridStatSoftServer','ComputerName','localhost');
  FileINI.Free;
  DCOMConnection.Connected := True;
  ClientID := DCOMConnection.AppServer.sm_ClientID;
  GetDir(0, First.ClientDir);
end;

procedure TdmStatSoftClient.DataModuleDestroy(Sender: TObject);
var
  Discriptor: Text;
begin
  DCOMConnection.AppServer.sm_DestroyClient(ClientID);
  DCOMConnection.Connected := False;

{$I-}
  ChDir(First.ClientDir);
  AssignFile(Discriptor, 'SQL_'+IntToStr(ClientID)+'.dar');
  Erase(Discriptor); // CloseFile не нужен
  AssignFile(Discriptor, 'Result_'+IntToStr(ClientID)+'.dar');
  Erase(Discriptor);
{$I+} // IOResult = 0
end;

end.
