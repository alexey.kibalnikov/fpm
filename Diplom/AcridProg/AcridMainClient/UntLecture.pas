unit UntLecture;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGrids, fcButton, fcImgBtn,
  fcClearPanel, fcButtonGroup, cbClasses, CaptionButton, FormRoller,
  fcImager, UntFrameModify;

type
  TfLecture = class(TForm)
    DBGLecture: TDBGrid;
    DBGPredmet: TDBGrid;
    DBGTeacher: TDBGrid;
    btnMenu: TfcImageBtn;
    FormRoller1: TFormRoller;
    fcImager3: TfcImager;
    btnSortS: TfcImageBtn;
    btnSortF: TfcImageBtn;
    fcImager1: TfcImager;
    btnSortPID: TfcButtonGroup;
    btnSortP: TfcImageBtn;
    btnSortPname: TfcImageBtn;
    fcImager2: TfcImager;
    fcButtonGroup1: TfcButtonGroup;
    btnSortIdT: TfcImageBtn;
    btnSortKurs: TfcImageBtn;
    btnSortDS: TfcImageBtn;
    btnSortRang: TfcImageBtn;
    btnSortWiner: TfcImageBtn;
    pnlKurs: TPanel;
    RGKurs: TfcButtonGroup;
    btnKurs_1: TfcImageBtn;
    btnKurs_2: TfcImageBtn;
    btnKurs_3: TfcImageBtn;
    btnKurs_4: TfcImageBtn;
    btnKurs_5: TfcImageBtn;
    pnlDS: TPanel;
    RGDS: TfcButtonGroup;
    btnDSF: TfcImageBtn;
    btnDST: TfcImageBtn;
    pnlRang: TPanel;
    RGRang: TfcButtonGroup;
    btnRangE: TfcImageBtn;
    btnRangZ: TfcImageBtn;
    pnlWiner: TPanel;
    RGWiner: TfcButtonGroup;
    btnWinerT: TfcImageBtn;
    btnWinerF: TfcImageBtn;
    frmModify: TfrmModify;
    procedure btnMenuClick(Sender: TObject);
    procedure DBGLectureCellClick(Column: TColumn);
    procedure btnSortSClick(Sender: TObject);
    procedure btnSortFClick(Sender: TObject);
    procedure btnSortPClick(Sender: TObject);
    procedure btnSortPnameClick(Sender: TObject);
    procedure btnSortIdTClick(Sender: TObject);
    procedure btnSortKursClick(Sender: TObject);
    procedure btnSortDSClick(Sender: TObject);
    procedure btnSortRangClick(Sender: TObject);
    procedure btnSortWinerClick(Sender: TObject);
    procedure btnKurs_1Click(Sender: TObject);
    procedure btnKurs_2Click(Sender: TObject);
    procedure btnKurs_3Click(Sender: TObject);
    procedure btnKurs_4Click(Sender: TObject);
    procedure btnKurs_5Click(Sender: TObject);
    procedure frmModifybtnAddClick(Sender: TObject);
    procedure frmModifybtnUpClick(Sender: TObject);
    procedure frmModifybtnDelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fLecture: TfLecture;

implementation

uses
  UntFirst,
  UntDMBaza;

{$R *.dfm}

procedure TfLecture.btnMenuClick(Sender: TObject);
begin
  First.Visible:=True;
  fLecture.Visible:=False;
  DBGLecture.DataSource:=nil;
  DBGTeacher.DataSource:=nil;
  DBGPredmet.DataSource:=nil;
end;

procedure TfLecture.DBGLectureCellClick(Column: TColumn);
begin
  dmBazaClient.CDSTeacher.First;
  while dmBazaClient.CDSTeacherID_T.Value<>dmBazaClient.CDSLectureID_T.Value
    do dmBazaClient.CDSTeacher.Next;
  dmBazaClient.CDSPredmet.First;
  while dmBazaClient.CDSPredmetID_P.Value<>dmBazaClient.CDSLectureID_P.Value
    do dmBazaClient.CDSPredmet.Next;
  Case dmBazaClient.CDSLectureKURS.Value of
    1: btnKurs_1.Down:=True;
    2: btnKurs_2.Down:=True;
    3: btnKurs_3.Down:=True;
    4: btnKurs_4.Down:=True;
    5: btnKurs_5.Down:=True;
   end; RGKurs.Tag:=dmBazaClient.CDSLectureKURS.Value;
  if dmBazaClient.CDSLectureDS.Value='T' then btnDST.Down:=True else btnDSF.Down:=True;
  if dmBazaClient.CDSLectureRANG.Value='ЗАЧ' then btnRangZ.Down:=True else btnRangE.Down:=True;
  if dmBazaClient.CDSLectureWINER.Value='T' then btnWinerT.Down:=True else btnWinerF.Down:=True;
end;

procedure TfLecture.btnSortSClick(Sender: TObject);
begin dmBazaClient.CDSTeacher.IndexFieldNames := 'ID_T';end;

procedure TfLecture.btnSortFClick(Sender: TObject);
begin dmBazaClient.CDSTeacher.IndexFieldNames := 'DeF;ID_T';end;

procedure TfLecture.btnSortPClick(Sender: TObject);
begin dmBazaClient.CDSPredmet.IndexFieldNames := 'ID_P';end;

procedure TfLecture.btnSortPnameClick(Sender: TObject);
begin dmBazaClient.CDSPredmet.IndexFieldNames := 'pName;ID_P';end;

procedure TfLecture.btnSortIdTClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames := 'ID_L';end;

procedure TfLecture.btnSortKursClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames := 'Kurs;ID_L';end;

procedure TfLecture.btnSortDSClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames := 'DS;ID_L';end;

procedure TfLecture.btnSortRangClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames := 'Rang;ID_L';end;

procedure TfLecture.btnSortWinerClick(Sender: TObject);
begin dmBazaClient.CDSLecture.IndexFieldNames := 'Winer;ID_L';end;

procedure TfLecture.btnKurs_1Click(Sender: TObject);
begin RGKurs.Tag:=1;end;

procedure TfLecture.btnKurs_2Click(Sender: TObject);
begin RGKurs.Tag:=2; end;

procedure TfLecture.btnKurs_3Click(Sender: TObject);
begin RGKurs.Tag:=3;end;

procedure TfLecture.btnKurs_4Click(Sender: TObject);
begin RGKurs.Tag:=4;end;

procedure TfLecture.btnKurs_5Click(Sender: TObject);
begin RGKurs.Tag:=5; end;

procedure TfLecture.frmModifybtnAddClick(Sender: TObject);
 var DS,Winer:char;
     Rang:string;
begin
 if btnDST.Down then DS:='T' else DS:='F';
 if btnRangZ.Down then Rang:='ЗАЧ' else Rang:='ЭКЗ';
 if btnWinerT.Down then Winer:='T' else Winer:='F';
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if dmBazaClient.DCOMConnection.AppServer.sm_AddLecture(0,dmBazaClient.CDSTeacherID_T.Value,dmBazaClient.CDSPredmetID_P.Value,RGKurs.Tag,DS,Rang,Winer) > 0
     then dmBazaClient.CDSLecture.Refresh;
 end;
end;

procedure TfLecture.frmModifybtnUpClick(Sender: TObject);
 var DS,Winer:char;
     Rang:string;
begin
 if btnDST.Down then DS:='T' else DS:='F';
 if btnRangZ.Down then Rang:='ЗАЧ' else Rang:='ЭКЗ';
 if btnWinerT.Down then Winer:='T' else Winer:='F';
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if MessageDlg('Хотите изменить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   if dmBazaClient.DCOMConnection.AppServer.sm_AddLecture(dmBazaClient.CDSLectureID_L.Value,dmBazaClient.CDSTeacherID_T.Value,dmBazaClient.CDSPredmetID_P.Value,RGKurs.Tag,DS,Rang,Winer) > 0
     then dmBazaClient.CDSLecture.Refresh;
  end;
 end;
end;

procedure TfLecture.frmModifybtnDelClick(Sender: TObject);
begin
 if First.edtPassword.Tag=0 then
 MessageDlg('Вы находитесь в режиме просмотра, модификация данных невозможна',mtInformation,[mbOK],0)
  else begin
 if MessageDlg('Хотите удалить',mtInformation,[mbYes,mbNo],0)= mrYes then
  begin
   dmBazaClient.DCOMConnection.AppServer.sm_DelLecture(dmBazaClient.CDSLectureID_L.Value);
   dmBazaClient.CDSLecture.Refresh;
  end;
 end;
end;

end.
