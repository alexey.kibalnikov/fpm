unit UntResultFileParser; // для UntSQLConstr паниели pnResult
{=========================================================================}
interface

uses
  UntInterface,
  Graphics;

const
  Col: array[0..5] of Integer = (clTeal, clRed, clSkyBlue, clBlue, clMoneyGreen, clNavy);

{=========================================================================}
  procedure ResultFileParser(FlagForGraph: tFlag);
{=========================================================================}
implementation
{=========================================================================}
uses
  UntDMStatSoft,
  UntSQLconstr,
  UntFirst,
  SysUtils;

{ BEGIN Help proceduer -------------------------------------------------- }

type
  tLook = (_up_, _down_, _left_, _right_);
procedure Strelka(x, y: Integer; Color: TColor; Look: tLook);
const
  dlina = 15; shirina = 3;
begin
  with fSQLconstructor.Pole do
    with Canvas do
      begin
        Pen.Color := Color;
        Pen.Width := 2;
        Pen.Style := psSolid;
        Case Look of
          _up_:
            begin
              MoveTo(x, y);
              LineTo(x + shirina, y + dlina);
              MoveTo(x, y);
              LineTo(x - shirina, y + dlina);
            end;
          _down_:
            begin
              MoveTo(x, y);
              LineTo(x + shirina, y - dlina);
              MoveTo(x, y);
              LineTo(x - shirina, y - dlina);
            end;
          _left_:
            begin
              MoveTo(x, y);
              LineTo(x + dlina, y + shirina);
              MoveTo(x, y);
              LineTo(x + dlina, y - shirina);
            end;
          _right_:
            begin
              MoveTo(x, y);
              LineTo(x - dlina, y + shirina);
              MoveTo(x, y);
              LineTo(x - dlina, y - shirina);
            end;
        end;//case
  end;//with
end;//Strelka

type
  tLook2 = (_vertical_, _horizontal_);
procedure Proekciya(x, y, A1, B1: Integer; Color: TColor; Look: tLook2);
const
  step = 4;
var
  cord: integer;
begin
  with fSQLconstructor.Pole do
    with Canvas do
      Case Look of
        _horizontal_:
          begin
            cord := ClientHeight - B1;
            while cord > y do
              begin
                Pixels[x, cord] := Color;
                cord := cord - step;
              end;
          end;
        _vertical_:
          begin
            cord := A1;
            while cord < x do
              begin
                Pixels[cord, y] := Color;
                cord := cord + step;
              end;
          end;
     end;//with
end;//Proekciya

procedure Cherta(x, y, A1, B1: Integer; Color: TColor; Look: tLook2; Str: string);
const
  size = 4;
begin
  with fSQLconstructor.Pole do
    with Canvas do
      begin
        Pen.Color := Color;
        Pen.Width := 2;
        Pen.Style := psSolid;
        Case Look of
          _horizontal_:
            begin
              MoveTo(A1 - size, y);
              LineTo(A1 + size, y);
              TextOut(1, y - 7, Str);
            end;
          _vertical_:
            begin
              MoveTo(x, ClientHeight - B1 - size);
              LineTo(x, ClientHeight - B1 + size);
              TextOut(x + 3, ClientHeight - B1 + size, Str);
            end;
        end;//case
      end;//widh
end;//Cherta

procedure SGFildsColor(ixStr, ixStolb: Integer; Clr: TColor);
var
  X1 ,Y1, X2, Y2: Integer;
begin
  with fSQLconstructor.sgFileResultData do
    with Canvas do
      begin
        Y1 := ixStr*(DefaultRowHeight + GridLineWidth)+1;
        X1 := ixStolb*(DefaultColWidth + GridLineWidth)+1;
        Y2 := (ixStr+1)*(DefaultRowHeight + GridLineWidth)-1;
        X2 := (ixStolb+1)*(DefaultColWidth + GridLineWidth)-1;
        Pen.Color := Clr;
        Pen.Width := 2;
        Brush.Style := bsClear;
        Rectangle(X1, Y1, X2, Y2);
        Pen.Width := 1;
      end;
end;//SGFildsColor

{ END Help proceduer ---------------------------------------------------- }

procedure ResultDataFileParser(StatMethodID: tStatMethodID; var DiscriptorIN: TextFile);
var
  Str, Lecsema: string;
  s, i, j, countDataStr, countDataStolb: integer;
begin
// -> это надо при визуализации DataFile
  fSQLconstructor.sgFileResultData.Visible := True;
  fSQLconstructor.lbRem1.Visible := True;
  fSQLconstructor.lbRem2.Visible := True;
// -> это НЕ надо при визуализации DataFile
  fSQLconstructor.pnlViborResult.Visible := False;
  fSQLconstructor.Pole.Visible := False;
  fSQLconstructor.pnlLegenda.Visible := False;

  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  countDataStr := StrToInt(Lecsema);

  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  countDataStolb := StrToInt(Lecsema);

  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  fSQLconstructor.edtCountAll.Text := Lecsema;

// -> размер результирующей таблицы
  fSQLconstructor.sgFileResultData.RowCount := countDataStr + 1;
  fSQLconstructor.sgFileResultData.ColCount := countDataStolb + 1;
  if (countDataStr  <= 10) AND (countDataStolb <=10) then
    with fSQLconstructor.sgFileResultData do
      begin
        Height := RowCount*(DefaultRowHeight + GridLineWidth) + 3;
        Width := ColCount*(DefaultColWidth + GridLineWidth) + 3;
      end;//with

// -> подпись коментариев строк и столбцов в матрице
  UntInterface.FoundBlok(DiscriptorIN, comment_blok, _first_);
  Readln(DiscriptorIN, Str);
  fSQLconstructor.lbCommentMethodID.Caption := Str;
  fSQLconstructor.sgFileResultData.Cells[0,0] := 'Gr/Var';
  Readln(DiscriptorIN, Str);//Label по строкам
  s := 1;
  for i := 0 to countDataStr - 1 do
    begin
      Lecsema := '';
      while Str[s] <> DARSeparator do
        begin
          Lecsema := Lecsema + Str[s];
          inc(s);
        end;
      fSQLconstructor.sgFileResultData.Cells[0,i+1] := Lecsema;
      inc(s);
    end; // for i

    Readln(DiscriptorIN, Str);//Label по столбцам
    s := 1;
    for i := 0 to countDataStolb - 1 do
      begin
        Lecsema := '';
        while Str[s] <> DARSeparator do
          begin
            Lecsema := Lecsema + Str[s];
            inc(s);
          end;
        fSQLconstructor.sgFileResultData.Cells[i+1,0] := Lecsema;
        inc(s);
      end; // for i

    Readln(DiscriptorIN, Str);//Rem1
    s := 1; Lecsema := '';
    while Str[s] <> DARSeparator do
      begin
        Lecsema := Lecsema + Str[s];
        inc(s);
      end;
    fSQLconstructor.lbRem1.Caption := Lecsema;
    Readln(DiscriptorIN, Str);//Rem2
    s := 1; Lecsema := '';
    while Str[s] <> DARSeparator do
      begin
        Lecsema := Lecsema + Str[s];
        inc(s);
      end;
    fSQLconstructor.lbRem2.Caption := Lecsema;
// -> запись значений матрицы
    UntInterface.FoundBlok(DiscriptorIN, data_blok, _first_);
    for i := 0 to countDataStr - 1 do
      begin
        Readln(DiscriptorIN, Str);
        s := 1;
        for j := 0 to countDataStolb - 1 do
          begin
            Lecsema := '';
            while Str[s] <> DARSeparator do
              begin
                Lecsema := Lecsema + Str[s];
                inc(s);
              end; {само значение}
            fSQLconstructor.sgFileResultData.Cells[j+1,i+1] := Lecsema;
            inc(s);
          end; // for j
      end; // for i
  fSQLconstructor.pnlResult.Visible := True;
// -> если значимый коэффициен еорреляции
  if StatMethodID = UntInterface._LineCorrelation_coef_ then
    begin
      fSQLconstructor.pnlResult.Repaint;
      for i := 0 to countDataStr - 1 do
        if StrToFloat(fSQLconstructor.sgFileResultData.Cells[2,i+1])>0 then
          begin
            Str := fSQLconstructor.sgFileResultData.Cells[3,i+1];
            SGFildsColor(i+1, 3, clRed);
//            fSQLconstructor.sgFileResultData.Cells[3,i+1] := Str;
          end;
    end;
end;//ResultDataFileParser

procedure ResultGraphFileParser(StatMethodID: tStatMethodID; var DiscriptorIN: TextFile; FlagForGraph: tFlag);
type
  tAr = (_X1_=0, _Y1_=1, _X2_=2, _Y2_=3);
  tArray = array [tAr] of tResultType;
var
  Str, Lecsema, LabelX: string;
  s, i, clr_count, elementGraph, countGraphElement, countStolB: integer;
  A1, B1, A2, B2: integer; // размеры Pole для рисования
  ArrayCord: tArray;
begin
// -> это надо при визуализации GraphFile
  fSQLconstructor.Pole.Visible := True;
  fSQLconstructor.pnlLegenda.Visible := True;
  fSQLconstructor.pnlViborResult.Visible := True;
  fSQLconstructor.btnViborGraph.Down := True;
// -> это НЕ надо при визуализации GraphFile
  fSQLconstructor.sgFileResultData.Visible := False;
  fSQLconstructor.lbRem1.Visible := False;
  fSQLconstructor.lbRem2.Visible := False;

  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  countGraphElement := StrToInt(Lecsema);

  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  countStolB := StrToInt(Lecsema);

  ReadLn(DiscriptorIN, Str);
  s := 1; Lecsema := '';
  while Str[s] <> DARSeparator do
    begin
      Lecsema := Lecsema + Str[s];
      inc(s);
    end;
  fSQLconstructor.edtCountAll.Text := Lecsema;

// -> размер результирующей таблицы (точные значения)
  if FlagForGraph = _first_ then
    begin
      fSQLconstructor.sgFileResultData.RowCount := countGraphElement + 1;
      fSQLconstructor.sgFileResultData.ColCount := countStolb;
      if (countGraphElement  <= 10) AND (countStolB <=10) then
        with fSQLconstructor.sgFileResultData do
          begin
            Height := RowCount*(DefaultRowHeight + GridLineWidth) + 3;
            Width := ColCount*(DefaultColWidth + GridLineWidth) + 3;
          end;//with
     end;
{отчистка и создание нового списка}
  if FlagForGraph = _first_ then
    for i := fSQLconstructor.clbLook.Items.Count - 1 downto 0 do
      fSQLconstructor.clbLook.Items.Delete(i);

// -> непосредственно визуализация результата =======================
{подгатовка Pole для рисования}
    with fSQLconstructor.Pole do
      with Canvas do
        begin
          A1 := 20;
          A2 := ClientWidth - A1;
          B1 := 20;
          B2 := ClientHeight - B1;
{отчистка}Pen.Color := clBlack;
          Pen.Style := psSolid;
          Pen.Width := 1;
          Brush.Style := bsSolid;
          Brush.Color := clCream;
          Rectangle(0, 0, ClientWidth, ClientHeight);
{ось X}   Pen.Width := 2;
          MoveTo (     A1 div 2, B2);
          LineTo (A2 + A1 div 2, B2);
          Strelka(A2 + A1 div 2, B2, clBlack, _right_);
{ось Y}   MoveTo (A1, B2 + B1 div 2);
          LineTo (A1,      B1 div 2);
          Strelka(A1,      B1 div 2, clBlack, _up_);
          Cherta (0, B1, A1, B1, clBlack, _horizontal_, '1');
          Cherta (0, B2, A1, B1, clBlack, _horizontal_, '0');

          UntInterface.FoundBlok(DiscriptorIN, comment_blok, _first_);
          Readln(DiscriptorIN, Str);
          fSQLconstructor.lbCommentMethodID.Caption := Str;
          ReadLn(DiscriptorIN, Str); // Label X
          s := 1;
            for i := 0 to countStolB do
            begin
              Lecsema := '';
              while Str[s] <> DARSeparator do
                begin
                  Lecsema := Lecsema + Str[s];
                  inc(s);
                end;
// -> подпись коментариев столбцов в матрице
              fSQLconstructor.sgFileResultData.Cells[i,0] := Lecsema;
              LabelX := Lecsema; // Save
              inc(s);

              Lecsema := '';
              while Str[s] <> DARSeparator do
                begin
                  Lecsema := Lecsema + Str[s];
                  inc(s);
                end;
              if (i <> countStolB) or (StatMethodID <> _Px_) then
                Cherta(round(StrToFloat(Lecsema)*(A2-A1)+A1),0,A1,B1,clBlack,_vertical_,LabelX);
              inc(s);
            end; // for i
        end;//canvas

  clr_count := 0;
  UntInterface.FoundBlok(DiscriptorIN, graph_blok, _first_);
  for elementGraph := 0 to countGraphElement - 1 do
    try
      ReadLn(DiscriptorIN, Str);
      s := 1; Lecsema := '';
      while Str[s] <> DARSeparator do
        begin
          Lecsema := Lecsema + Str[s];
          inc(s);
        end;
      if FlagForGraph = _first_ then
        begin
          fSQLconstructor.clbLook.Items.Add(Lecsema);
          fSQLconstructor.clbLook.Checked[elementGraph] := True;
// -> подпись коментариев строк в матрице
          fSQLconstructor.sgFileResultData.Cells[0,elementGraph+1] := Lecsema;
        end;
      if fSQLconstructor.clbLook.Checked[elementGraph] then
        begin
          ReadLn(DiscriptorIN, Str);
          while Str <> rem_blok do // если его нет, то raise
            begin
              s := 1;
              for i := 0 to 3 do
                begin
                  Lecsema := '';
                  while Str[s] <> DARSeparator do
                    begin
                      Lecsema := Lecsema + Str[s];
                      inc(s);
                    end;
                  ArrayCord[tAr(i)] := StrToFloat(Lecsema);
                  inc(s);
                end;//for i
{визуализация однлго из распознанных Graph элементов}
              with fSQLconstructor.Pole do
                with Canvas do
                  begin
{проекции на оси координат}
                    Proekciya(round(ArrayCord[_X1_]*(A2-A1)+A1), round(ArrayCord[_Y1_]*(B2-B1)+B1), A1, B1, clBlack, _horizontal_);
                    Proekciya(round(ArrayCord[_X1_]*(A2-A1)+A1), round(ArrayCord[_Y1_]*(B2-B1)+B1), A1, B1, clBlack, _vertical_);
                    Proekciya(round(ArrayCord[_X2_]*(A2-A1)+A1), round(ArrayCord[_Y2_]*(B2-B1)+B1), A1, B1, clBlack, _horizontal_);
                    Proekciya(round(ArrayCord[_X2_]*(A2-A1)+A1), round(ArrayCord[_Y2_]*(B2-B1)+B1), A1, B1, clBlack, _vertical_);
                    Cherta(round(ArrayCord[_X1_]*(A2-A1)+A1), round(ArrayCord[_Y1_]*(B2-B1)+B1), A1, B1, clBlack, _horizontal_, '');
                    Cherta(round(ArrayCord[_X2_]*(A2-A1)+A1), round(ArrayCord[_Y2_]*(B2-B1)+B1), A1, B1, clBlack, _horizontal_, '');
{сама линия}        Pen.Color := Col[clr_count];
                    Pen.Style := psSolid;
                    Pen.Width := 2;
                    MoveTo(round(ArrayCord[_X1_]*(A2-A1)+A1), round(ArrayCord[_Y1_]*(B2-B1)+B1));
                    LineTo(round(ArrayCord[_X2_]*(A2-A1)+A1), round(ArrayCord[_Y2_]*(B2-B1)+B1));
                    if StatMethodID = _Fx_ then
                      Strelka(round(ArrayCord[_X1_]*(A2-A1)+A1), round(ArrayCord[_Y1_]*(B2-B1)+B1), Col[clr_count], _left_);
                  end;//Canvas
              if not EOF(DiscriptorIN) then
                ReadLn(DiscriptorIN, Str)
              else
                raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                                       +'[ResultGraphFileParser;]'#13#10
                                       +'EXCEPTION: графический элемент имеет НЕ верную структуру'+#13#10
                                       +'УСТРАНЕНИЕ: Повторите запрос на AcridStatSoftServer');
            end;//while
// -> подпись точных значений
        if FlagForGraph = _first_ then // первый раз попадаем для ВСЕХ!!!
          begin
            ReadLn(DiscriptorIN, Str);
            s := 1;
            for i := 0 to countStolB - 1 do
              begin
                Lecsema := '';
                while Str[s] <> DARSeparator do
                  begin
                    Lecsema := Lecsema + Str[s];
                    inc(s);
                  end; // значение
                inc(s);
                fSQLconstructor.sgFileResultData.Cells[i+1, elementGraph+1] := Lecsema;

                Lecsema := '';
                while Str[s] <> DARSeparator do
                  begin
                    Lecsema := Lecsema + Str[s];
                    inc(s);
                  end; // координата
                inc(s);
              end; // for i
          end;
      end;//if fSQLconstructor.clbLook.Checked[elementGraph]
      if elementGraph <> countGraphElement - 1 then
        UntInterface.FoundBlok(DiscriptorIN, graph_blok, _next_);
      clr_count := (clr_count + 1) mod 7;
      fSQLconstructor.sgFileResultData.Cells[0,0] := 'Gr/Var';
    except
      raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                             +'[ResultGraphFileParser;]'#13#10
                             +'EXCEPTION: НЕ найден графический элемент'+#13#10
                             +'УСТРАНЕНИЕ: Повторите запрос на AcridStatSoftServer');
    end;//try

{расскраска clbLook.Canvase ТОЛЬКО после pnlResult.rePaint}
  fSQLconstructor.pnlResult.Visible := True;
  fSQLconstructor.pnlResult.Repaint;
  clr_count := 0;
  for i := 0 to fSQLconstructor.clbLook.Items.Count - 1 do
    begin
      if fSQLconstructor.clbLook.Checked[i] = True then
        begin
          fSQLconstructor.clbLook.Canvas.Brush.Color := Col[clr_count];
          fSQLconstructor.clbLook.Canvas.Rectangle(2, 1+13*i, 13, 12+13*i);
          fSQLconstructor.cbClear.Visible := True;
        end;
      clr_count := (clr_count + 1) mod 7;
    end;
end;//ResultGraphFileParser

procedure ResultFileParser(FlagForGraph: tFlag);
var
  DiscriptorIN: TextFile;
  Str, Lecsema: string;
  StatMethodID: tStatMethodID;
  s: Integer;
begin
  ChDir(First.ClientDir);
  AssignFile(DiscriptorIN, 'Result_'+IntToStr(dmStatSoftClient.ClientID)+'.dar');
{$I-} Reset(DiscriptorIN); {$I+}
  if IOResult <> 0 then
    raise Exception.Create('Место возникновения: AcridMainClient'+#13#10
                           +'[ResultFileDataParser;]'#13#10
                           +'EXCEPTION: НЕ найден файл результата Result.dar,'+#13#10
                           +'УСТРАНЕНИЕ: Пошлите запрост для вычисления на AcridStatSoftServer')
  else
    try
// -> определение кол-ва строк и столбцов в матрице
      UntInterface.FoundBlok(DiscriptorIN, system_blok, _first_);
// =============================
      ReadLn(DiscriptorIN, Str); //StatMethodID
      s := 1; Lecsema := '';
      while Str[s] <> DARSeparator do
        begin
          Lecsema := Lecsema + Str[s];
          inc(s);
        end;
      StatMethodID := tStatMethodID(StrToInt(Lecsema));
// =============================
      ReadLn(DiscriptorIN, Str); //тип результата
      s := 1; Lecsema := '';
      while Str[s] <> DARSeparator do
        begin
          Lecsema := Lecsema + Str[s];
          inc(s);
        end;
      Str := Lecsema;

      if Str = DataResult then
        ResultDataFileParser(StatMethodID, DiscriptorIN);
      if Str = GraphResult then
        ResultGraphFileParser(StatMethodID, DiscriptorIN, FlagForGraph);
    finally // IOResult = 0
      CloseFile(DiscriptorIN);
    end;
end;//ResultFileParser

end.
