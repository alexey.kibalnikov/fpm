Program Acrid_SKT;
{$R MyMenu.RES}
Uses WObjects, WinTypes, WinProcs, Strings;
Const
  cm_Input    = 100;
  id_Number   = 105;
  id_Group    = 200;
  id_data     = 201;
  id_bugs     = 202;
  id_rub      = 203;
  cm_About    = 300;
  Len  = 10;
Type NumStr = array[0..Len] of Char; {--------------------------- Social security number string}
{===================================================================== ��������� ����� (Number)}
  PForma = ^TSForma;
  TSForma = object(TEdit)
    constructor Init(AParent: PWindowsObject; AnId: Integer;
      ATitle: PChar; X, Y, W, H: Integer);
    constructor InitResource(AParent: PWindowsObject; ResourceID: Word);
    function CanClose: Boolean; virtual;
  end;

constructor TSForma.Init;
 begin TEdit.Init(AParent, AnId, ATitle, X, Y, W, H, Len + 1, False) end;
constructor TSForma.InitResource;
 begin TEdit.InitResource(AParent, ResourceID, Len + 1) end;
function TSForma.CanClose; { ���� �� ������ ������� ������ --------------------------- CanClose}
const NumSet = ['0'..'9'];
var
  Flag: Boolean;
  I, Ln: Integer;
  SSN: NumStr;
begin
  GetText(SSN, SizeOf(SSN));
  Ln := StrLen(SSN);
  Flag := (Ln = Len) and (SSN[1] = '.');
  I := 0;
  while Flag and (I < Len) do
  begin
    flag := (I = 1) or (SSN[I] in NumSet);
    Inc(I);
  end;
  if not flag then
  begin
    MessageBox(HWindow, 'Number format x.xx...', '������� ����� � ������ �������',
      mb_Ok or mb_IconExclamation);
    SetSelection(0, MaxInt);
    SetFocus(HWindow);
  end;
  CanClose :=  flag;
end;
{================================================================================ �������� ����}
Type  TForm = record
   number: NumStr;
  end;
{ Application main window }
  PDataWindow = ^TDataWindow;
  TDataWindow = object(TWindow)
    Rad1,Rad2 ,Rad3: PRadioButton;
    Group: PGroupBox;
    DataRec: TForm;
    GameOver: HBitMap;
    CursorUp: HCursor;
    constructor Init(AParent: PWindowsObject; TheTitle: PChar);
    procedure DrawBMP(DC: HDC; X, Y, BitMap: HBitmap);
    procedure GetWindowClass(var WndClass: TWndClass); virtual;
    procedure Paint(PaintDC: HDC; var PaintInfo: TPaintStruct); virtual;
    procedure HGroup(var Msg: TMessage); virtual id_First + id_Group;
    procedure Form(var Msg: TMessage); virtual cm_First + cm_Input;
    procedure About(var Msg: TMessage); virtual cm_First + cm_About;
  end;

constructor TDataWindow.Init; { ������ ������ � �������� ���� ---------------------------- Init}
var AButt: PButton;
begin
  TWindow.Init(AParent, TheTitle);
  Attr.W := 282;
  Attr.H := 280;
  Attr.Style := WS_Caption or WS_SysMenu or WS_MinimizeBox;
  Attr.Menu := LoadMenu(HInstance, 'Commands');
  FillChar(DataRec, SizeOf(DataRec), 0);
  Group := New(PGroupBox, Init(@Self, id_Group, '������ ������',50, 102, 176, 108));
  Rad1 := New(PRadioButton, Init(@Self, id_Data,' ����            ',   74, 125, 138, 24, Group));
  Rad2 := New(PRadioButton, Init(@Self, id_Bugs,' �������     ',   74, 150, 138, 24, Group));
  Rad3 := New(PRadioButton, Init(@Self, id_Rub ,' �����           ',   74, 175, 138, 24, Group))
end;{Init}
procedure TDataWindow.DrawBMP;{ ��������� ������� ------------------------------------- DrawBMP}
var
  MemDC: HDC;
  bm: TBitMap;
begin
  MemDC := CreateCompatibleDC(DC);
  SelectObject(MemDC, BitMap);
  GetObject(GameOver, SizeOf(bm), @bm);
  BitBlt(DC, X, Y, bm.bmWidth, bm.bmHeight, MemDC, 0, 0, SRCCopy);
  DeleteDC(MemDC);
  ReleaseDC(HWindow, DC);
end;{DrawBMP}
procedure TDataWindow.GetWindowClass;{ ������������ ����� ���� ----------------- GetWindowClass}
begin
  TWindow.GetWindowClass(WndClass);
  CursorUp := LoadCursor(hInstance, 'Malet');
  WndClass.Style := 0;
  WndClass.hCursor := CursorUp;
  WndClass.hbrBackGround := GetStockObject(White_Brush);
  WndClass.lpszMenuName := 'Menu';
  WndClass.hIcon := LoadIcon(hInstance, 'Nik');
  GameOver := LoadBitMap(hInstance, 'GameOver');
end;{GetWindowClass}
procedure TDataWindow.Paint;{------------------------------------------------------------ Paint}
 begin DrawBMP(PaintDC, 10, 1, GameOver) end;
{++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}
procedure TDataWindow.HGroup; { ������� �� ������� ������ ------------------------------ HGroup}
var Rez:PChar;                { c����� � ������� ����������� ���������}
   i,x_rez,x_num:word;        { ������ �������������� ���������}
   flag: boolean;             { �������� ������� ���� � �������� ����� First}
begin flag:=false;
 if DataRec.Number[1]<>'.' then Rez:='��������� ���� Number � ���� First !'
 else flag:=true; {---------------- �� ������� �����}
        if flag then begin
 Rez:='Number:            '+chr(13)+chr(13)+'  Forma:                        ';
 x_rez:=8; x_num:=0;
 for i:=x_num to Len-1 do begin Rez[x_rez]:=DataRec.Number[i]; inc(x_rez) end;
 x_rez:=30; x_num:=2;
  if Rad1^.GetCheck <> 0 then
    begin flag:=false; { �������� ���� --------------------------------------------------------}
                   if ((DataRec.Number[x_num]<='2') and (DataRec.Number[x_num+1]<='9') or
                       (DataRec.Number[x_num] ='3') and (DataRec.Number[x_num+1]<='1'))
                  and ((DataRec.Number[x_num+2]='0') and (DataRec.Number[x_num+3]<='9') or
    		       (DataRec.Number[x_num+2]='1') and (DataRec.Number[x_num+3]<='2'))
                  then  flag:=true else Rez:='�������� ������ ��� ���� ��/��/����';
   if flag then begin
      for i:=x_num to x_num+1 do
        begin Rez[x_rez]:=DataRec.Number[i]; inc(x_rez); inc(x_num) end;
      Rez[x_rez]:='/'; inc(x_rez);
      for i:=x_num to x_num+1 do
        begin Rez[x_rez]:=DataRec.Number[i]; inc(x_rez); inc(x_num) end;
      Rez[x_rez]:='/'; inc(x_rez);
      for i:=x_num to Len-1 do begin Rez[x_rez]:=DataRec.Number[i]; inc(x_rez) end;
      for i:=0 to 1 do Rez[x_rez+i]:=' '
   end;{if flag}
    end;{id_Data}
  if Rad2^.GetCheck <> 0 then
    begin
      Rez[x_rez]:='$'; Rez[x_rez+1]:=' '; x_rez:=x_rez+2;
      for i:=x_num to Len-1 do begin Rez[x_rez]:=DataRec.Number[i]; inc(x_rez) end;
      for i:=0 to 1 do Rez[x_rez+i]:=' '
    end;{id_Bags}
  if Rad3^.GetCheck <> 0 then
    begin
      for i:=x_num to Len-1 do begin Rez[x_rez]:=DataRec.Number[i]; inc(x_rez) end;
      Rez[x_rez]:=' '; Rez[x_rez+1]:='�'; Rez[x_rez+2]:='�'; Rez[x_rez+3]:='�'
    end{id_Rub}    end;{if flag}
  MessageBox(getFocus,Rez,' ���������� �� ��� ��������� ???',MB_Ok)
end;
{++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}
procedure TDataWindow.Form; { ����� ����� ��� ����� ����� -------------------------------- Form}
var
  Dialog: PDialog;
  P: PWindowsObject;
begin
  Dialog := New(PDialog, Init(@Self, 'DataDialog'));
  Dialog^.TransferBuffer := @DataRec;
  P := New(PForma, InitResource(Dialog, id_Number));
  if Application^.ExecDialog(Dialog) = id_OK then ;
end;
procedure TDataWindow.About; {----------------------------------------------------------- About}
var Dialog: PDialog;
begin
  Dialog := New(PDialog, Init(@Self, 'Aboutbox'));
  if Application^.ExecDialog(Dialog) = id_OK then Exit;
end;
{==============================================================================================}
Type { Application object }
  TApp = object(TApplication)
    procedure InitMainWindow; virtual;
  end;

procedure TApp.InitMainWindow;
 begin MainWindow := New(PDataWindow, Init(nil, 'SKT Home Work')) end;
{==============================================================================================}
Var Wnd: TApp;
Begin
  Wnd.Init(' ');
  Wnd.Run;
  Wnd.Done;
End.
