object Form1: TForm1
  Left = 177
  Top = 64
  BiDiMode = bdRightToLeftNoAlign
  BorderStyle = bsDialog
  Caption = 'SKT from Acrid #12'
  ClientHeight = 474
  ClientWidth = 458
  Color = clCream
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnPaint = FormPaint
  OnShow = ButBuildingClick
  PixelsPerInch = 96
  TextHeight = 13
  inline Pole: TFrame2
    Left = 0
    Top = 93
    Width = 457
    Height = 380
    Hint = #1060#1088#1101#1081#1084' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103' '#1092#1080#1075#1091#1088
    BiDiMode = bdRightToLeft
    Color = clBlack
    ParentBiDiMode = False
    ParentColor = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
  end
  object Group: TGroupBox
    Left = 152
    Top = 0
    Width = 305
    Height = 97
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 23
      Top = 23
      Width = 60
      Height = 24
      Caption = #1057#1090#1080#1083#1100':'
    end
    object Label2: TLabel
      Left = 145
      Top = 59
      Width = 120
      Height = 24
      Caption = #1042#1072#1088#1080#1072#1085#1090' '#8470'12'
    end
    object ComView: TComboBox
      Left = 90
      Top = 21
      Width = 199
      Height = 32
      Cursor = crArrow
      Style = csDropDownList
      ItemHeight = 24
      ItemIndex = 0
      ParentShowHint = False
      ShowHint = False
      Sorted = True
      TabOrder = 0
      Text = #1050#1072#1088#1082#1072#1089#1085#1099#1081
      OnChange = ButBuildingClick
      Items.Strings = (
        #1050#1072#1088#1082#1072#1089#1085#1099#1081
        #1057#1087#1083#1086#1096#1085#1086#1081
        #1058#1086#1095#1077#1095#1085#1099#1081' ')
    end
    object chCutting: TCheckBox
      Left = 17
      Top = 56
      Width = 104
      Height = 31
      Caption = #1057#1077#1095#1077#1085#1080#1077
      TabOrder = 1
      OnMouseDown = chCuttingMouseDown
    end
  end
  object ComFigure: TRadioGroup
    Left = 0
    Top = 0
    Width = 153
    Height = 97
    Caption = '     '#1060#1080#1075#1091#1088#1072':        '
    Color = clActiveBorder
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemIndex = 2
    Items.Strings = (
      #1058#1077#1090#1088#1072#1101#1076#1088
      #1044#1086#1076#1077#1082#1072#1101#1076#1088
      #1063#1072#1081#1085#1080#1082)
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    OnClick = ButBuildingClick
  end
end
