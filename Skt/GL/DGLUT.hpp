// Borland C++ Builder
// Copyright (c) 1995, 2002 by Borland Software Corporation
// All rights reserved

// (DO NOT EDIT: machine generated header) 'DGlut.pas' rev: 6.00

#ifndef DGlutHPP
#define DGlutHPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <OpenGL.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dglut
{
//-- type declarations -------------------------------------------------------
typedef float TGLfloat3v[3];

typedef int TInteger3v[3];

//-- var, const, procedure ---------------------------------------------------
extern PACKAGE float BoxPoints[6][3];
extern PACKAGE int BoxFaces[6][4];
extern PACKAGE float OctData[6][3];
extern PACKAGE int OctIndex[8][3];
static const Extended IcoX = 5.257311E-01;
static const Extended IcoZ = 8.506508E-01;
extern PACKAGE float IcoData[12][3];
extern PACKAGE int IcoIndex[20][3];
static const Extended TetT = 1.732051E+00;
extern PACKAGE float TetData[4][3];
extern PACKAGE int TetIndex[4][3];
extern PACKAGE int PatchData[10][16];
extern PACKAGE float TeaData[127][3];
extern PACKAGE float TeaTex[2][2][2];
extern PACKAGE Opengl::_GLUquadricObj *quadObj;
extern PACKAGE float dodec[20][3];
extern PACKAGE void __fastcall DrawBox(float Size, unsigned DrawType);
extern PACKAGE void __fastcall glutWireSphere(double Radius, int Slices, int Stacks);
extern PACKAGE void __fastcall glutSolidSphere(double Radius, int Slices, int Stacks);
extern PACKAGE void __fastcall glutWireCube(double Size);
extern PACKAGE void __fastcall glutSolidCube(double Size);
extern PACKAGE void __fastcall glutWireCone(double Base, double Height, int Slices, int Stacks);
extern PACKAGE void __fastcall glutSolidCone(double Base, double Height, int Slices, int Stacks);
extern PACKAGE void __fastcall glutPixelCone(double Base, double Height, int Slices, int Stacks);
extern PACKAGE void __fastcall glutWireTorus(double innerRadius, double outerRadius, int nsides, int rings);
extern PACKAGE void __fastcall glutPixelTorus(double innerRadius, double outerRadius, int nsides, int rings);
extern PACKAGE void __fastcall glutSolidTorus(double innerRadius, double outerRadius, int nsides, int rings);
extern PACKAGE void __fastcall Diff3(float a0, float a1, float a2, float b0, float b1, float b2, float * c, const int c_Size);
extern PACKAGE void __fastcall CrossProd(const float * v1, const int v1_Size, const float * v2, const int v2_Size, float * prod, const int prod_Size);
extern PACKAGE void __fastcall Normalize(float * v, const int v_Size);
extern PACKAGE void __fastcall glutWireDodecahedron(void);
extern PACKAGE void __fastcall glutPixelDodecahedron(void);
extern PACKAGE void __fastcall glutSolidDodecahedron(void);
extern PACKAGE void __fastcall Octaheadron(unsigned ShadeType);
extern PACKAGE void __fastcall glutWireOctaheadron(void);
extern PACKAGE void __fastcall glutSolidOctaheadron(void);
extern PACKAGE void __fastcall Icosahedron(unsigned ShadeType);
extern PACKAGE void __fastcall glutWireIcosahedron(void);
extern PACKAGE void __fastcall glutSolidIcosahedron(void);
extern PACKAGE void __fastcall Tetrahedron(unsigned ShadeType);
extern PACKAGE void __fastcall glutWireTetrahedron(void);
extern PACKAGE void __fastcall glutSolidTetrahedron(void);
extern PACKAGE void __fastcall Teapot(int Grid, double Scale, unsigned ShadeType);
extern PACKAGE void __fastcall glutWireTeapot(double Scale);
extern PACKAGE void __fastcall glutSolidTeapot(double Scale);

}	/* namespace Dglut */
using namespace Dglut;
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DGlut
