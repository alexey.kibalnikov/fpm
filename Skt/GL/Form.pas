unit Form;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,
  OpenGL, Menus, StdCtrls, dglut, Unit2 ;

type
  TForm1 = class(TForm)
    Pole: TFrame2;
    Group: TGroupBox;
    Label1: TLabel;
    ComView: TComboBox;
    chCutting: TCheckBox;
    ComFigure: TRadioGroup;
    Label2: TLabel;
    procedure ButBuildingClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure chCuttingMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    DC : HDC;
    hrc: HGLRC;
    quadObj : GLUquadricObj;
    end;

var
  Form1: TForm1;
  mode : (POINT, LINE, FILL, SILHOUETTE) = Fill;
  figure:(tetr,tea,dodec)=tetr;
  rep:boolean=false;
  cliping1:boolean=false;
implementation
 {$R *.DFM}
 procedure TForm1.ButBuildingClick(Sender: TObject);
begin
rep:=true;
if form1.ComView.ItemIndex=0 then mode:=line else
   if form1.ComView.ItemIndex=2 then mode:=point else
        if form1.ComView.ItemIndex=1 then mode:=Fill else
           rep:=false;
if form1.comFigure.ItemIndex=0 then Figure:=Tetr else
   if form1.comFigure.ItemIndex=1 then Figure:=Dodec else
        if form1.comFigure.ItemIndex=2 then Figure:=Tea else
           rep:=false;
sendmessage(handle,WM_paint,0,0);
end;

procedure Tform1.FormPaint(Sender: TObject);
const
 arCutting : Array [0..3] of GLdouble = (-5.0, 1.3, 2.9, 0.0);
begin
if  rep  then begin
 glClear (GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);      // ������� ������ �����
 glPushMatrix;
 glColor3f (0.0, 1.0, 1.0);
 glTranslatef (0.0, 0.0, -5.0);
 glClipPlane (GL_CLIP_PLANE0, @arCutting);
if cliping1 then  glEnable (GL_CLIP_PLANE0)else
        glDisable (GL_CLIP_PLANE0);

glRotatef (40.0, 1.0, -1.0, -0.6);

case figure of
tetr:  begin
        glLineWidth (2.5);  glPointSize(6);
        glRotatef (15.0, 1.0, -1.0, 0.6);
        glTranslatef (0.15,-0.5,0);
        glScalef (2.8,2.8,2.8);
       case mode of
        POINT : Tetrahedron(GL_Points);
        LINE  : glutWireTetrahedron;
        FILL  : glutSolidTetrahedron ;
        end;
       end;dodec: begin  glLineWidth (2.5);  glPointSize(4);
       case mode of
        POINT : glutPixelDodecahedron;
        LINE  : glutWireDodecahedron;
        FILL  : glutSolidDodecahedron ;
       end;
       end;
tea:   begin
        glLineWidth (1.5);  glPointSize(1.5);
        glTranslatef (-0.6,0,0);
       case mode of
        POINT : Teapot(10, 1.4, GL_Point);
        LINE  : glutWireTeapot(1.4);
        FILL  : glutSolidTeapot(1.4);
       end;
       end;
end;
  glPopMatrix;
  SwapBuffers(DC);
 end;
end;


procedure SetDCPixelFormat (hdc : HDC);
var
 pfd : TPixelFormatDescriptor;
 nPixelFormat : Integer;
begin
 FillChar (pfd, SizeOf (pfd), 0);
 pfd.dwFlags  := PFD_DRAW_TO_WINDOW or PFD_SUPPORT_OPENGL or PFD_DOUBLEBUFFER;
 nPixelFormat := ChoosePixelFormat (hdc, @pfd);
 SetPixelFormat (hdc, nPixelFormat, @pfd);
end;

procedure Tform1.FormCreate(Sender: TObject);
begin
 DC := GetDC (form1.pole.handle);
 SetDCPixelFormat(DC);
 hrc := wglCreateContext(DC);
 wglMakeCurrent(DC, hrc);
 glClearColor (0.0, 0.0, 0.0, 1.0); // ���� ����
 glEnable (GL_LIGHTING);
 glEnable (GL_LIGHT0);
 glEnable (GL_DEPTH_TEST);
 glEnable (GL_COLOR_MATERIAL);
 glColor3f (1.0, 0.0, 0.0);
 quadObj := gluNewQuadric;

 glViewport(0, 0, pole.Width,pole.Height );
 glMatrixMode(GL_PROJECTION);
 glLoadIdentity;
 gluPerspective(40.0,pole.Width/pole.Height,2.0, 20.0);
 glMatrixMode(GL_MODELVIEW);

 InvalidateRect(form1.pole.handle, nil, False);
end;

procedure Tform1.FormDestroy(Sender: TObject);
begin
 gluDeleteQuadric (quadObj);
 wglMakeCurrent(0, 0);
 wglDeleteContext(hrc);
 ReleaseDC (form1.pole.handle, DC);
 DeleteDC (DC);
end;

procedure TForm1.chCuttingMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 cliping1:=not cliping1;
 {sendmessage(handle,WM_paint,1,0);}
   ButBuildingClick(Sender);
end;

end.
