import java.awt.event.*;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.math.*;

class MyCl extends JPanel {
	private float [][] PixelVect = new float[4][2];
	final private int A;   // сторона квадрата
	final private float N; // отношение
    public MyCl(int newA, float newN) {
    	A = newA; N = newN;
    } // MyCl()
    
    private void next() {    	
    	float x1, y1, x2, y2;
    	float [][] Temp = new float [4][2];
        
        x1 = PixelVect[0][0]; y1 = PixelVect[0][1];
        x2 = PixelVect[1][0]; y2 = PixelVect[1][1];
        Temp[0][0] = (x1+N*x2)/(1+N);  
        Temp[0][1] = (y1+N*y2)/(1+N);
 		x1 = PixelVect[2][0]; y1=PixelVect[2][1];
 		Temp[1][0] = (x2+N*x1)/(1+N);  
 		Temp[1][1] = (y2+N*y1)/(1+N); 		
 		x2 = PixelVect[3][0]; y2 = PixelVect[3][1];
		Temp[2][0] = (x1+N*x2)/(1+N);
		Temp[2][1] = (y1+N*y2)/(1+N);
 		x1 = PixelVect[0][0]; y1 = PixelVect[0][1];
 		Temp[3][0] = (x2+N*x1)/(1+N);
 		Temp[3][1] = (y2+N*y1)/(1+N);
 		
  		for (int i = 0; i<4; i++) {
    			PixelVect[i][0] = Temp[i][0];
    			PixelVect[i][1] = Temp[i][1];  			
  		} // for
    } // next()
        
    public void paint(Graphics G) {
		int x1, y1, x2, y2;
		float x0, y0;
    	
    	x0 = 20; y0 = 20;   // верхний левый угол

		PixelVect[0][0] = x0;   PixelVect[0][1] = y0;
		PixelVect[1][0] = x0+A; PixelVect[1][1] = y0;
		PixelVect[2][0] = x0+A; PixelVect[2][1] = y0+A;
		PixelVect[3][0] = x0;   PixelVect[3][1] = y0+A;

		G.setColor(Color.black);	
//		G.drawRect(0, 0, 900, 900);// Clear
 
    	while (Math.sqrt(Math.pow(PixelVect[0][0]-PixelVect[1][0],2)+Math.pow(PixelVect[0][0]-PixelVect[1][0],2)) > A/(2*N))
 		{
            x1 = (int)PixelVect[0][0]; 
            y1 = (int)PixelVect[0][1];
 			x2 = (int)PixelVect[1][0]; 
 			y2 = (int)PixelVect[1][1];
			G.drawLine(x1,y1,x2,y2);
 			
 			x1 = (int)PixelVect[2][0]; 
 			y1 = (int)PixelVect[2][1];
			G.drawLine(x1,y1,x2,y2);
 			
 			x2 = (int)PixelVect[3][0]; 
 			y2 = (int)PixelVect[3][1];
			G.drawLine(x1,y1,x2,y2);
			
			x1 = (int)PixelVect[0][0]; 
			y1 = (int)PixelVect[0][1];
			G.drawLine(x1,y1,x2,y2);
			
			next();
		} // while
	} // paint()
	
} // class MyCl ----------------------------------------//

class Kvadrat extends JFrame {
		public Kvadrat() {
			addWindowListener ( new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			} // windowClosing()
		});
        setSize(500, 500);
	    setTitle("Kvadrat");
        MyCl mypanel = new MyCl(400, 100);
        Container cont = getContentPane();
        cont.add("Center",mypanel);
        setVisible(true);        
	} // Kvadrat()

} // class Kvadrat -------------------------------------//

public class Acrid {
	public static void main(String args[]) {
		System.out.println("Starting Kvadrat...");
		Kvadrat mainFrame = new Kvadrat();
	}

} // Acrid ---------------------------------------------//
