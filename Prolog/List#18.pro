/* List # 18 
   Определить предикат InpositLists (List1,List2),
   который равен True, если все положительные элементы
   списка List1 принадлежат списку List2. */

nowarnings
     
DOMAINS

 List = integer*
 
PREDICATES

 Search(integer,list)
 InpositLists(List,List)
 WriteList(List)	
 First			  

CLAUSES
 
 Search(H,[H|Ls]).
 Search(X,[H|Ls]):-X<>H,Search(X,Ls).

 InpositLists([],L2).
 InpositLists([X|L1s],L2):-X< 0, InpositLists(L1s,L2).
 InpositLists([X|L1s],L2):-X>=0, Search(X,L2), 
                                 InpositLists(L1s,L2).
                                 
 First:-MakeWindow (2,3,15, "     Условие задачи (List#18)      ",1,3,5 ,74,5,255,""),
          write(" Определить предикат InpositLists(List1,List2), который равен True,"),nl,
          write(" если все положительные элементы списка List1 принадлежат списку List2"),nl,
          write("           Кибальников Алексей, КубГУ, ФПМ, КИТ, 2004."), 
        MakeWindow (1,3,15," Prog FORM Acrid ",7,3,12,74,5,255,"╔╗╚╝═║"),
          write("Введите список List1 , Ex: [ a1,a2,...,an ] "),nl,
        ReadTerm(List, L1),
          write("Введите список List2 , Ex: [ b1,b2,...,bn ]"),nl,
        ReadTerm(List, L2),nl,
          write(" InpositLists( "),WriteList(L1),write(" , "),
          WriteList(L2),write(" )="),       
     InpositLists(L1,L2),write("True");write("False").

 WriteList([]).
 WriteList(H):-write(H),!.
 WriteList([H|L]):-WriteList([H]),write(","),WriteList(L).
 
GOAL First.