/*  #16 Найти число простых циклов в графе.
    #17 Найти длины простых циклов в графе. */

nowarnings

domains

  Vershina = integer
  Way      = Vershina*
  List_Way = Way*
  Rebro    = link(Vershina, Vershina)
  Graph    = Rebro*
  
predicates
 
  Input_Graph(Graph) 
  Link_Found(Graph, Rebro)
  Delete_Rebro(Graph, Rebro, Graph)    
  Search_Way_and_Save(Graph, Vershina, Vershina, Way)
  Search_Cycle(Graph, Way)  
  Search_All_Cycles(Graph, List_Way)
   
  Length_Cycle(Way, integer)         % Graph # 17
  Write_Rez(List_Way, integer)       % Graph # 16
  First
  
/* --------- Help predicats for SINGLE copy ---------
 Ex: [1,2,3,1]; [2,3,1,2]; [3,1,2,3] -> [3,1,2,3] ! 

   Основная идея в том, что из всех найденных циклов
   на выход идут только те, которые стартуют с 
   наибольшей вершины в цикле (см. Ex).
   -------------------------------------------------- */
  
  Max(Vershina, Vershina, Vershina)
  MaxElem(Vershina, Way)
  Single(Way)
 
clauses

  Input_Graph([ link(1, 2),
                link(2, 3),
                link(3, 4),
                link(4, 5),
                link(5, 3),
                link(3, 1),
                link(3, 3),
                link(5, 6),
                link(6, 2),
                link(1, 6) 
              ]).
  
  Link_Found([L|_], L).                      
  Link_Found([_|T], L) :- Link_Found(T, L).
  
  Delete_Rebro([L|Ts], L, Ts).           
  Delete_Rebro([H|Ts], L, [H|Rs]) :- 
    Delete_Rebro(Ts, L, Rs).    

% Ex: Del...([A,..,C, L ,Ts,...],L,[A,..,C,Ts,...])
    
  Search_Way_and_Save(G, V1, V2, [V1,V2]) :-
    Link_Found(G, link(V1, V2)).
  Search_Way_and_Save(G, V1, V2, [V1|Ts]) :-
    Link_Found(G, link(V1, Var)), 
    V1<>Var, V2<>Var,                      % БЕЗ ПЕТЕЛЬ ?!
    Delete_Rebro(G, link(V1, Var), New_G),
    Search_Way_and_Save(New_G, Var, V2, Ts). 

% T=G\{link(V1,V)} => ЦИКЛЫ - элементарные -------------
    
  Search_Cycle(G, W) :- 
    Search_Way_and_Save(G, Var, Var, W).
  
  Search_All_Cycles(G, Rez) :-
    FindAll(Var, Search_Cycle(G, Var), Rez).

%------------------- Help predicates -------------------
  MaxElem(L, [L]).
  MaxElem(M, [L|Ls]) :- MaxElem(M2, Ls), Max(M2, L, M).
          
  Max(X, Y, X) :- X>Y.
  Max(X, Y, Y) :- X<=Y.

  Single([T|Ts]) :- MaxElem(Var, [T|Ts]), Var=T.
%-------------------------------------------------------

  Length_Cycle([],-1).  % надо ребра, а не вершины
  Length_Cycle([_|Ts], C) :- Length_Cycle(Ts, N), C=N+1.

  Write_Rez([], 0). 
  Write_Rez([T|Ts], C) :- 
    Single(T), 
    Write_Rez(Ts, N),   % Loop ( гениальный ход )
    C=N+1, 
    Length_Cycle(T,T_len),
    write(C," # ",T," длина цикла = ",T_len," ░ "), nl 
  ; Write_Rez(Ts, C).   % Loop ( глупо но работает )
  
  First :-
    MakeWindow (2,3,15, "     Условия задач (Graph #16,#17)      ",1,3,5,74,5,255,""),
      write(" #16 Найти число простых циклов в графе."),nl,
      write(" #17 Найти длины простых циклов в графе."),nl,
      write("             Кибальников Алексей, КубГУ, ФПМ, КИТ, 2004."),        
    MakeWindow (1,3,15,"      MESSAGE     ",7,3,3,74,1,255,"╔╗╚╝═║"),
      write(" Отдельная петля - это цикл, но остальные циклы считаем без петель."),
    MakeWindow (1,3,15," Prog FORM Acrid ",11,3,14,74,3,255,"╔╗╚╝═║"),
    Input_Graph(G),
    Search_all_Cycles(G, L),
    Write_Rez(L, Cnt),nl, 
    write("Кол-во циклов ", Cnt," <НЕ ЗАВИСЛА> Ok").

goal First
  