domains
  Type = integer
  
database - scr1
  db1(Type)
 
database - scr2
  db2(Type)
 
predicates
  d(Type)
  assert_db 
  Main
  First
 
clauses
  d(1).
  d(2).
  d(3).
  d(2).
  d(5).
  d(6).
  d(7).
  
 assert_db:-d(T),assert(db1(T),scr1),fail.
 assert_db:-!.

 Main:-db1(T),        assert(db2(T) ,scr2), retract(db1(T),scr1),
       db1(T2),T<>T2, assert(db2(T2),scr2), retract(db1(T2),scr1),
       db1(T3),T<>T3,                       retract(db1(T3),scr1),
       fail.
 Main:-!.      

 First:-assert_db, Main,
        save("My.txt",scr1),
        save("My2.txt",scr2).
 
goal First.  
  