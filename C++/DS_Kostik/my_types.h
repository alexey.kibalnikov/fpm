#include <stdio.h>
#include <math.h>

int const prior[]={  3  , 2  , 1  , 0  };  // prioriteti operacii
enum znak         {_and_,_or_,_sz_,_so_};  // mn. dlya oboznachenii
const _buf_size_=100,_atom_=true; //&,v
#define null 0
struct arg  {short int maska; bool main; arg *next;};// spisok podstanovki
// maska - simvol peremennoi, main - znachenie v nabore
struct str  {short int main ; bool flag; arg *pVar;};/* raspoznanaya liksema
   main - znak, esli flag = _atom_ (t.e. & ili v)
   main - maska,esli flag!= _atom_ (t.e. simvol, sopostavlennii s peremennoi)
   pVar - ssilka na znachenie peremennoi             */
str  *Sp;  // Stack
void *Ss;
inline void Push(str& New) {*Sp++=New;}
inline void Pop (str& Old) {Old=*--Sp;}

struct list {str pole; list *next;}; // spisok raspoznanih liksem
#define _DEL_(X,headX,type)\
              X=(type)headX;\
              while(X->next!=null)\
               {headX=X;\
                X=X->next;\
                delete (type)headX;}

class FUNC {    char *Buf; // Stroka vvoda
                void *headElem;
                list *Elem; //Raspoznanie liksemi iz Buf
public:         void *headPoliz;
                list *Poliz;
private:               bool Scan();
                inline void UpCase()
                             {int i=0; while(Buf[i]!='\0')
                                        {if(Buf[i]>='a') Buf[i]-='a'-'A'; i++;}}
                inline void found_atom(short int z) // &,v
                             {Elem->pole.main=z; Elem->pole.flag=_atom_;
                              Elem=Elem->next= new list; Elem->next=null;}
                inline void found_var (char c)      // simvol peremennoi
                             {Elem->pole.main=(short int)c;
                              //potom EQVEL::List() poisk v Var spiske i pVar:=...
                              Elem=Elem->next=new list; Elem->next=null;}
                       bool POLIZ(); // Save (eto osnovnoi rezyl'tat)
public:                bool Poliz_test(); // Ostavit' tol'ko znachemie operandi
                       bool First(); // Rez -> Poliz
                       bool Math();  // Vichisleniya po Poliz na nabore EQVEL::Var
                inline void Descriptor() {_DEL_(Poliz,headPoliz,list*);}
}; //FUNC

class EQVEL {FUNC F1,F2;
                       void *headVar;
                       arg  *Var;   // spisok argumentov, perebor pri pomoshi EQVEL::Next()
                       bool List(); // sozdaet EQVEL::Var i pripisivaet FUNC::Poliz->pVar
                       bool Next(); // Ocherednoi nabor Var+1 (binarnoe slojenie)
public:                bool Main();
}; //EQVEL

// Print -------------------------------------------------------------------------------------
void Prt(arg *A) {int i; while(A->next!=null) {printf("%c=%d ",A->maska,A->main); A=A->next;}}

void Print_file(FILE *F)
#define SET_BEG 0
 {char simb;
  printf("\n");
  fseek(F,sizeof(char),SET_BEG);
  while ((simb=fgetc(F))!=EOF) printf("%c",simb);
  fseek(F,sizeof(char),SET_BEG);
}

void Print(list *A)
 {int i;
  const char *arr[]= {"AND","OR","(",")"};
  while(A->next!=null)
   {if (A->pole.flag==_atom_) printf("%s ",arr[(int)A->pole.main]); else
     {if ((char)A->pole.main=='T') printf("True "); else
       if ((char)A->pole.main=='F') printf("False "); else
        printf("%c ",A->pole.main);}
    A=A->next;}
  printf("\n");
}
// Stroki soobshenii pod DOS
#define _all_simvol_found_  "   OK : Все символы в строке распознаны\n"
#define _nedopustim_simvol_ "ERROR : В строке найденны не допустимые символы\n"
#define _skobki_true_       "   OK : Скобочная структура верна\n"
#define _skobki_false_      "ERROR : Не верная скобочная структура\n"
#define _hvataet_vsego_     "   OK : Верное кол-во операндов и знаков операций\n"
#define _not_operand_       "ERROR : Не хватает операндов\n"
#define _not_operasii_      "ERROR : Не хватает знаков операций\n"
#define _test_              " TEST : Нарушение ЭВРИСТИКИ список значимых переменных для F1 и F2 не совпал\n"
#define _mes_               "Просьба: вводите формулы составленные только из значимых переменных"
#define _perebor_           "           Перебор всех значений для значимых переменных ...\n"
#define _ne_znach_peremen_  "Я же просил!!! Неужели сложно преобразовать формулу\n"