#include <my_types.h>

bool FUNC::Scan()
//raspozmaet leksemi v stroke Buf + test, Rez -> Elem;
 {int i=0;
  UpCase();
  do {switch (Buf[i++])
      {case ' ':                    break;
       case '&': found_atom(_and_);
                 if(Buf[i]=='F') goto error2; break;
       case 'V': found_atom(_or_) ; break;
                 if(Buf[i]=='T') goto error2; break;
       case '(': found_atom(_so_) ; break;
       case ')': found_atom(_sz_) ; break;
        default: if(('A'<=Buf[i-1])&&(Buf[i-1]<='Z')) found_var (Buf[i-1]);
                  else goto error;  break;
     }//switch
   } while(Buf[i+1]!='\0');
          printf(_all_simvol_found_);  return true;
  error : printf(_nedopustim_simvol_); return false;
  error2: printf(_ne_znach_peremen_);  return false;
} // FUNC::Scan()

bool FUNC::POLIZ()
//postroenie pol'skoi inversnoi zapisi na osmove massiva Elem + test , Rez -> Poliz
#define _PLZ_ELEM_ Poliz->pole=Elem->pole; Poliz=Poliz->next= new list; Poliz->next=null
#define _PLZ_COPY_ Poliz->pole=Copy;       Poliz=Poliz->next= new list; Poliz->next=null
#define _CONTINUE_ Elem=Elem->next; continue
 {str Copy;
  while (Elem->next!=null)
   {if (Elem->pole.flag!=_atom_) {_PLZ_ELEM_;                 _CONTINUE_;}
    if (Ss==Sp) {Push(Elem->pole);                            _CONTINUE_;}
    Pop(Copy);
    if ((prior[(int)Elem->pole.main]>prior[(int)Copy.main] && Elem->pole.main!=_sz_)
         || (prior[(int)Elem->pole.main]==prior[_so_]))
     {Push(Copy); Push(Elem->pole);                           _CONTINUE_;}
    while(prior[(int)Elem->pole.main]<=prior[(int)Copy.main])
     {_PLZ_COPY_; if(Ss!=Sp) Pop(Copy); else {Push(Elem->pole); goto Ok;}}
    if (Elem->pole.main!=_sz_) {Push(Copy); Push(Elem->pole); _CONTINUE_;}
Ok: if (Elem->pole.main==_sz_)
     {if (prior[(int)Copy.main]==prior[_so_])
       {if (Copy.main!=_so_) {_PLZ_COPY_;                     _CONTINUE_;}}
      else goto error;
     }                                                        Elem=Elem->next;
   }//while
  while(Ss!=Sp)
   {Pop(Copy);
    if ((prior[(int)Copy.main]==prior[_so_])||(Copy.main==_sz_)) goto error;
    _PLZ_COPY_;
   }     printf(_skobki_true_);  return true;
  error: printf(_skobki_false_); return false;
} // FUNC::POLIZ()

bool FUNC::Poliz_test()
 {str S1,S2;
  Poliz=(list*)headPoliz;
  while (Poliz->next!=null)
   {if (Poliz->pole.flag!=_atom_) {Push(Poliz->pole); Poliz=Poliz->next; continue;}
    if (Ss!=Sp) Pop(S1); else goto error;
    if (Ss!=Sp) Pop(S2); else goto error;

    switch((int)Poliz->pole.main)
     {case _and_: Push(S1); break;
      case _or_ : Push(S1); break;
     }//switch
    Poliz=Poliz->next;} // while
  Pop(S1); if (Ss!=Sp) goto error2;
          printf(_hvataet_vsego_); return true;
  error : printf(_not_operand_);   return false;
  error2: printf(_not_operasii_);  return false;
} // FUNC::Poliz_test()

bool FUNC::Math()
//vichislenie zmacheniya Poliza (po znacheniyam EQVEL::Var) + test, Rez
 {str S1,S2;
  int i; Poliz=(list*)headPoliz;
  while (Poliz->next!=null)
   {if (Poliz->pole.flag!=_atom_) {Push(Poliz->pole); Poliz=Poliz->next; continue;}
    if (Ss!=Sp) {Pop(S1); if(S1.flag!=_atom_) S1.main=S1.pVar->main;}
    if (Ss!=Sp) {Pop(S2); if(S2.flag!=_atom_) S2.main=S2.pVar->main;}
    S2.flag=_atom_; // v S2 hranim znachenie operasii (dlya nego podstanovka ne nujna)
    switch((int)Poliz->pole.main)
     {case _and_: S2.main=(short int)((bool)S1.main & (bool)S2.main); Push(S2); break;
      case _or_ : S2.main=(short int)((bool)S1.main | (bool)S2.main); Push(S2); break;
     }//switch
    Poliz=Poliz->next;} // while
  Pop(S1); if(S1.flag!=_atom_) S1.main=S1.pVar->main; return S1.main;
} // FUNC::Math()

bool FUNC::First()
// Vvod formyli -> Poliz (Ostaetsya pamyat' pod Stack i Poliz)
 {Buf= new char[_buf_size_];
  headElem=Elem=new list; Elem->next=null;
  printf("length(Str)=%d, Str: ",_buf_size_);
  fgets(Buf,_buf_size_,stdin);
  bool flag=Scan();
  delete[] Buf; printf("Scan: ");
  if (flag) Print(Elem=(list*)headElem);
   else {_DEL_(Elem,headElem,list*); goto error;}
  headPoliz=Poliz=new list; Poliz->next=null;
  Elem=(list*)headElem;
  flag=POLIZ(); printf("Poliz: ");
  if (flag) {Print(Poliz=(list*)headPoliz);
             if(!Poliz_test()) goto error;}
              else {error: printf("ERROR!!!\n"); return false;}
                                                 return true;
} // FUNC::First()

bool EQVEL::List()
 // Svyazivaet perememmie v FUNC::Poliz s elementom v spiskoe ih znachenii EQVEL::Var
 // peremennie v F1 i F2 doljni sovpadat' polnost'u
 {bool found;

  headVar=Var= new arg; Var->next=null;
  Var->maska=(short int)'F'; Var->main=false; Var=Var->next= new arg; Var->next=null;
  Var->maska=(short int)'T'; Var->main=true ; Var=Var->next= new arg; Var->next=null;

  F1.Poliz=(list*)F1.headPoliz;
  while (F1.Poliz->next!=null)
   {if (F1.Poliz->pole.flag!=_atom_)
     {Var=(arg*)headVar; found=false;
      while (Var->next!=null) // ishim tekushee znachenie Poliz v Var spiske (net-dobavim)
       {if (Var->maska==F1.Poliz->pole.main) {found=true; F1.Poliz->pole.pVar=Var;}
        Var=Var->next;}
      if (!found) {Var->maska=F1.Poliz->pole.main; Var->main=true; // dobavili
                   F1.Poliz->pole.pVar=Var; Var=Var->next= new arg; Var->next=null;}} // if
    F1.Poliz=F1.Poliz->next;} // while
// spisok peremennih Var uje sformirovan, v F2 novih peremennih bit' ne doljno,
// esli v funcsiyah net const
  F2.Poliz=(list*)F2.headPoliz;
  while (F2.Poliz->next!=null)
   {if (F2.Poliz->pole.flag!=_atom_)
     {Var=(arg*)headVar; found=false;
      while (Var->next!=null) // ishim tekushee znachenie Poliz v Var spiske (False)
       {if (Var->maska==F2.Poliz->pole.main)
         {found=true; if(Var->maska!='T') Var->main=false; F2.Poliz->pole.pVar=Var; break;}
        Var=Var->next;}
      if (!found) goto error;} // if
    F2.Poliz=F2.Poliz->next;} // while
// proverka vozmojnosti primeneniya E'vristiki
  Var=(arg*)headVar; Var=Var->next; Var=Var->next;
  while (Var->next!=null) {if (Var->main) goto error; Var=Var->next;} // test na sovpadenie
                         return true;
  error: printf(_test_); return false;
} // EQVEL::List()

bool EQVEL::Next()
// Generasiya perebora vseh znachenii Var
 {bool flag=true;
  Var=(arg*)headVar;
  Var=Var->next; Var=Var->next; // propusk 2-x const znachenii True,False
  while (Var->next!=null)
   {if (flag&&Var->main) {Var->main=false; flag=true;} else {Var->main|=flag; flag=false;}
    Var=Var->next;}
  if (flag) goto vse; return true;
                 vse: return false;
} // EQVEL::Next()

bool EQVEL::Main()
// Proverka ekvivalentnosti
 {bool f1,f2,flag=true;
  printf(_mes_); printf("  F1...\n");
  if (!F1.First()) return false;
   printf(_mes_); printf("  F2...\n");
  if (!F2.First()) return false;
  if (!List())     {flag=false; goto jamp;}
  printf(_perebor_);
  do {Var=(arg*)headVar; Var=Var->next; Var=Var->next; Prt(Var);
      f1=F1.Math(); f2=F2.Math(); printf(" |  F1=%d  F2=%d\n",f1,f2);
      if (f1!=f2)        {flag=false; goto jamp;}
     } while (Next());
  jamp: F1.Descriptor();
        F2.Descriptor();
        _DEL_(Var,headVar,arg*);
        if(!flag) goto error;
         return true;
  error: return false;
} // EQVEL::Main()

void main ()
#define _Ok_    "uspeshno otkrit..."
#define _Err_   "ERROR : Ohibka pri otkritii"
 {FILE *help_ptr;
  char *Dir ="F1~F2.txt";
  help_ptr=fopen(Dir,"r");
  if(!help_ptr) printf("file %s %s \n",Dir,_Err_); else
   {printf("file %s %s \n",Dir,_Ok_);
    Print_file(help_ptr);
    fclose(help_ptr);
   }
  Ss=Sp=new str[_buf_size_];
  EQVEL E;
  if (E.Main()) printf("\nREZ=TRUE, t.e. F1~F2\n");
           else printf("\nREZ=FALSE, t.e. NOT(F1~F2)\n");
  Sp=(str*)Ss; delete[] Sp;
  printf(" Exit"); getchar();
} // main()