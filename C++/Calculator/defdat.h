// в кодировке OEM (cp866), для корректрого отображения в printf
#include <utility>

using namespace std;

const string MES_SS = "В %d c/c %s\n";

// for CParser ...
#define _GROUP_BASE 3 // длина значения элемента последовательности

// for Exception ...
#define _ERR_MES_NOT_FOUND "Значение %s не найдено в разделе %s словаря"
#define _ERR_MES_RANG_ATOM "Неверное значение ранга атома"
#define _ERR_MES_SEX       "Неверное значение пола в ранге"

typedef string tElem;
typedef const tElem tcArElem[];

// for CEnglishDictionary ...
tcArElem eElem0_9   = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}; // c 0
tcArElem eElem10_19 = {"ten", "elevn", "twelve", "thirteen", "fourteen", "fifteen", "sisteen", "seventeen", "eighteen", "nineteen"}; // c 10
tcArElem eElem20_90 = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"}; // c 2
tcArElem eRangs     = {"", "thousand", "million", "milliard", "2x?x", "2y?y", "2z?z"};

// for CRussianDictionary ...
tcArElem rElem0_9m    = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"}; // c 0
tcArElem rElem0_9w    = {"ноль", "одна", "две", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"}; // c 0
tcArElem rElem10_19   = {"десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"}; // c 10
tcArElem rElem20_90   = {"двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"}; // c 2
tcArElem rElem100_900 = {"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"}; // c 1
tcArElem rRangs       = {"", "тычяч", "миллион", "миллиард", "x?x", "y?y", "z?z"};

#define _MAN 1
#define _WOMAN 2

typedef int tRangSex;
typedef const tRangSex tcArRangSex[];

tcArRangSex rRangsSex = {_MAN, _WOMAN, _MAN, _MAN, _MAN, _MAN, _MAN};

// SpecialRules ...

/* Ex: 1 тысяч"а", 2 тысяч"и"; 1 миллион"", 2 миллион"а", 5 миллион"ов"
 rRangs   SpecialRules
{     0:  {}),
      1:  {1:"а", 2:"и", 3:"и", 4:"и"}),
      2:  {1:"",  2:"а", 3:"а", 4:"а", KEY_ALL:"ов"}),
      3:  {1:"",  2:"а", 3:"а", 4:"а", KEY_ALL:"ов"}),
      ...
} */

#define _SR_a "а"
#define _SR_i "и"
#define _SR_ov "ов"
