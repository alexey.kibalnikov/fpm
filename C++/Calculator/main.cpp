/*  Задание: необходимо вывести введенное 10-тичное число словами на русском языке в
    8-ми и 10-ной системах. Например.

    Вводим: 131
    Выводим:
    сто тридцать один в десятичной системе;
    двести три в восьмеричной системе.

    При проектировании подумайте о том, как ваше решение можно было бы локализовать на английский язык
    (тут обратите внимание, например, на цифру "двести", которая на английском будет состоять из двух слов).
    Укажите, сколько времени у вас заняло выполнение этого задания
*/

/*  NB: Алгоритм расчитан для положительных целых чисел, результат выводится в именительном падеже. !!!
    Параметр задается строкой Ex: "83943347543", что позволяет задавать "болшие" числа (в Python это не обязательно, для C++)

    Суть алгоритма: число приходящее на вход делится на элементы по три цифры.
    Ex: "83943347543" -> "83";"943";"347";"543".
    Элементам сопоставляется ранг (позиция в числе) от 0 справа налево.
    Ex: "83";"943";"347";"543" -> ("83",3);("943",2);("347",1);("543",0)
    Далее происходит формирование текстового представления числа путем конкатинации строк, сформированых элементами.
    Элемент на основе своего значения (Ex: "83") формирует текст, после дописывается "миллиарда" - этот текст
    определяется рангом. Кроме того рангом определяется пол (sex), в котором пишется значение элемента, т.е. существует
    взаимная зависимость: двЕ тысячИ, но двА миллиона. При этом две тысячИ, но пять тысяЧ.
    Справочник слов отделен от алгоритма.

    ("83",3);("943",2);("347",1);("543",0)
    восемьдесят три миллиарда
    девятьсот сорок три миллиона
    триста сорок семь тысяч
    пятьсот сорок три
*/

#include <iostream>
#include <map>
#include "defdat.h" // константы

using namespace std;
# define _BUFSIZE 80

// типы элементов из которых будет собираться текст
typedef map< int, tElem > tDictElem;
typedef tDictElem *tpDictElem;
typedef map< int, tRangSex > tDictRangSex;

// Мои исключения в программе
struct CException {
    string sMes;
    CException(string p_mes) {sMes = "ERROR: "+p_mes+ "\n";};
};
// абстрактный класс (публичный метод) - интерфейс для CDictionary
struct IDictionary {
    virtual tElem get_str_elem(int p_key100, int p_key10, int p_key1, int p_rang_elem) = 0; // чисто виртуальная функция
};
typedef IDictionary *tpDict;

// русский словарь с интерфейсом IDictionary
class CRussianDictionary: public IDictionary {
  public:
    CRussianDictionary();
    virtual ~CRussianDictionary() {ClearDictLine();};
  protected:
    tDictElem    dlElem0_9m;
    tDictElem    dlElem0_9w;
    tDictElem    dlElem10_19;
    tDictElem    dlElem20_90;
    tDictElem    dlElem100_900;
    tDictElem    dlRangs;
    tDictRangSex drsRangsSex;
    void ClearDictLine();

    // реализует интерфейс
    tElem get_str_elem(int p_key100, int p_key10, int p_key1, int p_rang_elem);

    // Получение значения в разделе справочника. Ex: 2 -> два, двести (в зависимости от раздела)
    tElem get_str_atom(tpDictElem pDictElem, int p_key);
    // Дописывание "окончания". Ex: тысяч
    virtual tElem get_str_rang(int p_key, int p_rang_elem);
    /* Элемент содержит три ключа p_key100, p_key10, p_key1 -> xyz
       Дальнейший алгоритм можно разделить на части от входящего значения. */
    // >>> сотни Ex: 235
    virtual tElem xyz(int p_key) {return get_str_atom(&dlElem100_900, p_key);};
    /* >>> сотни Ex: 200
       NB: В _y0_add_rang используется другой способ занания, согласно нему
           надо было писать get_str_atom(&dlElem100_900, p_key)+ ...
           Но в таком случае в английском словаре надо было бы переопределять
           на одну функцию больше. */
    tElem x00_add_rang(int p_key, int p_rang_elem) {return xyz(p_key)+ get_str_rang(p_key, p_rang_elem);};
    // >>> десятки Ex: 24
    virtual tElem yz(int p_key) {return get_str_atom(&dlElem20_90, p_key);};
    /* >>> десятки Ex: 20
       NB: Можно написать yz(p_key)+ get_str_rang(p_key, p_rang_elem),
           но тогда не будет гибкости в переопределении данного метода.
           В английском нужно ставить "дефис" */
    tElem y0_add_rang(int p_key, int p_rang_elem) \
            {return get_str_atom(&dlElem20_90, p_key)+ get_str_rang(p_key, p_rang_elem);};
    // >>> десятки (особая группа) Ex: 15
    tElem onez_add_rang(int p_key, int p_rang_elem) \
            {return get_str_atom(&dlElem10_19, p_key)+ get_str_rang(p_key, p_rang_elem);};
    // >>> единицы Ex: 3
    virtual tElem z_add_rang(int p_key, int p_rang_elem);
};

void CRussianDictionary::ClearDictLine()
{ dlElem0_9m.clear();
  dlElem0_9w.clear();
  dlElem10_19.clear();
  dlElem20_90.clear();
  dlElem100_900.clear();
  dlRangs.clear();
  drsRangsSex.clear();
}

CRussianDictionary::CRussianDictionary()
{ int i, base;
  base = 0;   for (i=0; i< sizeof(rElem0_9m)/sizeof(tElem);   i++) dlElem0_9m[base+ i]   = rElem0_9m[i];
              for (i=0; i< sizeof(rElem0_9w)/sizeof(tElem);   i++) dlElem0_9w[base+ i]   = rElem0_9w[i];
  base = 10;  for (i=0; i< sizeof(rElem10_19)/sizeof(tElem);  i++) dlElem10_19[base+ i]  = rElem10_19[i];
  base = 2;   for (i=0; i< sizeof(rElem20_90)/sizeof(tElem);  i++) dlElem20_90[base+ i]  = rElem20_90[i];
  base = 1;   for (i=0; i< sizeof(rElem100_900)/sizeof(tElem);i++) dlElem100_900[base+ i]= rElem100_900[i];
  base = 0;   for (i=0; i< sizeof(rRangs)/sizeof(tElem);      i++) dlRangs[base+ i]      = rRangs[i];
              for (i=0; i< sizeof(rRangsSex)/sizeof(tRangSex);i++) drsRangsSex[base+ i]  = rRangsSex[i];
} // constructor

tElem CRussianDictionary::get_str_elem(int p_key100, int p_key10, int p_key1, int p_rang_elem)
{ tElem sResult = "";
  if (p_key100 > 0) // >>> сотни
      if ((p_key10 > 0) or (p_key1 > 0))
          sResult += xyz(p_key100);
      else
          return sResult+ x00_add_rang(p_key100, p_rang_elem);
  if (p_key10 > 0)  // >>> десятки
      if (p_key10 == 1)
          return sResult+ onez_add_rang(10+ p_key1, p_rang_elem); // key на отрезке [10, 19]
      else
          if (p_key1 > 0)
              sResult += yz(p_key10);
          else
              return sResult+ y0_add_rang(p_key10, p_rang_elem);
  if (p_key1 > 0)   // >>> единицы
      return sResult+ z_add_rang(p_key1, p_rang_elem);
  return sResult; // на случай 000
} // CRussianDictionary::get_str_elem

tElem CRussianDictionary::get_str_atom(tpDictElem pDictElem, int p_key)
{ tDictElem::iterator it;
  it = pDictElem->find(p_key);
  if (it->first != p_key) throw CException("test throw");//(ERR_MES_NOT_FOUND %(p_key, pDictElem));
  return it->second+ " ";
} // CRussianDictionary::get_str_atom

tElem CRussianDictionary::get_str_rang(int p_key, int p_rang_elem)
{ tElem sResult = "";

// SpecialRules ...
/* Ex: 1 тысяч"а", 2 тысяч"и"; 1 миллион"", 2 миллион"а", 5 миллион"ов"
 rRangs   SpecialRules
{     0:  {}),
      1:  {1:"а", 2:"и", 3:"и", 4:"и"}),
      2:  {1:"",  2:"а", 3:"а", 4:"а", KEY_ALL:"ов"}),
      3:  {1:"",  2:"а", 3:"а", 4:"а", KEY_ALL:"ов"}),
      ...
} */

  sResult = get_str_atom(&dlRangs, p_rang_elem);
  sResult = sResult.substr(0, sResult.size() -1);

  p_key %= 20;
  if (p_rang_elem == 1)
    { if (p_key == 1) sResult += _SR_a;
      else if (2 <= p_key and p_key <= 4) sResult += _SR_i;
    }
  else if (p_rang_elem > 1)
    { if (2 <= p_key and p_key <= 4) sResult += _SR_a;
      else if (p_key >= 5) sResult += _SR_ov;
     }
  return sResult+ "\n";
} // CRussianDictionary::get_str_rang

tElem CRussianDictionary::z_add_rang(int p_key, int p_rang_elem)
{ tDictRangSex::iterator it;
  tpDictElem pDictElem;

  it = drsRangsSex.find(p_rang_elem);
  if (it->first != p_rang_elem) throw CException("test throw");//(ERR_MES_NOT_FOUND %(p_key, pDictElem));
  switch(it->second)
  {  case _MAN:   pDictElem = &dlElem0_9m; break;
     case _WOMAN: pDictElem = &dlElem0_9w; break;
     default: throw CException(_ERR_MES_SEX);
  } // switch
  return get_str_atom(pDictElem, p_key)+ get_str_rang(p_key, p_rang_elem);
} // CRussianDictionary::z_add_rang

// CRussianDictionary -----------------

// английский словарь (наследован от русского) с интерфейсом IDictionary
class CEnglishDictionary: public CRussianDictionary {
  public:
    CEnglishDictionary();
    virtual ~CEnglishDictionary() {ClearDictLine();};
  protected:
    tDictElem dlElem0_9;
    void ClearDictLine() {dlElem0_9.clear();};

    // >>> сотни Ex: 235
    virtual tElem xyz(int p_key) {return get_str_atom(&dlElem0_9, p_key)+ "hundred ";};
    // >>> десятки Ex: 24
    virtual tElem yz(int p_key) \
            {tElem sResult; \
             sResult = get_str_atom(&dlElem20_90, p_key); \
             return sResult.substr(0, sResult.size() -1)+ '-';};
    // >>> единицы # Ex: 3
    virtual tElem z_add_rang(int p_key, int p_rang_elem) \
            {return get_str_atom(&dlElem0_9, p_key)+ get_str_rang(p_key, p_rang_elem);};
    // Дописывание "окончания". Ex: thousand
    virtual tElem get_str_rang(int p_key, int p_rang_elem) \
            {return get_str_atom(&dlRangs, p_rang_elem)+ " \n";}
};

CEnglishDictionary::CEnglishDictionary()
{ int i, base;
  CRussianDictionary::ClearDictLine(); // отчистка русских данных в справочнике
  base = 0;   for (i=0; i< sizeof(eElem0_9)/sizeof(tElem);  i++) dlElem0_9[base+ i]   = eElem0_9[i];
  base = 10;  for (i=0; i< sizeof(eElem10_19)/sizeof(tElem);i++) dlElem10_19[base+ i] = eElem10_19[i];
  base = 2;   for (i=0; i< sizeof(eElem20_90)/sizeof(tElem);i++) dlElem20_90[base+ i] = eElem20_90[i];
  base = 0;   for (i=0; i< sizeof(eRangs)/sizeof(tElem);    i++) dlRangs[base+ i]     = eRangs[i];
} // constructor

// CEnglishDictionary -----------------

class CElement {
  public:
    CElement(tpDict p_pDict, tElem p_sValue, int p_iRang) \
      {pDict = p_pDict; sValue = p_sValue; iRang = p_iRang;};
    tElem get_str();
  private:
    tpDict pDict;
    tElem sValue;
    int    iRang;
};

tElem CElement::get_str()
{ int key100, key10, key1;
  // >>> сотни
  if (sValue.size() == 3) key100 = atoi(sValue.substr(0, 1).c_str());
  else                    key100 = 0;
  // >>> десятки
  if (sValue.size() >= 2) key10 = atoi(sValue.substr(sValue.size()-2, 1).c_str());
  else                    key10 = 0;
  // >>> единицы
  key1 = atoi(sValue.substr(sValue.size()-1, 1).c_str());
  return pDict->get_str_elem(key100, key10, key1, iRang);}
// CElement -----------------

class CParser {
  public:
    CParser() { pDict = NULL;}; // встроенный конструктор
    void set_dict(tpDict p_pDict) { pDict = p_pDict;}; // установка словаря inline
    virtual tElem get_str(tElem p_value);   // основной метод получения текстового представления числа
  private:
    tpDict pDict;
};

tElem CParser::get_str(tElem p_value)   // основной метод получения текстового представления числа
{ int count_element, a = 0, b, i, rang;
  tElem sResult = "", sValue;

  count_element = (p_value.size()- 1)/ _GROUP_BASE; // DIV целочисленное деленике
  b = p_value.size()- count_element* _GROUP_BASE;
  for (i=0; i < count_element+ 1; i++)
  { sValue = p_value.substr(a, b-a); // срез
    printf("value %s\n",sValue.c_str());
    rang = count_element- i;
    sResult += CElement(pDict, sValue, rang).get_str();
    a = b;
    b += _GROUP_BASE;
  };
  return sResult;
}
// CParser -----------------

int main(int argc, char *argv[])
{   __int64 iValue = 800232472;//16->78 195; маленькое
    int base[2] = {10, 8}, i, j;
    //tElem sValue;
    char *Buf= NULL; // надо от этого буфера уходить
    CRussianDictionary RusDict;
    CEnglishDictionary EngDict;
    tpDict pDict[2] = {&RusDict, &EngDict};
    CParser Parsrer;

    try
    {  Buf = new char[_BUFSIZE];
       for (i=0; i<2; i++)
         {  itoa(iValue, Buf, base[i]);
            printf(MES_SS.c_str(),base[i],Buf);
            for (j=0; j<2; j++)
              {  Parsrer.set_dict(pDict[j]);
                 printf(Parsrer.get_str(Buf).c_str());
              } // for j
         } // for
    } // try
    catch (CException E)
    {  printf(E.sMes.c_str());}
    delete Buf[];

    system("PAUSE");
    return EXIT_SUCCESS;
}
