#include  <stdlib.h> //for exit
#include  <stdio.h>

void Print(FILE *F)
#define SET_BEG 0
 {char simb;
  fseek(F,sizeof(char),SET_BEG);
  while ((simb=fgetc(F))!=EOF) printf("%c",simb);
  fseek(F,sizeof(char),SET_BEG);
  return;
}
void main()
#define _Ok_    "uspeshno otkrit..."
#define _Title_ "--------------------- ego sodergimoe -------------------------------\n\n"
#define _Line_  "-------------------------- citati ----------------------------------\n\n"
#define _Err_   "ERROR Ohibka pri otkritii"
 {FILE *file_ptr;
  char *Dir ="ishodnic.txt",simb;
  bool flag=true;
  file_ptr=fopen(Dir,"r");
  if(!file_ptr) {printf("file %s %s \n",Dir,_Err_); getchar(); exit(1);}
   else printf("file %s %s \n",Dir,_Ok_);
  printf(_Title_); Print(file_ptr); printf("\n%s",_Line_);
  while ((simb=fgetc(file_ptr))!=EOF)
   {if (simb=='"') {flag=!flag; if (flag) printf("\n"); }
    else if (!flag) printf("%c",simb);
   }
  if (!flag) printf("\nERROR V File-e ne chetnoe chislo \"\n");
  fclose(file_ptr);
  printf("\n Exit");
  getchar();
  return;
}
