#include <stdio.h>
#include <math.h>
//zn_prior soderjit prioriteti operacii po algoritmu Deikstre
//Eto mn. dlya kodirovki operacii. (Ex) "/" -> "_del_"=2
int const prior[]=
 {  2  ,  2  ,   2   ,  3  ,  3  ,  0  ,  0  ,  0  ,  0  ,  0  ,  0 ,  0  ,  0 , 1  };
enum znak
 {_add_,_sub_,_sub_u_,_del_,_mul_,_sin_,_cos_,_sqrt_,_exp_,_ln_,_lg_,_abs_,_so_,_sz_};
const _buf_size_=80, _atom_=true;
char *Buf;
#define null 0
struct str  {double main; bool flag;} *Sp;
struct list {str pole; list* next;} *Elem,*Poliz;

void *Ss,*headElem,*headPoliz;
inline void Push (str& New) {*Sp++=New;}
inline void Pop  (str& Old) {Old=*--Sp;}

void Print_file(FILE *F)
#define SET_BEG 0
 {char simb;
  printf("\n");
  fseek(F,sizeof(char),SET_BEG);
  while ((simb=fgetc(F))!=EOF) printf("%c",simb);
  fseek(F,sizeof(char),SET_BEG);
  printf("\n");
}

void Print(list* A)
 {int i;
  const char *arr[]=
   {"ADD","SUB","SUB_U","DEL","MUL","SIN","COS","SQRT","EXP","LN","LG","ABS","(",")"};
  while(A->next!=null)
   {if (A->pole.flag==_atom_) printf("%s ",arr[(int)A->pole.main]);
     else printf("%lf ",A->pole.main);
    A=A->next;}
  printf("\n");
}

inline void UpCase()  {int i=0; while(Buf[i]!='\0') {if(Buf[i]>='a') Buf[i]-='a'-'A'; i++;}}
#define _Lm_(x)  {Elem->pole.main=(double)x; Elem->pole.flag=_atom_;\
                  Elem=Elem->next= new list; Elem->next=null;}
inline void sub_u(int& i) {if (Buf[i]=='-') {_Lm_(_sub_u_) i++;}}
inline void found(int z)  {_Lm_(z)}

inline bool found_part(int z,int& i,char p1,char p2=' ',char p3=' ')
 {if (Buf[i]==p1 && Buf[i+1]=='('&& p2==' ')
   {found(z); i+=2; sub_u(i); return true;}
  if (Buf[i]==p1 && Buf[i+1]==p2 && Buf[i+2]=='('&& p3==' ')
   {found(z); i+=3; sub_u(i); return true;}
  if (Buf[i]==p1 && Buf[i+1]==p2 && Buf[i+2]==p3 && Buf[i+3]=='(')
   {found(z); i+=4; sub_u(i); return true;}
  else return false;
}

inline bool found_number(char Str[])
 {int s=0; Elem->pole.main=0; bool flag=false; float baza;
  while(Str[s]!='\0')
   {if(Str[s]=='.')
     if(!flag) {flag=true; baza=10; s++; continue;} else {s--; return false;}
    if(!flag)  {Elem->pole.main*=10; Elem->pole.main+=(Str[s++]-'0');}
     else{Elem->pole.main+=(Str[s++]-'0')/baza; baza*=10;}
   } Elem->pole.flag=!_atom_;
  Elem=Elem->next=new list; Elem->next=null;
  return true;
}

bool Scan()
//raspozmaet leksemi v stroke Buf + test, Rez -> Elem;
 {int i=0,j;
  char Str[20];
  UpCase();
  do {switch (Buf[i++])
      {case ' ':               break;
       case '+': found(_add_); break;
       case '-': found(_sub_); break;
       case '*': found(_mul_); break;
       case '/': found(_del_); break;
       case '(': found(_so_) ;
                  sub_u(i)   ; break;
       case ')': found(_sz_) ; break;
       case 'C': if (!found_part (_cos_ ,i,'O','S'    ))    goto error;  break;
       case 'E': if (!found_part (_exp_ ,i,'X','P'    ))    goto error;  break;
       case 'A': if (!found_part (_abs_ ,i,'B','S'    ))    goto error;  break;
       case 'L': if (!found_part (_ln_  ,i,'N'        ))
                  if(!found_part (_lg_  ,i,'G'        ))    goto error;  break;
       case 'S': if (!found_part (_sin_ ,i,'I','N'    ))
                  if(!found_part (_sqrt_,i,'Q','R','T'))    goto error;  break;
       case 'P': Elem->pole.main=2*acos(0); Elem->pole.flag=!_atom_;
                 Elem=Elem->next= new list; Elem->next=null;             break;
default: j=0; while(('0'<=Buf[i-1+j]&&Buf[i-1+j]<='9')||(Buf[i-1+j]=='.'))Str[j]=Buf[i-1+j++];
                                                  if (j==0) goto error;
         else {Str[j]='\0'; i+=j-1; if (!found_number(Str)) goto error;} break;
    }//switch
   } while(Buf[i+1]!='\0');
         printf("   OK : Vse simvili v stroke raspoznani\n");        return true;
  error: printf("ERROR : V stroke naideni ne dopustimie simvoli\n"); return false;
}
bool Poliz_proc(list* Elem)
//postroenie pol'skoi inversnoi zapisi na osmove massiva Elem + test , Rez -> Poliz
#define _PLZ_ELEM_ Poliz->pole=Elem->pole; Poliz=Poliz->next= new list; Poliz->next=null
#define _PLZ_COPY_ Poliz->pole=Copy;       Poliz=Poliz->next= new list; Poliz->next=null
#define _CONTINUE_ Elem=Elem->next; continue
 {str Copy;
  while (Elem->next!=null)
   {if (Elem->pole.flag!=_atom_) {_PLZ_ELEM_;                 _CONTINUE_;}
    if (Ss==Sp) {Push(Elem->pole);                            _CONTINUE_;}
    Pop(Copy);
    if ((prior[(int)Elem->pole.main]>prior[(int)Copy.main] && Elem->pole.main!=_sz_)
         || (prior[(int)Elem->pole.main]==prior[_so_]))
     {Push(Copy); Push(Elem->pole);                           _CONTINUE_;}
    while(prior[(int)Elem->pole.main]<=prior[(int)Copy.main])
     {_PLZ_COPY_; if(Ss!=Sp) Pop(Copy); else {Push(Elem->pole); goto Ok;}}
    if (Elem->pole.main!=_sz_) {Push(Copy); Push(Elem->pole); _CONTINUE_;}
Ok: if (Elem->pole.main==_sz_)
     {if (prior[(int)Copy.main]==prior[_so_])
       {if (Copy.main!=_so_) {_PLZ_COPY_;                     _CONTINUE_;}}
      else goto error;
     }                                                        Elem=Elem->next;
   }//while
  while(Ss!=Sp)
   {Pop(Copy);
    if ((prior[(int)Copy.main]==prior[_so_])||(Copy.main==_sz_)) goto error;
    _PLZ_COPY_;
   }     printf("   OK : Skobochnaya struktura verna\n");      return true;
  error: printf("ERROR : Ne vernaya scobochnaya structura\n"); return false;
}

double Math(list* Poliz)
//vichislenie zmacheniya Poliza + test, Rez { VSE !!! }
 {str S1,S2;
  int i;
   while (Poliz->next!=null)
   {if (Poliz->pole.flag!=_atom_) {Push(Poliz->pole); Poliz=Poliz->next; continue;}
    if (Ss!=Sp) Pop(S1); else goto error;
    switch((int)Poliz->pole.main)
     {case _add_  : if (Ss!=Sp)   Pop(S2); else goto error;
                    S2.main+=    S1.main ;           Push(S2); break;
      case _sub_  : if (Ss!=Sp)   Pop(S2); else goto error;
                    S2.main-=    S1.main ;           Push(S2); break;
      case _sub_u_: S1.main=-    S1.main ;           Push(S1); break;
      case _del_  : if (Ss!=Sp)   Pop(S2); else goto error;
                    S2.main/=    S1.main ;           Push(S2); break;
      case _mul_  : if (Ss!=Sp)   Pop(S2); else goto error;
                    S2.main*=    S1.main ;           Push(S2); break;
      case _sin_  : S1.main=sin (S1.main);           Push(S1); break;
      case _cos_  : S1.main=cos (S1.main);           Push(S1); break;
      case _sqrt_ : if (S1.main<0)
                     {printf("ERROR : sqrt(-a), a>0\n");         return 0;}
                    S1.main=sqrt(S1.main);           Push(S1); break;
      case _exp_  : S1.main=exp (S1.main);           Push(S1); break;
      case _ln_   : if (S1.main<0)
                     {printf("ERROR : ln(-a), a>0\n");           return 0;}
                    S1.main=log (S1.main);           Push(S1); break;
      case _lg_   : if (S1.main<0)
                     {printf("ERROR : lg(-a), a>0\n");           return 0;}
                    S1.main=log (S1.main)/log(10);   Push(S1); break;
      case _abs_  : S1.main=abs (S1.main);           Push(S1); break;
     }//switch
    Poliz=Poliz->next;
   }
  Pop(S1); if (Ss!=Sp) goto error2;
  printf("   OK : Vernoe kol-vo operandov i znakov operacii\n"); return S1.main;
  error : printf("ERROR : Ne hvataet operandov\n");              return 0;
  error2: printf("ERROR : Ne hvataet znakov operacii\n");        return 0;
}

void main ()
#define _Ok_    "uspeshno otkrit..."
#define _Err_   "ERROR : Ohibka pri otkritii"

#define _DEL_(X,headX) X=(list*)headX;\
                       while(X->next!=null)\
                        {headX=X;\
                         X=X->next;\
                         delete (list*)headX;}
 {FILE *help_ptr;
  char *Dir ="help.txt";
  help_ptr=fopen(Dir,"r");
  if(!help_ptr) printf("file %s %s \n",Dir,_Err_); else
   {printf("file %s %s \n",Dir,_Ok_);
    Print_file(help_ptr);
    fclose(help_ptr);
   }
  Buf= new char[_buf_size_];
  headElem=Elem=new list; Elem->next=null;
  printf("Str: ");
  fgets(Buf,_buf_size_,stdin);
  bool flag=Scan();
  delete[] Buf;
  printf("\nScan: ");
  if (flag) Print(Elem=(list*)headElem);
   else {printf("ERROR!!!"); _DEL_(Elem,headElem); goto error;}
  Ss=Sp=new str[_buf_size_];
  headPoliz=Poliz=new list; Poliz->next=null;
  flag=Poliz_proc(Elem=(list*)headElem);
  _DEL_(Elem,headElem);
  printf("\nPoliz: ");
  if (flag)
    {Print(Poliz=(list*)headPoliz); printf("\nRez= %f",Math(Poliz=(list*)headPoliz));}
   else printf("ERROR!!!");
  _DEL_(Poliz,headPoliz);
  Sp=(str*)Ss;
  delete[] Sp;
  error: printf(" Exit"); getchar();
}