//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "UntFirst.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFirst *First;
//---------------------------------------------------------------------------
__fastcall TFirst::TFirst(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFirst::btnRunClick(TObject *Sender)
{
# define P First->Pole->Canvas
# define _Color_(RGB_x) Color=256*256*int(RGB_x[2])+256*int(RGB_x[1])+int(RGB_x[0])
# define _RGB_(RGB_x)  RGB_x[0]=help%256;\
                       help=help/256;\
                       RGB_x[1]=help%256;\
                       RGB_x[2]=help/256

   long  Color;
   float RGB_1[3],RGB_2[3],RGB_H[3];
   long i, count, help;

    help=ColorBox1->Selected;
    _RGB_(RGB_1);
    help=ColorBox2->Selected;
    _RGB_(RGB_2);
    _Color_(RGB_1);
    for (i=0; i<3; i++) RGB_H[i]=RGB_2[i]-RGB_1[i];

    edtR1->Text=IntToStr(int(RGB_1[0]));
    edtG1->Text=IntToStr(int(RGB_1[1]));
    edtB1->Text=IntToStr(int(RGB_1[2]));
    edtR2->Text=IntToStr(int(RGB_2[0]));
    edtG2->Text=IntToStr(int(RGB_2[1]));
    edtB2->Text=IntToStr(int(RGB_2[2]));
    edtRh->Text=IntToStr(int(RGB_H[0]));
    edtGh->Text=IntToStr(int(RGB_H[1]));
    edtBh->Text=IntToStr(int(RGB_H[2]));

   if (RGStyle->ItemIndex==0) // горизонтальная
    {count = Pole->ClientHeight;
     for (i=0; i<3; i++) RGB_H[i]=RGB_H[i]/count;
     for (i=0; i<count; i++)
      {P->Pen->Color=Color;
       P->MoveTo(0,i);
       P->LineTo(ClientWidth,i);
       for (help=0; help<3; help++)
          RGB_1[help]=RGB_1[help]+RGB_H[help];
       _Color_(RGB_1);
      }
    }else

   if (RGStyle->ItemIndex==1) // вертикальная
    {count = Pole->ClientWidth;
     for (i=0; i<3; i++) RGB_H[i]=RGB_H[i]/count;
     for (i=0; i<count; i++)
      {P->Pen->Color=Color;
       P->MoveTo(i,0);
       P->LineTo(i,ClientHeight);
       for (help=0; help<3; help++)
          RGB_1[help]=RGB_1[help]+RGB_H[help];
       _Color_(RGB_1);
      }
    }else

   if (RGStyle->ItemIndex==2) // круговая
    {P->Brush->Style=bsSolid;
     P->Brush->Color=clWhite;
     P->Pen->Color=clWhite;
     P->Rectangle(0,0,Pole->ClientWidth,Pole->ClientHeight);
     count = Pole->ClientHeight/2+80;
     for (i=0; i<3; i++) RGB_H[i]=RGB_H[i]/count;
     for (i=0; i<count; i++)
      {P->Pen->Color=Color;
       P->Ellipse(i-80,i-80,Pole->ClientWidth-i+80,Pole->ClientHeight-i+80);
       for (help=0; help<3; help++)
          RGB_1[help]=RGB_1[help]+RGB_H[help];
       _Color_(RGB_1);
      }
    }
}
//---------------------------------------------------------------------------





