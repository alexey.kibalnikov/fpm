//---------------------------------------------------------------------------

#ifndef UntFirstH
#define UntFirstH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <CheckLst.hpp>
//---------------------------------------------------------------------------
class TFirst : public TForm
{
__published:	// IDE-managed Components
        TButton *btnRun;
        TRadioGroup *RGStyle;
        TPanel *pnlFrame;
        TImage *Pole;
        TPanel *Pnl1;
        TLabel *Label1;
        TLabel *Label3;
        TLabel *Label2;
        TColorBox *ColorBox1;
        TEdit *edtR1;
        TEdit *edtG1;
        TEdit *edtB1;
        TPanel *Panel1;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TColorBox *ColorBox2;
        TEdit *edtR2;
        TEdit *edtG2;
        TEdit *edtB2;
        TPanel *Panel2;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TEdit *edtRh;
        TEdit *edtGh;
        TEdit *edtBh;
        void __fastcall btnRunClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFirst(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFirst *First;
//---------------------------------------------------------------------------
#endif
