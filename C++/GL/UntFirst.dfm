object First: TFirst
  Left = 215
  Top = 64
  BorderStyle = bsDialog
  Caption = #1050#1080#1073#1072#1083#1100#1085#1080#1082#1086#1074' '#1040#1083#1077#1082#1089#1077#1081' '#1060#1055#1052', '#1050#1048#1058', 2004'
  ClientHeight = 451
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnRun: TButton
    Left = 315
    Top = 67
    Width = 150
    Height = 30
    Caption = #1057#1090#1072#1088#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = btnRunClick
  end
  object RGStyle: TRadioGroup
    Left = 1
    Top = -2
    Width = 155
    Height = 99
    Caption = #1058#1080#1087' '#1079#1072#1083#1080#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      #1075#1086#1088#1080#1079#1086#1085#1090#1072#1083#1100#1085#1072#1103
      #1074#1077#1088#1090#1080#1082#1072#1083#1100#1085#1072#1103
      #1082#1088#1091#1075#1086#1074#1072#1103)
    ParentFont = False
    TabOrder = 1
  end
  object pnlFrame: TPanel
    Left = 1
    Top = 100
    Width = 464
    Height = 349
    BevelInner = bvLowered
    TabOrder = 2
    object Pole: TImage
      Left = 5
      Top = 5
      Width = 454
      Height = 339
    end
  end
  object Pnl1: TPanel
    Left = 158
    Top = 8
    Width = 153
    Height = 57
    BevelInner = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 4
      Top = 28
      Width = 13
      Height = 24
      Caption = 'R'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 103
      Top = 29
      Width = 12
      Height = 24
      Caption = 'B'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 52
      Top = 29
      Width = 14
      Height = 24
      Caption = 'G'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ColorBox1: TColorBox
      Left = 4
      Top = 5
      Width = 144
      Height = 22
      ItemHeight = 16
      TabOrder = 0
    end
    object edtR1: TEdit
      Left = 19
      Top = 30
      Width = 31
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtG1: TEdit
      Left = 68
      Top = 30
      Width = 33
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object edtB1: TEdit
      Left = 115
      Top = 30
      Width = 33
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
  end
  object Panel1: TPanel
    Left = 313
    Top = 8
    Width = 153
    Height = 57
    BevelInner = bvLowered
    TabOrder = 4
    object Label4: TLabel
      Left = 4
      Top = 28
      Width = 13
      Height = 24
      Caption = 'R'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 103
      Top = 29
      Width = 12
      Height = 24
      Caption = 'B'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 52
      Top = 29
      Width = 14
      Height = 24
      Caption = 'G'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ColorBox2: TColorBox
      Left = 4
      Top = 5
      Width = 144
      Height = 22
      ItemHeight = 16
      TabOrder = 0
    end
    object edtR2: TEdit
      Left = 19
      Top = 30
      Width = 31
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtG2: TEdit
      Left = 68
      Top = 30
      Width = 33
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object edtB2: TEdit
      Left = 115
      Top = 30
      Width = 33
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 159
    Top = 67
    Width = 153
    Height = 31
    BevelInner = bvLowered
    TabOrder = 5
    object Label7: TLabel
      Left = 4
      Top = 2
      Width = 13
      Height = 24
      Caption = 'R'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 103
      Top = 3
      Width = 12
      Height = 24
      Caption = 'B'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 52
      Top = 2
      Width = 14
      Height = 24
      Caption = 'G'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edtRh: TEdit
      Left = 19
      Top = 4
      Width = 31
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object edtGh: TEdit
      Left = 68
      Top = 4
      Width = 33
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtBh: TEdit
      Left = 115
      Top = 4
      Width = 33
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
  end
end
