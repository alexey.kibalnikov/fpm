#include <stdio.h>
#include <conio.h> // clrscr()
#define _n_ 10
#define _m_ 15
#define _Laberint_                                  \
                 {{0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},\
                  {0,1,1,1,0,1,1,1,1,1,1,1,1,1,0},\
                  {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},\
                  {0,1,0,0,0,1,0,1,0,0,0,1,0,1,0},\
                  {0,1,1,1,1,1,0,1,1,1,0,1,1,1,0},\
                  {0,1,0,1,0,0,0,0,0,1,0,0,0,0,0},\
                  {0,1,0,1,0,1,1,1,0,1,1,1,1,1,0},\
                  {0,1,0,1,0,1,0,1,0,0,0,0,0,1,0},\
                  {0,1,1,1,0,1,0,1,1,1,1,1,1,1,0},\
                  {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0}}
typedef bool   mtr_type [_n_][_m_];
typedef bool (*ptr_type)[_m_];
typedef struct {int x;int y;} xy_type;

void Print(ptr_type Ptr)
 {int i,j;
  clrscr();
  for (i=0;i<_n_;i++)
   {for (j=0;j<_m_;j++)
    if (Ptr[i][j]) printf(" ");else printf("");
    printf("\n");
   }
  return;
}
void Go(ptr_type Matr)        //  =====(0,1)=====       X->
 {xy_type XY[4];              // (1,0) ->       ||     Y
  xy_type& Cord=XY[0];        //  ||     <- (-1,0)     |  Znachenie Step
  xy_type& Quit=XY[1];        //  =====(0,-1)====      v
  xy_type& Step=XY[2];
  xy_type& Buf =XY[3];
  void* Ss;
  xy_type *Sp; // (*ptr).elem or ptr->elem
  int i,count=0;
  int Right[2][2]={{0,-1},      // Matrix pravogo povorota
                   {1, 0}};
  int Left [2][2]={{ 0,1},
  					    {-1,0}};
  bool flag=true; // vivod s koordinatami
//Ishim vozmojnyyu tochku vhoda
  for (i=0;i<_m_;i++) if (Matr[0][i])    {XY[count].x=i;    XY[count++].y=0;}
  for (i=0;i<_m_;i++) if (Matr[_n_-1][i]){XY[count].x=i;    XY[count++].y=_n_-1;}
  for (i=0;i<_n_;i++) if (Matr[i][0])    {XY[count].x=0;    XY[count++].y=i;}
  for (i=0;i<_n_;i++) if (Matr[i][_m_-1]){XY[count].x=_m_-1;XY[count++].y=i;}
//Vibiraem
  printf("(x-stolbez,y-stroka)\n");
  for (i=0;i<2;i++) printf("    %d# (%2d,%2d)\n",i,XY[i].x,XY[i].y);
  do {printf("Vibirite nomer is spiska [0..1] #"); scanf("%d",&count);}
   while (0>count||count>1); getchar();
  printf("(Varianti vivoda 0-s koordinatami,1-bez nih)\n");
  do {printf("Vibirite nomer is spiska [0..1] #"); scanf("%d",&Buf.x);}
   while (0>Buf.x||Buf.x>1); if (Buf.x==1) flag=false; getchar();
//Stroim put' iz protivopolojnogo tochki, a zatem {Pop,Convert}
  if (count==0) {XY[2]=XY[0]; XY[0]=XY[1]; XY[1]=XY[2];}
//dal'she obrasenie tol'ko po ssilkam
//Vibrali vhod, formiruem Step (EX)...->(1,0); (-1,0)<-...
  if (Cord.x==0)    {Step.x= 1; Step.y= 0;}else
  if (Cord.x==_m_-1){Step.x=-1; Step.y= 0;}else
  if (Cord.y==0)    {Step.x= 0; Step.y= 1;}else
  if (Cord.y==_n_-1){Step.x= 0; Step.y=-1;}
//Vidilenie pamyati pod stack (naihydsii' variant- zmei'ka)
 Ss=Sp=new xy_type [_n_*_m_/2]; //Ss=Sp=(xy_type*)malloc(n_s*m_s/2); #include <malloc.h>

#define _Push_(New) *Sp++=New
#define _Pop_(Old)  Old=*--Sp
//Main part
#define _ROTxSTEP_(T){Buf.x=T[0][0]*Step.x+T[0][1]*Step.y;\
				          Buf.y=T[1][0]*Step.x+T[1][1]*Step.y; Step=Buf;}
#define _Search_ {_ROTxSTEP_(Right);\
		            while(!(Matr[Cord.y+Step.y][Cord.x+Step.x])) _ROTxSTEP_(Left);\
                  Cord.x+=Step.x; Cord.y+=Step.y;}
#define _Convert_(T) {T.x=-T.x; T.y=-T.y;}
#define _Rez_(T) {if(T.x==0) if(T.y==1) printf("");else printf("");\
                else if(T.x==1) printf("");else printf("");\
                if (flag) {Cord.x+=T.x; Cord.y+=T.y; printf("(%2d,%2d) ",Cord.x,Cord.y);\
                if(i++%8==0)printf("\n");}}
  _Push_(Step);
  while (!(Cord.x==Quit.x&&Cord.y==Quit.y))
   {_Search_;
//Vikidivaem pryamoi vozvrat, no ostautsya petli, t.o. put' ne min !!!
    _Pop_(Buf); if(!(Step.x==-Buf.x&&Step.y==-Buf.y)) {_Push_(Buf); _Push_(Step);}
   }
  Cord=Quit; //t.k. bila inversiya, dlya vhodov
  Print(Matr); i=1;
  printf("V puti net pryamogo vozvrata iz tupikov, no ostautsya petli, t.o. put' ne min!!!");
  printf("Put' naiden, tochka vhoda (%2d,%2d)...\n",Cord.x,Cord.y);
  while(true) if (Sp-1!=(xy_type*)Ss)
   {_Pop_(Buf); _Convert_(Buf); _Rez_(Buf);} else break;
//Vozvrat pamyati Stack-a v kuchu
  Sp=(xy_type*)Ss;
  delete []Sp; //free(Sp);
  return;
}
void main()
 {mtr_type Matr=_Laberint_;
  ptr_type Matr_ptr=Matr;
  Print(Matr_ptr);
  Go(Matr_ptr);
  printf("Exit");
  getchar();
  return;
}