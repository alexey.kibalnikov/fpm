#include <stdio.h>
#define _n_ 9
#define _Mtr_    {{11,12,13,14,15,16,17,18,19},\
                 {21,22,23,24,25,26,27,28,29},\
                 {31,32,33,34,35,36,37,38,39},\
                 {41,42,43,44,45,46,47,48,49},\
                 {51,52,53,54,55,56,57,58,59},\
                 {61,62,63,64,65,66,67,68,69},\
                 {71,72,73,74,75,76,77,78,79},\
                 {81,82,83,84,85,86,87,88,89},\
                 {91,92,93,94,95,96,97,98,99}}
typedef int mtr_type [_n_][_n_];
typedef int(*ptr_type)[_n_];

void PrintMatr(ptr_type Matr)
{ int i,j;
  for (i=0;i<_n_;i++)
    {for (j=0;j<_n_;j++) printf(" %3d",Matr[i][j]);
     printf("\n");
    }printf("\n");
  return;
}
void Rotation (ptr_type Matr,int n, int cnt)
{ int Save, i,k;
  for (k=0;k<cnt;k++)
    {for (i=k;i<n-k-1;i++)
      {Save=Matr[k][n-1-i];
       Matr[k][n-1-i]=Matr[i][k];
       Matr[i][k]=Matr[n-1-k][i];
       Matr[n-1-k][i]=Matr[n-1-i][n-1-k];
       Matr[n-1-i][n-1-k]=Save;
      }
    }
  return;
};
void main()
{ mtr_type Matr=_Mtr_;
  ptr_type Ptr=Matr;
  int k;
  printf("Matr (ishodnaja)\n");PrintMatr(Ptr);
  do {printf("Vvedite kol-vo sdvigov (povorotov) [1..%d] k=",(_n_+1)/2);
      scanf("%d",&k);} while (k<1||k>(_n_+1)/2); getchar();
  Rotation (Ptr,_n_,k);
  printf("Rotation(Matr)\n");PrintMatr(Ptr);
  printf("Exit"); getchar();
  return;
}