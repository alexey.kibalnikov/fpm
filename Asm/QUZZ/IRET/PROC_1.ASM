.model tiny
.code
org 100h
start:	jmp	@go
	myret proc far		;; новый обработчик
	  old dd ? 
	  push	ax
	  push	dx	
	  mov	dl,1h
	  mov	ah,02h
	  int	21h
	  pop	dx
	  pop	ax	
	  iret	
@size:	myret endp
@go:	mov	ah,35h		;; взятие адреса 
	mov	al,0f1h
	int	21h
	mov	word ptr old,es
	mov	word ptr old+2,bx
	
	lea	dx,myret	;; переопределение
	mov	ah,25h
	mov	al,0f1h
	int	21h

	lea	dx,@size	;; оставляет резидентной
	int	27h	
end	start
