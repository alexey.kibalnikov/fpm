title Работа со списком 
.186
include io.asm 

public Test_a_list,Test_d_list,Del_list,Add_list,init_heap,print_list	
extrn nil:abs,heap_size:abs,heap_ptr:word, elem:word,next:word

stk segment stack 'stack'
	dw 10 dup (?)
stk ends
 	
code segment 'code' 
assume cs:code,ss:stk

;(R) под адресом ссылки понимается адрес слова, которое хранит 
;адрес "нужного" элемента списка. 
;----------------------------------------------------------------------
;на выходе: создает ССП в heap_size элементов, сегментирует es
Init_heap proc	far
	push	si
	push	bx
	push 	cx
	mov	cx,seg heap_ptr	;т.к. heap_prt в начале сегмента кучи,			
	mov	es,cx	       		;то это установка es на начало кучи.		
	mov	cx,heap_size
	mov	bx,nil
	mov	si,heap_size
	add	si,si
	add	si,si	;4*heap_size
	sub	si,2 
init1:	mov	es:[si].next,bx
	mov	bx,si
	sub	si,4
	loop	init1
	mov	es:heap_ptr,bx
	pop	cx
	pop	bx
	pop	si
	ret
Init_heap endp
;----------------------------------------------------------------------
;на входе: di-адрес ненужного звена
Dispose	proc	far
	push	es:heap_ptr
	pop	es:[di].next
	mov	es:heap_ptr,di
	ret
Dispose	endp
;----------------------------------------------------------------------
;на выходе: di- адрес свободного звена
New	proc	far
	mov	di,es:heap_ptr
	cmp	di,nil
	je	empty_heap
	push	es:[di].next
	pop	es:heap_ptr
	ret
empty_heap:
	lds	dx,cs:aerr
	outstr
	finish
New	endp
;----------------------------------------------------------------------
;на входе: ax- искомый элемент, di-адрес list
;на выходе: ax- 0..1, di- адрес ссылкы (для удаления или вставки перед) 	
Test_d_list proc	far
	push	bx
	mov	bx,di
	mov	di,es:[di]
	cmp	di,nil
	je	@no
@go:	cmp	es:[di].elem,ax
	je	@yes
	cmp	es:[di].next,nil
	je	@no
	mov	bx,di
	add	bx,2		;новый адрес ссылки 
	push	es:[di].next
	pop	di
	jmp	@go
@yes:	mov	ax,1
	mov	di,bx
	pop	bx
	ret	
@no:	mov	ax,0
	pop	bx
	ret
Test_d_list endp
;----------------------------------------------------------------------
;на входе: ax- искомый элемент, di-адрес list
;на выходе: ax- 0..1, di- адрес ссылкы (для вставки или удаления после) 	
Test_a_list proc	far
 	call	Test_d_list
	cmp	ax,0
	je	@go3
	mov	di,es:[di]
	add	di,2		;адрес ссылки на найденный элемент 
@go3:	ret
Test_a_list endp
;(R)процедуры Test меняют значение di толко в случае найденого элемента
;----------------------------------------------------------------------
;на входе: di- адрес ссылки на удаляемый элемент
Del_list proc far
	push	ax
	push	bx
	cmp	di,nil
	je	@error
	mov	bx,di
	mov	di,es:[di]	;в di адрес удаляемого элемента
	cmp	di,nil
	je	@error
	mov	ax,es:[di].next
	call	Dispose
	mov	es:[bx],ax
	mov	di,bx	;di- адрес ссылки (для удаления за удаленным)  	
	jmp	@nex
@error:	lds	dx,cs:adr
	outstr
@nex:	pop	bx
	pop	ax	
	ret
Del_list endp
;----------------------------------------------------------------------
;на входе: di- адрес ссылки, после которой нужно добавить элемент
;	   ax- значение поля elem 
;на выходе: di- обновленный адрес ссылки  	
Add_list proc far
	push	cx
	push 	bx
	mov	bx,di
	mov 	cx,es:[di]
	call 	New
	mov	es:[di].elem,ax
	mov	es:[di].next,cx
	mov	es:[bx],di	
	add 	di,2	;новый адрес ссылки (вставка за вставленным)  
	pop	bx		
	pop	cx
	ret
Add_list endp
;---------------------------------------------------------------------- 
;на входе: di- адрес головы списка list
;	   ax- 0..1 символы или числа
Print_list proc far
	push	di
	push	bx
	mov	di,es:[di]
@go2:	cmp	di,nil
	je	@exit
	mov	bx,es:[di].elem
	cmp	ax,1
	jne	@q
	outint	bx   
	jmp	@q2
@q:	outch	bl
@q2:	outch	' '
	mov	di,es:[di].next
	jmp	@go2
@exit:	pop	bx
	pop	di
	outch	'$'
	newline
	ret
Print_list endp
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	aerr dd err
	err db 'Ошибка в New: исчерпание кучи ','$'
	adr dd mes
	mes db 'Ошибка в Del_list, удаляемого элемента не существует ','$'
code ends
end 
	

