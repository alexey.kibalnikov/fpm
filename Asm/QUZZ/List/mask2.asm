title #
.186
include io.asm 
include macros.asm      

data segment 'data'
 nil equ 0
 heap_size equ 100h	
data ends

heap segment private 'heap'
 heap_ptr dw ?
 node heap_size dup (<>)	
 list dw nil 
heap ends

stk segment stack 'stack'
	dw 100h dup(?)
stk ends
code segment 'code' 
assume cs:code,ds:data,ss:stk,es:heap
begin:	mov	ax,data		
	mov 	ds,ax		
	Init 	 

	finish
code ends
end begin
	

