.xlist
public nil,heap_size,heap_ptr,elem,next
extrn init_heap:far,test_a_list:far,test_d_list:far,del_list:far,add_list:far,print_list:far
node struc
 elem dw ?
 next dw ?
node ends                       
;----------------------------------------------------------------------
Help 	macro linc		;;вспомогательный для Del,Print,Get 
 	ifnb	<linc>		
	 lea	di,es:linc	;;параметр list задан явно
	endif
endm
;----------------------------------------------------------------------
Help2	macro op,linc	;;вспомогательный для Testa,Testd,Save
	 ifnb	<op>	
	  ifdif	<op>,<ax>	;;op задан and op<>'ax'	
	   mov	ax,op
 	  endif
	 endif
	Help	linc
endm
;----------------------------------------------------------------------
Init macro
	call 	Init_heap
endm
Print macro op,linc                                                                                       
	push	ax
	mov	ax,op
	Help 	linc
	call	Print_list
	pop	ax
endm
Testa	macro op,linc
	Help2 	op,linc
	call	Test_a_list 
endm
Testd	macro op,linc
	Help2 	op,linc	
	call	Test_d_list 
endm
Save	macro op,linc
	Help2	op,linc	
	call	Add_list
endm
Del	macro linc 
	Help 	linc
	call	Del_list
endm	
Get	macro linc		
;;на входе: di- адрес ссылки ,на выходе: ax- значение,di- перенастроен   
	Help 	linc          
	mov 	di,es:[di]	
	mov 	ax,es:[di].elem
	add	di,2                                       
endm       
.list		                                                                                                            