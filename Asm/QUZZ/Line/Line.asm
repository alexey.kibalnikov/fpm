public Line
extrn X:abs,Y:abs,Screen:byte
;-----------------------------------------
Videomode_03h macro
	push	ax
	mov	ah,00h
	mov	al,03h
	int	10h
	pop	ax
endm
;----------------------------------------------------------------------
Transformation	macro
local	@0,@1,@2,@3,@4,@exit
;;  in: ax=K,  bx=B 
;; out: si=x1, di=x2 
	push	bp
	mov	bp,sp
	sub	sp,4
		  mov	[bp-2],word ptr 0 ;; for error	
		  mov	[bp-4],word ptr 0
;; || Ox ----------------------------------
	cmp	ax,0	
	jne	@0
	  mov	[bp-2],word ptr 0
	  sub	bp,2	
	  mov	[bp-2],word ptr X-1 ;; (!) [bp-4]
	  jmp	@exit
;; 0<=y(0)<=X-1, t.k. y(0)=bx -------------
@0:	cmp	bx,0	
	jl	@1
	cmp	bx,Y-1
	jg	@1
	  mov	[bp-2],word ptr 0 ;; save X
	  sub	bp,2  
;; 0<=y(X-1)<=Y-1 -------------------------
@1: 	push	ax
	mov	dx,X-1
	imul	dx
	add	ax,bx	
	cmp	ax,0	
	jl	@2
	cmp	ax,Y-1
	jg	@2
	  mov	[bp-2],word ptr X-1 ;; save X
	  sub	bp,2	
@2:	pop	ax	;; rescued K	
;; K*x+B=0, 0<-B/K<=X-2 -------------------
	push	bx	;; save B
	xchg	ax,bx
	cwd
	idiv	bx
	neg	ax	
	xchg	ax,bx	;; ax<- rescued K
	cmp	bx,0
	jle	@3
	cmp	bx,X-1
	jge	@3
	  mov	[bp-2],bx	 ;; save X
	  sub	bp,2		
@3:	pop	bx	;; rescued B
;; (X-1)*K+B=0, 0<[(X-1)-b]/K<=X-2 -------- 
	push	bx
	xchg	ax,bx
	sub	ax,Y-1
	neg	ax
	cwd
	idiv	bx
	xchg	ax,bx	;; ax<- rescurd K
	cmp	bx,0
	jle	@4
	cmp	bx,X-1
	jge	@4
	  mov	[bp-2],bx	 ;; save X
	  sub	bp,2		
@4:	pop	bx		
@exit:	
	pop	si	;; rescued x1	
	pop	di	;; rescued x2	
	pop	bp
endm
;----------------------------------------------------------------------
MovP 	macro	color	
;;  in: ax=Y, si=X
	push	ax	;; Y>=0 X>=0
	push	dx
	  sub	ax,Y-1	;; Y-199
	  neg	ax	;; 199-Y 
	mov	dx,X	
	mul	dx 	;; ax:=ax*Y 		
	xchg	bx,ax
	pop	dx
	mov	es:Screen[si][bx],color
	xchg	bx,ax
	pop	ax
endm
;----------------------------------------------------------------------
.model small
.data	
Mes db 'Line with such parameters is not visible on the screen !','$'
.code
.186
.startup
Line proc	
;;  in: ax=K, bx=B, dx=color
	push	bp
	mov	bp,sp
	sub	sp,8	;; (!) [bp-2]~step; [bp-4]~Y_old; [bp-6]~Y_new 
	pusha		;; [bp-8]~color
	mov	[bp-8],dx; save color	
push	es		
mov	si,Seg Screen
mov	es,si
	Transformation	
; si=x1, di=x2 ------------------------------------- 
		  push	si
		  add 	si,di
		  cmp	si,0	
		  pop	si
		  jne	@Ok
		  jmp	@error	
@Ok:
push	ax
mov	ax,100
movP	1111b
push	si
mov	si,di
movP	1111b
pop	si
pop	ax

	mov	[bp-2],word ptr -1 ;; step <- (-1)
	mov	cx,si 	;; save x1
	sub	si,di
	cmp	si,0
	jg	@hlp	;; if x1>x2 then step<- (-1)	
	neg	word ptr [bp-2]	;;(!)
	neg	si	
@hlp:	xchg	si,cx	;; cx:=|x1-x2|, si:=x1
	inc	cx	
			  push	ax
			  imul	si
			  add	ax,bx
			  mov	[bp-4],ax	;; Y_old <- y1
			  pop	ax
@for:	push 	ax	;; Y=ax*si+bx (?) 
	imul	si	;; (dx,ax):=ax*si, t.k. dx=0, ax:=ax*si
	add	ax,bx	;; ax:=Y [Yes!!!]
			  mov	[bp-6],ax 	;; Y_new <- Y
	mov	dx,[bp-8]; dl:=color
	MovP 	dl	;; ax=Y, si=X, [bp-8]~color
	; Break -------------------------------------------------------    
		   	  sub 	si,[bp-2]	;; si:=X- step_X 		
			  push	cx		
			  push	[bp-2]		;; save step_X
			  mov	[bp-2],word ptr 1; new step_Y
			  mov	cx,[bp-4]	;; cx:=Y_old
			  sub	cx,ax	
			  cmp	cx,0
			  jg	@g
			  neg	cx		;; cx:=|Y_old-Y_new|
			  neg 	word ptr [bp-2]	
		@g:	  jcxz	@next		;; || Ox	
			  dec	cx
			  jcxz	@next		;; (2n+1)*pi/4
			  add	ax,[bp-2]	;; Y_new 	 	
		@prt:	  MovP	dl		;; ax=Y, si=X
			  add	ax,[bp-2]	;; Y:=Y+ step_Y
			  loop	@prt
 		@next:	  pop	[bp-2]		;; rescued step_X
			  pop	cx
			  add	si,[bp-2]
			  mov	ax,[bp-6]	;; ax:= Y_old
			  mov	[bp-4],ax	;; Y_old:=Y_new
	pop	ax
	add	si,[bp-2] ;; X_new:=X+ step_X
	loop	@w
	jmp	@s
@w:	jmp	@for
	@error:	  Videomode_03h
		  mov	ah,09h
		  lea	dx,mes
		  int	21h
@s:	pop	es
	popa
	add	sp,8
	pop	bp
	ret	
Line	endp
end
