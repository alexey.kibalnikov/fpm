title # Print Line (File)
.model small
.186
extrn Line:far
public Screen,X,Y
include io.asm

Y equ 200
X equ 320
;----------------------------------
Videomode_13h macro 
;; VGA 320x200 (XxY) 
	push 	ax  
	mov	ah,00h
	mov	al,13h
	int	10h
	pop	ax
endm
;----------------------------------
Inpute 	macro
;; ax<-K, bx<-B	
	mov	ah,09h
	lea	dx,mes1
	int	21h
	inint	ax
	push	ax	; save K
	mov	ah,09h
	lea	dx,mes2
	int	21h
	inint	bx	; bx:=B
	lea	dx,mes3
	int	21h
	inint	dx
	pop	ax	; rescued K
endm	
;------------------------------------
Video segment at 0A000h
	Screen db X dup (Y dup (?)) 
Video ends
.data
 mes1 db 'Inpute K: ','$'
 mes2 db 'Inpute B: ','$'
 mes3 db 'Inpute Color: ','$'
.code
;----------------------------------------------------------------------
.startup
	Inpute		; ax:=K, bx:=B, dx:=color
	Videomode_13h
	call	Line
	mov	ax,4c00h
	int	21h
end 
	

