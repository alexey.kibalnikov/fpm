title #
.186
include io.asm 
include macros.asm      

data segment 'data'
 nil equ 0
 a equ 5
 b equ 20
 heap_size equ 100h	
 mes db 'Введите элементы списка $'
data ends

heap segment private 'heap'
 heap_ptr dw ?
 node heap_size dup (<>)	
 list1 dw nil
 list2 dw nil
 rez dw nil 
heap ends

stk segment stack 'stack'
	dw 100h dup(?)
stk ends
code segment 'code' 
assume cs:code,ds:data,ss:stk,es:heap
begin:	mov	ax,data		
	mov 	ds,ax		
	Init 	 
	lea	dx,mes
	outstr
	newline
	mov	cx,a
	sub	ah,ah
	lea	di,es:list1
@int1:	inch	al
	save
	loop	@int1

	print	0,list1
	lea	dx,mes
	outstr
	newline
	mov	cx,b
	lea	di,es:list2
@int2:	inch	al
	save	
	loop	@int2

	print	0,list2	
	lea	di,es:list2
	mov	cx,b
@test:	get
	mov	bx,ax	;bx- save ax
	push	di
	testa	ax,list1
	cmp	ax,1
	je	@yes
@no:	mov	ax,bx
	save	ax,rez
	pop	di
	jmp	@next
@yes:	pop	di
@next:	loop	@test
@exit:	print	0,rez
	finish
code ends
end begin
	

