title k_byte 
data segment
 	k   db (7)		;k in 1..18
	ten db (10)
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
begin proc
assume cs:code,ds:data,ss:stk
	mov	ax,data		
	mov 	ds,ax		
	mov	dh,10
	mov 	dl,0		;count
	mov	cx,90		;[10..99]=90
@for:	mov	al,dh
	cbw
	div 	ten	
	add	ah,al
	cmp	ah,k
	jne	@1
	inc	dl		;inc(count)
@1:	inc	dh		;dh:=10..99
	loop 	@for
;-------------- print count
	mov	al,dl
	cbw
	div	ten
	cmp	al,0
	je	@2
	mov	dl,al
	add	dl,48		;0rd(0)=48
	mov	al,ah
	mov	ah,2
	int	21h
	mov	ah,al
@2:	mov	dl,ah
	add	dl,48
	mov 	ah,2
	int	21h

	mov 	ah,4ch
	int 	21h
begin endp
code ends
end begin
	
