title #
.xlist
.186
include io.asm 
.list
data segment
 n equ 10
 A db 'awwryuwxsd$'
 B db 'awwryuwxtd$'
data ends
stk segment stack
	dw 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk,es:data
begin:	mov	ax,data		
	mov 	ds,ax
	mov	es,ax
	
	cld			;df:=0 i->(i+1)
	lea	si,A		;ds:si
	lea	di,B		;es:di
	mov	cx,n
	repe	cmpsb
	jne	@f		;zf=0
	cmp	cx,0
	jne	@f
	outch	'='
	newline
	jmp	@t
@f:	outch	'<'
	outch	'>'
	newline
	inc	cx
	dec	di
	dec	si
	rep	movsb		;if cx<>0 then es:di<-ds:si
@t:	lea	dx,A
	outstr
	newline
	lea	dx,B
	outstr
	newline
	finish
code ends
end begin
	

