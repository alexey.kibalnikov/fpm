title #1
include io.asm 
data segment
Txt1 db ' Massiv: type word (+/-)$'
Txt2 db ' S[$'
Txt3 db ']= $'
two db 2
n equ 20
Mas dw n dup(?) 
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax		
	mov	cx,n	
	mov	si,0
	lea 	dx,txt1
	outstr
	newline
;-------------- Read Mas[si]
@int:	lea	dx,txt2	
	outstr
	mov	ax,si
	div	two
	cbw
	outint	ax
	lea	dx,txt3
	outstr
	inint	Mas[si]
 	add	si,2
	loop	@int
;-------------- Search Min=Mas[si],si=?
	mov	bx,mas[0]	;bx:=min
	mov	ax,0	
	mov	si,0
	mov	cx,n-1
@min:	add	si,2
	cmp	bx,mas[si]
	jle	@1		;min<=mas[si]
	mov	bx,mas[si]	;min>mas[si]
	mov	ax,si
@1:	loop	@min	
;-------------- print
	newline
	lea	dx,txt2
	outstr
	div	two
	cbw
	outint	ax
	lea	dx,txt3	
	outstr
	outint	bx
	finish
code ends
end begin
	

