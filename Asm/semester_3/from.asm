title # 5
.xlist
.186
include io.asm 
.list
data segment
 n equ 45
 from dw ?
 to dw ?
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax		
	mov	si,from
	mov	di,to
	mov	cx,n
@:	push 	[si]
	pop	[di]
	inc	si
	inc	di	
	loop	@	
	finish
code ends
end begin
	

