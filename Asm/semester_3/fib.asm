title fibonachi
include io.asm
data segment
text db 'Fibonachi:$'
one dw 1
data ends
stk segment
	db 100h dup(?)
stk ends
code segment
begin 	proc
assume cs:code,ds:data,ss:stk
	mov	ax,data		
	mov 	ds,ax		
	lea	dx,text
	outstr
	newline
	mov 	cx,2
	mov	si,1
;-------------- print 1 1
@11:	outint	si,2
	outint 	one,8
	inc	si
	newline
	loop	@11

	mov	bx,1
	mov	dx,1
	mov	cx,11	;13-2

@for:	mov	ax,dx
	add	ax,bx	;ax:=bx+dx
	mov	bx,dx
	mov	dx,ax	
;-------------- print ax
	outint	si,2
	inc	si
	outint	ax,8
	newline
	loop	@for
	finish		 	
begin endp
code ends
end begin