title ������ 
include io.asm
data segment
Q	dw 19 dup(5),160 dup(-15),20 dup(-17),0
n equ 200
text1 db 'min=$'
text2 db 'count=$'
min	dw (?)
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax		
	mov 	cx,n-1		
	mov 	si,0		
	mov	bx,Q[si]
	mov	min,bx  	;min:=Q[0]		
	mov 	ax,1		;count

@1:	add 	si,2
	mov	bx,Q[si]
	cmp 	bx,min
	jg 	@2		;bx>min
	je	@3		;bx=min
	jne	@4		;bx<min
@3:	inc	ax		;inc(count)
	jmp	@2
@4:	mov 	min,bx	       	;min:=bx		
	mov 	ax,1		;count:=1
@2:	loop	@1
;-------------- print ax max(Q)
	lea	dx,text1
	outstr
	outint	min
	newline
	lea	dx,text2
	outstr
	outword	ax

	finish
code ends
end begin
	

 	