title #5
.xlist
.186
include io.asm 
.list
data segment
 text1 db 'Nod= $'
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax		
	outch	'>'
	inint	ax
	outch	'>'
	inint	bx
;-------------- Nod
@w:	cmp 	ax,bx
	je	exit
	ja	@1	;>
	xchg	ax,bx
@1:	sub	ax,bx
	jmp 	@w
exit:	lea	dx,text1
	outstr
	outword	ax
	finish
code ends
end begin
	

