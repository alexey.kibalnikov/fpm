title # Print
.xlist
.186
include io.asm 
.list
data segment
 ten db 10
 copy dw ?
 flag db 0
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
;-------------- 2 c/c 
bin	proc
	newline
	mov	flag,0
	push	cx
	push	si
	mov	si,15
@b:	mov	cx,si
	dec	si	
	push	ax
	shr	ax,cl
	and	al,1b
	cmp	al,0
	je	@e
	mov	flag,1
@e:	je	@e1
	outch	'1'
	jmp	@q
@e1:	cmp	flag,0
	je	@q
	outch	'0'
@q:	pop	ax
	cmp	cx,0
	je	exit
	jmp	@b
exit:	pop	si
	pop	cx
	ret
 bin	endp
;-------------- 16 c/c
 hex	proc
	newline
	mov	flag,0
	push	cx
	push	si
	mov	si,12
@h:	mov	cx,si
	sub	si,4	
	push	ax
	shr	ax,cl
	and	ax,1111b
	cmp	ax,0
	je	@f
	mov	flag,1
@f:	jne	@x		;ax<>0
	cmp	flag,0
	je	@go
@x:	cmp	ax,10
	jae	@a	
	outword	ax
	jmp	@go
@a:	add	al,55  		;ord('A')=65
	outch	al
@go:	pop	ax
	cmp	cx,0
	je	e1
	jmp	@h
e1:	pop	si
	pop	cx
	ret
 hex	endp
;----------------------------------------------------------------------  	
begin:	mov	ax,data		
	mov 	ds,ax		
	outch	'>'
	inint	ax
	call	bin
	call	hex
	;call	dc
	finish
code ends
end begin
	

