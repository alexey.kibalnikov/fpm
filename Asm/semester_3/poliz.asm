title #
.xlist
.186
include io.asm 
.list
data segment
 n equ 30		;n:=length(Buf)
 Buf db n,?,n dup (?)
 flag db 't'
 copy dw ?
 copy2 dw ?
data ends
data2 segment
 mes db 'Enter Poliz : $'
 mes2 db 'Test : $'
data2 ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk,es:data2
	pp	proc
	pop	copy
	sub	dl,dl
	cmp	sp,bp
	je	@error
	pop	bx
	cmp	sp,bp
	je	@error
	pop	ax
	jmp	@ex
@error: mov	flag,'f'
@ex:	push	copy
	ret
 pp	endp

	sb	proc
	pop	copy2
	sub	dl,dl
	cmp	sp,bp
	je	@eror
	pop	ax
	cmp	sp,bp
	push	ax
	je	@e
	inc	si
	mov	al,Buf[si]
	cbw
	dec	si
	cmp	ax,'+'
	je	@e
	cmp	ax,'-'
	je	@e	
	cmp	ax,'*'
	je	@e
	cmp	ax,'/'
	je	@e
	jmp	@ne
@e:	cmp	sp,bp	;push(-ax)
	je	@eror
	pop	ax
	neg	ax
	push	ax	
	jmp	@go
@ne:	call	pp
	sub	ax,bx
	push	ax
	jmp	@go
@eror:	mov	flag,'f'	
@go:	push	copy2
	ret
	sb	endp
;----------------------------------------------------------------------
begin:	mov	ax,data		
	mov 	ds,ax		
	mov	ax,data2		
	mov 	es,ax		
;----------------------	Read string
	lea	dx,es:mes
	;outstr
	lea	dx,Buf
	mov	ah,0Ah
	int	21h
	newline
	mov 	bp,sp	
	mov	si,1
	sub	dl,dl
	sub	cx,cx
	mov	cl,buf[si]	;cx:=length (Buf)	
;---------------------- Test
@for:	inc	si
	mov	al,Buf[si]
	cbw
;----------------------	ax->'+'
	cmp	ax,'+'
	jne	n1
	call	pp
	cmp	flag,'f'
	je	@er
	add	ax,bx
	push	ax
	jmp	@
;---------------------- ax->'-'
n1:	cmp	ax,'-'
	jne	n2
	call	sb
	cmp	flag,'f'
	je	@er
	jmp	@
;---------------------- ax->'*'
n2:	cmp	ax,'*'
	jne	n3	
	call	pp
	cmp	flag,'f'
	je	@er
	mul	bl
	push	ax
	jmp	@
;---------------------- ax->'/'
n3:	cmp	ax,'/'
	jne	n4
	call	pp
	cmp	flag,'f'
	je	@er
	cmp	bl,0
	je	@er
	div	bl
	cbw
	push	ax
	jmp	@
;---------------------- ax->'0'..'9'
n4:	cmp	ax,'0'
	jae	@z	;ax>='0'
@er:	mov	flag,'f'
	jmp	exit
@z:	cmp	ax,'9'
	jbe	@y	;ax<='9'
	jmp	@er	;error symbol
@y:	sub	ax,'0'
	inc	dl
	cmp	dl,3
	je	@er
	push	ax
@:	loop	@f
	jmp	@q
@f:	jmp	far ptr @for
;----------------------	end Test
@q:	pop	ax	;rez
	cmp	sp,bp
	jne	@er
exit:	lea	dx,es:mes2
	;outstr
	outch	flag
	mov	bl,flag
	cmp	bl,'t'
	jne	@x
	newline
	outint	ax
@x:	newline
	finish
code ends
end begin
	

