title # labirinth
.xlist
.186
include io.asm 
.list
data segment
 n0 equ 1011111111111111b
 n1 equ 1011001101100001b
 n2 equ 1000011101101101b
 n3 equ 1111011000001101b
 n4 equ 1001011011101101b
 n5 equ 1101000010101101b
 n6 equ 1101111110101101b
 n7 equ 1000000000101101b
 n8 equ 1011111111101101b
 n9 equ 1010000000001101b
 na equ 1011101111111101b
 nb equ 1001101000000001b
 nc equ 1101101111111111b
 nd equ 1101100000000011b
 nw equ 1100001111011111b
 nf equ 1111111111011111b
 lab dw n0,n1,n2,n3,n4,n5,n6,n7,n8,n9,na,nb,nc,nd,nw,nf
 matr dw 2,0,3,1,3,1,2,0,1,2,0,3,0,3,1,2	
 exit_i dw 0	;(matr)->0..3	
 exit_j dw 0	;(matr)v 0..3	
data ends
stk segment stack
	dw 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
;------------------------------	print Lab
	Print	proc	near
	newline
	pusha
	sub	si,si
	mov	cx,16
@:	mov	ax,lab[si]
	add	si,2
	push	cx
	mov	cx,16
@go:	push 	ax
	dec	cx
	shr	ax,cl
	inc	cx
	and	ax,1b
	cmp	al,0
	je	@n
	inc	al
@n:	outch	al
	pop	ax
	loop	@go
exit:	pop	cx
	newline
	loop	@
	popa
	ret
 Print	endp
;------------------------------ Pp(i,j)->Lab(i,j)
  	Pp	proc	near		
	push	bp
	mov	bp,sp
	push	ax
	push	cx
	push	si
	mov	si,[bp+6]	;i
	add	si,si
	mov	ax,lab[si]
	mov	cx,15
	sub	cx,[bp+4]	;j
	shr	ax,cl
	and	ax,1b
	mov	[bp+8],ax	;rez
	pop	si
	pop	cx
	pop	ax
	pop	bp
	ret	4
 Pp	endp	
;------------------------------ fist_go-> i,j,go
	Fgo	proc	near	
	push	bp
	mov	bp,sp
	pusha
	mov	cx,16
	sub	si,si
	sub	bx,bx		;i:=0
@fg:	sub	sp,2
	push	bx		;i:=0 or 15
	dec	cx	
	push	cx		;j:=0..15
	call	Pp
	pop	dx	 	;dx:=Lab(i,j)
	cmp	dl,0
	jne	@w
	mov	exit_i,bx	
	mov	exit_j,cx	;cx-1	
	inc	si		;si-count
	cmp	si,2
	je	@xt
	mov	[bp+4],bx	;i
	mov	[bp+6],cx	;j
	cmp	bx,0
	jne	@1
	mov	[bp+8],word ptr 1
	jmp	@w
@1:	mov	[bp+8],word ptr 0
@w:	inc	cx
	loop	@fg
	mov	bx,15		;i:=15
	mov	cx,16
	jmp	@fg
@xt:	popa
	pop	bp
	ret
 Fgo 	endp
;------------------------------
	Next	proc	near
	pusha
	sub	sp,6
	call	Fgo
	pop	ax	;i
	pop	bx	;j
	pop	dx	;go
	newline
@nex:	sub	si,si
@j:	push	dx	;save go
	push	si	;flag
	shl	dx,3	;dx*8
	shl	si,1	;bx*2
	push	bx
	mov	bx,dx
	mov	dx,Matr[bx][si]	;dx=go(n)	
	pop	bx	
	push	ax	;save i
	push	bx	;save j
	cmp	dx,0	;go=0
	jne	q1
	dec	ax	;i:=i-1
	jmp	cor
q1:	cmp	dx,1	;go=1
	jne	q2
	inc	ax	;i:=i+1
	jmp	cor
q2:	cmp	dx,2	;go=2
	jne	q3
	inc	bx	;j:=j+1
	jmp	cor
q3:	dec	bx	;go=3 j:=j-1
cor:	sub	sp,2
	push	ax	;i(n)
	push	bx	;j(n)
	call	Pp
	pop	si	;si=Lab(i,j)
	cmp	si,0
	jne	@p
	add	sp,8	;del bx,ax,si,dx 
	add	dl,24
	outch	dl
	sub	dl,24
	cmp	ax,exit_i
	je	exitt
	jne	@nex
@p:	pop	bx
	pop	ax
	pop	si
	pop	dx
	inc	si
	jmp	@j
exitt:	outch	' '	
	outch	'!'
	popa
	ret
 Next	endp
;----------------------------------------------------------------------
begin:	mov	ax,data		
	mov 	ds,ax		
	call 	Print
	call	Next
	finish
code ends
end begin
	

