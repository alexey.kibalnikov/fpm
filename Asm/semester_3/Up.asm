title Up_case 
data segment
 Str db 'AlEx kiBaLniKov' 
 length=15
data ends
stk segment
	db 100h dup(?)
stk ends
code segment
begin 	proc
assume cs:code,ds:data,ss:stk
	mov	ax,data		
	mov 	ds,ax		
	mov 	cx,length
	mov 	ah,2h 
	mov	si,0
@for: 	mov	dl,Str[si] 
	inc	si
	cmp	dl,byte ptr 96	;ord(a)=97,ord(A)=65
	jb	@1		;dl<96	
	sub	dl,32	
@1:	int	21h
	loop	@for	
	mov	ah,4ch
	int	21h 	
begin endp
code ends
end begin