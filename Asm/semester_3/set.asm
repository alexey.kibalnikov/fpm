title #
.xlist
.186
include io.asm 
.list
data segment
 a1 equ	10
 an equ 99	;set a1..an
 n equ	(an-a1+1)/8+1
 A db n dup (0)
 B db n dup (0)
 Mes db 'Enter number of elements for $'
data ends
stk segment stack
	dW 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
;---------------------- Read
	Read	proc	near
	pusha
	mov	ah,'A'
	lea	bx,A
	mov	ah,'A'
@red:	lea	dx,Mes
	outstr
	outch	ah
	inint	cx
	outword	a1
	outch	'.'
	outch	'.'
	outword	an
	newline	
	sub	si,si
	newline
@go:	outch	'['
	outword	si
	outch	']'
	outch	'='
	inint	dx
	push	bx	;[X]
	push	dx	;k
	call	Ph
	inc	si
	loop	@go
	cmp	ah,'B'
	je	@exit
	sub	al,al
	mov	ah,'B'
	lea	bx,B
	jmp	@red
@exit:	popa
	ret
 Read 	endp
;---------------------- Print([X])	X={A,B,C}
	Print	proc	near
	push	bp
	mov	bp,sp
	pusha
	mov	cx,an-a1+1
	mov	si,a1
	mov	dx,[bp+4]
@:	sub	sp,2
	push	dx
	push	si
	call	Pp
	pop	ax
	cmp	ax,0
	je	@1
	dec	ax
	add	ax,si
	outword	ax
	outch	','
@1:	inc	si
	loop	@
	newline
	popa
	pop	bp
	ret	2
 Print	endp
;---------------------- Ph([X],k) 	X={A,B,C}, k={a1..an} 
	Ph	proc	near
	push	bp
	mov	bp,sp
	push	ax
	push	bx
	push	cx
	mov	ax,[bp+4]	;
	sub	ax,a1		;i-massiva
	mov	ch,8
	div	ch		;ah-byta,al-v bayte
	mov	cl,ah		;cx:=(k-a1) mod 8
	mov	bx,[bp+6]	;lea bx,X
	cbw
	add	bx,ax		;bx:=[X]+(k-1) div 8
	mov	ax,[bx]
	mov	ah,10000000b
	shr	ah,cl
	or	al,ah
	mov	[bx],al
	pop	cx
	pop	bx
	pop	ax
	pop	bp
	ret	4
 Ph 	endp
;---------------------- Pp([X],k) 	X={A,B,C}, k={a1..an}		
	Pp	proc	near
	push	bp
	mov	bp,sp
	push	ax
	push	bx
	push	cx
	mov	ax,[bp+4]	;
	sub	ax,a1		;i-massiva
	mov	ch,8
	div	ch		;ah-byta,al-v bayte
	mov	cl,ah		;cl:=(k-a1) mod 8
	mov	bx,[bp+6]	;lea bx,X
	cbw
	add	bx,ax		;bx:=[X]+(k-1) div 8
	mov	al,[bx]
	mov	ah,cl
	mov	cl,7
	sub	cl,ah		;cl:=7-cl	
	shr	al,cl
	and	al,1b
	cbw
	mov	[bp+8],ax
	pop	cx
	pop	bx
	pop	ax
	pop	bp
	ret	4
 Pp 	endp
;---------------------- AUB,A�B,A\B
 	U	proc	near
	Fal macro f,num,flag
	pusha
	lea	ax,A
	lea	bx,B
	mov	cx,an-a1+1
	mov	si,a1
L&num:	sub	sp,2
	push	ax	;[A]
	push	si	;k
	call	Pp
	pop	dx
	sub	sp,2
	push	bx	;[B]
	push	si	;k
	call	Pp
	pop	bp
	if	flag
	not	bp
	endif
	f	dx,bp
	cmp	dx,0
	je	G&num
	outword	si
	outch	','
G&num:	inc	si
	loop	L&num
	popa
	endm
	newline
	outch	'A'
	outch	'U'
	outch	'B'
	outch	' '
	Fal	or,1,0
	newline
	outch	'A'
	outch	143
	outch	'B'
	outch	' '
	Fal	and,2,0
	newline
	outch	'A'
	outch	'\'
	outch	'B'
	outch	' '
	Fal	and,3,1	;ai and (not bi)
	ret
 U	endp	
;----------------------------------------------------------------------
begin:	mov	ax,data		
	mov 	ds,ax	
	call	Read
	newline
	lea	dx,A
	push	dx
	outch	'A'
	outch	' '
	call	Print
	lea	dx,B
	push	dx
	outch	'B'
	outch	' '
	call	Print
	call	U
	finish
code ends
end begin
	

