title #
.xlist
.186
include io.asm 
.list
data segment
 STUDENT struc
  fio db 19 dup (' ')	
  grp db 21
  mark db (?)
 student ends
n equ 5		;count Mas	
mas student <'Ivanov$',21,5>,<'Petrov$',21,2>,<'Sidorov$',21,2>,<'Petja$',21,3>,<'Vasja$',21,2>
s equ 2		;mark
 count dw n	 
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax		
	sub	bx,bx
	sub	si,si
	mov	cx,n
@go:	mov	si,n
	sub	si,cx		;si:=n-cx
	mov	al,(mas[bx]).mark
	cmp	al,s
	jne	next
	push	cx
	push	bx
	mov	cx,n		;del(i)  mas[i]<-mas[i+1]..mas[n-1]<-mas[n] 
	sub	cx,si
@mas:	push	cx		;save n-cx
	mov	cx,type student	;22
	push	bx
	lea	di,Mas[bx]	;i:=[Mas]+i
	mov	bx,di	
	add	bx,type student	;j:=[Mas]+i+22
@zap:	mov	al,[bx]		;j
	mov	[di],al		;i<-j
	inc	bx
	inc	di
	loop	@zap
	pop	bx	
	pop	cx
	add	bx,type	student	;i:=i+1
	loop	@mas
	pop	bx
	pop	cx
	sub	bx,type student
	dec	count
next:	add	bx,type student
	loop	@go
	
	mov	cx,count
	outword cx
	newline
	xor	bx,bx	
	xor	ah,ah
@print:	lea	dx,(mas[bx]).fio
	outstr	
	mov	al,(mas[bx]).grp
	outword	ax,4
	mov	al,(mas[bx]).mark
	outword	ax,4
	newline
	add	bx,type student
	loop @print
	finish
code ends
end begin
	

