title #
.xlist
.186
include io.asm 
.list
data segment
 t1 db 'div= $'
 t2 db 'mod= $'
 del equ 14 
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax	
	outch	'>'	
	inint 	bx	;bx-mod
	mov	ax,0	;ax-div
@:	cmp	bx,del
	jb	exit
	sub	bx,del
	inc	ax
	jmp	@
exit:	lea	dx,t1
	outstr
 	outword ax
	newline
	lea	dx,t2
	outstr
  	outword bx
  	finish
code ends
end begin
	

