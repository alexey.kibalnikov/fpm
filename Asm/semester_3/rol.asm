title #2
.xlist
include io.asm 
data segment
 Txt0 db ' Massiv: type byte (+)$'	
 Txt1 db ' Rol Massiv: $'
 Txt2 db ' S[$'
 Txt3 db ']= $'
 Txt4 db ' Massiv: $'
 l db ' '
 Mas db 10 dup(?)
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
beg:	mov	ax,data		
	mov 	ds,ax		
.list
	mov	cx,10
	mov	si,0
	lea 	dx,txt0	
	outstr
	newline
;-------------- Read Mas
@int:	lea	dx,txt2	
	outstr
	outint	si
	lea	dx,txt3
	outstr
	inint	bx		;Mas[si]:=___ type byte  
	mov	Mas[si],bl	
	inc	si
	loop	@int
;-------------- print Mas
	newline
	lea	dx,txt4
	outstr
	mov	cx,4
@_:	outch	l
	loop 	@_
	mov	si,0
	mov	cx,10
@ptr:	mov	al,Mas[si]
	cbw
	outint	ax
	outch	l
	inc	si
	loop	@ptr
;-------------- build Rol Mas
	mov	bh,Mas[0]	;bx-buf
	mov	bl,Mas[1]
  	mov	si,2
	mov	cx,8
@rol:	mov	al,Mas[si]
	mov	Mas[si-2],al
	inc	si
	loop	@rol
	mov	Mas[8],bh
	mov	mas[9],bl
;-------------- print Rol Mas
	newline
	lea	dx,txt1
	outstr
	mov	cx,10
	mov	si,0
@pr:	mov	al,Mas[si]
	cbw
	outint	ax
	outch	l
	inc	si
	loop	@pr
	finish
code ends
end beg
	

