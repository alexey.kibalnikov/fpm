title #3
.xlist
include io.asm 
data segment
 Txt0 db ' Massiv: type byte (+)$'	
 Txt2 db ' S[$'
 Txt3 db ']= $'
 Txt4 db ' Massiv: $'
 l db ' '
 f db 'y'
 flag db '+'
 n equ 11
 Mas db n dup(?)
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
beg:	mov	ax,data		
	mov 	ds,ax		
.list
	mov	cx,n
	mov	si,0
	lea 	dx,txt0	
	outstr
	newline
;-------------- Read Mas
@int:	lea	dx,txt2	
	outstr
	outint	si
	lea	dx,txt3
	outstr
	inint	bx		;Mas[si]:=___ type byte  
	mov	Mas[si],bl	
	inc	si
	loop	@int
;-------------- print Mas
	newline
	lea	dx,txt4
	outstr
	mov	si,0
	mov	cx,n
@ptr:	mov	al,Mas[si]
	cbw
	outint	ax
	outch	l
	inc	si
	loop	@ptr
;-------------- Test
	newline
	mov	si,0
	mov	cx,5

@tt:	mov	di,n-1
	sub	di,si		;di:=n-1-si
	mov	al,Mas[di]
	cmp	al,Mas[si]
	jne	@no
	outch	'+'
	jmp	@yes
@no:	outch	'-'
	mov	f,'n'
@yes:	inc	si
	loop	@tt

 	outch	l
	outch	f
	finish
code ends
end beg
	

