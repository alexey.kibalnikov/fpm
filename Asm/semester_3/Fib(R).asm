title #
.xlist
.186
include io.asm 
.list
data segment

data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
	Fib_r	proc
	pusha
	outch	'>'
	inint	dx
	sub	cx,cx
	call Fib
	popa
	ret
 Fib_r 	endp

	Fib 	proc
	cmp	dx,1
	jne	@1
	mov	bx,1
	mov	cx,1
	outword	bx
	outch	' '
	ret
@1:	dec	dx
	push	dx
	call	Fib
	pop	dx
	mov	ax,bx	;ax<-bx
	mov	bx,cx	;bx<-cx
	add	cx,ax	;cx:=ax+bx
	outword	bx
	outch	' '
	ret
 Fib	endp

begin:	mov	ax,data
	mov	ds,ax
	call	Fib_r
	finish
code ends
end begin
	

