title #
.xlist
.186
include io.asm 
.list
data segment
 Mas db 26 dup (0)
data ends
stk segment stack
	db 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk
begin:	mov	ax,data		
	mov 	ds,ax
	mov	ah,0		
;-------------- Read Mas
@:	outch	'>'
  	inch	al
	cmp	al,'.'
	je	exit
	mov	si,ax
	sub	si,'A'
	mov	mas[si],1
	jmp	@
exit:	mov	cx,26
	mov	si,0
;-------------- Print Mas
@for:	cmp	mas[si],1
	jne	@1
	mov	dx,si
	add	dl,'A'
	outch 	dl
@1:	inc	si
	loop	@for
	finish
code ends
end begin
	

