title # count str1[i]<>str2[i]  i:=1..n
.xlist
.186
include io.asm 
.list
data segment
 n equ 20
 str1 db n,?,n dup (' ')
 str2 db n,?,n dup (' ')
data ends
stk segment stack
	dw 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk,es:data
begin:	mov	ax,data		
	mov 	ds,ax	
	mov	es,ax
;----------------------	input str1,str2	
 	mov	ah,0Ah	;for int 21h
	outch	'>'
	lea 	dx,str1
	int	21h
	newline
	outch	'>'
	lea	dx,str2
	int	21h
	newline
;----------------------
	mov	di,dx	;t.k. dx=lea(str2)
	lea	si,str1
	add	si,2	;Buf: max,n,...
	add	di,2
;---------------------- if length(str1)<>length(str2)
	sub	ch,ch
	mov	cl,str1[1]
	mov	al,str2[1]
	cmp	cl,al
	ja	@ok	;length(str1)>length(str2)
	mov	cl,al	;cx:=max length(str1,2)
;----------------------
@ok:	cld		;i -> i+1
	sub	ax,ax	;ax count
	jcxz	@exit	;if cx=0
@for:	cmpsb
	je	@go
	inc	ax
@go:	loop @for
@exit:	outword	ax
	finish
code ends
end begin
	

