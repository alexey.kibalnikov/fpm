extrn Line_P:far
public Screen,X

.186
atrib equ 00001100b
Y equ 200
X equ 320
Video segment at 0A000h
	Screen db X dup (Y dup (?)) 
Video ends
;----------------------------------------------------------------------
Videomode_13h macro 
;; VGA 320x200 (XxY)   
	mov	ah,00h
	mov	al,13h
	int	10h
endm
;----------------------------------------------------------------------
Line	macro 	color,x1,y1,x2,y2
	pusha
	mov	dx,x1
	mov	cx,y1
	mov	ax,x2
	mov	bx,y2
	mov	si,color
	call 	Line_P	
	popa
endm
;----------------------------------------------------------------------