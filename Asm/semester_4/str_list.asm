title #
.186
include io.asm 
include macros.asm

data segment 'data'
 nil equ 0
 heap_size equ 100h	
 n equ 6			;���-�� ��������� ��������. (+1)
 Buf db n,?,n dup (' ')
 mes db 'Print sequence: ','$'
 mes2 db 'Print lendth of the list: ','$'
 mes3 db 'Print elements of the list (char):','$'
data ends

heap segment private 'heap'
 heap_ptr dw ?
 node heap_size dup (<>)	
 list dw nil 
heap ends

stk segment stack 'stack'
	db 100h dup(?)
stk ends
code segment 'code' 
assume cs:code,ds:data,ss:stk,es:heap
begin:	mov	ax,data		
	mov 	ds,ax		
	Init 	
;----------------------------------------------------------------------
	mov	ah,0Ah
	lea	dx,mes
	outstr	
	lea	dx,Buf
	int	21h
	newline
;---------------------------------------------------------------------- 
	lea	dx,mes2
	outstr
	inint	cx
	lea	dx,mes3
	outstr
	newline
	jcxz	@exit
	jmp	@next
@exit:	newline
	outch	'N'
	outch	'o'
	outch	'!'
	finish	
@next:	inch	al		
	cbw
	Save	ax,list
	loop	@next
	Print	0,list
;----------------------------------------------------------------------
	mov	cl,Buf[1]	;lenght sequence
	sub	bx,bx		;bx- count
	lea	di,es:list
@test:	mov	si,2		;Buf:n,?,...
	mov	al,Buf[si]
	Testa	
	cmp	ax,1			
	je	@go		;������ ������ sequence �� ������
	newline
	cmp	bx,0
	je	@no
	jmp	@yes     
@go:push	cx   
	push	di		
	mov	cl,buf[1]
	dec	cx		;���� ��� ������    
	jcxz	@s
@t2:	 inc	si
	Get	
	cmp	al,buf[si]
	je	@www
@as:	pop	di
	pop	cx
	jmp	@test
@www:	loop	@t2     
@s:	inc	bx		;count
         	jmp	@as

@yes:	outch	'Y' 
	outch	'e'	
	outch	's'
	outch	' '
	outword	bx
	finish
@no: 	outch 	'N'
	outch	'o' 	
	finish
code ends
end begin
	

