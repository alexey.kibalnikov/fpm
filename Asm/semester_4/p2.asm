title #  
.xlist
.186           
i	  equ 160 
siz 	  equ 5  	;5X5
step	  equ 12
simbol equ 4
atrib 	  equ	 00001100b    
adel  	  equ	 0
;----------------------------------------------------------------------
Sleep macro time
	push cx
  	mov	cx,time
@sleep: push cx
    	mov	cx,6000
@sl:	loop @sl
      	pop	cx
       	loop @sleep
	pop	cx
endm 
;----------------------------------------------------------------------  
Tern&back macro flg
local  @1,@2,@3 
	pusha
	xor	si,si 
	mov	bx,siz        
	      if flg                           
		mov	ax,bx
		dec	ax		;ax:=siz-1
		mul	bl
		mov	bx,ax		
		add	bx,2		;bx:=siz*(siz-1)+2	,t.k. +2=i,j 
	       else  
		inc	bx  		;bx:=siz-1+2
	       endif
	mov	cx,siz 
@1:	push bx	
	push cx
	mov	cx,siz            
@2:	mov	al,matr[bx]
	mov	help[si],al
	inc	si                                        
	       if flg
		sub	bx,siz 
	       else
		add	bx,siz
	       endif
	loop	@2
	pop	cx 
	pop	bx   
	       if flg
		inc	bx
 	       else	
		dec	bx
	       endif
	loop	@1     
	
	xor	si,si
	mov	bx,2
	mov	cx,siz*siz
@3:	mov	ah,help[si]
	mov	matr[bx],ah
	inc	si
	inc	bx
	loop	@3
	popa				                                                                               
endm                                                                                            
;-----------------------------------------------------------------------
.list
Tst macro
local @n0,@n1,@n2,@hlp1,@hlp2
	cmp	al,50	;;V
	jne	@n0
				;? Tern&Back 1
	jmp	@go
@n0:	cmp	al,56	;;^
	jne	@n1
				;? Tern&Back 0
	jmp	@go
@n1:	cmp	al,52	;;<-
	jne	@n2
	cmp	si,step-1
	jg	@hlp1
	jmp	@go
@hlp1:	sub	si,step
	push	bx
	mov	bx,si
	mov	matr[1],bl
	pop	bx
	jmp	@go
@n2:	cmp	al,54	;;->
	jne	@go
	cmp	si,79-siz-step
	jl	@hlp2	
	jmp	@go
@hlp2:	add	si,step
	push	bx
	mov	bx,si
	mov	matr[1],bl
	pop	bx
;;@go:
endm
;-----------------------------------------------------------------------
.xlist
data segment
matr db 0,(80-siz)/2,1,1,1,1,1,0,0,1,0,0,0,0,1,1,0,1,1,1,0,0,0,0,1,1,0	    ;i,j,m11,m12,...
help db siz*siz dup (?)	
data ends
stk segment stack
	dw 100h dup(?)
stk ends
video segment at 0b800h
	screen db 25 dup (80 dup (?)) 
 video ends
code segment 
assume cs:code,ds:data,ss:stk,es:video
;-----------------------------------------------------------------------
Prnt proc         
	push bp  
	mov	bp,sp
	pusha       
	mov 	cx,siz   
	xor	bx,bx
	mov	bl,matr[0]		;i 
	mov	al,i
	mul	bl	
	mov	bx,ax
	mov	di,2			;m1
@i:	push	cx                                
 	mov	cx,siz    
	mov	al,matr[1] 		;j
	xor	ah,ah  
	mov	si,ax
	add	si,si                   
@j:	mov 	screen[bx][si],simbol
       	mov	ax,[bp+4]                                  
 	mov 	screen[bx][si+1],al
  	cmp	matr[di],0
 	je	@no                      
 	mov	ax,[bp+6]
 	mov	screen[bx][si+1],al
 @no:inc	di	
 	add	si,2 
 	loop	@j
 	pop	cx
 	add	bx,i
 	loop	@i 
 	popa                                 
 	pop	bp
 	ret	4       
Prnt endp 
;----------------------------------------------------------------------
Prn macro color,fcolor
	push color
	push fcolor
	call	Prnt 
endm
;======================================================================
begin:	mov	ax,data		
	mov 	ds,ax		                               
	mov	ax,video
	mov	es,ax 
.list    
 	mov	bl,matr[1]		
 	xor	bh,bh
  	mov	si,bx	      		;si:=j0                         
  	mov	bl,matr[0]             	;bx:=i0
	mov	cx,25-siz
	sub	cx,bx			;?
@for:	Prn atrib,adel
	Sleep	6000                                
 	Prn	0,0                                 
@next: 	inc	bx	
  	mov	matr[0],bl                      

	mov	ah,01h			;Buf empty ?
	int	16h			
	jnz	@test
	jmp	@go

@test:	Tst	

@go:	mov	ah,0ch			;Clear buf
	mov	al,0
	int	21h			

   	loop	@hlp 
	jmp	@f
@hlp:	jmp	@for
@f:   	Prn atrib,adel
	mov	ax,4c00h		;Exit
	int 	21h
code ends
end begin
	

