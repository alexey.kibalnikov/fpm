title #2
.xlist
.186
include io.asm 
.list
data segment
 n equ 30
 str1 db n,?,n dup (' ')
 str2 db n,?,n dup (' ')
 rez db n dup (' ')
data ends
stk segment stack
	dw 100h dup(?)
stk ends
code segment 
assume cs:code,ds:data,ss:stk,es:code
begin:	mov	ax,data		
	mov 	ds,ax		
	mov	es,ax
;----------------------	input str1,str2	
 	mov	ah,0Ah	;for int 21h
	outch	'>'
	lea 	dx,str1
	int	21h
	newline
	outch	'>'
	lea	dx,str2
	int	21h
	newline
;---------------------- if length(str1)<>length(str2)
	sub	ch,ch
	mov	cl,str1[1]
	mov	al,str2[1]
	cmp	cl,al
	ja	@ok	;length(str1)>length(str2)
	mov	cl,al	;cx:=max length(str1,2)
;----------------------
@ok:	mov	di,dx	;t.k. dx=lea(str2)
	lea	si,str1
	add	si,2	;Buf: max,n,...
	add	di,2
	cld		;i -> i+1
	
	push	cx
	sub	bx,bx
@for:	cmpsb	
	jne	@ne
	xchg	si,di	;t.k. es=ds, str1<->str2
	jmp	@g
@ne:	mov	al,[si-1]	;cmpsb si:=si+1
	mov	rez[bx],al
	inc	bx	;type str 
@g:	loop	@for
	
	pop	cx
	sub	bx,bx
@print:	outch	rez[bx]
	inc	bx
	loop	@print
	finish
code ends
end begin
	

