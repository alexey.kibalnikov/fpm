.model Large,Pascal
.186
 x equ 320
 y equ 200

Videomode_13h macro
	pushf
	push	ax
	mov	ah,00h
	mov	al,13h
	int	10h
	pop	ax
	popf
endm

public vm

Video segment at 0A000h
 Screen db X dup (Y dup (?))
Video ends 
.stack
.code
	vm proc
 videomode_13h
	vm endp
.startup
	Videomode_13h

	push 	ax
	mov	ax,4c00h
	int	21h
end